//
//  NetworkManager.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import SystemConfiguration
import CommonCrypto


public typealias CompletionBlock = (_ finished : Bool, _ statusCode : Int?, _ response: [String :AnyObject]?) -> Void

enum HomeTask
{
    case recent,favorite,viewed,none
}


class NetworkManager{
    
    
    
    public static func  deleteNotification(isDel:Bool,notfID:Int,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
        let urlString = isDel ?  ServiceUtils.BETA_BASE_URL + "deleteNotification" : ServiceUtils.BETA_BASE_URL + "markAsReadNotification"
                let rData = NotificationDeleteModel(notID: notfID)
                requestRaw(urlStr: urlString, parameters: rData.toJSON() ,completionHandler: completionHandler, errorHandler: errorHandler)
         }
    
    
    public static func getMYNotifications(completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
            
              let urlString = ServiceUtils.BETA_BASE_URL + "notifications/\(Utility.getPlistValueforKey(key: YLA.USER_ID))"
              request(urlStr: urlString, completionHandler: completionHandler, errorHandler: errorHandler)
          }
    
    
    public static func  reportAd(ad_id:Int,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
              let urlString = ServiceUtils.BETA_BASE_URL + "report-ad" + "/" + String(ad_id)
              let rData = ReportModel()
              requestRaw(urlStr: urlString, parameters: rData.toJSON() ,completionHandler: completionHandler, errorHandler: errorHandler)
       }
    
    
    public static func  updateDevice(completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
           let urlString = ServiceUtils.BETA_BASE_URL + "SaveLoginHistoryForIos"
           let vData = UpdateDeviceInfo()
           print(vData.toJSON())
           requestRaw(urlStr: urlString, parameters: vData.toJSON() ,completionHandler: completionHandler, errorHandler: errorHandler)
    }
    
    
    
    public static func getWallet(completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
           
             let urlString = ServiceUtils.BETA_BASE_URL + "getWalletInfo/\(Utility.getPlistValueforKey(key: YLA.USER_ID))"
        print(urlString)
             request(urlStr: urlString, completionHandler: completionHandler, errorHandler: errorHandler)
    }
    
    
    public static func  vaidateCredential(info: String ,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
        let urlString = ServiceUtils.BETA_BASE_URL + "validateCredentials"
        let vData = ValidateCredentialModel(inf: info)
        print(vData.toJSON())
        requestRaw(urlStr: urlString, parameters: vData.toJSON() ,completionHandler: completionHandler, errorHandler: errorHandler)
       }
       
    
    
    public static func  send_to_support(sModel: SupportModel ,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
             let urlString = ServiceUtils.BETA_BASE_URL + "contactUs"
        print(sModel.toJSON())
            requestRaw(urlStr: urlString, parameters: sModel.toJSON() ,completionHandler: completionHandler, errorHandler: errorHandler)
    }
    
    
    
    
    public static func  sortAdByNearBy(cat_ID: Int,dist:Int,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
          let urlString = ServiceUtils.BETA_BASE_URL + "getAdsByArea"
         let disData = DistanceModel(catID: cat_ID, dstn: dist)
        print(disData.toJSON())
         requestRaw(urlStr: urlString, parameters: disData.toJSON() ,completionHandler: completionHandler, errorHandler: errorHandler)
    }
    
    public static func  advanceFilter(fData: AdvanceFilterModel,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
                   let urlString = ServiceUtils.BETA_BASE_URL + "ProcessAdvanceFilter"
                 print(fData.toJSON())
        requestRaw(urlStr: urlString, parameters: fData.toJSON() ,completionHandler: completionHandler, errorHandler: errorHandler)
       }
    
    
    
    public static func deleteDoc(dcType: String,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
            let urlString = ServiceUtils.BETA_BASE_URL + "delete_document"
            let dData = DeleteDocumentModel(dtype: dcType)
        print(dData.toJSON())
            requestRaw(urlStr: urlString, parameters: dData.toJSON(),completionHandler: completionHandler, errorHandler: errorHandler)
    }
       
       
       
    
    public static func viewUerInfo(completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
     let urlString = ServiceUtils.BETA_BASE_URL + "userinfo/\(Utility.getPlistValueforKey(key: YLA.USER_ID))"
        print(urlString)
        request(urlStr: urlString, completionHandler: completionHandler, errorHandler: errorHandler)
    }
    
    
    public static func getMasterDataFilter(catID: Int,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
            
              let urlString = ServiceUtils.BETA_BASE_URL + "advanceFilterParentItems/\(String(catID))?user_id=\(Utility.getPlistValueforKey(key: YLA.USER_ID))"
              request(urlStr: urlString, completionHandler: completionHandler, errorHandler: errorHandler)
    }
       
    
    
    
    
    
    public static func sendOfferToSeller(status: String,rate:String,sendTo:Int,aID:Int,msg:String,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
              let urlString = ServiceUtils.BETA_BASE_URL + "sendoffer"
              let dData = SendAnOfferModel(withStatus: status, rate: rate, toUser: sendTo, adID: aID, message: msg)
        print(dData.toJSON())
              requestRaw(urlStr: urlString, parameters: dData.toJSON(),completionHandler: completionHandler, errorHandler: errorHandler)
        }
    
    
    
    
    
    
    
    public static func forgotPassword(info: String,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
                   let urlString = ServiceUtils.BETA_BASE_URL + "forget_password"
                   let dData = ForgotPasswordModel(inf: info).toJSON()
                   print(urlString)

                   print(dData)

                requestRaw(urlStr: urlString, parameters: dData ,completionHandler: completionHandler, errorHandler: errorHandler)
    }
       
    
    
    
    public static func  myAdDeleteAction(adID: Int,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
                let urlString = ServiceUtils.BETA_BASE_URL + "remove_ad"
                let dData = AdsDeleteModel(ad: adID).toJSON()
                print(urlString)

                print(dData)

             requestRaw(urlStr: urlString, parameters: dData ,completionHandler: completionHandler, errorHandler: errorHandler)
    }
    
    
    
    public static func getMyAds(fStr: String,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
         
           let urlString = ServiceUtils.BETA_BASE_URL + "user-adds/\(Utility.getPlistValueforKey(key: YLA.USER_ID))?ad_verify_status=\(fStr)"
           request(urlStr: urlString, completionHandler: completionHandler, errorHandler: errorHandler)
       }
    
    public static func viewDeletedAds(completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
            
              let urlString = ServiceUtils.BETA_BASE_URL + "view_deleted_ads/\(Utility.getPlistValueforKey(key: YLA.USER_ID))"
        print(urlString)
              request(urlStr: urlString, completionHandler: completionHandler, errorHandler: errorHandler)
          }
    
    
    
    public static func viewDocuments(completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
          let urlString = ServiceUtils.BETA_BASE_URL + "viewDoc?user_id=\(Utility.getPlistValueforKey(key: YLA.USER_ID))"
             print(urlString)
             request(urlStr: urlString, completionHandler: completionHandler, errorHandler: errorHandler)
         }
    
    
    public static func getSearchKeywordAds(search: String,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
           let urlString = ServiceUtils.BETA_BASE_URL + "searchbykeyword"
            let dData = KeywordSearchModel(key: search).toJSON()
            requestRaw(urlStr: urlString, parameters: dData ,completionHandler: completionHandler, errorHandler: errorHandler)
          }
    
    public static func getUserFavouriteAds(completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
        let urlString = ServiceUtils.BETA_BASE_URL + "favourite_ads"
        let dData = HomeParameters(offset: 0 ,limit: 500).toJSON()
        print(dData)
           requestRaw(urlStr: urlString, parameters: dData ,completionHandler: completionHandler, errorHandler: errorHandler)
       }
    
    
    public static func add_to_favorite(isAdFav:Bool,adID: Int,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
           let urlString = ServiceUtils.BETA_BASE_URL + "make-favorite-ads"
        let adfav = isAdFav ? "Enable" : "Disable"
           let favData = FavoriteModel(ad: adID,statusAd: adfav).toJSON()
           print(urlString)

           print(favData)

        requestRaw(urlStr: urlString, parameters: favData ,completionHandler: completionHandler, errorHandler: errorHandler)
       }
    
    
    
    
    
    
    
    public static func myAdAction(action: String,adID: Int,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
              let urlString = ServiceUtils.BETA_BASE_URL + "ads-action"
              let favData = AdsActionModel(ad: adID,act: action).toJSON()
              print(urlString)

              print(favData)

           requestRaw(urlStr: urlString, parameters: favData ,completionHandler: completionHandler, errorHandler: errorHandler)
          }
          
       
       
  public static func adUnSold(adID: Int,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
                  let urlString = ServiceUtils.BETA_BASE_URL + "deactivate_sold"
                  let favData = AdsUnSoldModel(ad: adID).toJSON()
                  print(urlString)
               requestRaw(urlStr: urlString, parameters: favData ,completionHandler: completionHandler, errorHandler: errorHandler)
    }
       
    public static func adSold(action: String,adID: Int,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
                 let urlString = ServiceUtils.BETA_BASE_URL + "activate_sold"
                 let favData = AdsSoldModel(ad: adID,act: action).toJSON()
                 print(urlString)

                 print(favData)

              requestRaw(urlStr: urlString, parameters: favData ,completionHandler: completionHandler, errorHandler: errorHandler)
             }
    
    public static func moveAdToDeleted(action: String,adID: Int,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
                    let urlString = ServiceUtils.BETA_BASE_URL + "delete_ad"
                    let favData = AdsSoldModel(ad: adID,act: action).toJSON()
                    print(urlString)

                    print(favData)

                 requestRaw(urlStr: urlString, parameters: favData ,completionHandler: completionHandler, errorHandler: errorHandler)
                }
                
             
    
    
    
    
    
    public static func uploadImage(isDOC:Bool,filename: String,image: UIImage,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
        let urlString = isDOC == true ? ServiceUtils.BETA_BASE_URL + "uploadDoc?type=" + (UserDataManager.sharedInstance.user?.userData!.usertype)! : ServiceUtils.BETA_BASE_URL + "imageupload"
        let params = isDOC == true ? ["user_id": Utility.getPlistValueforKey(key: YLA.USER_ID)] : ["type":(UserDataManager.sharedInstance.user?.userData!.usertype)!]


        requestPostImage(urlStr: urlString, fName: "file.jpg", imgData: image.jpegData(compressionQuality: 0.2)!, wName: filename, parameters: params as! [String : String], completionHandler: completionHandler, errorHandler: errorHandler)
       }
    
    
    
    
    
    public static func UpdateIndividualUserRegistration(inUserData: UpdateIndividualUserParameters,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
             print(inUserData.toJSON())
          let urlString = ServiceUtils.BETA_BASE_URL + "indiv_profile_overview"
          print(urlString)
    requestRaw(urlStr: urlString, parameters: inUserData.toJSON(),completionHandler: completionHandler, errorHandler: errorHandler)
    }
    
    public static func UpdateBusinessUserRegistration(bsnUserData: UpdateBusinessUserParameters,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
                    let urlString = ServiceUtils.BETA_BASE_URL + "biz_profile"
                    print(urlString)
        print(bsnUserData.toJSON())
              requestRaw(urlStr: urlString, parameters: bsnUserData.toJSON(),completionHandler: completionHandler, errorHandler: errorHandler)
             }
    
    
    public static func getMessageItemList(off: Int,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
          //Utility.getPlistValueforKey(key: YLA.USER_ID
        let urlString = ServiceUtils.BETA_BASE_URL + "fetch-chat-session/\(Utility.getPlistValueforKey(key: YLA.USER_ID))" //?page=\(String(off))"
        print(urlString)
        request(urlStr: urlString, completionHandler: completionHandler, errorHandler: errorHandler)
    }
    
    public static func getMessageItems(off: Int,fromID:Int,adID:Int,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
           let urlString = ServiceUtils.BETA_BASE_URL + "fetch-chat-history"
            print(urlString)
           let mData = MessageModel(tuser: fromID,aID:adID)
        print(mData.toJSON())

           requestRaw(urlStr: urlString, parameters: mData.toJSON(),completionHandler: completionHandler, errorHandler: errorHandler)
       }
    public static func sendMessage(msg: String,to_user:Int,adId: Int,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
              let urlString = ServiceUtils.BETA_BASE_URL + "post-chat/\(String(adId))"
               print(urlString)
              let mData = SendMessageModel(tuser: to_user, msgStr: msg)
              print(mData.toJSON())

              requestRaw(urlStr: urlString, parameters: mData.toJSON(),completionHandler: completionHandler, errorHandler: errorHandler)
          }
    
    
    public static func getItemList(sub_cat_id: Int, offset: Int, limits: Int ,keywords: String,emirate: String,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
                    
        let loginDataJSON = getItemParameters(subID: sub_cat_id, off: offset, limits: limits, sortBy: keywords, emirate: emirate).toJSON()
        print(loginDataJSON)
        let urlString = ServiceUtils.BETA_BASE_URL +     "getitemslist1"
        requestRaw(urlStr: urlString, parameters: loginDataJSON,completionHandler: completionHandler, errorHandler: errorHandler)
    }
    
    
    
    
    public static func otpVerification(userID: Int,otp: String,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
           let urlString = ServiceUtils.BETA_BASE_URL + "signup_otp_verify"
        let otpData = OTPVerification(uID: userID, otp: otp).toJSON()
        print(urlString)

        print(otpData)

     requestRaw(urlStr: urlString, parameters: otpData ,completionHandler: completionHandler, errorHandler: errorHandler)
    }
    
    
    public static func businessUserRegistration(bsnUserData: BusinessUserRegister,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
                    print(bsnUserData.toJSON())
                 let urlString = ServiceUtils.BETA_BASE_URL + "user-register"
                 print(urlString)
           requestRaw(urlStr: urlString, parameters: bsnUserData.toJSON(),completionHandler: completionHandler, errorHandler: errorHandler)
          }
    
    public static func individualUserRegistration(indUserData: IndividualUserRegister,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
                 print(indUserData.toJSON())
              let urlString = ServiceUtils.BETA_BASE_URL + "user-register"
              print(urlString)
        requestRaw(urlStr: urlString, parameters: indUserData.toJSON(),completionHandler: completionHandler, errorHandler: errorHandler)
       }
    
    public static func userLogin(username: String, password: String, type: String ,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
                    print(username)
        let loginDataJSON = UserLoginParameters(u_type: type, pass_word: password, uname: username).toJSON()
        print(loginDataJSON)
        let urlString = ServiceUtils.BETA_BASE_URL + "login"
        requestRaw(urlStr: urlString, parameters: loginDataJSON,completionHandler: completionHandler, errorHandler: errorHandler)
    }
    
    
    public static func getAddDetail(itemID: Int ,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
                 
                     
        let urlString = ServiceUtils.BETA_BASE_URL + "get-ad-data/\(itemID)?userID=\(Utility.getPlistValueforKey(key: YLA.USER_ID))"
        request(urlStr: urlString, completionHandler: completionHandler, errorHandler: errorHandler)
                     
    }
    
    public static func getSellerAds(sellerID: Int ,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
      
        let urlString = ServiceUtils.BETA_BASE_URL + "get-seller-ads/\(sellerID)?userID=\(Utility.getPlistValueforKey(key: YLA.USER_ID))"
        request(urlStr: urlString, completionHandler: completionHandler, errorHandler: errorHandler)
    }
    
    public static func getSellerInfo(sellerID: Int ,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
        let urlString = ServiceUtils.BETA_BASE_URL + "get-seller-info/\(sellerID)?user_id=\(Utility.getPlistValueforKey(key: YLA.USER_ID))"
        print(urlString
        )
        request(urlStr: urlString, completionHandler: completionHandler, errorHandler: errorHandler)
    }
    public static func featureAds(completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
           let urlString = ServiceUtils.BETA_BASE_URL + "featuredAds?user_id=\(Utility.getPlistValueforKey(key: YLA.USER_ID))"
           print(urlString
           )
           request(urlStr: urlString, completionHandler: completionHandler, errorHandler: errorHandler)
       }
    public static func classicAds(completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
             let urlString = ServiceUtils.BETA_BASE_URL + "classiccars?user_id=\(Utility.getPlistValueforKey(key: YLA.USER_ID))"
             print(urlString)
             request(urlStr: urlString, completionHandler: completionHandler, errorHandler: errorHandler)
         }
    
    public static func getCategoryData(categID: Int,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
              
           let hmDataJSON = CategoryParameters(cID: categID).toJSON()
           let urlString = ServiceUtils.BETA_BASE_URL + "getSubCatById?user_id=\(Utility.getPlistValueforKey(key: YLA.USER_ID))"
           print(urlString)
           requestRaw(urlStr: urlString, parameters: hmDataJSON,completionHandler: completionHandler, errorHandler: errorHandler)
    }
    
    public static func getHomeData(offset: Int,limit: Int,task: HomeTask,completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void){
           
        let hmDataJSON = HomeParameters(offset: offset ,limit: limit).toJSON()
        print(hmDataJSON)
        var urlString = ""
        switch task {
        case .favorite:
            urlString = ServiceUtils.BETA_BASE_URL +  "favourite_ads"
            break
        case .viewed:
            urlString = ServiceUtils.BETA_BASE_URL + "recently_viewed"
            break
        case .recent:
            urlString = ServiceUtils.BETA_BASE_URL + "recent_ads"
            break
     
        case .none:
            break
        }
        print(urlString)
        requestRaw(urlStr: urlString, parameters: hmDataJSON,completionHandler: completionHandler, errorHandler: errorHandler)
    }
    
    
    
    
    

//////////// UTITLITY METHODS ////////////////
    
    
    private static func requestPostImage(urlStr: String,fName:String,imgData: Data,wName: String, parameters:[String:String],completionHandler:@escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void)
    {
        if ServiceUtils.isConnectedToNetwork() {
//        print(urlStr)
//         print(wName)
//            print(fName)
//            print(parameters)

            
        Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imgData, withName: wName,fileName: fName, mimeType: "image/jpg")
                for (key, value) in parameters {
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    } //Optional for extra parameters
            },
                         to: urlStr)
        { (result) in
            switch result {
            case .success(let upload, _, _):

                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })

                upload.responseJSON { response in
                    print(response)

                    processResponse(response: response, completionHandler: completionHandler, errorHandler: errorHandler)

                }

            case .failure(let encodingError):
                print(encodingError)
                errorHandler(encodingError.localizedDescription)

            }
        }
        }
        else {
            
            errorHandler("Please check your internet connection")
        }
    }
    
    
    
    
    private static func request(urlStr: String, method: HTTPMethod = .get, parameters: Parameters = [:],completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void) {
        if ServiceUtils.isConnectedToNetwork() {
            
            var urlRequest = URLRequest(url: URL(string: urlStr)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: ServiceUtils.Network_timeout)
            urlRequest.httpMethod = "GET"
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
          

            let dataRequest = Alamofire.request(urlRequest)
            dataRequest.validate().responseJSON { response in
                switch (response.result) {
                case .success:
                    //print(response)
                    processResponse(response: response, completionHandler: completionHandler, errorHandler: errorHandler)
                    break
                case .failure(let error):
                    errorHandler("Unable to connect to server. Please try again later.")
                    break
                }
            }
            
            
        } else {
            
            errorHandler("Please check your internet connection")
        }
    }
    
    public static func requestRaw(urlStr: String, method: HTTPMethod = .post, parameters: Parameters, completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void) {
        if ServiceUtils.isConnectedToNetwork() {
            let url = URL(string: urlStr)!
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = method == .put ? "PUT" : "POST"
            
            
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            } catch {
                errorHandler("Please check your internet connection")
            }
            
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            urlRequest.timeoutInterval = ServiceUtils.Network_timeout
            let dataRequest = Alamofire.request(urlRequest)
            dataRequest.validate().responseJSON { response in
                switch (response.result) {
                case .success:
                    processResponse(response: response, completionHandler: completionHandler, errorHandler: errorHandler)
                    break
                case .failure(let error):
                print(error.localizedDescription)
                    errorHandler("Unable to connect to server. Please try again later.")
                    break
                }
            }
        } else {
            
            errorHandler("Please check your internet connection")
            
        }
    }
    
    
    private static func processResponse(response: DataResponse<Any>, completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void) {
        if ServiceUtils.DEBUG_MODE {
            print("Request: \(String(describing: response.request))")
            print("Response: \(String(describing: response.response))")
        }
        
        switch response.result {
        case .success:
            if let JSON = response.result.value {
                print(JSON)

                if ServiceUtils.DEBUG_MODE {
                    print(JSON)
                }
                
                if let requestResult = Mapper<RequestResult>().map(JSONObject: JSON) {
                    completionHandler(requestResult)
                }
                else {
                    errorHandler("There is something wrong parsing the object from server.")
                }
            }
        case .failure(let error):
            var errorString = error.localizedDescription
            if let data = response.data, let responseDataStr = String(data: data, encoding: String.Encoding.utf8) {
                errorString = responseDataStr
            }
            errorHandler(errorString)
        }
    }
}

class ServiceUtils {
    public static let DEBUG_MODE = false
    public static let BETA_BASE_URL = "https://ylaa.com/api_data/index.php/api/"  // "https://app.ylaa.com/apis_ylaa/index.php/api/"
    public static let BETA_IMGBASE_URL = "https://ylaa.com/ylaa_ads_images/thumbs/"
   // public static let BETA_IMGBASE_DETAIL_URL = "http://ylaa.com/ylaa_ads_images/thumbs/"
    public static let BETA_USERIMGBASE_URL = "https://ylaa.com/"  //"https://ylaa.com/"
    public static let BETA_DOMAIN_URL = "https://ylaa.com/"
    public static let BETA_DOMAIN_VIDEOURL = "https://ylaa.com/videos/"
    public static let TERMS_CONDNS = "https://ylaa.com/app-terms-conditions.php"
    public static let PVC_PLCY = "https://ylaa.com/app-privacy-policy.php"
    public static let AD_EDIT = "https://ylaa.com/app-edit-ad?"

                





    /*****************************        Method Names        ********************************/
    
        
    
    class func isConnectedToNetwork() -> Bool {
        return Reachability.isConnectedToNetwork()
    }
    
    
    
    public static func createHashToken(parameters: Parameters) -> String? {
        let SECURE_SECRET = "6CkCNr8lHHcxRfL"
        //let SECOND_SECURE_SECRET = "SS49974"
        
        let sortedKeys = Array(parameters.keys).sorted(by: <)
        
        var string = SECURE_SECRET
        sortedKeys.forEach { (key) in
            if parameters[key] is [[String: Any]] {
            } else if let val = parameters[key] as? Int {
                string += String(describing: val)
            } else if let val = parameters[key] as? Double {
                string += String(format: "%g", val)
            }
            else if  key != "token" {
                string += String(describing: parameters[key]!)
            }
        }
        //string += SECOND_SECURE_SECRET
        
        return  string.sha256().uppercased()
    }
    
    
    
    //****************************** CONSTANTS ****************************************
    
    
    public static let Network_timeout = 60.0
}




extension String {
    
    func sha256() -> String{
        if let stringData = self.data(using: String.Encoding.utf8) {
            return hexStringFromData(input: digest(input: stringData as NSData))
        }
        return ""
    }
    
    private func digest(input : NSData) -> NSData {
        let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
        var hash = [UInt8](repeating: 0, count: digestLength)
        CC_SHA256(input.bytes, UInt32(input.length), &hash)
        return NSData(bytes: hash, length: digestLength)
    }
    
    private  func hexStringFromData(input: NSData) -> String {
        var bytes = [UInt8](repeating: 0, count: input.length)
        input.getBytes(&bytes, length: input.length)
        
        var hexString = ""
        for byte in bytes {
            hexString += String(format:"%02x", UInt8(byte))
        }
        
        return hexString
    }
    
}

