//
//  NetworkModel.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import Foundation

import ObjectMapper





//MARK: -ReportModel

class ReportModel: Mappable {
    
    
    var user_id:String?


     
     required init?(map: Map) {
         
     }
     
     func mapping(map: Map) {
         user_id <- map["user_id"]

     }
     
    init()
   {
     user_id  = Utility.getPlistValueforKey(key: YLA.USER_ID)as String
   }
     
}

//MARK: -NotificationDeleteModel

class NotificationDeleteModel: Mappable {
    
    
    var user_id:String?
    var notification_id:Int?


     
     required init?(map: Map) {
         
     }
     
     func mapping(map: Map) {
         user_id <- map["user_id"]
        notification_id <- map["notification_id"]

     }
     
   init(notID:Int)
   {
     user_id  = Utility.getPlistValueforKey(key: YLA.USER_ID)as String
     notification_id = notID

   }
     
}





class UpdateDeviceInfo: Mappable
{
    
    var device_id: String?
    var userID: String?
    var username: String?



  
    required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           
        device_id <- map["device_id"]
        userID <- map["userID"]
        username <- map["username"]
       }
    init()
    {
        device_id =  Utility.getPlistValueforKey(key: YLA.DEVICE_TOKEN) as String
        userID = Utility.getPlistValueforKey(key: YLA.USER_ID) as String
        username = Utility.getPlistValueforKey(key: YLA.UNAME) as String
    }
       
 
}




class ValidateCredentialModel: Mappable
{
    
    var info: String?
    var userid: String?


  
    required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           
        info <- map["info"]
        userid <- map["userid"]

      

       }
    init(inf: String)
    {
        info = inf
        userid = Utility.getPlistValueforKey(key: YLA.USER_ID) as String
        
    }
       
 
}





class SupportModel: Mappable
{
    
    var inqtype: String?
    var name: String?
    var email: String?
    var detail: String?
    var mob: String?



  
    required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           
        inqtype <- map["inqtype"]
        name <- map["name"]
        email <- map["email"]
        detail <- map["detail"]
        mob <- map["mob"]

       }
  
 
    init()
    {
        
    }
}





class DistanceModel: Mappable
{
    
    var catId: Int?
    var lat: String?
    var long: String?
    var distance: Int?
    var user_id: Int?



  
    required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           
        catId <- map["catId"]
        lat <- map["lat"]
        long <- map["long"]
        distance <- map["distance"]
        user_id <- map["user_id"]

        
       }
    init(catID: Int,dstn: Int)
    {
        catId = catID
        distance = dstn
        lat = AppDelegate.sharedAppdelegate().lattitude
        long = AppDelegate.sharedAppdelegate().longitude
        user_id = Int(Utility.getPlistValueforKey(key: YLA.USER_ID) as String)

    }
       
 
}



class AdvanceFilterModel: Mappable{
    
    var user_id: Int?

    var category_id: Int?
    //var offset: Int?
   // var limit: Int?
    var info = [[String:String]]()//[String:Any]()
    //var range = [[String:String]]()



    
    
    required init?(map: Map) {
           
    }
    func mapping(map: Map) {
        user_id <- map["user_id"]

        category_id <- map["cat_id"]
        //offset <- map["offset"]
        info <- map["info"]
        //limit <- map["limit"]
        //range <- map["range"]
    }
    init()
    {
        user_id = Int(Utility.getPlistValueforKey(key: YLA.USER_ID) as String)
    }
}




class SendAnOfferModel: Mappable
{
    
    var status: String?
    var price: String?
    var fromuser: String?
    var touser: Int?
    var ad_id: Int?
    var messagetext: String?


    required init?(map: Map) {
           
    }
       
       func mapping(map: Map) {
           
        status <- map["status"]
        price <- map["price"]
        fromuser <- map["fromuser"]
        touser <- map["touser"]
        ad_id <- map["ad_id"]
        messagetext <- map["messagetext"]

       }
    init(withStatus: String,rate: String,toUser:Int,adID:Int,message: String)
    {
           status = withStatus
           price = rate
          fromuser = Utility.getPlistValueforKey(key: YLA.USER_ID)as String
           touser = toUser
           ad_id = adID
           messagetext = message

    }
    
    
    
   
       
 
}




class ForgotPasswordModel: Mappable
{
    
    var info: String?
    var user_id: Int?



  
    required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           
        info <- map["info"]
        user_id <- map["user_id"]

      

       }
    init(inf: String)
    {
        info = inf
        user_id = Int(Utility.getPlistValueforKey(key: YLA.USER_ID) as String)

        
    }
       
 
}





class FavoriteModel: Mappable
{
    
    var user_id: Int?
    var ad_id: Int?
    var status: String?

  
    required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           
        user_id <- map["userID"]
        ad_id <- map["ad_ID"]
        status <- map["status"]
      

       }
    init(ad: Int,statusAd: String)
    {
        user_id = Int(Utility.getPlistValueforKey(key: YLA.USER_ID) as String)
        ad_id = ad
        status = statusAd
        
    }
       
 
}

class KeywordSearchModel: Mappable
{
    
    var keyword: String?
    var user_id: Int?

  
    required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           
        keyword <- map["keyword"]
        user_id <- map["user_id"]

      
       }
    init(key: String)
    {
        keyword = key
        user_id = Int(Utility.getPlistValueforKey(key: YLA.USER_ID) as String)
        
    }
       
 
}


class AdsDeleteModel: Mappable
{
    
    var user_id: Int?
    var ad_id: Int?

  
    required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           
        user_id <- map["user_id"]
        ad_id <- map["ad_id"]
      

       }
    init(ad: Int)
    {
        user_id = Int(Utility.getPlistValueforKey(key: YLA.USER_ID) as String)
        ad_id = ad
        
    }
       
 
}







class MessageModel: Mappable
{
    
    var fromuser: Int?
    var touser: Int?
    var activeuser: Int?
    var ad_id:Int?
      required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           
        fromuser <- map["fromuser"]
        touser <- map["touser"]
        activeuser <- map["activeuser"]
        ad_id <- map["ad_id"]

      

       }
    init(tuser: Int,aID:Int)
    {
        fromuser = tuser
        touser = Int(Utility.getPlistValueforKey(key: YLA.USER_ID) as String)
        activeuser = Int(Utility.getPlistValueforKey(key: YLA.USER_ID) as String)
        ad_id = aID
        
    }
       
 
}



class DeleteDocumentModel: Mappable
{
    
    var user_id: Int?
    var document_type: String?
    
      required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           
        user_id <- map["user_id"]
        document_type <- map["document_type"]

       }
    init(dtype: String)
    {
        user_id = Int(Utility.getPlistValueforKey(key: YLA.USER_ID) as String)
        document_type = dtype
    }
       
 
}






class SendMessageModel: Mappable
{
    
    var fromuser: Int?
    var touser: Int?
    var msgtxt: String?
    
      required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           
        fromuser <- map["fromuser"]
        touser <- map["touser"]
        msgtxt <- map["msgtxt"]

       }
    init(tuser: Int,msgStr: String)
    {
        fromuser = Int(Utility.getPlistValueforKey(key: YLA.USER_ID) as String)
        touser = tuser
        msgtxt = msgStr

        
    }
       
 
}





class AdsActionModel: Mappable
{
    
    var user_id: Int?
    var ad_id: Int?
    var action: String?

  
    required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           
        user_id <- map["user_id"]
        ad_id <- map["ad_list_id"]
        action <- map["action"]
      

       }
    init(ad: Int,act: String)
    {
        user_id = Int(Utility.getPlistValueforKey(key: YLA.USER_ID) as String)
        ad_id = ad
        action = act
        
    }
       
 
}



class AdsUnSoldModel: Mappable
{
    
    var user_id: Int?
    var ad_id: Int?

  
    required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           
        user_id <- map["user_id"]
        ad_id <- map["ad_id"]
      

       }
    init(ad: Int)
    {
        user_id = Int(Utility.getPlistValueforKey(key: YLA.USER_ID) as String)
        ad_id = ad
        
    }
       
 
}



class AdsSoldModel: Mappable
{
    
    var user_id: Int?
    var ad_id: Int?
    var message: String?

  
    required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           
        user_id <- map["user_id"]
        ad_id <- map["ad_id"]
        message <- map["message"]
      

       }
    init(ad: Int,act: String)
    {
        user_id = Int(Utility.getPlistValueforKey(key: YLA.USER_ID) as String)
        ad_id = ad
        message = act
        
    }
       
 
}



















class UpdateIndividualUserParameters: Mappable
{
    var user_id: String?
    var Email: String?
    var Mobile: String?
    var Password: String?
    var PersonName: String?
    var Gender: String?
    var Dob: String?
    var Location: String?
    var Pic: String?
    var Facebook: String?
    var Website: String?
    var Instagram: String?
    var Twitter: String?
    var LinkedIn : String?
    var Youtube : String?



    required init?(map: Map) {
              
        }
          
    func mapping(map: Map) {
        
        
         user_id <- map["user_id"]
         Email <- map["Email"]
         Mobile <- map["Mobile"]
         Password <- map["Password"]
         PersonName <- map["PersonName"]
         Gender <- map["Gender"]
         Dob <- map["Dob"]
         Location <- map["Location"]
         Pic <- map["pic"]
         Facebook <- map["Facebook"]
         Instagram <- map["Instagram"]
         Twitter <- map["Twitter"]
         LinkedIn <- map["LinkedIn"]
         Youtube <- map["Youtube"]
        Website <- map["Website"]



    }
    init(uData: UserDataModel)
    {
//        let datestr = UserDataManager.sharedInstance.user?.userData!.DOB
//               var dateString = ""
//               if datestr!.isEmpty{
//                   dateString = ""
//               }
//               else{
//                   dateString = Utility.formatDoBSTR(dateStr: (UserDataManager.sharedInstance.user?.userData!.DOB)!)
//               }
               
        user_id = Utility.getPlistValueforKey(key: YLA.USER_ID) as String
        Email = uData.Email
        Mobile = uData.Mobile
        Password = uData.password
        PersonName = uData.Name
        Gender = uData.Gender
        Dob = uData.DOB
        Location = uData.locationstr
        Pic = uData.Pic
        Facebook = uData.facebook
        Instagram = uData.instagram
        Twitter = uData.twitter
        LinkedIn = uData.linkedin
        Youtube = uData.youtube
        Website = uData.website

        
      
    }

}








class UpdateBusinessUserParameters: Mappable
{
    var BusinessName: String?
    var user_id: String?
    var Email: String?
    var Password: String?
    var Name: String?
    var Gender: String?
    var DateofBirth: String?
    var Designation: String?
    var Location: String?
    var BusinessType: String?
    var BusinessPhone: String?
    var BusinessMobile: String?
    var BrandName: String?
    var BusinessWebsite: String?
    var BusinessAddress: String?
    var About: String?
    var Facebook: String?
    var Instagram: String?
    var Twitter: String?
    var Banner: String?
    var Logo: String?
    var LinkedIn : String?
    var Youtube : String?
    var bizlogo : String?
    var bizbanner : String?





    required init?(map: Map) {
              
        }
          
    func mapping(map: Map) {
        BusinessName <- map["BusinessName"]
        user_id <- map["user_id"]
        Email <- map["Email"]
        Password <- map["Password"]
        Name <- map["Name"]
        Gender <- map["Gender"]
        DateofBirth <- map["DateofBirth"]
        Designation <- map["Designation"]
        Location <- map["Location"]
        BusinessType <- map["BusinessType"]
        BusinessPhone <- map["BusinessPhone"]
        BusinessMobile <- map["BusinessMobile"]
        BrandName <- map["BrandName"]
        BusinessWebsite <- map["BusinessWebsite"]
        BusinessAddress <- map["BusinessAddress"]
        About <- map["About"]
        Facebook <- map["Facebook"]
        Instagram <- map["Instagram"]
        Twitter <- map["Twitter"]
        LinkedIn <- map["LinkedIn"]
        Youtube <- map["Youtube"]
        bizbanner <- map["bizbanner"]
        bizlogo <- map["bizlogo"]


    }
    init(uData: UserDataModel)
    {
//        let datestr = UserDataManager.sharedInstance.user?.userData!.DOB
//               var dateString = ""
//               if datestr!.isEmpty{
//                   dateString = ""
//               }
//               else{
//                   dateString = Utility.formatDoBSTR(dateStr: (UserDataManager.sharedInstance.user?.userData!.DOB)!)
//               }
//
//        print(dateString)
        
        BusinessName  = uData.BusinessName
        user_id = Utility.getPlistValueforKey(key: YLA.USER_ID) as String
        Email = uData.Email
        Password = uData.password
        Name  =  uData.Name
        Gender =  uData.Gender
        DateofBirth  = uData.DOB //dateString
        Designation = uData.contactposition
        Location = uData.locationstr
        BusinessType = uData.BusinessEntity
        BusinessPhone = uData.bizphone
        BusinessMobile = uData.Mobile
        BrandName = uData.Brand
        BusinessWebsite = uData.website
        BusinessAddress = uData.address
        About = uData.bizdetail
        Facebook = uData.facebook
        Instagram = uData.instagram
        Twitter = uData.twitter
        Banner = uData.bizbanner
        Logo  = uData.bizlogo
        bizbanner = uData.bizbanner
        bizlogo  = uData.bizlogo

    }

}














class getItemParameters: Mappable
{
    var subcat_id: Int?
    var userID: String?
    var offset: Int?
    var limit: Int?
    var sort: String?
    var getlocation: String?




    
    
    
    required init?(map: Map) {
              
        }
          
    func mapping(map: Map) {
        subcat_id <- map["subcat_id"]
        userID <- map["user_id"]
        offset <- map["offset"]
        limit <- map["limit"]
        sort <- map["sort"]
        getlocation <- map["getlocation"]




    }
    init(subID: Int, off: Int,limits: Int,sortBy: String,emirate: String)
    {
         subcat_id = subID
         offset  = off
         limit = limits
         userID = Utility.getPlistValueforKey(key: YLA.USER_ID) as String
         sort = sortBy
         getlocation =  emirate

    }

}



class UserLoginParameters: Mappable
{ 
    var user_type: String?
    var password: String?
    var username: String?
    var device_id: String?
    var login_source: String?
    var Mobile: String?
    var Email: String?






    
    
    
    required init?(map: Map) {
              
        }
          
    func mapping(map: Map) {
        user_type <- map["user_type"]
        username <- map["username"]
        password <- map["password"]
        device_id <- map["device_id"]
        login_source <- map["login_source"]
        Mobile <- map["Mobile"]
        Email <- map["Email"]




    }
    init(u_type: String, pass_word: String,uname: String)
    {
         user_type = u_type
         password  = pass_word
         username =  user_type == "email" ? uname : ""
         device_id = Utility.getPlistValueforKey(key: YLA.DEVICE_TOKEN)as String
         login_source = "ios"
         Mobile = user_type == "email" ? "" : uname
         Email = user_type == "email" ? uname : ""

         

    }

}


class CategoryParameters: Mappable
{
    var yla_categID: Int?
    var user_id: Int?

    required init?(map: Map) {
              
          }
          
    func mapping(map: Map) {
        yla_categID <- map["cat_id"]
        user_id <- map["user_id"]

    }
    init(cID: Int)
    {
      self.yla_categID = cID
      user_id = Int(Utility.getPlistValueforKey(key: YLA.USER_ID) as String)

         
    }

}

class HomeParameters: Mappable
{
    
    var yla_UserID: String?
    var yla_Offset: Int?
    var yla_Limit: Int?

    required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           
        yla_UserID <- map["user_id"]
        yla_Offset <- map["offset"]
        yla_Limit <- map["limit"]

       }
       
     
    
     init(offset: Int, limit: Int)
    {
        self.yla_UserID = Utility.getPlistValueforKey(key: YLA.USER_ID)as String
        self.yla_Offset = offset
        self.yla_Limit = limit
    }
    
  
}






class IndividualUserRegister: Mappable
{
    
    var Name: String?
    var Mobile: String?
    var Email: String?
    var password: String?
    var usertype: String?
    var type_id: Int?
    var regSource: String?


    required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           
        Name <- map["Name"]
        Mobile <- map["Mobile"]
        Email <- map["Email"]
        password <- map["password"]
        usertype <- map["usertype"]
        type_id <- map["type_id"]
        regSource <- map["regSource"]

       }
    init()
    {
         Name = ""
         Mobile = ""
         Email  = ""
         password = ""
         usertype  = ""
        regSource = ""
         type_id  = 0
    }
    
    
   
 
}





class BusinessUserRegister: Mappable
{
    
    var BusinessName: String?
    var BusinessEntity: String?
    var EntityType: String?
    var Mobile: String?
    var Email: String?
    var password: String?
    var usertype: String?
    var regSource: String?



    required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           
        BusinessName <- map["BusinessName"]
        BusinessEntity <- map["BusinessEntity"]
        EntityType <- map["EntityType"]
        Mobile <- map["Mobile"]
        Email <- map["Email"]
        password <- map["password"]
        usertype <- map["usertype"]
        regSource <- map["regSource"]


       }
    init()
    {
         BusinessName = ""
         BusinessEntity = ""
         EntityType = ""
         Mobile = ""
         Email  = ""
         password = ""
         usertype  = ""
        regSource  = ""

    }
    
    
    
   
       
 
}



class OTPVerification: Mappable
{
    
    var user_id: Int?
    var user_otp: String?
  
    required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           
        user_id <- map["user_id"]
        user_otp <- map["user_otp"]
      

       }
    init(uID: Int, otp: String)
    {
         user_id = uID
         user_otp = otp
        
    }
       
 
}































class RequestResult: Mappable {
    var value: Any?
    //private var status: StatusModel?
    
    required init?(map: Map) {
        
    }
    
    init() {
    }
    
//    init(status: StatusModel) {
//        self.status = status
//    }
    
    init(value: Any?) {
        self.value = value
    }
    
    func mapping(map: Map) {
        value <- map["response"]
        //status <- map["response"]
    }
//    func isOK() -> Bool {
//        return self.status?.status ?? false
//    }
//    func message() -> String{
//        return self.status?.message ?? ""
//    }
//    func code() -> String{
//        return self.status?.code ?? ""
//    }
}


//class StatusModel:Mappable{
//
//
//    var code: String?
//    var status: Bool?
//    var message :  String?
//    required init?(map: Map) {
//
//    }
//
//    init() {
//    }
//
//    func mapping(map: Map) {
//        code <- map["code"]
//        status <- map["status"]
//        message <- map["message"]
//    }
//
//
//}



class RequestParent: Mappable {
    var request: RequestObject?
    var key: String?
    
    required init?(map: Map) {
        
    }
    
    init() {
    }
    
    init(key: String?, userid: Int) {
        self.key = key
        self.request = RequestObject(userid: userid)
    }
    
    func mapping(map: Map) {
        request <- map["request"]
        key <- map["key"]
    }
}

class RequestObject: Mappable {
    var user_id: Int?
    
    required init?(map: Map) {
        
    }
    
    init() {
    }
    
    init(userid: Int) {
        self.user_id = userid
    }
    
    func mapping(map: Map) {
        user_id <- map["user_id"]
    }
}


class UploadObject: Mappable {
    var form_id: Int?
    var file_description: String?
    var file_name: String?
    var file_uploaded: String?
    var file_extension: String?
    
    required init?(map: Map) {
        
    }
    
    init() {
    }
    
    init(form_id: Int,file_description:String,file_name:String,file_uploaded:String,file_extension:String) {
        self.form_id = form_id
         self.file_description = file_description
         self.file_name = file_name
         self.file_uploaded = file_uploaded
         self.file_extension = file_extension
    }
    
    func mapping(map: Map) {
        form_id <- map["form_id"]
         file_description <- map["file_description"]
         file_name <- map["file_name"]
         file_uploaded <- map["file_uploaded"]
         file_extension <- map["file_extension"]
    }
}


