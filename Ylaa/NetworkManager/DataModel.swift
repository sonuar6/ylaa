//
//  DataModel.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

//MARK -UIDataModel
class RegisterUI:NSObject
{
    
    var i = 0
    var j = 0

    
    let indArray = ["Fullname","By Mobile Number","+971","Password"]
    let indImage = [UIImage(named: "agent"),UIImage(named: "smartphone"),UIImage(named: "flag_uae"),UIImage(named: "password")]

    let bsnArray = ["Company name","Business Type","Agent","+971","Email","Password"]
    let bsnImage = [UIImage(named: "comName"),UIImage(named: "smartphone"),UIImage(named: "agent"),UIImage(named: "flag_uae"),UIImage(named: "email"),UIImage(named: "password")]

    
    var individualUser = [RegisterModel]()
    var businessUser = [RegisterModel]()
    
    
    override init() {
        super.init()
        var isDrop:Bool = true

        for individual in indArray{
            isDrop =  individual == "By Mobile Number" ? false : true
            self.individualUser.append(RegisterModel(titleName: individual, dropDown: isDrop, image: self.indImage[i]!))
            i += 1
              }
        for business in bsnArray{
            if business == "Business Type" ||  business == "Agent"
            {
                isDrop = false
            }
            else{
                isDrop = true

            }
            self.businessUser.append(RegisterModel(titleName: business, dropDown: isDrop, image: self.bsnImage[j]!))
            j += 1

        }
        
    }
    
   
    
    
    

}

class RegisterModel: NSObject
{
    var titleStr: String?
    var isDropDown: Bool?
    var imageThumb: UIImage?
    
    
    override init()
    {
        
    }
    init(titleName:String, dropDown:Bool, image: UIImage)
    {
        super.init()
        self.titleStr = titleName
        self.isDropDown = dropDown
        self.imageThumb = image
        
    }
}

class CategorySegueData: NSObject
{
    var titleStr: String?
    var categID: Int?
    
    
    override init()
    {
        
    }
    init(titleName:String, cID:Int)
    {
        super.init()
        self.titleStr = titleName
        self.categID = cID
        
    }
}








//MARK: -APIDataModel


//MARK: -HomeModel









//MARK: -ImageModel

class ImageModel: Mappable {
    
    
    var pathData:PathModel?
    var Status:String?

     
     required init?(map: Map) {
         
     }
     
     func mapping(map: Map) {
         pathData <- map["data"]
         Status <- map["status"]
     }
     
     static func toObject(json: String) -> ImageModel? {
           return Mapper<ImageModel>().map(JSONString: json)
       }
       
       static func toObject(value: Any?) -> ImageModel? {
           return Mapper<ImageModel>().map(JSONObject: value)
       }
       
       static func toObjectArray(value: Any?) -> [ImageModel]? {
           return Mapper<ImageModel>().mapArray(JSONObject: value)
       }
     
}



class DocumentResponseModel: Mappable {
    
    
    var image:String?
    var msg:String?
    var result:String?


     
     required init?(map: Map) {
         
     }
     
     func mapping(map: Map) {
         image <- map["image"]
         msg <- map["msg"]
        result <- map["result"]
     }
     
     static func toObject(json: String) -> DocumentResponseModel? {
           return Mapper<DocumentResponseModel>().map(JSONString: json)
       }
       
       static func toObject(value: Any?) -> DocumentResponseModel? {
           return Mapper<DocumentResponseModel>().map(JSONObject: value)
       }
       
       static func toObjectArray(value: Any?) -> [DocumentResponseModel]? {
           return Mapper<DocumentResponseModel>().mapArray(JSONObject: value)
       }
     
}








class ForgotModel: Mappable {
    
    
    var MOTP: Int?
    var EOTP: Int?
    var status: String?
    var email: String?
    var mobile: String?
    var user_id: Int?
    var password: String?
    var message: String?



    
    required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            MOTP <- map["MOTP"]
            EOTP <- map["EOTP"]
            status <- map["status"]
            email <- map["email"]
            mobile <- map["mobile"]
            user_id <- map["user_id"]
            password <- map["password"]
            message <- map["message"]



        }
        
        static func toObject(json: String) -> ForgotModel? {
              return Mapper<ForgotModel>().map(JSONString: json)
          }
          
          static func toObject(value: Any?) -> ForgotModel? {
              return Mapper<ForgotModel>().map(JSONObject: value)
          }
          
          static func toObjectArray(value: Any?) -> [ForgotModel]? {
              return Mapper<ForgotModel>().mapArray(JSONObject: value)
          }
    
}





class PathModel: Mappable {
    
    
    var logopath: String?
    var bannerpath: String?
    var filepath: String?


    
    
    required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            logopath <- map["logopath"]
            bannerpath <- map["bannerpath"]
            filepath <- map["filepath"]


        }
        
        static func toObject(json: String) -> PathModel? {
              return Mapper<PathModel>().map(JSONString: json)
          }
          
          static func toObject(value: Any?) -> PathModel? {
              return Mapper<PathModel>().map(JSONObject: value)
          }
          
          static func toObjectArray(value: Any?) -> [PathModel]? {
              return Mapper<PathModel>().mapArray(JSONObject: value)
          }
    
}



class ItemModel: Mappable {
    
    var homeData:[HomeData]?
    var topData:[HomeData]?
    var totalitemcount:Int?

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        homeData <- map["data"]
        topData <- map["topAds"]
        totalitemcount <- map["totalitemcount"]

    }
    
    static func toObject(json: String) -> ItemModel? {
          return Mapper<ItemModel>().map(JSONString: json)
      }
      
      static func toObject(value: Any?) -> ItemModel? {
          return Mapper<ItemModel>().map(JSONObject: value)
      }
      
      static func toObjectArray(value: Any?) -> [ItemModel]? {
          return Mapper<ItemModel>().mapArray(JSONObject: value)
      }
    
    
    
}
















class HomeModel: Mappable {
    
    var homeData:[HomeData]?
    var totalitemcount:Int?
    var countMessages:Int?
    var countItems:Int?



    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        homeData <- map["data"]
        totalitemcount <- map["totalitemcount"]
        countMessages <- map["countMessages"]
        countItems <- map["countItems"]

    }
    
    static func toObject(json: String) -> HomeModel? {
          return Mapper<HomeModel>().map(JSONString: json)
      }
      
      static func toObject(value: Any?) -> HomeModel? {
          return Mapper<HomeModel>().map(JSONObject: value)
      }
      
      static func toObjectArray(value: Any?) -> [HomeModel]? {
          return Mapper<HomeModel>().mapArray(JSONObject: value)
      }
    
    
    
}



class MakeFavoriteModel: Mappable
{
    var msg: String?
    var result: String?



    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        msg <- map["msg"]
        result <- map["result"]
    }
    
    static func toObject(json: String) -> MakeFavoriteModel? {
             return Mapper<MakeFavoriteModel>().map(JSONString: json)
         }
         
         static func toObject(value: Any?) -> MakeFavoriteModel? {
             return Mapper<MakeFavoriteModel>().map(JSONObject: value)
         }
         
         static func toObjectArray(value: Any?) -> [MakeFavoriteModel]? {
             return Mapper<MakeFavoriteModel>().mapArray(JSONObject: value)
         }
    
    
}




class HomeData: Mappable
{
    var ID: Int?
    var cate_gory_ID: Int?
    var sellerId: Int?
    var logo: String?
    var submitdate: String?
    var origionalSubmitDate: String?
    var catdetail: ItemDetail?



    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        ID <- map["ID"]
        cate_gory_ID <- map["cate_gory_ID"]
        sellerId <- map["sellerId"]
        logo <- map["logo"]
        submitdate <- map["submitdate"]
        catdetail <- map["catdetail"]
        origionalSubmitDate <- map["origionalSubmitDate"]

    }
    
    static func toObject(json: String) -> HomeData? {
             return Mapper<HomeData>().map(JSONString: json)
         }
         
         static func toObject(value: Any?) -> HomeData? {
             return Mapper<HomeData>().map(JSONObject: value)
         }
         
         static func toObjectArray(value: Any?) -> [HomeData]? {
             return Mapper<HomeData>().mapArray(JSONObject: value)
         }
    
    
}


class ItemDetail: Mappable
{
    
    var image: String?
    var isFavorite: Int?
    var isAdHighLight: Int?
    var isAdFeatured: Int?
    var isTopPaid: Int?
    var isUrgent: Int?
    var isSold: Int?
    var isVideo: Int?
    var totalImages: Int?
    var viewsCount: Int?
    var ptype: String?
    var videourl: String?
    var location: String?
    var price: Int?
    var title: String?
    var emerate: String?
    var seller_name: String?
    var seller_type: String?
    var make: String?
    var model: String?
    var mileage: String?
    var noOfBathRooms: String?
    var noOfBedRooms: String?
    var year: String?
    var favCount: Int?









     required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        image <- map["image"]
        isFavorite <- map["isFavorite"]
        isAdHighLight <- map["isAdHighLight"]
        isAdFeatured <- map["isAdFeatured"]
        isTopPaid <- map["isTopPaid"]
        isUrgent <- map["isUrgent"]
        totalImages <- map["totalImages"]
        viewsCount <- map["viewsCount"]
        isSold <- map["isSold"]
        location <- map["location"]
        price <- map["price"]
        title <- map["title"]
        ptype <- map["ptype"]
        emerate <- map["emerate"]
        videourl <- map["videourl"]
        isVideo <- map["isVideo"]
        seller_name <- map["seller_name"]
        seller_type <- map["seller_type"]
        make <- map["make"]
        model <- map["model"]
        mileage <- map["mileage"]
        noOfBathRooms <- map["noOfBathRooms"]
        noOfBedRooms <- map["noOfBedRooms"]
        year <- map["year"]
        favCount <- map["favCount"]

    }
    
    static func toObject(json: String) -> ItemDetail? {
             return Mapper<ItemDetail>().map(JSONString: json)
         }
         
         static func toObject(value: Any?) -> ItemDetail? {
             return Mapper<ItemDetail>().map(JSONObject: value)
         }
         
         static func toObjectArray(value: Any?) -> [ItemDetail]? {
             return Mapper<ItemDetail>().mapArray(JSONObject: value)
         }
    
    
}


//MARK: -DocListModel

class DocumentListModel: Mappable{
    
    var docSpecs:[Specifications]?
       
       required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
        //data
           docSpecs <- map["data"]
       }
       
       static func toObject(json: String) -> DocumentListModel? {
             return Mapper<DocumentListModel>().map(JSONString: json)
         }
         
         static func toObject(value: Any?) -> DocumentListModel? {
             return Mapper<DocumentListModel>().map(JSONObject: value)
         }
         
         static func toObjectArray(value: Any?) -> [DocumentListModel]? {
             return Mapper<DocumentListModel>().mapArray(JSONObject: value)
         }
    
}






//MARK: -CategoryListModel

class CategoryListModel: Mappable{
    
    var categoryItemData:[CategoryItemData]?
       
       required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
        //data
           categoryItemData <- map["subCategories"]
       }
       
       static func toObject(json: String) -> CategoryListModel? {
             return Mapper<CategoryListModel>().map(JSONString: json)
         }
         
         static func toObject(value: Any?) -> CategoryListModel? {
             return Mapper<CategoryListModel>().map(JSONObject: value)
         }
         
         static func toObjectArray(value: Any?) -> [CategoryListModel]? {
             return Mapper<CategoryListModel>().mapArray(JSONObject: value)
         }
    
}


class CategoryItemData: Mappable
{
    
    var ID: Int?
    var head_ID: Int?
    var cat_name: String?
    var nextdata: String?


    
    required init?(map: Map) {
              
    }
    init(catID: Int, headID:Int, name: String, next: String)
    {
        ID  = catID
        head_ID = headID
        cat_name = name
        nextdata = next
    }
          
    func mapping(map: Map) {
        
        ID <- map["ID"]
        head_ID <- map["head_ID"]
        cat_name <- map["cat_name"]
        nextdata <- map["nextdata"]

    }
          
    static func toObject(json: String) -> CategoryItemData? {
                return Mapper<CategoryItemData>().map(JSONString: json)
    }
            
    static func toObject(value: Any?) -> CategoryItemData? {
                return Mapper<CategoryItemData>().map(JSONObject: value)
    }
            
    static func toObjectArray(value: Any?) -> [CategoryItemData]? {
                return Mapper<CategoryItemData>().mapArray(JSONObject: value)
    }
}


//MARK: ItemDetail
class ADItemDetailData: Mappable
{
    
    var date: String?
    var description: String?
    var id: String?
    var images: [Images]?
    var isFavorite: Int?
    var lat_location: String?
    var location: String?
    var logo: String?
    var long_location: String?
    var price: String?
    var result: Int?
    var seller_id: Int?
    var seller_mobile: String?
    var specification: [Specifications]?
    var title: String?
    var user_id: String?
    var pic: String?
    var seller_name: String?
    var otherAttributes: ItemDetail?
    var shareUrl: String?
    var breadCrumbs: String?




    
    required init?(map: Map) {
              
    }
          
    func mapping(map: Map) {
        
        date <- map["date"]
        description <- map["description"]
        id <- map["id"]
        images <- map["images"]
        isFavorite <- map["isFavorite"]
        lat_location <- map["lat_location"]
        location <- map["location"]
        logo <- map["logo"]
        long_location <- map["long_location"]
        price <- map["price"]
        result <- map["result"]
        seller_id <- map["seller_id"]
        seller_mobile <- map["seller_mobile"]
        specification <- map["specification"]
        title <- map["title"]
        user_id <- map["user_id"]
        pic <- map["pic"]
        seller_name <- map["seller_name"]
        otherAttributes <- map["otherAttributes"]
        shareUrl <- map["shareUrl"]
        breadCrumbs <- map["breadCrumbs"]



    }
          
    static func toObject(json: String) -> ADItemDetailData? {
                return Mapper<ADItemDetailData>().map(JSONString: json)
    }
            
    static func toObject(value: Any?) -> ADItemDetailData? {
                return Mapper<ADItemDetailData>().map(JSONObject: value)
    }
            
    static func toObjectArray(value: Any?) -> [ADItemDetailData]? {
                return Mapper<ADItemDetailData>().mapArray(JSONObject: value)
    }
}



class Specifications: Mappable
{
    
    
    var item_name: String?
    var item_value: String?
    var filename: String?


    required init?(map: Map) {
              
    }
          
    func mapping(map: Map) {
        
        item_name <- map["item_name"]
        item_value <- map["item_value"]
        filename <- map["filename"]

    }
    
    static func toObject(json: String) -> Specifications? {
                  return Mapper<Specifications>().map(JSONString: json)
      }
              
      static func toObject(value: Any?) -> Specifications? {
                  return Mapper<Specifications>().map(JSONObject: value)
      }
              
      static func toObjectArray(value: Any?) -> [Specifications]? {
                  return Mapper<Specifications>().mapArray(JSONObject: value)
      }
}

class Images: Mappable
{
    
    var ad_img: String?
    
    required init?(map: Map) {
            
    }
    
    func mapping(map: Map) {
        ad_img <- map["ad_img"]

    }
    
    
    static func toObject(json: String) -> Images? {
                return Mapper<Images>().map(JSONString: json)
    }
            
    static func toObject(value: Any?) -> Images? {
                return Mapper<Images>().map(JSONObject: value)
    }
            
    static func toObjectArray(value: Any?) -> [Images]? {
                return Mapper<Images>().mapArray(JSONObject: value)
    }
    
}


class UserProfileInfoModel: Mappable
{
    
    var userProfile: [SellerInfoDetail]?




    
    required init?(map: Map) {
            
    }
    
    func mapping(map: Map)
    {
        userProfile <- map["data"]
    }
    
    
    static func toObject(json: String) -> UserProfileInfoModel? {
                return Mapper<UserProfileInfoModel>().map(JSONString: json)
    }
            
    static func toObject(value: Any?) -> UserProfileInfoModel? {
                return Mapper<UserProfileInfoModel>().map(JSONObject: value)
    }
            
    static func toObjectArray(value: Any?) -> [UserProfileInfoModel]? {
                return Mapper<UserProfileInfoModel>().mapArray(JSONObject: value)
    }
    
}



//MARK: SellerInfo


class SellerInfo: Mappable
{
    
    var joining_date: String?
    var UserName: String?
    var image: String?
    var msg: String?
    var result: Int?
    var seller_info: [SellerInfoDetail]?




    
    required init?(map: Map) {
            
    }
    
    func mapping(map: Map)
    {
        joining_date <- map["Joining Date"]
        UserName <- map["UserName"]
        image <- map["image"]
        msg <- map["msg"]
        result <- map["result"]
        seller_info <- map["seller_info"]


    }
    
    
    static func toObject(json: String) -> SellerInfo? {
                return Mapper<SellerInfo>().map(JSONString: json)
    }
            
    static func toObject(value: Any?) -> SellerInfo? {
                return Mapper<SellerInfo>().map(JSONObject: value)
    }
            
    static func toObjectArray(value: Any?) -> [SellerInfo]? {
                return Mapper<SellerInfo>().mapArray(JSONObject: value)
    }
    
}

class SellerInfoDetail: Mappable
{
    var item_name: String?
    var item_value: String?
    
    required init?(map: Map) {
               
       }
       
       func mapping(map: Map)
       {
           item_name <- map["item_name"]
           item_value <- map["item_value"]
         

       }
       
       
       static func toObject(json: String) -> SellerInfoDetail? {
                   return Mapper<SellerInfoDetail>().map(JSONString: json)
       }
               
       static func toObject(value: Any?) -> SellerInfoDetail? {
                   return Mapper<SellerInfoDetail>().map(JSONObject: value)
       }
               
       static func toObjectArray(value: Any?) -> [SellerInfoDetail]? {
                   return Mapper<SellerInfoDetail>().mapArray(JSONObject: value)
       }
}


//MARK: -Individual USR


class UserRegistrationModel: Mappable
{
   

    var eotp: Int?
    var message: String?
    var motp: Int?
    var type: Int?
    var user_id: Int?
    var Status: String?




    
    required init?(map: Map) {
               
       }
       
       func mapping(map: Map)
       {
           eotp <- map["eotp"]
           message <- map["message"]
           motp <- map["motp"]
           type <- map["type"]
           user_id <- map["user_id"]
           Status <- map["status"]

       

       }
       
       
       static func toObject(json: String) -> UserRegistrationModel? {
                   return Mapper<UserRegistrationModel>().map(JSONString: json)
       }
               
       static func toObject(value: Any?) -> UserRegistrationModel? {
                   return Mapper<UserRegistrationModel>().map(JSONObject: value)
       }
               
       static func toObjectArray(value: Any?) -> [UserRegistrationModel]? {
                   return Mapper<UserRegistrationModel>().mapArray(JSONObject: value)
       }
}


class UserOTPVerifiedModel: Mappable
{
   

    var message: String?
    var Status: String?




    
    required init?(map: Map) {
               
       }
       
       func mapping(map: Map)
       {
           message <- map["message"]
           Status <- map["status"]

       

       }
       
       
       static func toObject(json: String) -> UserOTPVerifiedModel? {
                   return Mapper<UserOTPVerifiedModel>().map(JSONString: json)
       }
               
       static func toObject(value: Any?) -> UserOTPVerifiedModel? {
                   return Mapper<UserOTPVerifiedModel>().map(JSONObject: value)
       }
               
       static func toObjectArray(value: Any?) -> [UserOTPVerifiedModel]? {
                   return Mapper<UserOTPVerifiedModel>().mapArray(JSONObject: value)
       }
}









class UserDataManager {

    static let sharedInstance = UserDataManager()
    var user: LoggedUserModel?
    
    init()
    {
        
    }
    
  

}






class LoggedUserModel: Mappable
{
   

    var mobile_login: String?
    var msg: String?
    var Status: String?

    var result: Int?
    var token: String?
    var userData: UserDataModel?
    var user_id: Int?





    
    required init?(map: Map) {
       }
       
       func mapping(map: Map)
       {
           mobile_login <- map["mobile_login"]
           msg <- map["message"]
        Status <- map["status"]

           result <- map["result"]
           token <- map["token"]
           userData <- map["userdata"]
        print(map["user_id"])
           user_id <- map["user_id"]
           if let id = user_id{
                  Utility.resetPlistValueForKey(key: YLA.USER_ID, value: String(id))}


       }
       
       
       static func toObject(json: String) -> LoggedUserModel? {
                   return Mapper<LoggedUserModel>().map(JSONString: json)
       }
               
       static func toObject(value: Any?) -> LoggedUserModel? {
                   return Mapper<LoggedUserModel>().map(JSONObject: value)
       }
               
       static func toObjectArray(value: Any?) -> [LoggedUserModel]? {
                   return Mapper<LoggedUserModel>().mapArray(JSONObject: value)
       }
}

class UserDataModel: Mappable
{
       var AccountStatus: String?
       var Brand: String?
       var BusinessEntity: String?
       var BusinessName: String?
       var DOB: String?
       var EOTP: String?
       var Email: String?
       var EntityType: String?
       var Gender: String?
       var ID: Int?
       var MOTP: Int?
       var Mobile: String?
       var Name: String?
       var Pic: String?
       var RegConfirm: String?
       var RegDateTime: String?
       var SubmitDate: String?
       var address: String?
       var auth_doc: String?
       var bizapprove: String?
       var bizbanner: String?
       var bizdetail: String?
       var bizlogo: String?
       var bizphone: String?
       var career_level: String?
       var contactemail: String?
       var contactmob: String?
       var contactposition: String?
       var cur_employee: String?
       var cur_location: String?
       var cur_position: String?
       var cv: String?
       var device_id: String?
       var e_id: String?
       var education: String?
       var emailconfirm: String?
       var experience: String?
       var facebook: String?
       var imp_note: String?
       var industry: String?
       var instagram: String?
       var ios_device_id: String?
       var language: String?
       var linkedin: String?
       var loc_lat: String?
       var loc_lng: String?
       var locationstr: String?
       var mobileconfirm: String?
       var otherdoc1: String?
       var otherdoc2: String?
       var passport: String?
       var password: String?
       var reraid: String?
       var tradelic: String?
       var twitter: String?
       var updated_at: String?
       var usertype: String?
       var visapage: String?
       var website: String?
       var youtube: String?



    
    
    required init?(map: Map) {
                 
         }
    
    
      func mapping(map: Map)
     {
        AccountStatus <- map["AccountStatus"]
        Brand <- map["Brand"]
        BusinessEntity <- map["BusinessEntity"]
        BusinessName <- map["BusinessName"]
        DOB <- map["DOB"]
        EOTP <- map["EOTP"]
        Email <- map["Email"]
        EntityType <- map["EntityType"]
        Gender <- map["Gender"]
        ID <- map["ID"]
        MOTP <- map["MOTP"]
        Mobile <- map["Mobile"]
        Name <- map["Name"]
        Pic <- map["Pic"]
        RegConfirm <- map["RegConfirm"]
        RegDateTime <- map["RegDateTime"]
        SubmitDate <- map["SubmitDate"]
        address <- map["address"]
        auth_doc <- map["auth_doc"]
        bizapprove <- map["bizapprove"]
        bizbanner <- map["bizbanner"]
        bizdetail <- map["bizdetail"]
        bizlogo <- map["bizlogo"]
        bizphone <- map["bizphone"]
        contactemail <- map["contactemail"]
        contactmob <- map["contactmob"]
        contactposition <- map["contactposition"]
        cur_employee <- map["cur_employee"]
        cur_location <- map["cur_location"]
        cur_position <- map["cur_position"]
        cv <- map["cv"]
        device_id <- map["device_id"]
        e_id <- map["e_id"]
        education <- map["education"]
        emailconfirm <- map["emailconfirm"]
        experience <- map["experience"]
        facebook <- map["facebook"]
        imp_note <- map["imp_note"]
        industry <- map["industry"]
        instagram <- map["instagram"]
        ios_device_id <- map["ios_device_id"]
        language <- map["language"]
        linkedin <- map["linkedin"]
        loc_lat <- map["loc_lat"]
        loc_lng <- map["loc_lng"]
        locationstr <- map["locationstr"]
        mobileconfirm <- map["mobileconfirm"]
        otherdoc1 <- map["otherdoc1"]
        otherdoc2 <- map["otherdoc2"]
        passport <- map["passport"]
        password <- map["password"]
        if let pw = password{
        Utility.resetPlistValueForKey(key: YLA.PASWRD, value: pw)}
        reraid <- map["reraid"]
        tradelic <- map["tradelic"]
        twitter <- map["twitter"]
        updated_at <- map["updated_at"]
        usertype <- map["usertype"]
        visapage <- map["visapage"]
        website <- map["website"]
        youtube <- map["youtube"]



             

    }
           
           
           static func toObject(json: String) -> UserDataModel? {
                       return Mapper<UserDataModel>().map(JSONString: json)
           }
                   
           static func toObject(value: Any?) -> UserDataModel? {
                       return Mapper<UserDataModel>().map(JSONObject: value)
           }
                   
           static func toObjectArray(value: Any?) -> [UserDataModel]? {
                       return Mapper<UserDataModel>().mapArray(JSONObject: value)
    }
    
    
    
    
}





//MARK: -ChatSessionModel
class ChatSessionModel: Mappable {
    
    
    var chat:[ChatSessionDataModel]?
    var msg:String?
    var result:Int?
    var countMessages:Int?
    


     
     required init?(map: Map) {
         
     }
     
     func mapping(map: Map) {
         chat <- map["data"]
         msg <- map["msg"]
         countMessages <- map["countMessages"]
         result <- map["result"]
     }
     
     static func toObject(json: String) -> ChatSessionModel? {
           return Mapper<ChatSessionModel>().map(JSONString: json)
       }
       
       static func toObject(value: Any?) -> ChatSessionModel? {
           return Mapper<ChatSessionModel>().map(JSONObject: value)
       }
       
       static func toObjectArray(value: Any?) -> [ChatSessionModel]? {
           return Mapper<ChatSessionModel>().mapArray(JSONObject: value)
       }
     
}




class ChatSessionDataModel: Mappable {
    
    var ad_detail:HomeData?
    var user_detail:UserDetailModel?
    var ad_id:Int?
    var chat_datetime:String?
    var chat_id:Int?
    var reciever_id:Int?
    var sender_id:Int?
    var text:String?

     required init?(map: Map) {
         
     }
     
     func mapping(map: Map) {
         ad_detail <- map["ad_info"]
         user_detail <- map["userinfo"]
         ad_id <- map["ad_id"]
         chat_datetime <- map["chat_datetime"]
         chat_id <- map["chat_id"]
         reciever_id <- map["reciever_id"]
         sender_id <- map["sender_id"]
         text <- map["text"]
     }
     
     static func toObject(json: String) -> ChatSessionDataModel? {
           return Mapper<ChatSessionDataModel>().map(JSONString: json)
       }
       
       static func toObject(value: Any?) -> ChatSessionDataModel? {
           return Mapper<ChatSessionDataModel>().map(JSONObject: value)
       }
       
       static func toObjectArray(value: Any?) -> [ChatSessionDataModel]? {
           return Mapper<ChatSessionDataModel>().mapArray(JSONObject: value)
       }
     
}



class UserDetailModel: Mappable {
    
  
    var pic:String?
    var name:String?
    var id:Int?
    var unreadMsgsCount:Int?
    var email:String?
    var lastTextDateTime:String?
    var lastTextMessage:String?


     required init?(map: Map) {
         
     }
     
     func mapping(map: Map) {
         pic <- map["pic"]
         name <- map["name"]
         id <- map["id"]
         lastTextDateTime <- map["lastTextDateTime"]
         lastTextMessage <- map["lastTextMessage"]
         unreadMsgsCount <- map["unreadMsgsCount"]

     }
     
     static func toObject(json: String) -> UserDetailModel? {
           return Mapper<UserDetailModel>().map(JSONString: json)
       }
       
       static func toObject(value: Any?) -> UserDetailModel? {
           return Mapper<UserDetailModel>().map(JSONObject: value)
       }
       
       static func toObjectArray(value: Any?) -> [UserDetailModel]? {
           return Mapper<UserDetailModel>().mapArray(JSONObject: value)
       }
     
}


class ADDetailModel: Mappable {
    
  
    var image:String?
    var price:Int?
    var title:String?
    var id:Int?
    var location:String?
    var submitdate:String?


  






     required init?(map: Map) {
         
     }
     
     func mapping(map: Map) {
         image <- map["image"]
         price <- map["price"]
         title <- map["title"]
         location <- map["location"]
         submitdate <- map["submitdate"]
         id <- map["id"]


    
     }
     
     static func toObject(json: String) -> UserDetailModel? {
           return Mapper<UserDetailModel>().map(JSONString: json)
       }
       
       static func toObject(value: Any?) -> UserDetailModel? {
           return Mapper<UserDetailModel>().map(JSONObject: value)
       }
       
       static func toObjectArray(value: Any?) -> [UserDetailModel]? {
           return Mapper<UserDetailModel>().mapArray(JSONObject: value)
       }
     
}






class ChatHistorySessionModel: Mappable {
    
    
    var chat:[ChatHistoryDataModel]?
    var status:String?
    var countMessages:Int?
    var onlineStatus:String?




     
     required init?(map: Map) {
         
     }
     
     func mapping(map: Map) {
         chat <- map["data"]
         status <- map["status"]
         countMessages <- map["countMessages"]
         onlineStatus <- map["onlineStatus"]
     }
     
     static func toObject(json: String) -> ChatHistorySessionModel? {
           return Mapper<ChatHistorySessionModel>().map(JSONString: json)
       }
       
       static func toObject(value: Any?) -> ChatHistorySessionModel? {
           return Mapper<ChatHistorySessionModel>().map(JSONObject: value)
       }
       
       static func toObjectArray(value: Any?) -> [ChatHistorySessionModel]? {
           return Mapper<ChatHistorySessionModel>().mapArray(JSONObject: value)
       }
     
}


class ChatHistoryDataModel: Mappable {
    
    
    var datetime:String?
    var fromuser:String?
    var fromuserid:Int?
    var message:String?
    var touser:String?
    var touserid:Int?





     
     required init?(map: Map) {
         
     }
     
     func mapping(map: Map) {
         datetime <- map["datetime"]
         fromuser <- map["fromuser"]
         fromuserid <- map["fromuserid"]
        message <- map["message"]
        touser <- map["touser"]
        touserid <- map["touserid"]

     }
     
     static func toObject(json: String) -> ChatHistoryDataModel? {
           return Mapper<ChatHistoryDataModel>().map(JSONString: json)
       }
       
       static func toObject(value: Any?) -> ChatHistoryDataModel? {
           return Mapper<ChatHistoryDataModel>().map(JSONObject: value)
       }
       
       static func toObjectArray(value: Any?) -> [ChatHistoryDataModel]? {
           return Mapper<ChatHistoryDataModel>().mapArray(JSONObject: value)
       }
     
}


class AdvanceMasterModel: Mappable {
    
    var filterMaster:[AdvanceMasterItemModel]?
    var status:String?

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        filterMaster <- map["data"]
        status <- map["status"]

    }
    
    static func toObject(json: String) -> AdvanceMasterModel? {
          return Mapper<AdvanceMasterModel>().map(JSONString: json)
      }
      
      static func toObject(value: Any?) -> AdvanceMasterModel? {
          return Mapper<AdvanceMasterModel>().map(JSONObject: value)
      }
      
      static func toObjectArray(value: Any?) -> [AdvanceMasterModel]? {
          return Mapper<AdvanceMasterModel>().mapArray(JSONObject: value)
      }
    
    
    
}

class AdvanceMasterItemModel: Mappable {
    
    
    var ID:Int?
    var isNext:Int?
    var item_compulsory:String?
    var item_data_type4db:String?
    var item_field_name:String?
    var item_field_type:String?
    var item_lable:String?
    var item_options:[String]?
    var item_placeholder:String?
    var item_title:String?









       
       required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           ID <- map["ID"]
           isNext <- map["isNext"]
           item_compulsory <- map["item_compulsory"]
           item_data_type4db <- map["item_data_type4db"]
           item_field_name <- map["item_field_name"]
           item_field_type <- map["item_field_type"]
           item_lable <- map["item_lable"]
           var str:String = ""
           str <- map["item_options"]
           item_options = str.components(separatedBy:",")
           item_placeholder <- map["item_placeholder"]
           item_title <- map["item_title"]

       }
       
       static func toObject(json: String) -> AdvanceMasterItemModel? {
             return Mapper<AdvanceMasterItemModel>().map(JSONString: json)
         }
         
         static func toObject(value: Any?) -> AdvanceMasterItemModel? {
             return Mapper<AdvanceMasterItemModel>().map(JSONObject: value)
         }
         
         static func toObjectArray(value: Any?) -> [AdvanceMasterItemModel]? {
             return Mapper<AdvanceMasterItemModel>().mapArray(JSONObject: value)
         }
    
    
}




class CredentialModel: Mappable {
    
    
    var motp: Int?
    var eotp: Int?
    var status: String?
    var message: String?
   

    
    required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            motp <- map["motp"]
            eotp <- map["eotp"]
            status <- map["status"]
            message <- map["message"]

        }
        
        static func toObject(json: String) -> CredentialModel? {
              return Mapper<CredentialModel>().map(JSONString: json)
          }
          
          static func toObject(value: Any?) -> CredentialModel? {
              return Mapper<CredentialModel>().map(JSONObject: value)
          }
          
          static func toObjectArray(value: Any?) -> [CredentialModel]? {
              return Mapper<CredentialModel>().mapArray(JSONObject: value)
          }
    
}




class WalletListModel: Mappable {
    
    
  
    var status: String?
    var message: String?
    var data: WalletDataModel?

    required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            status <- map["status"]
            message <- map["message"]
            data <- map["data"]


        }
        
        static func toObject(json: String) -> WalletListModel? {
              return Mapper<WalletListModel>().map(JSONString: json)
          }
          
          static func toObject(value: Any?) -> WalletListModel? {
              return Mapper<WalletListModel>().map(JSONObject: value)
          }
          
          static func toObjectArray(value: Any?) -> [WalletListModel]? {
              return Mapper<WalletListModel>().mapArray(JSONObject: value)
          }
    
}



class WalletDataModel: Mappable {
    
    
  
    var userData: WalletUserInfoModel?
    var transctions: [WalletTransactionModel]?

    required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            userData <- map["userInfo"]
            transctions <- map["transactions"]
        }
        
        static func toObject(json: String) -> WalletDataModel? {
              return Mapper<WalletDataModel>().map(JSONString: json)
          }
          
          static func toObject(value: Any?) -> WalletDataModel? {
              return Mapper<WalletDataModel>().map(JSONObject: value)
          }
          
          static func toObjectArray(value: Any?) -> [WalletDataModel]? {
              return Mapper<WalletDataModel>().mapArray(JSONObject: value)
          }
    
}







class WalletUserInfoModel: Mappable {
    
    
  
    var ID: Int?
    var userID: Int?
    var curr: String?
    var avail_bal: String?
    var ac_status: String?
    var status: String?
    var lastupdateon: String?

    required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            ID <- map["ID"]
            userID <- map["userID"]
            curr <- map["curr"]
            avail_bal <- map["avail_bal"]
            ac_status <- map["ac_status"]
            status <- map["status"]
            lastupdateon <- map["lastupdateon"]
        }
        
        static func toObject(json: String) -> WalletUserInfoModel? {
              return Mapper<WalletUserInfoModel>().map(JSONString: json)
          }
          
          static func toObject(value: Any?) -> WalletUserInfoModel? {
              return Mapper<WalletUserInfoModel>().map(JSONObject: value)
          }
          
          static func toObjectArray(value: Any?) -> [WalletUserInfoModel]? {
              return Mapper<WalletUserInfoModel>().mapArray(JSONObject: value)
          }
    
}



class WalletTransactionModel: Mappable {
    
    
  
    var tdate: String?
    var tdetail: String?
    var tdebit: String?
    var tcredit: String?
    var tbal: String?

    required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            tdate <- map["tdate"]
            tdetail <- map["tdetail"]
            tdebit <- map["tdebit"]
            tcredit <- map["tcredit"]
            tbal <- map["tbal"]
        }
        
        static func toObject(json: String) -> WalletTransactionModel? {
              return Mapper<WalletTransactionModel>().map(JSONString: json)
          }
          
          static func toObject(value: Any?) -> WalletTransactionModel? {
              return Mapper<WalletTransactionModel>().map(JSONObject: value)
          }
          
          static func toObjectArray(value: Any?) -> [WalletTransactionModel]? {
              return Mapper<WalletTransactionModel>().mapArray(JSONObject: value)
          }
    
}



class NotificationListModel: Mappable {
    
    
  
    var data: [NotificationItemModel]?
    var status: String?
   

    required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            data <- map["data"]
            status <- map["status"]
          
        }
        
        static func toObject(json: String) -> NotificationListModel? {
              return Mapper<NotificationListModel>().map(JSONString: json)
          }
          
          static func toObject(value: Any?) -> NotificationListModel? {
              return Mapper<NotificationListModel>().map(JSONObject: value)
          }
          
          static func toObjectArray(value: Any?) -> [NotificationListModel]? {
              return Mapper<NotificationListModel>().mapArray(JSONObject: value)
          }
    
}



class NotificationItemModel: Mappable {
    
    
  
    var id: Int?
    var ndate: String?
    var pic: String?
    var readStatus: Int?
    var text: String?
    var title: String?


    required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            id <- map["id"]
            ndate <- map["ndate"]
            pic <- map["pic"]
            readStatus <- map["readStatus"]
            text <- map["text"]
            title <- map["title"]
        }
        
        static func toObject(json: String) -> NotificationItemModel? {
              return Mapper<NotificationItemModel>().map(JSONString: json)
          }
          
          static func toObject(value: Any?) -> NotificationItemModel? {
              return Mapper<NotificationItemModel>().map(JSONObject: value)
          }
          
          static func toObjectArray(value: Any?) -> [NotificationItemModel]? {
              return Mapper<NotificationItemModel>().mapArray(JSONObject: value)
          }
    
}




