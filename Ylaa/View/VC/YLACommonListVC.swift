//
//  YLACommonListVC.swift
//  Ylaa
//
//  Created by Arun on 06/02/2020.
//  Copyright © 2020 Arun. All rights reserved.
//

import UIKit
protocol YLACommonListDelegate {
    func didSelectedItem(item: String, userType: String,atIndex: Int)
}
class YLACommonListVC: UIViewController,UITableViewDelegate,UITableViewDataSource{

    var listArray: [String]?
    var listdelegate: YLACommonListDelegate?
    var user_type: String?
    var selectedIndex: Int?

    
    
    
    
    
    
    
    @IBAction func closePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
             
        return listArray!.count
       }
         
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
        let cell = tableView.dequeueReusableCell(withIdentifier:"listcell",
                                                                         for: indexPath) as! profileTableCell
        cell.lblTitle.text = self.listArray![indexPath.row]
        return cell
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.user_type == "MyAds" ?         self.listdelegate?.didSelectedItem(item: self.listArray![indexPath.row], userType: self.user_type!,atIndex: 0) : self.listdelegate?.didSelectedItem(item: self.listArray![indexPath.row], userType: self.user_type!,atIndex: selectedIndex!)
     
        self.dismiss(animated: true, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
