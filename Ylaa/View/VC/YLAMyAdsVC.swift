//
//  YLAMyAdsVC.swift
//  Ylaa
//
//  Created by APPLE on 19/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class YLAMyAdsVC: BaseTabBarController,YLACommonListDelegate {
    
    
   
    
    
    
    
    @IBOutlet weak var lblResult: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCount: UILabel!

    @IBOutlet weak var listTable: UITableView!
    @IBOutlet weak var lblFilter: UILabel!
    var myAds: [HomeData]?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getMyAd()
      

        // Do any additional setup after loading the view.
    }
    
    
    func getMyAd()
    {
        self.showProgressYLA()
        NetworkManager.getMyAds(fStr: self.lblFilter.text!, completionHandler: self.requesMyAdsResponse(response:)) { (ERR) in
                  self.showToast(message: ERR)
                  self.hideProgressYLA()
                  self.lblCount.text = ""

              }
    }
    
    
    func getDeletedMyAd()
       {
           NetworkManager.viewDeletedAds(completionHandler: self.requesMyAdsResponse(response:)) { (ERR) in
                     self.showToast(message: ERR)
                 }
       }
    
    
    
    
    @IBAction func filterPressed(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "YLACommonListVCSBID") as! YLACommonListVC
                                  vc.modalPresentationStyle = .overCurrentContext
                                  vc.modalTransitionStyle = .crossDissolve
                   vc.listdelegate = self
                   vc.user_type = "MyAds"
                   vc.listArray = ["Approved","Pending","Decline","Expired","Deleted","Paused","Sold"]
                   self.present(vc, animated: true, completion: nil)
        
        
        
    }
    
    func requesMyAdsResponse(response: RequestResult)
    {
        self.hideProgressYLA()

        if  let jsonObject = response.value as? [String : Any?],let model = HomeModel.toObject(value: jsonObject) {
            if let cItem = model.countItems, cItem > 0{
                          self.lblCount.text  = "(" + String(cItem) + ")"
            }else{
                 self.lblCount.text  = ""
            }
            
           guard let home = model.homeData else{
            if self.myAds != nil{
                                          self.myAds?.removeAll()
                                          self.myAds = nil
                                      }
                           self.listTable.reloadData()
            self.lblResult.text = " Results (0) "
            self.showToast(message: "No result found")
            return}
            if home.count > 0{
            if self.myAds != nil{
                self.myAds?.removeAll()
                self.myAds = nil
            }
            
           self.myAds = home
            self.lblResult.text = " Results "  + "(" + String(home.count) + ") "
              

            self.listTable.reloadData()
            }
            else{
               
                if self.myAds != nil{
                                              self.myAds?.removeAll()
                                              self.myAds = nil
                                          }
                               self.listTable.reloadData()
                self.lblResult.text = " Results (0) "
                self.showToast(message: "No result found")
            }
        }
        else{
            self.showToast(message: "No result found")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension YLAMyAdsVC: UITableViewDelegate,UITableViewDataSource,MYADSDelegate{
    
    
    func didAlertAction(mData: HomeData, isDelete: String) {
        
        if isDelete == "SOLD"{
            setSoldAction(hData: mData)
        }
        else if isDelete == "DELETED"{
            getDeletedMyAd()
        }
        else if isDelete == "EDIT"{
            editAd(adData: mData)
        }
        else if isDelete == "UNSOLD"{
            sendMarkAsUnSold(mAd: mData)
        }
        else{
//            let alertController = UIAlertController(title: "Move item to deleted", message: "comments", preferredStyle: .alert)
//            alertController.addAction(UIAlertAction(title: "Submit", style: .default, handler: { alert -> Void in
//                let textField = alertController.textFields![0] as UITextField
//
//                if !textField.text!.isEmpty
//                {
                    self.sendMoveItemToDelete(mAd: mData, cmnts: "Delete")
//                }
//
//            }))
//            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//            alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
//                textField.placeholder = "comments"
//            })
//            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    
    func editAd(adData: HomeData)
    {
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let controller =  storyboardMain.instantiateViewController(withIdentifier: "YLAPostAdsVCSBID") as! YLAPostAdsVC
        controller.urlType = "EDIT"
        controller.adData = adData
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    
    
    func setSoldAction(hData: HomeData)
    {
        
        let optionMenu = UIAlertController(title: nil, message: "From where did you sold!", preferredStyle: .actionSheet)

                  let yla = UIAlertAction(title: "Sold on Ylaa.com", style: .default, handler: {
                      (alert: UIAlertAction!) -> Void in
                    self.sendMarkAsSold(mAd: hData,cmnts:"Sold on Ylaa.com")

                  })
                  let othweb = UIAlertAction(title: "Sold on other websites", style: .default, handler: {
                      (alert: UIAlertAction!) -> Void in
                   self.sendMarkAsSold(mAd: hData,cmnts:"Sold on other websites")
                  })
                 let sOff = UIAlertAction(title: "Sold Offline", style: .default, handler: {
                         (alert: UIAlertAction!) -> Void in
                   self.sendMarkAsSold(mAd: hData,cmnts:"Sold Offline")

                   })

               let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                   (alert: UIAlertAction!) -> Void in
               })

               
                  optionMenu.addAction(yla)
                  optionMenu.addAction(othweb)
                  optionMenu.addAction(sOff)
                  optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    
    func sendMarkAsSold(mAd: HomeData,cmnts: String)
    {
        NetworkManager.adSold(action: cmnts, adID: mAd.ID!, completionHandler: self.requestResponse(response: )) { (ERR) in
            self.showToast(message: ERR)
        }
    }
    
    func sendMarkAsUnSold(mAd: HomeData)
       {
           NetworkManager.adUnSold(adID: mAd.ID!, completionHandler: self.requestResponse(response: )) { (ERR) in
               self.showToast(message: ERR)
           }
       }
 
    
  
    func sendMoveItemToDelete(mAd: HomeData,cmnts: String)
       {
           NetworkManager.moveAdToDeleted(action: cmnts, adID: mAd.ID!, completionHandler: self.requestResponse(response: )) { (ERR) in
               self.showToast(message: ERR)
           }
       }
    
    
    
    func requestResponse(response: RequestResult)
       {
           if  let jsonObject = response.value as? [String : Any?],let model = UserOTPVerifiedModel.toObject(value: jsonObject) {
                      if model.Status == "success"
                      {
                        self.showToast(message: model.message!)
                        self.getMyAd()


                      }
                      else{
                     self.showToast(message: "Unable to complete operation")

                      }
                  }
       }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let count = self.myAds == nil ? 0 : self.myAds?.count
        return count!
       }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"adCells",
                                                            for: indexPath) as! YLAAdsTableCell
        cell.adDelegate = self
        if let aData = self.myAds{
            let myAD = aData[indexPath.section]
            cell.setMYAdCellData(adData: myAD, fstr: self.lblFilter.text!)
        }
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
      {
          
          
              let headerView = UIView(frame: CGRect(x: 0,y: 0,width: tableView.bounds.size.width,height: 10))
              return headerView
          
          
      }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let ads = self.myAds{
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                        let controller = storyboardMain.instantiateViewController(withIdentifier: "YLADetailVCSBID") as! YLADetailVC
            controller.adData = ads[indexPath.section]
                        self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    func didActionDone(isDone: Bool, msg: String) {
            self.showToast(message: msg)
            self.getMyAd()
    }

    
    func didEditPressed() {
          
    }
      
    func didAdDeleted(isDeleted: Bool) {
          
    }
    
    //MARK: YLACommonListDelegate
       func didSelectedItem(item: String, userType: String, atIndex: Int) {
        
        self.lblFilter.text = item
        item == "Deleted" ? self.getDeletedMyAd() : self.getMyAd()
            
        
        
           
       }
    
   
   
    
}
