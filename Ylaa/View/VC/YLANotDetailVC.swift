//
//  YLANotDetailVC.swift
//  Ylaa
//
//  Created by Arun on 02/05/2020.
//  Copyright © 2020 Arun. All rights reserved.
//

import UIKit

class YLANotDetailVC:BaseTabBarController,UITableViewDelegate,UITableViewDataSource {
    
    var notificationModel: NotificationItemModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        if let  not = self.notificationModel{
        self.readNotification(notf: not)
        }

        // Do any additional setup after loading the view.
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
           return 2
      }
      
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
               return 1
         }
           
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier:"notificationCell",
                                                                           for: indexPath) as! DocTableCell
            
            if let notf = self.notificationModel
            {
                cell.lblTitle.text = notf.title
                cell.lblDate.text = Utility.getStringDate(dateStr: notf.ndate!)
                if let pic =  notf.pic{
                   cell.thumbImgView.setImageForYlaaUser(url: pic, placeHolder: "no_pic")
                }
            }
            
            return cell

        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier:"dCell",
                                                                                     for: indexPath) as! DocTableCell
            if let notf = self.notificationModel
            {
               cell.lblTitle.text = notf.text
            }

            return cell
        }
         
    }
      
      func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
      {
          let headerView = UIView(frame: CGRect(x: 0,y: 0,width: tableView.bounds.size.width,height: 10))
          return headerView
                            
      }
      func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
      {
           return true
      }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    func readNotification(notf:NotificationItemModel)
       {
        NetworkManager.deleteNotification(isDel: false, notfID: notf.id!, completionHandler: self.deleteResponse(response:)) { (ERR) in
               self.showToast(message: ERR)
           }
       }
          
       func deleteResponse(response: RequestResult)
       {
           if  let jsonObject = response.value as? [String : Any?],let model = UserOTPVerifiedModel.toObject(value: jsonObject) {
               if model.Status == "success"
              {
               }
               else{
                 self.showToast(message: "Unable to complete operation")
               }
           }
       }
}
