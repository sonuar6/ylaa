//
//  YLAPostAdsVC.swift
//  Ylaa
//
//  Created by APPLE on 17/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit
import WebKit
class YLAPostAdsVC: BaseTabBarController,UIScrollViewDelegate, WKNavigationDelegate {

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var webView: WKWebView!
    var urlType: String?
    var adData: HomeData?

    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.isOpaque = false
        self.webView.scrollView.delegate = self


        //webView.isz = NO;


        self.webView!.navigationDelegate = self
        var url:NSURL?
        if self.urlType == "EDIT"{
            btnBack.isHidden = false
            titleLabel.text = "Edit Ad"
            if let adID =  self.adData?.ID{
                let urlStr = ServiceUtils.AD_EDIT + "p_id=" + String(adID) + "&user_id=\(Utility.getPlistValueforKey(key: YLA.UNAME))&user_pwd=\(Utility.getPlistValueforKey(key: YLA.PASWRD))&user_src=IOS"
            url = NSURL(string: urlStr)
         }
        }
        else if self.urlType == "POSTAD"{
            btnBack.isHidden = true
            titleLabel.text = "Post an Ad"
            url = NSURL(string:"https://ylaa.com/app-post-an-ad?user_id=\(Utility.getPlistValueforKey(key: YLA.UNAME))&user_pwd=\(Utility.getPlistValueforKey(key: YLA.PASWRD))&user_src=IOS")
        }
        else if self.urlType == "PVCY"{
            btnBack.isHidden = false
            titleLabel.text = "Privacy Policy"

            url = NSURL(string:ServiceUtils.PVC_PLCY)
        }else{
            btnBack.isHidden = false
            titleLabel.text = "Terms and Conditions"

            url = NSURL(string:ServiceUtils.TERMS_CONDNS)
        }
        self.showProgressYLA()
        let req = NSURLRequest(url:url! as URL)
        self.webView!.load(req as URLRequest)

        
        // Do any additional setup after loading the view.
    }
    func buttonClickEventTriggeredScriptToAddToDocument() ->String{
           let script:String = "window.webkit.messageHandlers.test.postMessage('Hello, world!');"
           // window.webkit.messageHandlers.test.postMessage("Hello, world!");
           return script
           
       }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    //MARK: WKNavigationDelegate
    /*
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
       if let adID = self.adData?.ID{
        if request.url?.scheme == "hrupin" {
                print("hrupin")
            if request.url?.host == "ad_detail_activity?p_id="  + String(adID) {
                print("ad_detail_activity")
            }
        }
        }
        return true
    }
 */
    
       func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.hideProgressYLA()
      }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.hideProgressYLA()

    }
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
               if navigationAction.navigationType == WKNavigationType.linkActivated {
                   print("link")
                 if let adData = self.adData{
                let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                          let controller = storyboardMain.instantiateViewController(withIdentifier: "YLADetailVCSBID") as! YLADetailVC
                          controller.adData = adData
                          self.navigationController?.pushViewController(controller, animated: true)
                }


                   decisionHandler(WKNavigationActionPolicy.cancel)
                   return
               }
               print("no link")
        if let adID = self.adData?.ID{
        if((webView.url?.absoluteString.contains("hrupin://ad_detail_activity?p_id="  + String(adID)))! && navigationAction.navigationType.rawValue == -1){
                   print("user click the button")
               }
        }
               decisionHandler(WKNavigationActionPolicy.allow)
                 
           }

  
    
    //MARK: - UIScrollViewDelegate
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }

}


