//
//  YLARegisterVC.swift
//  Ylaa
//
//  Created by Arun on 21/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class YLARegisterVC:BaseViewController, RegsiterTextDelegate,YLACommonListDelegate{
    
    

    
    var tempButton: UIButton?
    var registerUI: RegisterUI?
    var userSelectedItem: String = ""
    var isByMobile: Bool = true
    
    var businessType: String = ""
    var businessEntityType: String = ""


    
    var indUser:IndividualUserRegister?
    var bsnsUser:BusinessUserRegister?
    
    var task_Home: HomeTask?
    var onPopView: ((_ task:HomeTask)->())?




    @IBOutlet weak var bgScroll: UIScrollView!
    @IBOutlet weak var btnReg: UIButton!
    @IBOutlet weak var btnBsnuser: UIButton!
    @IBOutlet weak var listTable: UITableView!
    
    
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerUI = RegisterUI()
        tempButton = self.btnBsnuser
        
        
        
        if self.bsnsUser == nil
        {
            self.bsnsUser = BusinessUserRegister()
            //self.indUser?.type_id = 1
            self.bsnsUser?.usertype = "biz"
            self.bsnsUser?.regSource = "IOS"


        }
        
        if self.indUser == nil
        {
                  self.indUser = IndividualUserRegister()
                  //self.indUser?.type_id = 1
        }
        
        self.view.hideKeyboardOnTap()


        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.listTable.reloadData()

    }
    
    @IBAction func signUpPressed(_ sender: Any) {
     let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

        let btn = sender as! UIButton
        if btn.tag == 999{
        
        if self.tempButton?.tag == 111
        {
            self.indUser?.type_id = self.isByMobile == true ? 1 : 2
            self.indUser?.usertype = "cmn"
            self.indUser?.regSource = "IOS"

     
            let str = isValidForSend(obj: self.indUser!)
            str.isEmpty ? senduserReg() : showToast(message: str)
            
        }
        else{
           
            self.bsnsUser?.usertype = "biz"
            self.bsnsUser?.regSource = "IOS"

            let str = isValidForSend(obj: self.bsnsUser!)
            str.isEmpty ? sendBusinessUserReg() : showToast(message: str)
            
          

        }
        }  else  if btn.tag == 888 || btn.tag == 889 {
                       let controller =  mainStoryboard.instantiateViewController(withIdentifier: "YLAPostAdsVCSBID") as! YLAPostAdsVC
                     controller.urlType = btn.tag == 888 ? "TERMS" : "PVCY"
                       self.navigationController?.pushViewController(controller, animated: false)
              }
        else{
            let controller = mainStoryboard.instantiateViewController(withIdentifier: "YLALoginVCSBID") as! YLALoginVC
            controller.isFromReg = true
            self.navigationController?.pushViewController(controller, animated: false)
            
            
        }
        
        
    }
    
    func senduserReg()
    {
        NetworkManager.individualUserRegistration(indUserData: self.indUser!, completionHandler: self.requestResponse(response:)) { (ERR) in
            print(ERR)
        }
    }
    
    func sendBusinessUserReg()
       {
         self.showProgressYLA()

           NetworkManager.businessUserRegistration(bsnUserData: self.bsnsUser!, completionHandler: self.requestResponse(response:)) { (ERR) in
               print(ERR)
            self.hideProgressYLA()

           }
       }
    
    
    
    func requestResponse(response: RequestResult)
    {
        self.hideProgressYLA()

        if  let jsonObject = response.value as? [String : Any?],let model = UserRegistrationModel.toObject(value: jsonObject) {
            if model.Status == "Success" {
            if let userID = model.user_id, userID > 0
            {
                let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                
                let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAOtpVerifyVCSBID") as! YLAOtpVerifyVC
                controller.regUser = model
                self.navigationController?.pushViewController(controller, animated: true)
            }

            }
            else{
                showToast(message: model.message!)
            }
            
        }

    }
    
    
    @IBAction func optionPressed(_ sender: Any) {
        

        
        businessType = ""
        businessEntityType = ""
         userSelectedItem = ""
        isByMobile = true

       
           
        let btn = sender as! UIButton
        if btn == self.btnReg
        {
           
//            self.indUser = nil
//             self.bsnsUser =  nil
//            self.indUser = IndividualUserRegister()
            self.btnReg.backgroundColor = .white
            self.btnReg.setTitleColor(.customBlackColor, for: .normal)
            self.btnBsnuser.backgroundColor = .customBlackColor
            self.btnBsnuser.setTitleColor(.white, for: .normal)
        }
        else{
            
//            self.indUser = nil
//            self.bsnsUser = nil
//            self.bsnsUser = BusinessUserRegister()

            
            self.btnBsnuser.backgroundColor = .white
            self.btnBsnuser.setTitleColor(.customBlackColor, for: .normal)
            self.btnReg.backgroundColor = .customBlackColor
            self.btnReg.setTitleColor(.white, for: .normal)

        }
           tempButton = btn
           self.listTable.reloadData()

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func isValidForSend(obj: IndividualUserRegister)->String{
           
           var str = ""
           if obj.Name!.isEmpty{
               str =  "Name field is empty"
           }
           if obj.type_id == 1{
           if obj.Mobile!.isEmpty {
                      str =  "Mobile field is empty"
                  }
           }
           if obj.type_id == 2{
           if obj.Email!.isEmpty{
                             str =  "Email field is empty"
                         }
           }
           if obj.password!.isEmpty{
                                      str =  "Password field is empty"
                     }
                   if obj.password!.count < 8{
                         str =  "Password should have atleast 8 characters"
                             }
         
                  
           
           
           return str
       }
          
    
    func isValidForSend(obj: BusinessUserRegister)->String{
           
           var str = ""
           if obj.BusinessName!.isEmpty{
               str =  "Name field is empty"
           }
          
           if obj.Mobile!.isEmpty {
                      str =  "Mobile field is empty"
                  }
           
           if obj.Email!.isEmpty{
                             str =  "Email field is empty"
           }
           if obj.BusinessEntity!.isEmpty{
               str =  "Business Entity is empty"
           }
           if obj.EntityType!.isEmpty{
                      str =  "Business entity type field is empty"
                  }
           if obj.password!.isEmpty{
                             str =  "Password field is empty"
            }
          if obj.password!.count < 8{
                str =  "Password should have atleast 8 characters"
                    }
                          
           
           
           return str
       }

}
extension YLARegisterVC: UITableViewDelegate,UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.tempButton?.tag == 111
        {
            return (registerUI?.individualUser.count)! + 1
        }else{
            return (registerUI?.businessUser.count)! + 1

        }

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.tempButton?.tag == 111{
            if registerUI?.individualUser.count == indexPath.row{
                
                 let cell = tableView.dequeueReusableCell(withIdentifier:"submitCell",
                                                                            for: indexPath)
                return cell
            }else{
                if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier:"regCell",
                                                                                  for: indexPath) as! profileTableCell
                cell.textDelegate = self
                    cell.txtInput.text = self.indUser?.Name
                    cell.setTextFieldDelegate(textTag: indexPath.row, isText: true)
                return cell
                }
                else if indexPath.row == 1{
                    
                 
                    let cellIdentifier = isByMobile ? "mobCell" : "emailCell"
                                let cell = tableView.dequeueReusableCell(withIdentifier:cellIdentifier,
                                                                                                  for: indexPath) as! profileTableCell
                                cell.textDelegate = self
                    if isByMobile{
                                          cell.setTextBusinessFieldDelegate(textTag: indexPath.row, isText: true, image: #imageLiteral(resourceName: "smartphone"), text: "By Mobile Number")
                                      }else{
                                          cell.setTextBusinessFieldDelegate(textTag: indexPath.row, isText: true, image: #imageLiteral(resourceName: "email-1"), text: "By Email")

                                      }
                    //cell.setTextFieldDelegate(textTag: indexPath.row, isText: true)

                                return cell
                                }
                else if indexPath.row == 2{
                    let cellIdentifier = isByMobile ? "phoneCell" : "emailValCell"

                    let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                                        for: indexPath) as! profileTableCell
                    cell.txtInput.text = isByMobile ? self.indUser?.Mobile : self.indUser?.Email
                    isByMobile ? cell.setTextFieldDelegate(textTag: indexPath.row, isText: false) : cell.setTextFieldDelegate(textTag: indexPath.row, isText: true)

                    cell.textDelegate = self
                    return cell
                }
                else {
                                                             let cell = tableView.dequeueReusableCell(withIdentifier:"pasValCell",
                                                                                                                               for: indexPath) as! profileTableCell
                    cell.txtInput.text = self.indUser?.password
                                                             cell.textDelegate = self
                    cell.setTextFieldDelegate(textTag: indexPath.row, isText: true)

                                                             return cell
                                                             }
                

        }
        }
        else{
            if registerUI?.businessUser.count == indexPath.row{
                           
                            let cell = tableView.dequeueReusableCell(withIdentifier:"submitCell",
                                                                                       for: indexPath)
                           return cell
                       }else{
                         
                     if indexPath.row == 0{
                     let cell = tableView.dequeueReusableCell(withIdentifier:"comCell",
                                                                                       for: indexPath) as! profileTableCell
                        cell.txtInput.text = self.bsnsUser?.BusinessName

                     cell.textDelegate = self
                         cell.setTextFieldDelegate(textTag: indexPath.row, isText: true)
                     return cell
                     }
                  else if indexPath.row == 1{
                    let cell = tableView.dequeueReusableCell(withIdentifier:"mobCell",
                                                                                                          for: indexPath) as! profileTableCell
                                        cell.textDelegate = self
                        cell.txtInput.text = ""

                        let textStr = self.businessType.isEmpty ? "Business Type" : self.businessType
                        cell.setTextBusinessFieldDelegate(textTag: indexPath.row, isText: true, image: #imageLiteral(resourceName: "smartphone"),text: textStr)
                                        return cell
                    }
                else if indexPath.row == 2{
                                   let cell = tableView.dequeueReusableCell(withIdentifier:"mobCell",
                                                                                                                         for: indexPath) as! profileTableCell
                                                       cell.textDelegate = self
                        cell.txtInput.text = ""

                        let textStr = self.businessEntityType.isEmpty ? "Business Entity Type" : self.businessEntityType

                        cell.setTextBusinessFieldDelegate(textTag: indexPath.row, isText: true, image: #imageLiteral(resourceName: "agent"), text: textStr)
                                                       return cell
                                   }
                else if indexPath.row == 3{
                                                  let cell = tableView.dequeueReusableCell(withIdentifier:"phoneCell",
                                                                                                                                        for: indexPath) as! profileTableCell
                        cell.txtInput.text = self.bsnsUser?.Mobile

                                                                      cell.textDelegate = self
                                                                          cell.setTextFieldDelegate(textTag: indexPath.row, isText: false)
                                                                      return cell
                                                  }
                else if indexPath.row == 4{
                                                                let cell = tableView.dequeueReusableCell(withIdentifier:"emailValCell",
                                                                                                                                                      for: indexPath) as! profileTableCell
                        cell.txtInput.text = self.bsnsUser?.Email

                                                                                    cell.textDelegate = self
                                                                                        cell.setTextFieldDelegate(textTag: indexPath.row, isText: true)
                                                                                    return cell
                                                                }
                     else {
                let cell = tableView.dequeueReusableCell(withIdentifier:"pasValCell",
                                                                                                      for: indexPath) as! profileTableCell
                        cell.txtInput.text = self.bsnsUser?.password

                                    cell.textDelegate = self
                                        cell.setTextFieldDelegate(textTag: indexPath.row, isText: true)
                                    return cell
                }
                
                
                
                
                
                
                
                


                   }
        }
            
            
      
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

        if self.tempButton?.tag == 111
        {
            if indexPath.row == 1
            {
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "YLACommonListVCSBID") as! YLACommonListVC
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                vc.listArray = ["By Mobile Number","By Email"]
                vc.listdelegate = self
                vc.user_type = "individual"
                vc.selectedIndex = indexPath.row
                self.present(vc, animated: true, completion: nil)
            }
        }
        else{
            if indexPath.row == 1 || indexPath.row == 2{
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "YLACommonListVCSBID") as! YLACommonListVC
                           vc.modalPresentationStyle = .overCurrentContext
                           vc.modalTransitionStyle = .crossDissolve
            vc.listdelegate = self
            vc.user_type = "business"
            vc.selectedIndex = indexPath.row

            if indexPath.row == 1
            {
                vc.listArray = ["Automotive","Real Estate","Recruitment Company","Service Provider","Traders","Others"]

            }
            if indexPath.row == 2
                       {
                           vc.listArray = ["Authorized(Brand)","Private Business","Agent"]

                       }
            self.present(vc, animated: true, completion: nil)
        }

        }
    }
    
    
    
    
    func didSelectedItem(item: String, userType: String,atIndex: Int) {
        userSelectedItem = ""
        if userType == "individual"
        {
            userSelectedItem = item
            
            
            isByMobile = item == "By Mobile Number" ? true : false
            

            let indexPath = IndexPath(item: 1, section: 0)
            listTable.reloadRows(at: [indexPath], with: .right
            )
            
            let indexPaths = IndexPath(item: 2, section: 0)
            listTable.reloadRows(at: [indexPaths], with: .left)
            
        }
        else{
           
            if atIndex == 1
            {
                self.businessType = item
                self.bsnsUser?.BusinessEntity = self.businessType

            }
            if atIndex == 2
            {
                self.businessEntityType = item
                self.bsnsUser?.EntityType = self.businessEntityType

            }
            let indexPaths = IndexPath(item: atIndex, section: 0)
            listTable.reloadRows(at: [indexPaths], with: .left)
            
        }
    }
    
    func didMoveScrollForStartEdit(cellTextfield: UITextField) {
        
        let pointInTable:CGPoint = cellTextfield.superview!.convert(cellTextfield.frame.origin, to:listTable)

        var contentOffset:CGPoint = listTable.contentOffset
              contentOffset.y  = pointInTable.y
              if let accessoryView = cellTextfield.inputAccessoryView {
                  contentOffset.y -= accessoryView.frame.size.height
              }
        
         UIView.animate(withDuration: 0.25) {
            self.listTable.contentOffset = contentOffset
        }
    }
    
    
    func reloadTableCell(atIndex: Int)
    {
        let indexPath = IndexPath(item: atIndex, section: 0)
        listTable.reloadRows(at: [indexPath], with: .fade)
    }
    
    func textFieldDidEndEditing(textTag: Int, text: String,textfield: UITextField) {
        print(textTag)
        print(text)
    if self.tempButton?.tag == 111
     {
        switch textTag {
        case 0:
            
       
            self.indUser?.Name = text
            break
        case 2:
            if isByMobile{
                
                do{
                    _ =  try textfield.validatedText(validationType: textValidatorType.phonenumber)
                }
                catch
                {
                    showToast(message: (error as! textValidationError).message)
                    self.reloadTableCell(atIndex: textTag)
                    return

                }
                
                self.indUser?.Email = ""
                self.indUser?.Mobile = "9715" + text
            }else{
                self.indUser?.Mobile = ""
                do{
                                   _ =  try textfield.validatedText(validationType: textValidatorType.email)
                               }
                               catch
                               {
                                   showToast(message: (error as! textValidationError).message)
                                   self.reloadTableCell(atIndex: textTag)
                                   return

                               }
                self.indUser?.Email = text
            }
           break
        case 3:
//            do{
//                    _ =  try textfield.validatedText(validationType: textValidatorType.password)
//                        }
//                    catch
//                        {
//                          showToast(message: (error as! textValidationError).message)
//                          self.reloadTableCell(atIndex: textTag)
//                          return
//                        }
            self.indUser?.password = text
            break
        default:
            break
        }
     }
    else{
        switch textTag {
        case 0:
            self.bsnsUser?.BusinessName = text
            break
       
        case 3:
            do{
                               _ =  try textfield.validatedText(validationType: textValidatorType.phonenumber)
                           }
                           catch
                           {
                               showToast(message: (error as! textValidationError).message)
                               self.reloadTableCell(atIndex: textTag)
                               return

                           }
            self.bsnsUser?.Mobile =  "9715" + text
            break
        case 4:
            do{
                               _ =  try textfield.validatedText(validationType: textValidatorType.email)
                           }
                           catch
                           {
                               showToast(message: (error as! textValidationError).message)
                               self.reloadTableCell(atIndex: textTag)
                               return

                           }
            self.bsnsUser?.Email = text

            break
        case 5:
//            do{
//                                          _ =  try textfield.validatedText(validationType: textValidatorType.password)
//                                      }
//                                      catch
//                                      {
//                                          showToast(message: (error as! textValidationError).message)
//                                          self.reloadTableCell(atIndex: textTag)
//                                          return
//
//                                      }
            self.bsnsUser?.password = text
            break

        default:
            break
        }
        

    }
}

 
    
}
