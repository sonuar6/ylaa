//
//  YLAMessageVC.swift
//  Ylaa
//
//  Created by APPLE on 17/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class YLAMessageListVC: BaseTabBarController {
    
    var chatSession: [ChatSessionDataModel]?
    @IBOutlet weak var chatList: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showProgressYLA()
     

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NetworkManager.getMessageItemList(off: 0, completionHandler: self.responseMessageList(response: )) { (ERR) in
            self.showToast(message: ERR)
            self.hideProgressYLA()
        }
    }
    
    
    
    func responseMessageList(response: RequestResult)
    {
        self.hideProgressYLA()

        if  let jsonObject = response.value as? [String : Any?],let model =  ChatSessionModel.toObject(value: jsonObject) {
            self.tabView?.unReadMSg =  model.countMessages
            if model.chat!.count > 0 {
            self.chatSession = model.chat
            }
            else{
                self.showToast(message: "No message found")
            }
            chatList.reloadData()
        }

    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension YLAMessageListVC: UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count  = 0
        if let chat = self.chatSession
        {
            count = chat.count
        }
             
           return count
       }
         
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
               let cell = tableView.dequeueReusableCell(withIdentifier:"messageCell",
                                                                         for: indexPath) as! DocTableCell
        cell.lblCount.text = ""
        cell.lblCount.isHidden = true
        
        if let chat = self.chatSession
        {
               let chatData = chat[indexPath.row]
               cell.setChatCellData(chat: chatData)
        }
               return cell
            
          
       
       }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let chat = self.chatSession {
            let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAMessageVCSBID") as! YLAMessageVC
            controller.msgUserData = chat[indexPath.row]
            self.navigationController?.pushViewController(controller, animated: true)
        }

    }
    
    
}
