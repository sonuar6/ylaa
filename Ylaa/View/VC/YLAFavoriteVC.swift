//
//  YLAFavoriteVC.swift
//  Ylaa
//
//  Created by Arun on 20/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class YLAFavoriteVC: BaseTabBarController,YLACollectionDelegate {
   
    
    
    
    var searchText: String?
    var home: HomeData?

    
    
    
    func didFavoriteDone(done: Bool) {
        if done
        {
            getFavorite()
        }
    }
    
    
    
    
    
   
    
  
    
    func didPageScrollToIndex(pageIndex: NSInteger) {
        
    }
    func userLogin() {
          
    }
    func movetoSellerAds(home: HomeData) {
        if self.home == nil {
          let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                          let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAFavoriteVCSBID") as! YLAFavoriteVC
                          controller.home = home
                          self.navigationController?.pushViewController(controller, animated: true)
        }
    }
      
    
      func didVideoPressed(hData: HomeData) {
       
        if let vUrl = hData.catdetail!.videourl{
            self.playVideo(urlStr: vUrl)
        }
     }
   
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblFavorite: UILabel!
    @IBOutlet weak var favCollection: YLACollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.favCollection.initCollection(rowCount: 1, collection: .Home, scrollingDirection: .vertical)
        self.favCollection.collection_Type = .List
        self.favCollection.ylaDelegate = self
        if let sTxt = searchText{
            lblFavorite.text =  sTxt
            getSearchItem(txt: sTxt)
        } else if let hData = self.home{
            lblFavorite.text = hData.catdetail!.seller_name
            self.getSellerAd(home: hData)

        }
        else{
            lblFavorite.text = "Favorites"
            getFavorite()

        }

        
       


        // Do any additional setup after loading the view.
    }
    
    func getSellerAd(home: HomeData)
    {
        
        self.showProgressYLA()
//        print(home.catdetail?.seller_name)
//        print(home.sellerId)
        if let id = home.sellerId{
        NetworkManager.getSellerAds(sellerID: id, completionHandler: self.favoriteResponse(response:)) { (ERR) in
          self.reset(err: ERR)


            }}else
        {
            
            self.reset(err: "No Data Found")

        }
    }
       
    
    func getSearchItem(txt: String)
    {
        self.showProgressYLA()

        NetworkManager.getSearchKeywordAds(search: txt, completionHandler: self.favoriteResponse(response:)) { (ERR) in
                    self.reset(err: ERR)

                 }
    }
    
    func getFavorite()
    {
        self.showProgressYLA()

        NetworkManager.getUserFavouriteAds(completionHandler: self.favoriteResponse(response:)) { (ERR) in
            self.reset(err: ERR)

               }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func reset(err: String)
    {
        self.lblCount.text = ""
        self.showToast(message: err)
        self.hideProgressYLA()

    }
    
    
    func favoriteResponse(response: RequestResult)
    {
        self.hideProgressYLA()

        if  let jsonObject = response.value as? [String : Any?],let model = HomeModel.toObject(value: jsonObject) {
            guard let home = model.homeData else{return}
            self.favCollection.collection_Type = .List
            self.favCollection.homeData = home
            self.lblCount.text = "(" + String(home.count) + ")"
        }
    }
    
    
    
    
    
// MARK: -YLACollectionDelegate
  func didLoadMoreItems(offset: Int) {
         
     }
    
    func didSelectedItem(adData: HomeData) {
        self.hideProgressYLA()

        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboardMain.instantiateViewController(withIdentifier: "YLADetailVCSBID") as! YLADetailVC
        controller.adData = adData
               self.navigationController?.pushViewController(controller, animated: true)
    }
}
