//
//  YLAProfileVC.swift
//  Ylaa
//
//  Created by Arun on 20/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit
import MobileCoreServices
import Photos


class YLAProfileVC: BaseTabBarController,YLATextFieldDelegate,YLACommonListDelegate {
   
    
    
    
    @IBOutlet weak var listTable: UITableView!
    
    @IBOutlet weak var editImage: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var profImageView: UIImageView!
    
    var profImg:String = ""
    var bsnsImg:String = ""
    var bsnsLogo:String = ""


    
    
    
    var onProfileUpdated: ((_ isFinished:Bool)->())?

    var picker:UIImagePickerController? = UIImagePickerController()

    
    var viewCont: String?
    var imageUploadType: String?
    
    var logoImage: UIImage?
    var bannerImage: UIImage?
    
    var profileInfo:[SellerInfoDetail]?
    var bizImage:[SellerInfoDetail]?

    
    var updateEmail:Bool = false
    var updatePhone:Bool = false
    
    var emailText: String = ""
    var mobileText: String = ""

    
    var crdModel: CredentialModel?




    
//    var arrayF = ["Manager","Bachelors Degree","Dubai,UAE","Marketing Manager","Ylaa"]
//    var arrayE = ["Career Level","Education","Current Location","Current Position","Current Employee"]
    
    
    
    var arrayS1 = ["Name"]
    var arrayS2 = [UserDataManager.sharedInstance.user?.userData?.Name]
    
    var arrayS3 =  [String]()
    var arrayS4 : [String]?
    
    var arrayS5 =  [String]()
    var arrayS6 = [String]()

    var bsnsUserUpdate: UpdateBusinessUserParameters?
    var IndUserUpdate: UpdateIndividualUserParameters?


    
    
    
    
    @IBAction func imgBtnPressed(_ sender: Any) {
        self.imageUploadType = "profile"
        showFilterActionSheet()
        
    }
    
    
    
    @IBAction func submitPressed(_ sender: Any) {
        
        if (UserDataManager.sharedInstance.user?.userData!.usertype) == "biz"
        {
            NetworkManager.UpdateBusinessUserRegistration(bsnUserData: self.bsnsUserUpdate!, completionHandler: self.updateInResponse(response: )) { (ERR) in
                self.showToast(message: ERR)
            }
        }
        else{
            NetworkManager.UpdateIndividualUserRegistration(inUserData: self.IndUserUpdate!, completionHandler: self.updateInResponse(response: )) { (ERR) in
                           self.showToast(message: ERR)
                       }
        }

        
        
    }
    
    func updateInResponse(response: RequestResult)
       {
          self.hideProgressYLA()
           if  let jsonObject = response.value as? [String : Any?],let model = LoggedUserModel.toObject(value: jsonObject) {
               
               UserDataManager.sharedInstance.user = model
            if let userID = UserDataManager.sharedInstance.user!.userData?.ID, userID > 0
               {
                  self.onProfileUpdated!(true)
                  showToast(message: "Profile updated successfully")

               }
               
           }
               else{
                   Utility.resetLoginCredentials()
                   showToast(message: "Error occured")
               }
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func showFilterActionSheet()  {

              let optionMenu = UIAlertController(title: nil, message: "Picture", preferredStyle: .actionSheet)

              let camAction = UIAlertAction(title: "Camera", style: .default, handler: {
                  (alert: UIAlertAction!) -> Void in
                self.openCamera()


              })
              let libAction = UIAlertAction(title: "From Library", style: .default, handler: {
                  (alert: UIAlertAction!) -> Void in
             
                self.openGallary()

              })
           
              let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                  (alert: UIAlertAction!) -> Void in
              })


              // 4
              optionMenu.addAction(camAction)
              optionMenu.addAction(libAction)
              optionMenu.addAction(cancelAction)

              // 5
              self.present(optionMenu, animated: true, completion: nil)
          }
          
       
    
    
    
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.hideKeyboardOnTap()
        self.lblTitle.text = self.viewCont
        picker?.delegate = self
        
        self.editImage.isHidden = self.viewCont == YLA.PROF_OVRVIEW ? true : false


        
        let datestr = UserDataManager.sharedInstance.user?.userData!.DOB
        var dateString = ""
        if datestr!.isEmpty{
            dateString = ""
        }
        else{
            dateString = Utility.formatDoBSTR(dateStr: (UserDataManager.sharedInstance.user?.userData!.DOB)!)
        }
        
        
        
        
        if self.viewCont == YLA.PROF_OVRVIEW{
            
            self.showProgressYLA()
            
            NetworkManager.viewUerInfo(completionHandler: self.userResponse(response:)) { (ERR) in
                self.showToast(message: ERR)
            }
            
            
           
        }
        
        else{

        if (UserDataManager.sharedInstance.user?.userData!.usertype) == "biz"
        {
            self.editImage.isHidden = true
            profImageView.setImageForYlaaUser(url: (UserDataManager.sharedInstance.user?.userData!.bizlogo)!, placeHolder: "business_logo")

            arrayS3 = ["Gender","Business Entity","Business Description","Entity Type","Brand Name","Business Name","Birthday","Location","Address","Contact Position","Contact Mobile","Contact Email"]
            bsnsUserUpdate = UpdateBusinessUserParameters(uData: (UserDataManager.sharedInstance.user?.userData!)!)

            

            
            arrayS4 = [bsnsUserUpdate!.Gender!,bsnsUserUpdate!.BusinessType!,bsnsUserUpdate!.About!,bsnsUserUpdate!.BusinessType!,bsnsUserUpdate!.BrandName!,bsnsUserUpdate!.BusinessName!,bsnsUserUpdate!.DateofBirth!,bsnsUserUpdate!.Location!,bsnsUserUpdate!.BusinessAddress!,bsnsUserUpdate!.Designation!,bsnsUserUpdate!.BusinessPhone!,bsnsUserUpdate!.Email!]
            
            
            
            
                arrayS5 = ["Facebook","Twitter","Instagram","Youtube","LinkedIn"]
                arrayS6 = [(UserDataManager.sharedInstance.user?.userData!.facebook)!,(UserDataManager.sharedInstance.user?.userData!.twitter)!,(UserDataManager.sharedInstance.user?.userData!.instagram)!,(UserDataManager.sharedInstance.user?.userData!.youtube)!,(UserDataManager.sharedInstance.user?.userData!.linkedin)!]
        }
        else{
            IndUserUpdate = UpdateIndividualUserParameters(uData: (UserDataManager.sharedInstance.user?.userData!)!)
            profImageView.setImageForYlaaUser(url: (UserDataManager.sharedInstance.user?.userData!.Pic)!, placeHolder: "agent")

            
            arrayS3 = ["Gender","Date of Birth","Language","Location","Website"]
            arrayS4 = [(UserDataManager.sharedInstance.user?.userData!.Gender)!,dateString,"English",(UserDataManager.sharedInstance.user?.userData!.locationstr)!,(UserDataManager.sharedInstance.user?.userData!.website)!]

            
            arrayS5 = ["Facebook","Twitter","Instagram","Youtube","LinkedIn"]
            arrayS6 = [(UserDataManager.sharedInstance.user?.userData!.facebook)!,(UserDataManager.sharedInstance.user?.userData!.twitter)!,(UserDataManager.sharedInstance.user?.userData!.instagram)!,(UserDataManager.sharedInstance.user?.userData!.youtube)!,(UserDataManager.sharedInstance.user?.userData!.linkedin)!]
           }
        }

        
        
        
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    func userResponse(response: RequestResult)
    {
        self.hideProgressYLA()
        if  let jsonObject = response.value as? [String : Any?],let model = UserProfileInfoModel.toObject(value: jsonObject) {
            
            if let modelUser = model.userProfile
            {
                self.sortUserResp(uData: modelUser)
            }
        }
    }
    
    
    func sortUserResp(uData: [SellerInfoDetail])
    {
        self.profileInfo = [SellerInfoDetail]()
        self.bizImage = [SellerInfoDetail]()

        for uVal in  uData
        {
            if uVal.item_name == "Pic" || uVal.item_name == "bizlogo"
            {
                let imStr  =  uVal.item_name == "Pic" ? "agent" : "business_logo"
                self.profImageView.setImageForYlaaUser(url: uVal.item_value, placeHolder: imStr)
            }
            else if uVal.item_name == "bizbanner"
            {
                self.bizImage?.append(uVal)
            }
            else if uVal.item_name != "password"
            {
                self.profileInfo?.append(uVal)
            }
        }
        self.listTable.reloadData()

    }
    
    
    
    
    
    

}

extension YLAProfileVC: UITableViewDelegate,UITableViewDataSource,TextDelegate,RegsiterTextDelegate{
   
    
    func textFieldDidEndEditing(textTag: Int, text: String, textfield: UITextField) {
//        if textTag == 0 {
//            self.IndUserUpdate?.PersonName  = textfield.text
//        }
//        else if textTag == 1 {
//            editCredentials(titleStr: "Contact Phone", msg: "Are you sure, do you want change your contact number?", isMove: false,value: textfield.text!)
//
//        }
//        else if textTag == 2 {
//            editCredentials(titleStr: "Contact Email", msg: "Are you sure, do you want change your contact email?", isMove: false,value: textfield.text!)
//
//        }
    }
    
    
    func didTextEditStarted(cellTextField: UITextField) {
      
          //setTableScroll(textField: cellTextField)
        
        if cellTextField.tag == 0 {
                  self.IndUserUpdate?.PersonName  = cellTextField.text
              }
        else if cellTextField.tag == 1 {
            if (UserDataManager.sharedInstance.user?.userData!.Mobile)! == cellTextField.text{
                return
            }else{
                  mobileText = cellTextField.text!
                  editCredentials(titleStr: "Contact Phone", msg: "Are you sure, do you want change your contact number?", isMove: false,value: cellTextField.text!)
            }
            

              }
        else if cellTextField.tag == 2 {
            if (UserDataManager.sharedInstance.user?.userData!.Mobile)! == cellTextField.text{
                          return
                      }else{
                  emailText = cellTextField.text!
                  editCredentials(titleStr: "Contact Email", msg: "Are you sure, do you want change your contact email?", isMove: false,value: cellTextField.text!)
            }

              }

    }
      
      func didMoveScrollForStartEdit(cellTextfield: UITextField) {
          setTableScroll(textField: cellTextfield)
      }
      
      
 
 
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.viewCont == YLA.PROF_OVRVIEW{
            if ((UserDataManager.sharedInstance.user?.userData!.usertype)! == "biz"){
              return 2
            }else{
                return 1

            }
        }else{
            if (UserDataManager.sharedInstance.user?.userData!.usertype)! == "biz"{
                return 6

            }
            else{
                return 5
            }
       }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        
        var rowCount  = 0
        if self.viewCont == YLA.PROF_OVRVIEW{
           if ((UserDataManager.sharedInstance.user?.userData!.usertype)! == "biz"){
            switch section {
            case 0:
                rowCount = self.profileInfo != nil ? self.profileInfo!.count : 0
                break
            case 1:
                rowCount = self.bizImage != nil ? self.bizImage!.count : 0
                break
            default:
                break
            }
            
           }else{
              rowCount = self.profileInfo != nil ? self.profileInfo!.count : 0

            }


      
        }
        else{
          if (UserDataManager.sharedInstance.user?.userData!.usertype)! == "biz"{
                
                switch section {
                                  case 0:
                                      rowCount = 3
                                      break
                                  case 1:
                                   rowCount = arrayS1.count
                                      break
                                  case 2:
                                      rowCount = arrayS3.count

                                      break
                               case 3:
                                   rowCount = arrayS5.count
                                   break
                               case 4:
                               rowCount = 2
                               break
                    case 5:
                                             rowCount = 1
                                             break
                                  default:
                                      break
                                  }
                
                
                
              }else{

                switch section {
                       case 0:
                           rowCount = 3
                           break
                       case 1:
                        rowCount = arrayS1.count
                           break
                       case 2:
                           rowCount = arrayS3.count

                           break
                    case 3:
                        rowCount = arrayS5.count
                        break
                    case 4:
                    rowCount = 1
                    break
                       default:
                           break
                       }
                }
                
            }
            

            return rowCount

        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.viewCont == YLA.PROF_OVRVIEW{

       if indexPath.section == 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier:"profCell",
                                                            for: indexPath) as! profileTableCell
                
        if let uInfo = self.profileInfo{
            let uData = uInfo[indexPath.row]
            var titleStr: String = ""
            var valueStr: String = ""
            
            if let str = uData.item_name{
                
                if str == "usertype"{
                    titleStr = "Account Type"

                } else if str == "address"{
                    titleStr = "Address"
                }else if str == "locationstr"{
                    titleStr = "Location"
                }
                else if str == "AccountStatus"{
                    titleStr = "Account Status"
                }
                else if str == "website"{
                    titleStr = "Website"
                }
               else if str == "contactmob"{
                    titleStr = "Contact Mobile"
               }
               else if str == "bizdetail"{
                    titleStr = "Business Email"
               }
               else if str == "contactemail"{
                    titleStr = "Contact Email"
               }
               else if str == "facebook"{
                    titleStr = "Facebook"
               }
               else if str == "twitter"{
                    titleStr = "Twitter"
               }
               else if str == "instagram"{
                    titleStr = "Instagram"
               }
               else if str == "youtube"{
                    titleStr = "Youtube"
               }
               else if str == "linkedin"{
                    titleStr = "Linked In"
               }
               else if str == "language"{
                    titleStr = "Language"
                }
               
             
                else{
                    titleStr = str
                }
                
            }
            if let str = uData.item_value{
                if str == "cmn"{
                    valueStr = "Individual"
                }else if str == "biz"{
                    valueStr = "Bussiness"
                }
                else{
                valueStr = str
                }
            }

           

            cell.setProfileCell(titleStr: titleStr, subTitleStr: valueStr, isImgHidden: true)
        }
            return cell
        }
        else {
              let cell = tableView.dequeueReusableCell(withIdentifier:"logoCell",for: indexPath)as! profileTableCell
              if let bInfo = self.bizImage{
                let bData = bInfo[indexPath.row]
                cell.lblTitle.text = bData.item_name == "bizlogo" ? "Business Logo" : "Business Banner"
                let imStr  =  bData.item_name == "bizlogo" ? "business_logo" : "banner"

                cell.iconImageView.setImageForYlaaUser(url: bData.item_value, placeHolder: imStr)
             }
              return cell
            }

        }


        else{
               if indexPath.section == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier:"textCell",
                                                                           for: indexPath) as! textTableCell
                cell.textDelegate = self
                if indexPath.row == 0 || indexPath.row == 1{
                    indexPath.row == 0 ? cell.setTextCell(thumb: UIImage(named: "passwords")!, caption: "Change Password", textVal: (UserDataManager.sharedInstance.user?.userData!.password)!, isSecured: true, enabled: true,tTag: indexPath.row) : cell.setTextCell(thumb: UIImage(named: "smartphones")!, caption: "Change Phone Number", textVal: (UserDataManager.sharedInstance.user?.userData!.Mobile)!, isSecured: false,enabled: true,tTag: indexPath.row)
                }
                else{
                    cell.setTextCell(thumb: UIImage(named: "message-closed-envelope")!, caption: "Change Contact Email", textVal: (UserDataManager.sharedInstance.user?.userData!.Email)!, isSecured: false,enabled: true,tTag: indexPath.row)
                }
                return cell
                
                
            }
            else if indexPath.section == 4  && (UserDataManager.sharedInstance.user?.userData!.usertype)! != "biz"{
                    let cell = tableView.dequeueReusableCell(withIdentifier:"subCell",
                                                                               for: indexPath)
                    return cell
                    
                    
            }
            else if indexPath.section == 4  && (UserDataManager.sharedInstance.user?.userData!.usertype)! == "biz"{
                               let cell = tableView.dequeueReusableCell(withIdentifier:"logoCell",
                                                                                          for: indexPath)as! profileTableCell
                if indexPath.row == 0{
                    if let logo = self.logoImage{
                    cell.iconImageView.image = logo

                    }else{
                        cell.iconImageView.setImageForYlaaUser(url: self.bsnsUserUpdate?.Logo, placeHolder: "business_logo")
                        
                    }

                cell.lblTitle.text = "Business Logo"
                    
                }else{
                    if let banr = self.bannerImage{
                                       cell.iconImageView.image = banr

                                       }else{
                        cell.iconImageView.setImageForYlaaUser(url: self.bsnsUserUpdate?.Banner, placeHolder: "banner")
                    }
                    cell.lblTitle.text = "Banner Image"
                                     
                }
                return cell
                               
                               
                       }
                       
            
            
            
           else if indexPath.section == 5  && (UserDataManager.sharedInstance.user?.userData!.usertype)! == "biz"{
                                  let cell = tableView.dequeueReusableCell(withIdentifier:"subCell",
                                                                                             for: indexPath)
                                  return cell
                                  
                                  
                          }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier:"profCell",
                                                                           for: indexPath) as! profileTableCell
                cell.textDelegate = self

                if  indexPath.section == 3{
                    //arrayS6[indexPath.row]
                    cell.setProfileCell(titleStr: arrayS5[indexPath.row], subTitleStr: arrayS6[indexPath.row], isImgHidden: true)
                }
                else{
                    indexPath.section == 1 ? cell.setProfileCell(titleStr: arrayS1[indexPath.row], subTitleStr: arrayS2[indexPath.row]!, isImgHidden: true) :  cell.setProfileCell(titleStr: arrayS3[indexPath.row], subTitleStr: arrayS4!    [indexPath.row], isImgHidden: false)
                }
                return cell

            }
          
            
            
        }

    }

   func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
   {
          
       
              let headerView = UIView(frame: CGRect(x: 0,y: 0,width: tableView.bounds.size.width,height: 1))
              headerView.backgroundColor = section == 0 ? .clear : .customBlackColor
              return headerView
       
   }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        if indexPath.section == 0{
            
            if indexPath.row == 0
            {
            }
            else if indexPath.row == 1
            {
                if !self.updatePhone{
                }
            }
            else{
                
                }

            }
            
        
        else{
        let cell = tableView.cellForRow(at: indexPath) as! profileTableCell

                if  (UserDataManager.sharedInstance.user?.userData!.usertype)! == "biz"{
                    if indexPath.section == 4 {
                        self.imageUploadType = ""
                        self.imageUploadType = indexPath.row == 0 ? "logo" : "banner"
                    self.showFilterActionSheet()
                    }else if indexPath.section == 1
                    {
                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "YLATextFieldVCSBID") as! YLATextFieldVC
                        vc.modalPresentationStyle = .overCurrentContext
                        vc.modalTransitionStyle = .crossDissolve
                        vc.textStr = cell.lblSubTitle.text
                        vc.forIndexPath = indexPath
                        vc.txtDelegate = self
                        self.present(vc, animated: true, completion: nil)
                    }
                    else if indexPath.section == 2
                    {
                        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 3
                        {
                            let vc = mainStoryboard.instantiateViewController(withIdentifier: "YLACommonListVCSBID") as! YLACommonListVC
                                                      vc.modalPresentationStyle = .overCurrentContext
                                                      vc.modalTransitionStyle = .crossDissolve
                                       vc.listdelegate = self
                                       vc.user_type = "business"
                                       vc.selectedIndex = indexPath.row
                            switch indexPath.row {
                            case 0:
                                vc.listArray =  ["Male","Female"]
                                break
                            case 1:
                                vc.listArray = ["Automotive","Real Estate","Recruitment Company","Service Provider","Traders","Others"]
                                break
                            case 3:
                                vc.listArray = ["Authorized(Brand)","Private Business","Agent"]
                                break
                            default:
                                break
                            }
                            self.present(vc, animated: true, completion: nil)
                        }
                        else if indexPath.row == 2 || indexPath.row == 4  || indexPath.row == 5 || indexPath.row == 7 || indexPath.row == 8 || indexPath.row == 9 || indexPath.row == 10 || indexPath.row == 11 || indexPath.row == 6
                        {
                            let vc = mainStoryboard.instantiateViewController(withIdentifier: "YLATextFieldVCSBID") as! YLATextFieldVC
                                           vc.modalPresentationStyle = .overCurrentContext
                                           vc.modalTransitionStyle = .crossDissolve
                                           vc.textStr = cell.lblSubTitle.text
                                           vc.forIndexPath = indexPath
                            if indexPath.row == 6 {
                                vc.typeStr = "Picker"}
                                           vc.txtDelegate = self
                                           self.present(vc, animated: true, completion: nil)
                        }
                    }
                    else if indexPath.section == 3
                    {
                                 let vc = mainStoryboard.instantiateViewController(withIdentifier: "YLATextFieldVCSBID") as! YLATextFieldVC
                                 vc.modalPresentationStyle = .overCurrentContext
                                 vc.modalTransitionStyle = .crossDissolve
                                 vc.textStr = cell.lblSubTitle.text
                                 vc.forIndexPath = indexPath
                                 vc.txtDelegate = self
                                 self.present(vc, animated: true, completion: nil)
                    }
                   
                    
             }
        else{
            if indexPath.section == 1
            {
                if indexPath.row == 0 || indexPath.row == 3 || indexPath.row == 4{
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "YLATextFieldVCSBID") as! YLATextFieldVC
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                vc.textStr = cell.lblSubTitle.text
                vc.forIndexPath = indexPath
                vc.txtDelegate = self
                self.present(vc, animated: true, completion: nil)
                }
            }
            else if indexPath.section == 2
            {
              if  indexPath.row == 0
              {
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "YLACommonListVCSBID") as! YLACommonListVC
                                              vc.modalPresentationStyle = .overCurrentContext
                                              vc.modalTransitionStyle = .crossDissolve
                               vc.listdelegate = self
                               vc.user_type = "individual"
                               vc.selectedIndex = indexPath.row
                    switch indexPath.row {
                    case 0:
                        vc.listArray =  ["Male","Female"]
                        break
                      
                    default:
                        break
                    }
                    self.present(vc, animated: true, completion: nil)
              }
                
               if  indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 1{
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "YLATextFieldVCSBID") as! YLATextFieldVC
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                vc.textStr = cell.lblSubTitle.text
                if  indexPath.row == 1
                {
                    vc.typeStr = "Picker"
                }
                vc.forIndexPath = indexPath
                vc.txtDelegate = self
                self.present(vc, animated: true, completion: nil)
                }
            }
            else if indexPath.section == 3
                      {
                                   let vc = mainStoryboard.instantiateViewController(withIdentifier: "YLATextFieldVCSBID") as! YLATextFieldVC
                                   vc.modalPresentationStyle = .overCurrentContext
                                   vc.modalTransitionStyle = .crossDissolve
                                   vc.textStr = cell.lblSubTitle.text
                                   vc.forIndexPath = indexPath
                                   vc.txtDelegate = self
                                   self.present(vc, animated: true, completion: nil)
                      }
        }
        }
    }
    
    func didSelectedItem(item: String, userType: String, atIndex: Int) {
       if  (UserDataManager.sharedInstance.user?.userData!.usertype)! == "biz"
       {
        switch atIndex {
        case 0:
            bsnsUserUpdate?.Gender = item
            break
        case 1:
            bsnsUserUpdate?.BusinessType = item
            break
        case 3:
            break
            case 6:
                bsnsUserUpdate?.DateofBirth = Utility.formatedDOB(dateStr: item)

                       break
        default:
            break
        }
        arrayS4![atIndex] = item
        let indexPaths = IndexPath(item: atIndex, section: 2)
            listTable.reloadRows(at: [indexPaths], with: .fade)
        
        }
       else{
        switch atIndex {
        case 0:
            IndUserUpdate?.Gender = item
            break
            default:
                       break
        }
            arrayS4![atIndex] = item
               let indexPaths = IndexPath(item: atIndex, section: 2)
                   listTable.reloadRows(at: [indexPaths], with: .fade)
        
        }

    }
    
    
      func didSelectedItem(item: String, atIndex: IndexPath) {
          
        if  (UserDataManager.sharedInstance.user?.userData!.usertype)! == "biz"{
            
            if atIndex.section == 1{
                bsnsUserUpdate?.Name = item
                arrayS2[atIndex.row] = item
            }
            else if atIndex.section == 2{
                switch atIndex.row {
                       case 2:
                           bsnsUserUpdate?.About = item
                           break
                       case 4:
                           bsnsUserUpdate?.BrandName = item
                           break
                       case 5:
                        bsnsUserUpdate?.BusinessName = item
                           break
                      case 7:
                            bsnsUserUpdate?.Location = item
                            break
                    case 8:
                    bsnsUserUpdate?.BusinessAddress = item
                    break
                    case 9:
                                      bsnsUserUpdate?.Designation = item
                                      break
                    case 10:
                                      bsnsUserUpdate?.BusinessPhone = item
                                      break
                    case 11:
                                      bsnsUserUpdate?.Email = item
                                      break
                       default:
                           break
                       }
                arrayS4![atIndex.row] = item
            }
            else if atIndex.section == 3{
                switch atIndex.row {
                 case 0:
                     bsnsUserUpdate?.Facebook = item
                     break
                 case 1:
                     bsnsUserUpdate?.Twitter = item
                     break
                 case 2:
                  bsnsUserUpdate?.Instagram = item
                     break case 3:
                                        bsnsUserUpdate?.Youtube = item

                                          break
                                        case 4:
                                            bsnsUserUpdate?.LinkedIn = item

                                        break
                default:
                    break
                }
                arrayS6[atIndex.row] = item

            }
                
            
         
        }
        else{
            if atIndex.section == 1{
                self.IndUserUpdate?.PersonName = item
                arrayS2[atIndex.row] = item
                }
            if atIndex.section == 2{
                switch atIndex.row {
                    case 1:
                        IndUserUpdate?.Dob = Utility.formatedDOB(dateStr: item)
                                      break
//                case 3:
//                    IndUserUpdate?.Email = item
//                    break
                case 3:
                    IndUserUpdate?.Location = item
                    break
                case 4:
                    IndUserUpdate?.Website = item
                    break
                default:
                    break
                }
                arrayS4![atIndex.row] = item

            }
            else if atIndex.section == 3{
                switch atIndex.row {
                 case 0:
                     IndUserUpdate?.Facebook = item
                     break
                 case 1:
                     IndUserUpdate?.Twitter = item
                     break
                 case 2:
                  IndUserUpdate?.Instagram = item
                     break
                case 3:
                    IndUserUpdate?.Youtube = item

                      break
                    case 4:
                        IndUserUpdate?.LinkedIn = item

                    break
                default:
                    break
                }
                arrayS6[atIndex.row] = item

            }

        }
          listTable.reloadRows(at: [atIndex], with: .right)

          
      }
    
    
    
    
  
    
    
    
    
    
    
    
    
    
    
    
     
    func setTableScroll(textField: UITextField){
    //          let pointInTable:CGPoint = cellTextfield.superview!.convert(cellTextfield.frame.origin, to:tab)
               //
               //          var contentOffset:CGPoint = listTable.contentOffset
               //                contentOffset.y  = pointInTable.y
               //                if let accessoryView = cellTextfield.inputAccessoryView {
               //                    contentOffset.y -= accessoryView.frame.size.height
               //                }
               //
               //           UIView.animate(withDuration: 0.25) {
               //              self.listTable.contentOffset = contentOffset
               //          }
    }
    
}

extension YLAProfileVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
     private func openGallary()
        {
//          if Utility.checkPhotoEnabled()
//          {
            picker!.allowsEditing = false
            picker!.sourceType = UIImagePickerController.SourceType.photoLibrary
            present(picker!, animated: true, completion: nil)
//          }else
//          {
//            self.showToast(message: "Please enable access for camera and Photo Library to complete the operation")
          //}
        }
        
        
        private func openCamera()
        {
//          if Utility.checkMediaEnabled()
//          {
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
                picker!.allowsEditing = false
                picker!.sourceType = UIImagePickerController.SourceType.camera
                picker!.cameraCaptureMode = .photo
                present(picker!, animated: true, completion: nil)
            }else{
                let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
                let ok = UIAlertAction(title: "Ok", style:.default, handler: nil)
                alert.addAction(ok)
                present(alert, animated: true, completion: nil)
            }
//            }else
//          {
//            self.showToast(message: "Please enable access for camera and Photo Library to complete the operation")
//          }
        }
        
        
        //delegate for gallery or camera picker
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            
            
        
            var selectedImage: UIImage?
            var fileName : String?
            if let editedImage = info[.editedImage] as? UIImage {
                selectedImage = editedImage
                picker.dismiss(animated: true, completion: nil)
            } else if let originalImage = info[.originalImage] as? UIImage {
                selectedImage = originalImage
                picker.dismiss(animated: true, completion: nil)
            }
         /*
            if let assetPath = info[UIImagePickerController.InfoKey.referenceURL] as? NSURL{
                
                if (assetPath.absoluteString?.hasSuffix("JPG"))! || (assetPath.absoluteString?.hasSuffix("JPEG"))! || (assetPath.absoluteString?.hasSuffix("jpg"))! || (assetPath.absoluteString?.hasSuffix("jpeg"))!  {
                    if let jpegdata = selectedImage?.compressTo(3)!.base64EncodedString(){
                        self.IndUserUpdate?.Pic = jpegdata
                    }
                }
                if (assetPath.absoluteString?.hasSuffix("PNG"))! || (assetPath.absoluteString?.hasSuffix("png"))! {
                    if let pngdata = PngImageFileB64(uploadImage: selectedImage){
                         self.IndUserUpdate?.Pic = pngdata
                        
                    }
                }
            }else{
                if let jpegdata = selectedImage?.compressTo(3)!.base64EncodedString(){
                    self.IndUserUpdate?.Pic = jpegdata
                }
                
            }
            
            if #available(iOS 11.0, *) {
                if let asset = info[UIImagePickerController.InfoKey.phAsset] as? PHAsset {
                    if let Name = (asset.value(forKey: "filename")) as? String {
                        fileName = Name
                    }
                }
            } else {
                // Fallback on earlier versions
                if let imageURL = info[UIImagePickerController.InfoKey.referenceURL] as? URL {
                    let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
                    if let first = result.firstObject {
                        if let assetResourcesFirst = PHAssetResource.assetResources(for:first ).first{
                            fileName = assetResourcesFirst.originalFilename
                        }
                    }
                    
                }
            }
 */
            //adding item to uploadList
            
         
            
            //        if let pngdata = PngImageFileB64(uploadImage: selectedImage){
            //            uploadItem.file_uploaded = pngdata
            //            uploadItem.file_extension = "png"
            //        }
            //        else if let jpegdata = JpegImageFileB64(uploadImage: selectedImage){
            //            uploadItem.file_uploaded = jpegdata
            //            uploadItem.file_extension = "jpeg"
            //        }
//            if uploadItem.file_extension == nil{
//                showMessage(message: "Image format not supports".localized(), viewController: self, handler: nil)
//                return
//            }
//            DocList.append(uploadItem)
//            //adding selected item to collection
//            addedItem()
            
            var file = ""
            switch self.imageUploadType {
            case "logo":
                self.logoImage = selectedImage
                file = "logo"
                let indexPaths = IndexPath(item: 0, section: 4)
                listTable.reloadRows(at: [indexPaths], with: .fade)
                break
            case "banner":
                self.bannerImage = selectedImage
                file = "banner"
                let indexPaths = IndexPath(item: 1, section: 4)
                listTable.reloadRows(at: [indexPaths], with: .fade)
                  break
            case "profile":
                  file = "userpic"
                  self.profImageView.image = selectedImage
                  break
            default:
                break
            }
//            if let pngdata = PngImageFileB64(uploadImage: selectedImage){
//                 self.IndUserUpdate?.Pic = pngdata
//
//            }
            
            
            NetworkManager.uploadImage(isDOC: false, filename: file, image: selectedImage!, completionHandler: self.imageResponse(response: )) { (ERR) in
                self.showToast(message: ERR)
            }
            
        }
    
  
    
    func imageResponse(response: RequestResult)
    {
        if  let jsonObject = response.value as? [String : Any?],let model = ImageModel.toObject(value: jsonObject) {
            if model.Status == "success"
            {
                switch self.imageUploadType {
                         case "logo":
                            self.bsnsUserUpdate?.bizlogo = model.pathData?.logopath
                             break
                         case "banner":
                             self.bsnsUserUpdate?.bizbanner = model.pathData?.bannerpath
                               break
                         case "profile":
                             self.IndUserUpdate?.Pic = model.pathData?.filepath
                               break
                         default:
                             break
                         }
            }
        }

    }
    
    
    
    
    
    func editCredentials(titleStr: String, msg: String,isMove:Bool,value:String)
       {
           
           let alertController = UIAlertController(title: titleStr, message: msg, preferredStyle: .alert)
           alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { alert -> Void in
            isMove ? self.pushToOTP() : self.validateCredential(val: value)
           }))
           alertController.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
           self.present(alertController, animated: true, completion: nil)
           
           
       }
    
    
    
    func pushToOTP()
    {
        if let mod = self.crdModel{
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAOtpVerifyVCSBID") as!YLAOtpVerifyVC
        controller.credential = mod
        controller.onCredenetialEdited = {  mobile  in
              if (UserDataManager.sharedInstance.user?.userData!.usertype) == "biz"
              {
                if mobile{
                    self.bsnsUserUpdate?.BusinessMobile = self.mobileText
                }else{
                    self.bsnsUserUpdate?.Email = self.emailText

                }
              }else{
                if mobile{
                    self.IndUserUpdate?.Mobile = self.mobileText
                              }else{
                    self.IndUserUpdate?.Email = self.emailText

                              }
             }

            self.submitPressed(self)
            
            }
        self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    
    
    func validateCredential(val: String){
        self.showProgressYLA()
//        let str = isEMail ? (UserDataManager.sharedInstance.user?.userData!.Email)! : (UserDataManager.sharedInstance.user?.userData!.Mobile)!
        print(val)
        NetworkManager.vaidateCredential(info: val, completionHandler: self.credentialresponse(response:)) { (ERR) in
            self.hideProgressYLA()
            self.showToast(message: ERR)
        }
        
    }
    
    
    func credentialresponse(response: RequestResult)
    {
        self.hideProgressYLA()
           if  let jsonObject = response.value as? [String : Any?],let model = CredentialModel.toObject(value: jsonObject) {
               if model.status == "Success"
               {
                self.crdModel = model
                let str = model.motp! > 0 ?  "Please verify OTP send to your phone" : "Please verify OTP send to your email"
                editCredentials(titleStr: "", msg: str, isMove: true, value: "")
               }else{
                self.showToast(message: model.message!)
            }
        }
    }
    
    
    
    
    
}
