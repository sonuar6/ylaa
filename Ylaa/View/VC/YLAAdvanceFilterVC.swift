//
//  YLAAdvanceFilterVC.swift
//  Ylaa
//
//  Created by Arun on 17/03/2020.
//  Copyright © 2020 Arun. All rights reserved.
//

import UIKit

enum SubOptions {
    case Radio,Text,DropDown,CheckBox,Price,Year
}



class YLAAdvanceFilterVC: BaseViewController {
    
    @IBOutlet weak var mTable: UITableView!
    @IBOutlet weak var sTable: UITableView!
    @IBOutlet weak var txtStitle: UILabel!
    
    
    var tempCell:DocTableCell?
    var tempSCell:DocTableCell?

    var advFilter:[AdvanceMasterItemModel]?
    var selctedMaster:AdvanceMasterItemModel?

    var slaveFilter = [String]()
    var itemCatID: Int?
    var subRowCount: Int = 0
    
    var maxYear: String = ""
    var minYear: String = ""
    
    var filterModel = AdvanceFilterModel()
    var infoDict = [String:String]()
    
    var onAdvanceFilterDone: ((_ filter:AdvanceFilterModel,_ isReset:Bool)->())?




    
    
    
    
    var subRow:SubOptions?{
                      didSet {
                       guard let sOption = subRow  else { return }
                        switch sOption {
                        case .Radio:
                              subRowCount = slaveFilter.count
                            break
                        case .Text:
                              subRowCount = 2
                            break
                        case .DropDown:
                              subRowCount = slaveFilter.count
                            break
                        case .CheckBox:
                              subRowCount = slaveFilter.count
                              break
                        case .Price:
                              subRowCount = 2
                              break
                   
                        case .Year:
                              subRowCount = 2
                            break
                        }
                        
                    
                      }
                  }
           
    
    
    @IBAction func closePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func resetDone(_ sender: Any) {
        if self.filterModel.info.count > 0 {
        self.filterModel.info.removeAll()
        self.minYear = ""
        self.maxYear = ""
        tempCell = nil
        tempSCell = nil
        self.mTable.reloadData()
        self.sTable.reloadData()
        self.onAdvanceFilterDone!(filterModel, true)
        self.dismiss(animated: true, completion: nil)
        }else{
            self.showToast(message: "No filters are applied yet")
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let catID = filterModel.category_id, catID > 0
        {
           
        }else{
            filterModel.category_id = itemCatID

        }
//        filterModel.offset = 0
//        filterModel.limit = 10
     
        
        
        
        
        self.view.hideKeyboardOnTap()
        self.getMasterData()


        // Do any additional setup after loading the view.
    }
    

   func getMasterData()
      {
        NetworkManager.getMasterDataFilter(catID: self.itemCatID!, completionHandler: self.masterResponse(response:)) { (ERR) in
              
          }
      }
      
      
      
      
      func masterResponse(response: RequestResult)
      {
          if  let jsonObject = response.value as? [String : Any?],let model = AdvanceMasterModel.toObject(value: jsonObject) {
            if let adModel = model.filterMaster{
                self.advFilter = adModel
                self.mTable.reloadData()
//                if let catID = filterModel.category_id, catID > 0{
//                    self.mTable.reloadData()
//                }
//
//               else if
                    if let mData = self.advFilter{
                        if mData.count > 0{
                        
                              let mdata = mData[0]
                              self.selctedMaster = mdata
                            setSlaveTable(mData: mdata)
                        }else{
                            showToast(message: "No filters found")
                        }
                }
            }
            
        }

      }

}


extension YLAAdvanceFilterVC: UITableViewDataSource,UITableViewDelegate,YLACommonListDelegate,TextDelegate{
    func didTextEditStarted(cellTextField: UITextField) {

        if cellTextField.tag == 0
       {
        self.minYear = cellTextField.text!
          
       }else{
           if let mnYr = Int(self.minYear){
            if Int(mnYr) < Int(cellTextField.text!)!
           {
            self.removeKeyValue(model: self.selctedMaster!)

               self.maxYear = cellTextField.text!
               let dict  = ["name" : self.selctedMaster?.item_field_name,"type":"range", "min": self.minYear, "max": self.maxYear]
               self.filterModel.info.append(dict as! [String : String])
            self.onAdvanceFilterDone!(filterModel, false)
               
            
           } else{
               self.showToast(message: "Please select larger value than minimum")
           }
         }
        }
            
    }
    
    func didSelectedItem(item: String, userType: String , atIndex: Int) {
        
        
        if atIndex == 0
        {
            self.minYear = item
           
        }else{
            if let mnYr = Int(self.minYear){
            if Int(mnYr) < Int(item)!
            {
                self.removeKeyValue(model: self.selctedMaster!)

                self.maxYear = item
                let dict  = ["name" : self.selctedMaster?.item_field_name,"type":"range", "min": self.minYear, "max": self.maxYear]
                self.filterModel.info.append(dict as! [String : String])
                self.onAdvanceFilterDone!(filterModel, false)
                            
                
                
            } else{
                self.showToast(message: "Please select larger value than minimum")
            }
          }
             
        }
        let indexPath = IndexPath(item: atIndex, section: 0)
        sTable.reloadRows(at: [indexPath], with: .right)
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
              return 1
          }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if tableView == mTable{
            count = self.advFilter != nil ? self.advFilter!.count : 0
        }
        else{
            count = self.subRowCount
        }
        return count
    }

      
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           if tableView == mTable{

           let cell = tableView.dequeueReusableCell(withIdentifier:"mCell",
                                                               for: indexPath) as! DocTableCell
           
            if indexPath.row == 0 && tempCell == nil
            {
                tempCell = cell
//                if let bView = cell.contentView.viewWithTag(111){
                    cell.backgroundColor = .orangeColor
                //}
            }
           
            if let mData = self.advFilter{
                let mdata = mData[indexPath.row]
                cell.lblTitle.text =  mdata.item_placeholder
            }
            
           return cell
        }
           else{
            if self.subRow == .CheckBox || self.subRow == .Radio || self.subRow == .DropDown{
            
            let cell = tableView.dequeueReusableCell(withIdentifier:"sCell",
                                                                         for: indexPath) as! DocTableCell
            cell.lblTitle.text =  self.slaveFilter[indexPath.row]
            

            if let valDict = getValueForMaster(model: self.selctedMaster!)
            {
                         if let val = valDict["value"], val == cell.lblTitle.text{
                            cell.thumbImgView.image =  #imageLiteral(resourceName: "iconSel")
                            tempSCell = cell
                            
                         }else{
                            cell.thumbImgView.image =   #imageLiteral(resourceName: "iconUnSel")
                        }
                          
            }else
            {
                cell.thumbImgView.image =   #imageLiteral(resourceName: "iconUnSel")
                tempSCell = cell
            }

//            else if indexPath.row == 0 && tempSCell == nil{
//                cell.thumbImgView.image = indexPath.row == 0 ? #imageLiteral(resourceName: "iconSel") : #imageLiteral(resourceName: "iconUnSel")
//                tempSCell = cell
//            }
 
                
          
                
                
            return cell
            }
            else if self.subRow == .Text || self.subRow == .Price || self.subRow == .Year
            {
                let cell = tableView.dequeueReusableCell(withIdentifier:"textCell",
                for: indexPath) as! textTableCell
                cell.textDelegate = self
               
                
                
                
                if self.subRow == .Year{
                   
                    if let valDict = getValueForMaster(model: self.selctedMaster!)
                    {
                        self.minYear = valDict["min"]!
                        self.maxYear = valDict["max"]!
                    }

                    indexPath.row == 0 ? cell.setFilterTextCell(caption: "From", textVal:  self.minYear , isNumber: true, enabled: false, tTag: indexPath.row) : cell.setFilterTextCell(caption: "To", textVal:  self.maxYear , isNumber: true,enabled: false, tTag: indexPath.row)
                }else{
                    if let valDict = getValueForMaster(model: self.selctedMaster!)
                    {
                           self.minYear = valDict["min"]!
                           self.maxYear = valDict["max"]!
                    }else{
                            self.minYear = ""
                            self.maxYear = ""
                    }
                    
                    indexPath.row == 0 ? cell.setFilterTextCell(caption: "Minimum", textVal: self.minYear, isNumber: true, enabled: true, tTag: indexPath.row) : cell.setFilterTextCell(caption: "Maximum", textVal: self.maxYear, isNumber: true,enabled: true,tTag: indexPath.row)

                }
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier:"textCell",
                for: indexPath) as! textTableCell
                
                return cell

            }
         }
       }
    
    
    
    
    func setSlaveTable(mData: AdvanceMasterItemModel)
    {
        guard var options = mData.item_options else {
            return
        }
       
        options.removeAll(where: { $0.hasPrefix("++") })

         switch mData.item_field_type {
            
                    case "DropDown":
                        self.slaveFilter = options
                        self.subRow = mData.item_placeholder == "Year" ? .Year : .DropDown
                        break
                    case "Radio":
                          self.slaveFilter = options
                          self.subRow = .Radio
                          break
                    case  "Text":
                        if mData.item_placeholder == "Price"
                        {
                             self.subRow = .Price
                        }else{
                            self.subRow = .Text
                        }
                        break
                    case  "CheckBox":
                          self.subRow = .CheckBox
                          break
                    default:
                        break
                    
                    }
        
       
           
            self.sTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == mTable{
            self.slaveFilter.removeAll()
            //if let bView = tempCell?.contentView.viewWithTag(111){
            tempCell!.backgroundColor = .clear
            tempCell = nil

            //}
            let cell = mTable.cellForRow(at: indexPath) as! DocTableCell
           // if let bView = cell.contentView.viewWithTag(111){
                cell.backgroundColor = .orangeColor
            //}

            tempCell = cell

            if let mData = self.advFilter{
               let mdata = mData[indexPath.row]
               self.selctedMaster = mdata
               //txtStitle.text =  mdata.item_title
                setSlaveTable(mData: mdata)
              
        }
            

        }
        if tableView == sTable{
            
            if self.subRow == .CheckBox || self.subRow == .Radio || self.subRow == .DropDown{
            let cell = sTable.cellForRow(at: indexPath) as! DocTableCell
            tempSCell?.thumbImgView.image = #imageLiteral(resourceName: "iconUnSel")
            cell.thumbImgView.image = cell.thumbImgView.image ==  #imageLiteral(resourceName: "iconUnSel") ? #imageLiteral(resourceName: "iconSel") : #imageLiteral(resourceName: "iconUnSel")
            tempSCell = cell
                
                if let mast = self.selctedMaster
                {

//                    filterModel.info.removeValue(forKey: mast.item_field_name!)
//                    filterModel.info[mast.item_field_name!] = cell.lblTitle.text!
                    if cell.lblTitle.text == "All"
                    {
                        removeKeyValue(model: mast)
                    }else{
                        removeKeyValue(model: mast)
                        let dict  = ["name" : mast.item_field_name,"type":"text", "value": cell.lblTitle.text!]
                        self.filterModel.info.append(dict as! [String : String])
                        
                    }
                    self.onAdvanceFilterDone!(filterModel, false)
                }
                
                
            }
            else if self.subRow == .Year
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "YLACommonListVCSBID") as! YLACommonListVC
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                vc.listArray = self.slaveFilter
                vc.listdelegate = self
                vc.user_type = ""
                vc.selectedIndex = indexPath.row
                self.present(vc, animated: true, completion: nil)
             }

        }
        
    }
    
    
//    func getValueForMasterOption(model: AdvanceMasterItemModel)->String?
//    {
//        var isPresent:String = ""
//        if let val = self.filterModel.info[model.item_field_name!]
//        {
//            isPresent = val as! String
//        }
//
//
//        return isPresent
//
//    }

    
    
    func removeKeyValue(model: AdvanceMasterItemModel)
    {
        if self.filterModel.info.count > 0
        {
            let filteredDetails = self.filterModel.info.filter { !$0.values.contains(model.item_field_name!) }
            print(filteredDetails)
            self.filterModel.info = filteredDetails
        }

    }
    
    func getValueForMaster(model: AdvanceMasterItemModel)-> [String:String]?
    {
        var dict: [String: String]?
        if self.filterModel.info.count > 0
        {
            for mdict in filterModel.info
            {
                if mdict["name"] == model.item_field_name
                {
                    dict = (mdict as? [String : String])!
                    break
                }
            }
        }
        
        return dict
    }

   
}
