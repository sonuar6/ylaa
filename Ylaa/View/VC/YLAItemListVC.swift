//
//  YLAItemListVC.swift
//  Ylaa
//
//  Created by Arun on 14/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class YLAItemListVC: BaseTabBarController,YLACollectionDelegate {
    func didVideoPressed(hData: HomeData) {
        if let vUrl = hData.catdetail!.videourl{
               self.playVideo(urlStr: vUrl)
               }
        
    }
    
    func didFavoriteDone(done: Bool) {
        
    }
    
   
    
   
  
    
  
   

    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var btnDrop: UIButton!
    @IBOutlet weak var topAdHgtConst: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var lblLatest: UILabel!
    @IBOutlet weak var topViewHgtConst: NSLayoutConstraint!
    
    
    
    var category: CategoryItemData?
    var itemData: ItemModel?
    var keyword: String = ""
    var emirate: String = ""
    var CurrentPage: Int = 0
    var totalPageCount: Int = 0
    var filter: AdvanceFilterModel?

    
    
    func didPageScrollToIndex(pageIndex: NSInteger) {
        
    }
    
   
    
    func userLogin() {
           
       }
       
      func movetoSellerAds(home: HomeData) {
              let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                   let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAFavoriteVCSBID") as! YLAFavoriteVC
                   controller.home = home
                   self.navigationController?.pushViewController(controller, animated: true)
      }
       
    
    func reset(err: String)
       {
           self.lblCount.text = ""
           self.showToast(message: err)
           self.hideProgressYLA()

       }
    
    
    @IBAction func tabPressed(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        switch btn.tag {
        case 0:
              self.locationView.isHidden = false

              UIView.animate(withDuration: 0.2, animations: {() -> Void in
                                                      self.locationView.frame.origin.x = 0
                                                        }, completion: {(_ finished: Bool) -> Void in
                                                            
                                                          })
            break
        case 778:
            showFilterActionSheet()
            break
        case 779:
            self.itemCollectionView.collection_Type = .List
            break
        case 780:
            self.itemCollectionView.collection_Type = .Column

            break
        case 781:
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "YLAAdvanceFilterVCSBID") as! YLAAdvanceFilterVC
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.itemCatID = self.category?.ID
            if let adftr = self.filter{
                vc.filterModel = adftr
            }
            vc.onAdvanceFilterDone = { filter, isReset in
                
                self.resetSearch()
                if isReset{
                  self.filter  = nil
                  self.getItems(off: 0)
                }else{
                    self.filter  = filter
                    self.getAdvanceItem(fModel: filter)
                }
            }
            self.present(vc, animated: true, completion: nil)
            break
            
        default:
            break
        }
        
        
        
        
    }
    
    
    
    
    
    
    func showFilterAd(fltr: String)
    {
        resetSearch()
        self.topView.isHidden = false
        self.topAdHgtConst.constant = 236.0
        self.keyword = fltr
        self.getItems(off: self.CurrentPage)
    }
    
    
    func showFilterActionSheet()  {

        self.cancelView()
           let optionMenu = UIAlertController(title: nil, message: "Sort By", preferredStyle: .actionSheet)

           let newAction = UIAlertAction(title: "Latest Ads", style: .default, handler: {
               (alert: UIAlertAction!) -> Void in
            self.lblLatest.text = "Latest Ads"
            self.showFilterAd(fltr: "Latest")
           

           })
           let oldAction = UIAlertAction(title: "Oldest Ads", style: .default, handler: {
               (alert: UIAlertAction!) -> Void in
            self.lblLatest.text = "Oldest Ads"
            self.showFilterAd(fltr: "Oldest Ads")
           })
          let cheapAction = UIAlertAction(title: "Cheapest Price", style: .default, handler: {
                  (alert: UIAlertAction!) -> Void in
            self.lblLatest.text = "Cheapest Price"
            self.showFilterAd(fltr: "Cheapest Ads")

            })

        
          let hghAction = UIAlertAction(title: "Highest Price", style: .default, handler: {
               (alert: UIAlertAction!) -> Void in
            self.lblLatest.text = "Highest Price"
            self.showFilterAd(fltr: "Highest Ads")
           })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })

        
           optionMenu.addAction(newAction)
           optionMenu.addAction(oldAction)
           optionMenu.addAction(cheapAction)
           optionMenu.addAction(hghAction)
        
        if self.category?.head_ID == 1
        {
            let yLAction = UIAlertAction(title: "Year(Latest to Oldest)", style: .default, handler: {
                         (alert: UIAlertAction!) -> Void in
                self.lblLatest.text = "Year(Latest to Oldest)"
                      self.showFilterAd(fltr: "Year Newest")
                     })
            let yOAction = UIAlertAction(title: "Year(Oldest to Latest)", style: .default, handler: {
                                    (alert: UIAlertAction!) -> Void in
                self.lblLatest.text = "Year(Oldest to Latest)"
                                 self.showFilterAd(fltr: "Year Oldest")
                                })
            let mHAction = UIAlertAction(title: "Mileage(High to Low)", style: .default, handler: {
                                              (alert: UIAlertAction!) -> Void in
                self.lblLatest.text = "Mileage(High to Low)"

                                           self.showFilterAd(fltr: "Mileage High")
                                          })
            let mLAction = UIAlertAction(title: "Mileage(Low to High)", style: .default, handler: {
                                                         (alert: UIAlertAction!) -> Void in
                self.lblLatest.text = "Mileage(Low to High)"

                                                      self.showFilterAd(fltr: "Mileage Low")
            })
            
            optionMenu.addAction(yLAction)
            optionMenu.addAction(yOAction)
            optionMenu.addAction(mHAction)
            optionMenu.addAction(mLAction)
            
            
        }
        
        
        
           optionMenu.addAction(cancelAction)

           // 5
           self.present(optionMenu, animated: true, completion: nil)
       }
       
    
    
    
    
    
    
    
    
    
    
    
    func didSelectedItem(adData: HomeData) {
           let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                 let controller = storyboardMain.instantiateViewController(withIdentifier: "YLADetailVCSBID") as! YLADetailVC
                 controller.adData = adData
                 self.navigationController?.pushViewController(controller, animated: true)
    }
   
    

    @IBOutlet weak var topAdCollection: YLACollectionView!
    @IBOutlet weak var itemCollectionView: YLACollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.itemCollectionView.initCollection(rowCount: 2, collection: .Home, scrollingDirection: .vertical)
        self.itemCollectionView.ylaDelegate = self
        
        self.topAdHgtConst.constant = 0.0
        self.lblTitle.text = self.category?.cat_name
        

        
        self.topAdCollection.initCollection(rowCount: 2, collection: .Home, scrollingDirection: .vertical)
        self.topAdCollection.ylaDelegate = self
        
        self.onLocationClosed = { finished in
            self.cancelView()
        }
        
        
        self.onLocationDetected = { location,distance  in
            //self.topPressed(self)

            self.resetSearch()
            //self.topView.isHidden = false
            //self.lblLatest.isHidden = false
            if !location.isEmpty{
                self.emirate = location == "All" ? "" : location
                self.getItems(off: self.CurrentPage)
                return
            }else{
                self.getNearItems(off: self.CurrentPage, dist: distance)
                return
            }
        }
        
     
      

     
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        resetSearch()
        self.getItems(off: self.CurrentPage)

    }
  
    
    
    func getNearItems(off: Int,dist: Int)
    {
        if self.CurrentPage == 0{
            self.itemCollectionView.homeData?.removeAll()
            self.showProgressYLA()
        }

        if !keyword.isEmpty
        {
          self.itemCollectionView.homeData?.removeAll()
        }
        self.lblLatest.text = "Latest Ads"
        NetworkManager.sortAdByNearBy(cat_ID: self.category!.ID!, dist: dist, completionHandler: self.getItemResponse(response:)) { (ERR) in
            self.reset(err: ERR)
        }
        
    }
    
    
    func getItems(off: Int)
    {
        if self.CurrentPage == 0{ self.showProgressYLA()}

        
        if !keyword.isEmpty
        {
            self.itemCollectionView.homeData?.removeAll()
        }
        NetworkManager.getItemList(sub_cat_id: self.category!.ID!, offset: self.CurrentPage, limits: 10, keywords: self.keyword,emirate: self.emirate, completionHandler: self.getItemResponse(response:), errorHandler: { (ERR) in
            self.reset(err: ERR)
            
        })
    }
    
    func getAdvanceItem(fModel: AdvanceFilterModel)
   {
   // if self.CurrentPage == 0{ self.showProgressYLA()}
       if !keyword.isEmpty
           {
               self.itemCollectionView.homeData?.removeAll()
           }
       self.lblLatest.text = "Latest Ads"

       NetworkManager.advanceFilter(fData: fModel, completionHandler: self.getItemResponse(response:)) { (ERR) in
                    self.reset(err: ERR)

    }

  }
    
  func didLoadMoreItems(offset: Int) {
   

          if self.totalPageCount > 1 && (self.CurrentPage != self.totalPageCount) && (self.CurrentPage <= self.totalPageCount){
            self.getItems(off: 0)
    }
           
 }
    

    @IBAction func topPressed(_ sender: Any) {
        
        let const =  self.topAdHgtConst.constant == 0.0 ? 236.0 : 0.0
        self.topAdHgtConst.constant = CGFloat(const)


        UIView.animate(withDuration: 0.6, animations: {() -> Void in
            self.view.layoutIfNeeded()

                                        }, completion: {(_ finished: Bool) -> Void in
                                            self.btnDrop.transform = self.topAdHgtConst.constant == 0.0 ? .identity : CGAffineTransform(rotationAngle: .pi * 0.999)
                                            
                                           
                                          })
        
     

        
        
        
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
//    override func selectedEmirateIndex(distance: Int, location: String)
//    {
//        
//    }
    
    func cancelView() {
           UIView.animate(withDuration: 0.2, animations: {() -> Void in
                                                                self.locationView.frame.origin.x = -300
                                                                  }, completion: {(_ finished: Bool) -> Void in
                                                                    self.locationView.isHidden = true

                       
                                                                    })
    }
       
    
    func getItemResponse(response: RequestResult)
    {
        self.hideProgressYLA()
        
        if  let jsonObject = response.value as? [String : Any?],let model = ItemModel.toObject(value: jsonObject) {
            self.itemData = model
            if let itemcount = model.totalitemcount{
            self.totalPageCount = itemcount
            }
            
            
            if let top = model.topData {
                if top.count > 0{
                 self.topView.isHidden = false
                 self.topViewHgtConst.constant = 32.0
                 self.topAdCollection.homeData = top
                    if self.CurrentPage == 0 {
                        self.topPressed(self)}
                }else{
                  self.topAdCollection.homeData?.removeAll()
                  self.topView.isHidden = true
                  self.topViewHgtConst.constant = 0.0
                
                }
            }
            
             if let home = model.homeData {
               
            
            if self.CurrentPage == 0 {
                            self.itemCollectionView.homeData = home
                            }
                              else{
                               self.itemCollectionView.homeData!  += home
                              }
            
                if self.itemCollectionView.homeData!.count > 0{
                    self.lblLatest.isHidden = false
                    self.lblCount.text =  "(" + String(self.itemCollectionView.homeData!.count) + ")"
                self.CurrentPage =  self.itemCollectionView.homeData!.count
                }else{
                    self.lblCount.text =     "(0)"
                    self.showToast(message: "No record found")

                }


            
            
            
      
        }
            else{
               // self.lblTitle.text =  "0 Results"
            }
        }
        else{
            if self.CurrentPage == 0{
                          self.lblCount.text =  " (0)"
                       }
        }

    }
    
    func resetSearch()
    {
        self.CurrentPage = 0
        self.totalPageCount = 0
        self.topAdCollection.homeData?.removeAll()
        self.itemCollectionView.homeData?.removeAll()
        self.topAdHgtConst.constant = 0.0
        self.topView.isHidden = true
        self.topViewHgtConst.constant = 0.0
        self.lblLatest.isHidden = true
        self.emirate = ""
        self.keyword = ""
        self.lblCount.text =  " (0)"
    }
    
    

}
