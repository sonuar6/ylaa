//
//  YLATextFieldVC.swift
//  Ylaa
//
//  Created by Arun on 08/02/2020.
//  Copyright © 2020 Arun. All rights reserved.
//

import UIKit

protocol YLATextFieldDelegate {
    func didSelectedItem(item: String, atIndex: IndexPath)
}

class YLATextFieldVC: UIViewController,UITextViewDelegate {
    
    
    @IBOutlet weak var datePickr: UIDatePicker!
    
    var txtDelegate: YLATextFieldDelegate?
    
    @IBOutlet weak var pcView: UIView!
    
    @IBOutlet weak var txtView: UITextView!
    
    var textStr: String?
    var typeStr: String?

    var forIndexPath: IndexPath?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let type = self.typeStr
        {
            if type == "Picker"
            {
                self.pcView.isHidden = false
                self.txtView.isHidden = true

            }
        }else{
            self.pcView.isHidden = true
            self.txtView.isHidden = false
            self.txtView.becomeFirstResponder()
            self.txtView.text  = textStr

        }
        
        
        

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func donePressed(_ sender: Any) {
        
        if let type = self.typeStr
        {
            if type == "Picker"
            {
                print(datePickr.date)
                 let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "MMM dd yyyy"
                self.txtDelegate?.didSelectedItem(item: dateFormatterPrint.string(from: datePickr.date), atIndex: forIndexPath!)

            }
        }
        else{
              self.txtDelegate?.didSelectedItem(item: self.txtView.text, atIndex: forIndexPath!)
        }
      
        self.dismiss(animated: true, completion: nil)

        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.txtDelegate?.didSelectedItem(item: self.txtView.text, atIndex: forIndexPath!)
        self.dismiss(animated: true, completion: nil)


    }
}
