//
//  YLALoginVC.swift
//  Ylaa
//
//  Created by Arun on 05/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class YLALoginVC: BaseViewController ,UITextFieldDelegate{
    
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var lblCode: UITextField!
    @IBOutlet weak var userNameTypeImgview: UIImageView!
    @IBOutlet weak var btnPhone: UIButton!
    @IBOutlet weak var txtPasword: UITextField!
    @IBOutlet weak var txtUname: UITextField!
    var tempButton: UIButton?
    var userLogged: LoggedUserModel?
    var typeUser: String = "email"
    var isFromReg: Bool = false
    var task_Home: HomeTask?
    var onPopView: ((_ task:HomeTask)->())?


    
    
    
    
    @IBOutlet weak var logView: UIView!
    
    @IBAction override func backPressed(_ sender: Any) {
        if let home  = self.task_Home{
            let type = Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES ? home : .recent
            self.onPopView!(type)
        }
             self.navigationController?.popViewController(animated: true)
         }

    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tempButton = self.view.viewWithTag(111) as? UIButton
       // self.logView.bindToKeyboard()
        self.view.hideKeyboardOnTap()


        // Do any additional setup after loading the view.
    }
    
    @IBAction func optionPressed(_ sender: Any) {
        
        
        
        self.txtPasword.text = ""
        self.txtUname.text = ""
        self.txtPasword.delegate = self
        self.txtUname.delegate = self

        let btn = sender as! UIButton
        if btn == self.btnEmail
        {
            self.txtUname.keyboardType = .emailAddress
            self.txtUname.placeholder = "Username"

            self.btnEmail.backgroundColor = .white
            self.btnEmail.setTitleColor(.customBlackColor, for: .normal)
            self.btnPhone.backgroundColor = .customBlackColor
            self.btnPhone.setTitleColor(.white, for: .normal)
            self.userNameTypeImgview.image = #imageLiteral(resourceName: "email-1")
            lblCode.isHidden = true
            self.typeUser = "email"
        }
        else{
            self.txtUname.keyboardType = .numberPad
            self.btnPhone.backgroundColor = .white
            self.btnPhone.setTitleColor(.customBlackColor, for: .normal)
            self.btnEmail.backgroundColor = .customBlackColor
            self.btnEmail.setTitleColor(.white, for: .normal)
            self.userNameTypeImgview.image = #imageLiteral(resourceName: "flag_uae")
            lblCode.isHidden = false
            self.txtUname.placeholder = "-- --- --"

            self.typeUser = "phone"

        }
           

    }
    
    @IBAction func signPressed(_ sender: Any) {
        
        let btn = sender as! UIButton
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)

        if btn.tag == 111
        {
            self.sendData()
        }
        else  if btn.tag == 112{
        
     
        
        let controller = storyboardMain.instantiateViewController(withIdentifier: "YLARegisterVCSBID") as! YLARegisterVC
        self.navigationController?.pushViewController(controller, animated: true)
        }
        else  if btn.tag == 113{
            
            self.forgotPassword()

        }
        
        else  if btn.tag == 888 || btn.tag == 889 {
                 let controller =  storyboardMain.instantiateViewController(withIdentifier: "YLAPostAdsVCSBID") as! YLAPostAdsVC
                 controller.urlType = btn.tag == 888 ? "TERMS" : "PVCY"
                 self.navigationController?.pushViewController(controller, animated: false)
        }
        
        
    }
    
    func sendData()
    {
        self.txtUname.resignFirstResponder()
        self.txtPasword.resignFirstResponder()

        
        if self.typeUser == "email"{
            do{
                       _ =  try txtUname.validatedText(validationType: textValidatorType.email)
                   }
                   catch
                   {
                       showToast(message: (error as! textValidationError).message)
                       return

                   }
        }
        
        else{
            do{
                                 _ =  try txtUname.validatedText(validationType: textValidatorType.phonenumber)
                             }
                             catch
                             {
                                 showToast(message: (error as! textValidationError).message)
                                 return

                             }
        }
        

        guard let pw = self.txtPasword.text, !pw.isEmpty   else{
            showToast(message: "Password field is empty")
            return
        }
        if pw.count < 8
        {
            showToast(message: "Password should have atleast 8 characters")
            return

        }
        
        self.login()
    }
    
    
    
    
  

    func login()
    {
        let uName =  self.typeUser == "phone" ? "9715" + self.txtUname.text! : self.txtUname.text!
        
        Utility.resetPlistValueForKey(key: YLA.UNAME, value: uName)
        Utility.resetPlistValueForKey(key: YLA.PASWRD, value: self.txtPasword.text!)
        Utility.resetPlistValueForKey(key: YLA.TYPEUSER, value: self.typeUser)


        self.showProgressYLA()

        NetworkManager.userLogin(username: uName, password: self.txtPasword.text!, type: self.typeUser, completionHandler: self.loggedInResponse(response:), errorHandler: { (ERR) in
            Utility.resetLoginCredentials()
            self.hideProgressYLA()
            self.showToast(message: ERR)

        })
    }
    
    func loggedInResponse(response: RequestResult)
    {
        self.hideProgressYLA()

        if  let jsonObject = response.value as? [String : Any?],let model = LoggedUserModel.toObject(value: jsonObject) {
            
            self.userLogged = model
            UserDataManager.sharedInstance.user = model
            if self.userLogged!.Status != "Error"{
            if let userID = self.userLogged!.user_id, userID > 0
            {
                Utility.resetPlistValueForKey(key: YLA.USER_LOGGEDIN, value: YLASTATUS.YES)
                sendDeviceToken()
                 if self.isFromReg {
                    self.navigationController?.popToRootViewController(animated: true)
                 }else{
                    //self.navigationController?.popViewController(animated: true)
                    self.backPressed(self)
                }
               
            }
            
        }
            else{
                Utility.resetLoginCredentials()
                showToast(message: self.userLogged!.msg!)
            }
        }

    }
    
    
    
    func forgotPassword()
    {
        
        let alertController = UIAlertController(title: "Forgot Password?", message: "Username (Email or Mobile)", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Submit", style: .default, handler: { alert -> Void in
            let textField = alertController.textFields![0] as UITextField
            
            if !textField.text!.isEmpty
            {
                self.sendForgotPW(uName: textField.text!)
            }
            
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Username"
        })
        self.present(alertController, animated: true, completion: nil)
        
        
    }
   
    
    
    
    func sendForgotPW(uName: String)
    {
        NetworkManager.forgotPassword(info: uName, completionHandler: self.forgotResponse(response:)) { (ERR) in
            self.showToast(message: ERR)
        }
    }
    
    
    
    func forgotResponse(response: RequestResult)
    {
        if  let jsonObject = response.value as? [String : Any?],let model = ForgotModel.toObject(value: jsonObject) {
        if model.status == "Success" {
            
            
            let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                           
                           let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAOtpVerifyVCSBID") as! YLAOtpVerifyVC
                           controller.fPassword = model
                           self.navigationController?.pushViewController(controller, animated: true)
            
        }else{
            self.showToast(message: model.message!)
            }
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
          {
              if typeUser  == "phone"{
              let  char = string.cString(using: String.Encoding.utf8)!
              let isBackSpace = strcmp(char, "\\b")
              if (textField == self.txtUname)
              {
               
                        
                        if ((self.txtUname.text?.count)!  > 7 && (isBackSpace != -92))
                        {
                            return false
                            
                        }
                      
                        guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string))
                            
                            
                            else {
                                return false
                        }
              }
              print(self.txtUname.text!.count)
      //        if (self.txtPhone.text!.count <= 4)
      //                        {
      //                            self.txtPhone.text = "+971"
      //
      //                        }
                             
             
              }
              return true

      }
    
}
