//
//  YLAImageVC.swift
//  Ylaa
//
//  Created by APPLE on 22/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class YLAImageVC: UIViewController {
    
    
    
    var imageColl: [Images]?
    var indexPath:IndexPath?

    @IBOutlet weak var imageCollectionView: YLACollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageCollectionView.isHidden = true
        self.imageCollectionView.initCollection(rowCount: 1, collection: .ZoomImage, scrollingDirection: .horizontal)
        self.imageCollectionView.image = self.imageColl





        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

    }
    override func viewDidAppear(_ animated: Bool) {
       super.viewDidAppear(animated)
        self.imageCollectionView.scrollToItem(at: indexPath!, at: .centeredHorizontally, animated: false)
        self.imageCollectionView.isHidden = false


    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()



    }
    
    
    
    
    @IBAction func closePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
