//
//  YLAMyYlaaVC.swift
//  Ylaa
//
//  Created by APPLE on 17/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class YLAMyYlaaVC: BaseTabBarController {
    
    
    let pArray = ["Profile Overview","Documents","My Favorite","Yalla Wallet","Support","Terms & Conditions","Logout"]
    //let sArray = ["Support","Terms & Conditions","Logout"]
    
    let pImageArray = ["prof","doc","heart","walet","supprt","terms","logout"]
    //let sImageArray = ["supprt","terms","logout"]
    
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var profImgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES{
        lblDate.text = Utility.getStringDate(dateStr: (UserDataManager.sharedInstance.user?.userData!.RegDateTime)!)
       // profImgView.setImageWithURL(url: UserDataManager.sharedInstance.user?.userData!.Pic, placeholder: "placeHolder")
        self.setUserInfo()
       }
    }
        
        
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func optionPressed(_ sender: Any) {
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)

        let btn = sender as! UIButton
        
        if btn.tag == 111
        {
            let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAMyAdsVCSBID") as! YLAMyAdsVC
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else{
            let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAProfileVCSBID") as! YLAProfileVC
            controller.viewCont = YLA.SETTINGS
            controller.onProfileUpdated = { finished in
                if finished{
                    self.setUserInfo()
                }
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
        
    }
        
        
    func setUserInfo()
    {
           lblType.text = UserDataManager.sharedInstance.user?.userData!.usertype == "biz" ? "(Business)" : "(Individual)"
        UserDataManager.sharedInstance.user?.userData!.usertype == "biz" ? self.profImgView.setImageForYlaaUser(url: UserDataManager.sharedInstance.user?.userData!.bizlogo, placeHolder: "placeHolder") :
                        self.profImgView.setImageForYlaaUser(url: UserDataManager.sharedInstance.user?.userData!.Pic, placeHolder: "placeHolder")
                      
           lblName.text = UserDataManager.sharedInstance.user?.userData!.usertype == "biz" ? UserDataManager.sharedInstance.user?.userData!.BusinessName : UserDataManager.sharedInstance.user?.userData!.Name
 
    }
            
    
    
    

}

extension YLAMyYlaaVC: UITableViewDelegate,UITableViewDataSource{
   
    func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if section == 0{
            return pArray.count
        
    //}
//        else{
//            return sArray.count}

    }

   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"profileCell",
                                                            for: indexPath) as! YLAEmiratesCell
//        if indexPath.section == 0{
            cell.setProfileCell(titleSTr: self.pArray[indexPath.row], thumb: UIImage(named: self.pImageArray[indexPath.row])!)
//        }
//               else {
//            cell.setProfileCell(titleSTr: self.sArray[indexPath.row], thumb: UIImage(named: self.sImageArray[indexPath.row])!)
//
//            }

        return cell
    }

  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
  {
         
      
             let headerView = UIView(frame: CGRect(x: 0,y: 0,width: tableView.bounds.size.width,height: 1))
             headerView.backgroundColor = section == 1 ? .clear : .lineColor
             return headerView
      
  }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)

        if indexPath.section == 0
        {
            if indexPath.row == 0
            {
                let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAProfileVCSBID") as! YLAProfileVC
                controller.viewCont = YLA.PROF_OVRVIEW
                self.navigationController?.pushViewController(controller, animated: true)
            }
            if indexPath.row == 1
            {
                let controller = storyboardMain.instantiateViewController(withIdentifier: "YLADocumentVCSBID") as! YLADocumentVC
                self.navigationController?.pushViewController(controller, animated: true)
            }
            if indexPath.row == 2
            {
                let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAFavoriteVCSBID") as! YLAFavoriteVC
                self.navigationController?.pushViewController(controller, animated: true)
            }
            if indexPath.row == 3
            {
               let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAWalletVCSBID") as! YLAWalletVC
               self.navigationController?.pushViewController(controller, animated: true)
            }
        }
//        else if indexPath.section == 1
//        {
            
            if indexPath.row == 4
            {
                let controller = storyboardMain.instantiateViewController(withIdentifier: "YLASupportVCSBID") as! YLASupportVC
                self.navigationController?.pushViewController(controller, animated: true)
            }
            if indexPath.row == 5
            {
            let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAPostAdsVCSBID") as! YLAPostAdsVC
                                       controller.urlType = "TERMS"
                                      self.navigationController?.pushViewController(controller, animated: false)
            }
            
            if indexPath.row == 6
            {
                Utility.resetLoginCredentials()
                let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAHomeVCSBID") as! YLAHomeVC
                                        controller.transitioningDelegate = self
                                        self.navigationController?.pushViewController(controller, animated: false)
            }
       // }

    }
     
    
}
