//
//  YLAMessageVC.swift
//  Ylaa
//
//  Created by APPLE on 17/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class YLAMessageVC: BaseViewController,SendMessageDelegate {
   
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var msgUserData: ChatSessionDataModel?
    var adDetail:ADItemDetailData?
    var chatData:[ChatHistoryDataModel]?
    var chatString:String?
    var fID:Int = 0
    var adID:Int = 0

    @IBOutlet weak var lblLastSeen: UILabel!
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var userImagView: UIImageView!
    @IBOutlet weak var lblAdtitle: UILabel!
    @IBOutlet weak var AdImageview: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var adImgView: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblLoc: UILabel!
    @IBOutlet weak var messageList: UITableView!
    var msgSendView: MessageFooterView = UIView.fromNib()

    
    @IBAction func detailPressed(_ sender: Any) {
        if let addata = self.msgUserData?.ad_detail{
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                         let controller = storyboardMain.instantiateViewController(withIdentifier: "YLADetailVCSBID") as! YLADetailVC
                         controller.adData = addata
                         self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    
    
    
    func setUI()
    {
        
        
        if Int(UIScreen.main.bounds.size.height) == 667 || Int(UIScreen.main.bounds.size.height) == 736{
            self.msgSendView.frame =   CGRect(x: self.view.frame.origin.x, y: self.view.frame.height - 50, width: self.view!.frame.width, height: 55.0)

        }else{
            self.msgSendView.frame =   CGRect(x: self.view.frame.origin.x, y: self.view.frame.height - 100, width: self.view!.frame.width, height: 55.0)

        }

//        self.tabView.tabDelegate = self
        self.view.addSubview(self.msgSendView)
        
       
        
        if let chat = self.msgUserData
        {
            self.lblTitle.text = chat.user_detail?.name
            self.lblAdtitle.text = chat.ad_detail?.catdetail?.title
            if let prc = chat.ad_detail?.catdetail?.price{
                self.lblPrice.text = String(prc)
            }
            if let dateStr = chat.ad_detail?.origionalSubmitDate
            {
                self.lblDate.text = dateStr.isEmpty ? ""  : Utility.getStringDate(dateStr: dateStr)
            }
            self.userImagView.setImageForYlaaUser(url: chat.user_detail?.pic, placeHolder: "placeHolder")
            self.lblLoc.text = chat.ad_detail?.catdetail?.emerate
            self.adImgView.setImageWithURL(url: chat.ad_detail?.catdetail!.image, placeholder: "no_pic")
        }
        
        if let detail = self.adDetail
        {
            if let dateStr = detail.date
            {
               self.lblDate.text = dateStr.isEmpty ? ""  : dateStr
            }
            if let lStr = detail.otherAttributes!.emerate
            {
            self.lblLoc.text = lStr
            }
            
            self.userImagView.setImageForYlaaUser(url: detail.pic, placeHolder: "placeHolder")

            if let prc = detail.price{
            self.lblPrice.text = prc
            }
            self.msgSendView.txtMsg.text = chatString
            self.lblTitle.text = detail.seller_name
            self.lblAdtitle.text = String(detail.title!)
            if let img = detail.images
            {
               if img.count > 0 {
                let image = img[0]
                self.adImgView.setImageWithURL(url: image.ad_img, placeholder: "no_pic")}
               else{
                self.adImgView.image = #imageLiteral(resourceName: "no_pic")
                }
            }



        }
        
        _ = Timer(timeInterval: 0.3, target: self, selector: #selector(timerFired), userInfo: nil, repeats: true)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        msgSendView.bindToKeyboard()
//        self.messageList.hideKeyboardOnTap()
        msgSendView.txtMsg.delegate = self
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)

        msgSendView.msgDelegate = self
        if let chat = self.msgUserData
        {
           fID = (chat.user_detail?.id!)!
            adID = (chat.ad_detail?.ID!)!
        }
        if let detData = self.adDetail
        {
            fID = detData.seller_id!
            adID = Int(detData.id!)!
        }
        
        
        

      
        
            self.setUI()
          self.showProgressYLA()

        self.getAllMessage()
        

        // Do any additional setup after loading the view.
    }
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        self.msgSendView.txtMsg.resignFirstResponder()
    }
    
    @objc func timerFired()
    {
        self.getAllMessage()
    }
    
    
    func getAllMessage()
    {
        NetworkManager.getMessageItems(off: 0, fromID: fID,adID: self.adID, completionHandler: self.requestmsgResponse(response:)) { (ERR) in
            self.showToast(message: ERR)
        }
    }
    
    
    
    func requestmsgResponse(response: RequestResult)
    {
        self.hideProgressYLA()

        if  let jsonObject = response.value as? [String : Any?],let model =  ChatHistorySessionModel.toObject(value: jsonObject) {
            self.lblLastSeen.text = model.onlineStatus
            self.chatData = model.chat
            if self.chatData!.count > 0 {
            messageList.reloadData()
            self.scrollToBottom()
            }
            
        }
    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
             let lastRow: Int = self.messageList.numberOfRows(inSection: 0) - 1
             let indexPath = IndexPath(row: lastRow, section: 0);
             self.messageList.scrollToRow(at: indexPath, at: .top, animated: false)
        }
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func sendMessage(msgStr: String) {
        
        NetworkManager.sendMessage(msg: msgStr, to_user: fID, adId: adID, completionHandler: self.sendMessageResponse(response:)) { (ERR) in
            self.showToast(message: ERR)
        }
           
    }
   
    
    func sendMessageResponse(response:RequestResult)
    {
        if  let jsonObject = response.value as? [String : Any?],let model =  MakeFavoriteModel.toObject(value: jsonObject) {
            if model.result == "1"
            {
                self.msgSendView.txtMsg.text = ""
                self.getAllMessage()
            }
        }

    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
       }

}


extension YLAMessageVC:UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.scrollToBottom()
        
        
        if Int(UIScreen.main.bounds.size.height) == 667 || Int(UIScreen.main.bounds.size.height) == 736{
                  self.bottomConst.constant = 315;

        }else{
            self.bottomConst.constant = 420;
        }
        self.view.layoutIfNeeded()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.scrollToBottom()
        self.bottomConst.constant = 65;
        self.view.layoutIfNeeded()
    }
}


extension YLAMessageVC: UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count  = 0
        if let chat = self.chatData{
            count = chat.count
        }
             
           return count
       }
         
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let chat = self.chatData![indexPath.row]
      

        
        if chat.fromuserid == Int(Utility.getPlistValueforKey(key: YLA.USER_ID) as String){
            let cell = tableView.dequeueReusableCell(withIdentifier:"rightCell",
                                                                                    for: indexPath) as! DocTableCell
            cell.setChatHistoryCellData(chat: chat)

            return cell
           

        }
        else{
           let cell = tableView.dequeueReusableCell(withIdentifier:"leftCell",
                                                                          for: indexPath) as! DocTableCell
                      cell.setChatHistoryCellData(chat: chat)
                      return cell
        }
          
       
       }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//            let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
//            let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAMessageVCSBID") as! YLAMessageVC
//            self.navigationController?.pushViewController(controller, animated: false)

    }
    
    
}
