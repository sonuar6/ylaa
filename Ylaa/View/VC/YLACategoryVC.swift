//
//  YLACategoryVC.swift
//  Ylaa
//
//  Created by Arun on 05/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class YLACategoryVC: BaseTabBarController,FilterCollectionDelegate{
   
    
    
  
    
    var currentIndex = 0
    var segueData:CategorySegueData?


    final private var  filterItems = [CategoryItemData]()
    
    var categoryData:[CategoryItemData]?



    
    var filterView: FilterView = UIView.fromNib()

    @IBOutlet weak var txtSearch: UITextField!

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var listTable: UITableView!
    

    override func viewDidLoad() {
        self.removeFilter()
        super.viewDidLoad()
        self.lblTitle.text  = self.segueData!.titleStr
        self.filterView.fCollectionView.initCollection(rowCount: 4, collection: .Filter, scrollingDirection: .vertical)
        self.filterView.fCollectionView.fDelegate = self
       getCategory(catID: self.segueData!.categID!)

        // Do any additional setup after loading the view.
    }
    
    func getCategory(catID: Int)
    {
        NetworkManager.getCategoryData(categID: catID, completionHandler: self.categoryResponse(response:)) { (ERR) in
                       
               }
    }

   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func closePressed(catItem: CategoryItemData){
          currentIndex = currentIndex - 1
          if currentIndex >= 0
          {
            self.getCategory(catID: catItem.head_ID!)
            if currentIndex == 0{
                self.filterItems.removeAll()
                self.filterView.fCollectionView.filter?.removeAll()
                self.filterView.fCollectionView.reloadData()
            }
          }
    }
    
    
    func categoryResponse(response: RequestResult)
    {
        if  let jsonObject = response.value as? [String : Any?],let model = CategoryListModel.toObject(value: jsonObject) {
            guard let category = model.categoryItemData else{return}
           
            if self.categoryData != nil{
                self.categoryData?.removeAll()
            }
            self.categoryData = category
//            if currentIndex == 0{
//                self.categoryData?.insert(CategoryItemData(catID: self.segueData!.categID!, headID:0, name: "All in " + self.segueData!.titleStr!, next: "No"), at: 0)
//            }
            self.listTable.reloadData()
        }
    }
    @IBAction func searchPressed(_ sender: Any) {
        
        guard let serachStr = self.txtSearch.text, !serachStr.isEmpty else {
            return
        }
        self.searchPressed(search: serachStr)
        
    }
    
    func searchPressed(search: String) {
              
           self.txtSearch.text = ""
           self.txtSearch.resignFirstResponder()
           let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
           let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAFavoriteVCSBID") as! YLAFavoriteVC
           controller.searchText = search
           self.navigationController?.pushViewController(controller, animated: true)
       }

}


extension YLACategoryVC: UITableViewDelegate,UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // var count = 0
//        switch currentIndex {
//               case 0:
//                 count = self.catItems.count
//                   break
//               case 1:
//                count = self.brandItems.count
//                  break
//                   case 2:
//                    count = self.vehicleItems.count
//                   break
//               default:
//                   break
//               }
        
        return self.categoryData != nil ? self.categoryData!.count : 0
    }
    
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
          let cell = tableView.dequeueReusableCell(withIdentifier: "catItemCells",
                                                   for: indexPath) as! categoryTablecell
    
          var fltStr = ""
          fltStr = self.categoryData![indexPath.row].cat_name!
       
      
          cell.lblTitle.text = fltStr
    
          return cell
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
        if section == 0{
        
            let height = self.filterView.fCollectionView.collectionViewLayout.collectionViewContentSize.height

            let headerView = UIView(frame: CGRect(x: 15,y: 0,width: tableView.bounds.size.width,height: height + 20))
            headerView.backgroundColor = .clear
            self.filterView.hgtConst.constant = height
            self.filterView.frame =   CGRect(x: 0, y: 0, width: headerView.bounds.size.width  , height:headerView.bounds.size.height)
            headerView.addSubview(filterView)
        return headerView
        }
        else{
            let headerView = UIView(frame: CGRect(x: 0,y: 0,width: tableView.bounds.size.width,height: 1))
            return headerView
        }
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {

     
        return CGFloat(50.0)

       }
    
//  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//      cell.alpha = 0
//      UIView.animate(withDuration: 0.3, animations: {
//          cell.alpha = 1
//      })
//  }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
      if let catData = self.categoryData
      {
        
        let cData = catData[indexPath.row]
        
        self.filterItems.append(cData)
        self.filterView.fCollectionView.filter = self.filterItems
        
     if  cData.nextdata! != YLASTATUS.YES
        {

            self.filterItems.removeAll()
            self.filterView.fCollectionView.filter?.removeAll()
            self.filterView.fCollectionView.reloadData()
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                        let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAItemListVCSBID") as! YLAItemListVC
            controller.category = cData
                        self.navigationController?.pushViewController(controller, animated: true)
            
        }
        else{
            getCategory(catID: cData.ID!)
            currentIndex = currentIndex + 1

        }
        }

   
    }
    
}
