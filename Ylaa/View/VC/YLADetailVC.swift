//
//  YLADetailVC.swift
//  Ylaa
//
//  Created by Arun on 10/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit
import  MessageUI
class YLADetailVC: BaseViewController,YLAOfferDelegate,SellerDelegate,MFMailComposeViewControllerDelegate,SegmentDelegate {
  
    
  
    
    
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var listTable: UITableView!
    @IBOutlet weak var footerView: UIView!
    var offerView: YLAOfferView = UIView.fromNib()
    var sellerView: YLASellerInfoView = UIView.fromNib()



    var isDesc:Bool  = false
    var isSpec:Bool  = true
    var adData: HomeData?
    var adDetailData: ADItemDetailData?
    var task_Home: HomeTask?
    var onPopView: ((_ task:HomeTask)->())?


    
    
    
    override func backPressed(_ sender: Any) {
        if let home  = self.task_Home{
            self.onPopView!(home)
        }
        self.navigationController?.popViewController(animated: true)

    }
    
    func sendfavorite(isFav:Bool)
    {
            NetworkManager.add_to_favorite(isAdFav:isFav ,adID: self.adData!.ID!, completionHandler: self.favoriteResponse(response: ), errorHandler: { (ERR) in
                
            })
        
    }
    
    
    
    
    func favoriteResponse(response: RequestResult)
    {
        self.hideProgressYLA()
        if  let jsonObject = response.value as? [String : Any?],let model = MakeFavoriteModel.toObject(value: jsonObject) {
            if model.result != "1"
            {
//                let image = tempButton?.currentImage == #imageLiteral(resourceName: "heart") ? #imageLiteral(resourceName: "heartfill") : #imageLiteral(resourceName: "heart")
//                tempButton?.setImage(image, for: .normal)
            }
            else{
                //self.adDelegate?.didFavoriteRequestDone(isDone: true)
                self.showToast(message: model.msg!)
            }
        }

    }
    
    
    
    
    
    
    
    @IBAction func actionPressed(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        
        switch btn.tag {
        case 555:
            if Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES{
                  showReportActionSheet()
            }else{
                let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboardMain.instantiateViewController(withIdentifier: "YLALoginVCSBID") as! YLALoginVC
                self.navigationController?.pushViewController(controller, animated: true)
            }
            break
        case 556:
            
            if Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES{
            var isFav = false
            
            if btn.currentImage == UIImage(named: "Path 3118")
            {
                
                btn.setImage(UIImage(named: "star_Sel"), for: .normal)
                isFav = true
            }
            else{
                btn.setImage(UIImage(named: "Path 3118"), for: .normal)
                isFav = false


            }
            self.sendfavorite(isFav: isFav)
            }
            else{
                let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                               let controller = storyboardMain.instantiateViewController(withIdentifier: "YLALoginVCSBID") as! YLALoginVC
                               self.navigationController?.pushViewController(controller, animated: true)
            }

            break
        case 557:
            self.offerView.removeKeyBoardObserver()
            let str  = "Hi, I just explored this Ad on Ylaa.com and want you to have a look.\r\n" + self.adDetailData!.title! + "\r\n" + "\r\n" + ServiceUtils.BETA_DOMAIN_URL + self.adDetailData!.shareUrl!

            let activityController = UIActivityViewController(activityItems: [str, ""], applicationActivities: nil)
                              activityController.excludedActivityTypes = [.airDrop, .postToTwitter, .assignToContact]
            activityController.completionWithItemsHandler = { (activityType, completed:Bool, returnedItems:[Any]?, error: Error?) in
                self.offerView.bindToMakeOfferKeyboard()
            }

            self.present(activityController, animated: true, completion: nil)
                           
            break
            case 558:
                if self.adData!.catdetail!.isVideo == 1{
                              
                    guard let str =  self.adData!.catdetail!.videourl else
                    {
                         return
                    }
                  self.playVideo(urlStr: str)
                }
            break
        default:
            break
        }
        
        
        
    }
    
    
    func showReportActionSheet()  {
        
        
        
        
        let alertController = UIAlertController(title: "Report Ad", message: "Are you sure you want to report this Ad", preferredStyle: .alert)
               alertController.addAction(UIAlertAction(title: "YES", style: .default, handler: { alert -> Void in
                 if let id = self.adData?.ID{
                        self.showProgressYLA()
                        NetworkManager.reportAd(ad_id: id, completionHandler: self.favoriteResponse(response: )) { (ERR) in
                            self.showToast(message: ERR)
                            self.hideProgressYLA()
                            }
                            
                        }else{
                            self.showToast(message: YLAMSG.MSG_INCOMPLT)
                            self.hideProgressYLA()


                        }
                   
               }))
               alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
              
               self.present(alertController, animated: true, completion: nil)
               
        
        
        

        
       
        
        
    }
    
    
    
    
    
    @IBAction func tabPressed(_ sender: Any) {
        
        let btn = sender as! UIButton
        var yPos:CGFloat = 300
        if Int(UIScreen.main.bounds.size.height) == 667 || Int(UIScreen.main.bounds.size.height) == 736{
           yPos =  170
        }
        
        if btn.tag == 114 ||  btn.tag == 112
        {
            if Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES{
            if let str = self.adData?.catdetail!.ptype
            {

            self.listTable.isUserInteractionEnabled = false
            switch btn.tag {
            case 114:
                self.offerView.initFor(type: .Offer, caption: "Make Your Best Offer", titleStr:"Make an Offer" , action: "Send Offer", priceType: str)
                break
            case 112:
                if let adDtl = self.adDetailData{
                    self.offerView.initFor(type: .Phone, caption: "Would you like to call?", titleStr: adDtl.seller_mobile!, action: " Call", priceType: str)
                }

                break
            default:
                break
            }
            
            UIView.animate(withDuration: 0.2, animations: {() -> Void in
                                          self.offerView.frame.origin.y = yPos
                                            }, completion: {(_ finished: Bool) -> Void in
 
            })}
                                      
        }else{
                
             let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                                           let controller = storyboardMain.instantiateViewController(withIdentifier: "YLALoginVCSBID") as! YLALoginVC
                                           self.navigationController?.pushViewController(controller, animated: true)
         }
    
        }
         else if btn.tag == 115
        {
            UIView.animate(withDuration: 0.2, animations: {() -> Void in
                                                     self.sellerView.frame.origin.y = yPos
                                                       }, completion: {(_ finished: Bool) -> Void in
                         self.listTable.isUserInteractionEnabled = false

                                                         })
        }
        else if btn.tag == 113
        {
      
            if Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES{
            
            let optionMenu = UIAlertController(title: nil, message: "Send message!", preferredStyle: .actionSheet)

                   let fullAction = UIAlertAction(title: "Is this still available", style: .default, handler: {
                       (alert: UIAlertAction!) -> Void in
                    self.PushToMessage(push: "Is this still available")

                   })
                   let addvanceAction = UIAlertAction(title: "Is this price is negotiable?", style: .default, handler: {
                       (alert: UIAlertAction!) -> Void in
                    self.PushToMessage(push: "Is this price is negotiable?")

                   })
                  let avanceAction = UIAlertAction(title: "I am interested, contact me", style: .default, handler: {
                                  (alert: UIAlertAction!) -> Void in
                    self.PushToMessage(push: "I am interested, contact me")

                              })

                   //
                   let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                       (alert: UIAlertAction!) -> Void in
                   })


                   // 4
                   optionMenu.addAction(fullAction)
                   optionMenu.addAction(addvanceAction)
                   optionMenu.addAction(avanceAction)
                   optionMenu.addAction(cancelAction)

                   // 5
                   self.present(optionMenu, animated: true, completion: nil)
            }else{
                   
                let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                                              let controller = storyboardMain.instantiateViewController(withIdentifier: "YLALoginVCSBID") as! YLALoginVC
                                              self.navigationController?.pushViewController(controller, animated: true)
            }
            
            
            
        }
        else if btn.tag == 111
        {
            //self.sendEmail()
            self.navigationController?.popToRootViewController(animated: true)
        }
                   
        
        
        
        
        
        
    }
    
    
    
    
    
    func PushToMessage(push: String)
    {
        if let detailData = self.adDetailData{
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
       
        if Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES{

                                  let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAMessageVCSBID") as! YLAMessageVC
                                  controller.chatString = push
                                  controller.adDetail = detailData
                                  self.navigationController?.pushViewController(controller, animated: false)
                               }else{
                                   let controller = storyboardMain.instantiateViewController(withIdentifier: "YLALoginVCSBID") as! YLALoginVC
                                   self.navigationController?.pushViewController(controller, animated: false)
                               }
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

         //(self.adData?.catdetail!.make)! + " " + (self.adData?.catdetail!.model)!
        
       
        
        self.offerView.frame =   CGRect(x: 0, y: 1000, width: self.view.bounds.size.width  , height: self.offerView.bounds.size.height)
        self.offerView.ofDelegate = self
        self.listTable.hideKeyboardOnTap()
        self.offerView.bindToMakeOfferKeyboard()
        self.view.addSubview(self.offerView)
        
        self.sellerView.frame =   CGRect(x: 0, y: 1000, width: self.view.bounds.size.width  , height: self.sellerView.bounds.size.height)
        self.sellerView.sDelegate = self
        self.view.addSubview(self.sellerView)
        

        

        
        NetworkManager.getAddDetail(itemID: self.adData!.ID!, completionHandler: self.detailResponse(response:)) { (ERR) in
            self.showToast(message: ERR)
        }
        
      


        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.sellerView.listTable.reloadData()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func cancelView() {
        
        UIView.animate(withDuration: 0.2, animations: {() -> Void in
                                                 self.offerView.frame.origin.y = 1000
                                                   }, completion: {(_ finished: Bool) -> Void in
        
                                                    self.listTable.isUserInteractionEnabled = true

                                                     })
        
        UIView.animate(withDuration: 0.2, animations: {() -> Void in
                                                        self.sellerView.frame.origin.y = 1000
                                                          }, completion: {(_ finished: Bool) -> Void in
                                                            self.sellerView.listTable.reloadData()

                                                            self.listTable.isUserInteractionEnabled = true

                                                            })
           
    }
    
    
    
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([""])
            mail.setMessageBody("<p></p>", isHTML: true)

            present(mail, animated: true)
        } else {
            self.showToast(message: "No email accounts are configured")
        }
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    
    
    func detailResponse(response: RequestResult)
    {
        if  let jsonObject = response.value as? [String : Any?],let model = ADItemDetailData.toObject(value: jsonObject) {
            
            self.adDetailData = model
            self.offerView.adData = model
            self.lblTitle.text = self.adDetailData?.breadCrumbs
            self.sellerView.setInitSellerInfo(detailData:  self.adDetailData!)
            self.listTable.reloadData()
            

        }
    }
    
    
    func didCallSellerMobile(phNum: String) {
        if Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES{
                let url = URL(string: "tel://\(phNum)")
                    UIApplication.shared.open(url!)
//            }else{
//                self.showToast(message: "Unable to make call from this device")
//            }
            
        }else{
            let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboardMain.instantiateViewController(withIdentifier: "YLALoginVCSBID") as! YLALoginVC
            self.navigationController?.pushViewController(controller, animated: true)
        }

    }
      
      func didSelectSellerPost() {
          
    }
    
    
}


extension YLADetailVC: UITableViewDelegate,UITableViewDataSource,BannerDelegate,InfoDelegate
{
   
    
    
   
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
        if self.adDetailData != nil{
           return 4
        }else{
            return 0

        }
    }
    
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
     if indexPath.row == 0{
        let cell = tableView.dequeueReusableCell(withIdentifier: "bannerTableCell",
                                                         for: indexPath) as! bannerTableCell
        if let detail = self.adDetailData{
            cell.setBannercell(image: detail.images!, home: self.adData!)
            adData!.catdetail?.isFavorite == 1 ?  cell.btnFavorite.setImage(#imageLiteral(resourceName: "star_Sel"), for: .normal) :  cell.btnFavorite.setImage(#imageLiteral(resourceName: "Path 3118"), for: .normal)


        }
        cell.bDelegate = self
        return cell
     }
     else if indexPath.row == 1
     {
        let cell = tableView.dequeueReusableCell(withIdentifier: "infoCell",
                                                               for: indexPath) as! infoTableCell
        cell.iDelegate = self
        if let adDetail = self.adDetailData{
            cell.setInfoCellData(adDetail: adDetail, home: self.adData!)
        }
        return cell
     }
     else if  indexPath.row == 2 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "segmentCell",
                                                                      for: indexPath) as! segmentTableCell
        cell.sDelegate = self
        return cell
      }
    else
     {
        
        if isDesc{
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell",
                                                                                      for: indexPath) as! itemDescTableCell
            if let adDetail = self.adDetailData{
            cell.setAdDescData(detail: adDetail)
            }
                  return cell
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "spectableCell",
                   
                                                 for: indexPath) as! descTableCell
            if let spec = self.adDetailData?.specification{
            cell.setDescCell(specs: spec)
            cell.listTable.reloadData()

            }
        return cell
        }
    }
        
    }
    
    
   
    
    
   
    func didSegmentPressed(isDesc: Bool) {
        
        if isDesc{
            self.isDesc = true
            self.isSpec = false

        }else{
            self.isDesc = false
            self.isSpec = true
        }
        //self.listTable.reloadData()
        let indexPath = IndexPath(item: 3, section: 0)
        listTable.reloadRows(at: [indexPath], with: .right)
      
          
    }
      
        
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
   
    }
    // MARK: -InfoDelegate

    func logoPressed() {
           let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                                   let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAFavoriteVCSBID") as! YLAFavoriteVC
           controller.home = self.adData
           self.navigationController?.pushViewController(controller, animated: true)
    }
       
    
    // MARK: -BannerDelegate
    func didSelectedBanner(image: [Images], index: IndexPath) {
           let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
           let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAImageVCSBID") as! YLAImageVC
            controller.imageColl = image
            controller.indexPath = index
           controller.modalTransitionStyle = .crossDissolve
           controller.modalPresentationStyle = .fullScreen
           self.present(controller, animated: true, completion: nil)
       }
    
}
