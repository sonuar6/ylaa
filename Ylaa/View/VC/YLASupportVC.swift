//
//  YLASupportVC.swift
//  Ylaa
//
//  Created by Arun on 09/03/2020.
//  Copyright © 2020 Arun. All rights reserved.
//

import UIKit

class YLASupportVC: BaseTabBarController {

    @IBOutlet weak var listTable: UITableView!
    var inqType: String = ""
    var support = SupportModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.hideKeyboardOnTap()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func submitPressed(_ sender: Any) {
        
        guard let name = self.support.name, !name.isEmpty else{
             showToast(message: "Name field is empty")
             return
        }
        guard let phone = self.support.mob, !phone.isEmpty else{
                    showToast(message: "Mobile number field is empty or invalid")
                    return
          }
        guard let email = self.support.email, !email.isEmpty else{
                          showToast(message: "Email ID field is empty or invalid")
                          return
          }
        
        guard let inq = self.support.inqtype, !inq.isEmpty else{
                  showToast(message: "Select an enquiry type")
                  return
        }
      guard let cmnts = self.support.detail, !cmnts.isEmpty else{
                    showToast(message: "Comments field is empty")
                    return
          }
          
      
      
        
            self.showProgressYLA()
            NetworkManager.send_to_support(sModel: self.support, completionHandler: self.requestResponse(response:)) { (ERR) in
                self.showToast(message: ERR)
                self.hideProgressYLA()
        }
    }
    
    
    func requestResponse(response: RequestResult)
    {
        self.hideProgressYLA()

        if  let jsonObject = response.value as? [String : Any?],let model = UserOTPVerifiedModel.toObject(value: jsonObject) {
            if model.Status == "success"
            {
               self.showToast(message: model.message!)
                self.navigationController?.popViewController(animated: true)
            }
            else{
                self.showToast(message: "Unable to complete operation")
            }
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension YLASupportVC: UITableViewDelegate,UITableViewDataSource,RegsiterTextDelegate,YLACommonListDelegate
{
   
    
   
    
    func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
       }
       
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if indexPath.row == 0{
           let cell = tableView.dequeueReusableCell(withIdentifier: "textCell",
                                                            for: indexPath) as! profileTableCell
            cell.textDelegate = self
            cell.setSupportTextFieldDelegate(textTag: indexPath.row, isText: true, text: "Fullname", isEnabled: true)
            return cell
        }
        else if indexPath.row == 1
        {
           let cell = tableView.dequeueReusableCell(withIdentifier: "mobCell",
                                                                  for: indexPath) as! profileTableCell
            cell.textDelegate = self

            cell.setTextFieldDelegate(textTag: indexPath.row, isText: false)

        
           return cell
        }
        else if  indexPath.row == 2 || indexPath.row == 3{
           let cell = tableView.dequeueReusableCell(withIdentifier: "textCell",
                                                                         for: indexPath) as! profileTableCell
            if indexPath.row == 2 {
                cell.setSupportTextFieldDelegate(textTag: indexPath.row, isText: true, text: "Email",isEnabled: true)
            }else{
                let Str = self.inqType.isEmpty ? "Inquiry Type" : self.inqType
                cell.setSupportTextFieldDelegate(textTag: indexPath.row, isText: true, text: Str,isEnabled: false)
            }
            cell.textDelegate = self

           return cell
         }
            else if  indexPath.row == 4 {
                      let cell = tableView.dequeueReusableCell(withIdentifier: "textViewCell",
                                                                                    for: indexPath) as! profileTableCell
            cell.textDelegate = self
            cell.setCommentTextFieldDelegate(textTag: indexPath.row, isText: true)
                    
                      return cell
                    }
       else
        {
           
         
               let cell = tableView.dequeueReusableCell(withIdentifier: "subCell",
                                                                                         for: indexPath)
                return cell
          
          
       }
           
       }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 3
        {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "YLACommonListVCSBID") as! YLACommonListVC
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.listArray = ["Advertise with Ylaa","Automotive Section","Real Estate Section","Job Section","Community Section","My Ads","Featured Options","Suggestions","Report Spam", "Technical Issue","Refund","Other"]
            vc.listdelegate = self
            vc.user_type = "individual"
            vc.selectedIndex = indexPath.row
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    
    
      func didMoveScrollForStartEdit(cellTextfield: UITextField) {
           
       }
       
       func textFieldDidEndEditing(textTag: Int, text: String, textfield: UITextField) {
        
        switch textTag {
               case 0:
                if text.isEmpty{
                    showToast(message: "Please enter your name")
                    return
                }else{
                    self.support.name = text
                }
                   break
              
               case 1:
                   do{
                                      _ =  try textfield.validatedText(validationType: textValidatorType.phonenumber)
                                  }
                                  catch
                                  {
                                      showToast(message: (error as! textValidationError).message)
                                      return

                                  }
                   //self.bsnsUser?.Mobile =  "971" + text
                   self.support.mob = "9715" + text

                   break
               case 2:
                   do{
                                      _ =  try textfield.validatedText(validationType: textValidatorType.email)
                                  }
                                  catch
                                  {
                                      showToast(message: (error as! textValidationError).message)
                                      return

                                  }
                  self.support.email = text

                   break
               case 4:
                if text.isEmpty{
                    showToast(message: "Please enter your comments")
                    return
                }

                 self.support.detail = text
                   break

               default:
                   break
               }
            
      


           
       }
    
    
       func didSelectedItem(item: String, userType: String, atIndex: Int) {
        self.support.inqtype = item
        self.inqType = item
            let indexPath = IndexPath(item: atIndex, section: 0)
                   listTable.reloadRows(at: [indexPath], with: .right
                   )
        
              
       }
    
    
    
       
}
