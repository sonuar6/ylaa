//
//  YLAHomeVC.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class YLAHomeVC: BaseTabBarController, CategoryDelegate,FeaturedDelegate,classicDelegate,SegmentedDelegate{
   
    
    func didVideoPlay(homeData: HomeData) {
        if let vUrl = homeData.catdetail!.videourl{
        self.playVideo(urlStr: vUrl)
        }
    }
    
    
    
    
    
  
    
    
    var recentAds: [HomeData]?
    var viewedAds: [HomeData]?
    var favoriteAds: [HomeData]?
    var featuredAds: [HomeData]?
    var classicAds: [HomeData]?

    
    var task_type: HomeTask = .recent
    var CurrentPage: Int = 0
    var totalPageCount: Int = 50

    
    



    
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var listTable: UITableView!
    var categoryView: categoryMenuView = UIView.fromNib()
    var segView: SegmentedView = UIView.fromNib()

    
    
   

    override func viewDidLoad() {
        self.removeFilter()
        


        sendDeviceToken()
        
     

        super.viewDidLoad()
        var yPos:Int = 90
        print(UIScreen.main.bounds.size.height)
        if Int(UIScreen.main.bounds.size.height) == 667 || Int(UIScreen.main.bounds.size.height) == 736{
            yPos = 70
        }
        print(Int(UIScreen.main.bounds.size.width))

        categoryView.frame = CGRect(x: 0, y: yPos, width: Int(UIScreen.main.bounds.size.width), height: 100)
        categoryView.clipsToBounds = true
        self.view.addSubview(categoryView)
        
        
        self.view.hideKeyboardOnTap()
        self.categoryView.categoryDelegate = self
        self.segView.sDelegate = self
        self.segView.setTempBtn()

        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.showProgressYLA()

        self.getHomeData(typeHome: .recent, off: CurrentPage)
        NetworkManager.featureAds(completionHandler: self.featuredResponse(response:)) { (ERR) in
                self.reset(err: ERR)
        }
        
        
      
        

        // Do any additional setup after loading the view.
    }
    
    
    
    
   
    override func viewWillAppear(_ animated: Bool) {    
        super.viewWillAppear(true)
       
       // getClassicCars()
        self.listTable.reloadData()

    }
    /*
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        var row = 1
        if Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES{
            row = 0
        }

        let indexPath = IndexPath(row: row, section: 0);
        let cell = self.listTable.cellForRow(at: indexPath) as! featuredTableCell
        cell.featuredCollection.timer?.invalidate()
        cell.featuredCollection.timer = nil


    }
 */
    
    func getClassicCars()
    {
        NetworkManager.classicAds(completionHandler: self.classicResponse(response: )) { (ERR) in
                         self.reset(err: ERR)
                     }
    }
    

    @IBAction func searchPressed(_ sender: Any) {
        
        guard let serachStr = self.txtSearch.text, !serachStr.isEmpty else {
            return
        }
        self.searchPressed(search: serachStr)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func reset(err: String)
    {
        self.showToast(message: err)
        self.hideProgressYLA()

    }
    
    func getHomeData(typeHome: HomeTask, off: Int)
    {
        NetworkManager.getHomeData(offset: 0, limit: 200, task: typeHome, completionHandler: self.homeRequestResponse(response: )) { (ERR) in
            self.reset(err: ERR)
        }
    }
    func getFavorite()
       {
           NetworkManager.getUserFavouriteAds(completionHandler: self.homeRequestResponse(response: )) { (ERR) in
                      self.reset(err: ERR)
                  }
       }
    
    
    @IBAction func loginPressed(_ sender: Any) {
      let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
      let controller = storyboardMain.instantiateViewController(withIdentifier: "YLALoginVCSBID") as! YLALoginVC
        controller.task_Home = self.task_type
        controller.onPopView = { task in
                  self.showProgressYLA()
                  self.getHomeData(typeHome: task, off: 0)
              }
        
      self.navigationController?.pushViewController(controller, animated: true)
            
    }
    
    
    @IBAction func regPressed(_ sender: Any) {
        
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboardMain.instantiateViewController(withIdentifier: "YLARegisterVCSBID") as! YLARegisterVC
//        controller.task_Home = self.task_type
//               controller.onPopView = { task in
//                         self.getHomeData(typeHome: task, off: 0)
//                     }
        self.navigationController?.pushViewController(controller, animated: true)
        
        
    }
    
    
    
        
    
    // MARK: - CategoryDelegate
    func didSelectCategory(selCat: CategorySegueData) {
        
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
              let controller = storyboardMain.instantiateViewController(withIdentifier: "YLACategoryVCSBID") as! YLACategoryVC
              controller.segueData = selCat
              self.navigationController?.pushViewController(controller, animated: true)
        
    }
    //MARK: - HomeItemDelegate
    
    func didFeaturedPressed(adData: HomeData)
    {
        self.pushToDetail(hData: adData)
    }
    
    func didSelectedAdType(type: HomeTask) {
        CurrentPage = 0
        
        
        
        //self.listTable.reloadData()
        if self.task_type == type
        {
            return
        }
        
        switch type {
                   case .favorite:
                    if favoriteAds != nil {
                        self.favoriteAds?.removeAll()
                        self.favoriteAds = nil
                        self.listTable.reloadData()
                    }
//                    guard let favorite = self.favoriteAds else {return }
                    if Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES
                    {
                        self.showProgressYLA()
                      if let favorite = self.favoriteAds {
                          favorite.count ==  0 ? self.getFavorite() : self.listTable.reloadData()
                        }
                        
                    }else{
                        self.loginPressed(self)
                    }

                  
                    break
                   case .recent:
                       guard let recent = self.recentAds else {return}
                       self.showProgressYLA()

                       recent.count ==  0 ? self.getHomeData(typeHome: type, off: CurrentPage) : self.listTable.reloadData()
                       break
                   case .viewed:
                    if Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES
                    {
                        self.showProgressYLA()

                    if let view = self.viewedAds {
                       view.count ==  0 ? self.getHomeData(typeHome: type, off: CurrentPage) : self.listTable.reloadData()
                    }
                    }else{
                        self.loginPressed(self)
                    }
                       break
                  
                 
        case .none:
            loginPressed(self)
            return
            
        }
        
        
        self.task_type = type
        self.task_type == .favorite ? self.getFavorite() :  self.getHomeData(typeHome: type, off: CurrentPage)
        let lastSectionIndex = self.listTable!.numberOfSections - 1
               let lastRowIndex = self.listTable!.numberOfRows(inSection: lastSectionIndex) - 1

               let indexPath =  IndexPath(row: lastRowIndex, section: 1);
               UIView.animate(withDuration: 1.0, animations: {() -> Void in
               self.listTable.scrollToRow(at: indexPath, at: .top, animated: false)
                 }, completion: {(_ finished: Bool) -> Void in
                     
                   })
       
    }
     
     func didHomeItemPressed(adData: HomeData) {
            self.pushToDetail(hData: adData)
       }
    
    func didLoadMoreAds(off: Int) {
        
//        print(off)
//        self.CurrentPage = off
//         if self.totalPageCount > 1 && (self.CurrentPage != self.totalPageCount){
//                        
//                self.getHomeData(typeHome: self.task_type, off: self.CurrentPage)
//
//        }
        
    }
    
    
    func featuredResponse(response: RequestResult)
    {
        if  let jsonObject = response.value as? [String : Any?],let model = HomeModel.toObject(value: jsonObject) {
            self.tabView?.unReadMSg =  model.countMessages
            guard let featured = model.homeData else{return}
            self.featuredAds = featured
            let index = Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES ? 0 : 1
            let indexPaths = IndexPath(item: index, section: 0)
            listTable.reloadRows(at: [indexPaths], with: .fade)
            

        }

    }
    
    func classicResponse(response: RequestResult)
       {
           if  let jsonObject = response.value as? [String : Any?],let model = HomeModel.toObject(value: jsonObject) {
               guard let featured = model.homeData else{return}
               self.classicAds = featured
//               let index = Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES ? 0 : 1
//               let indexPaths = IndexPath(item: index, section: 1)
//               listTable.reloadRows(at: [indexPaths], with: .fade)
               

           }

       }
       
    
    func homeRequestResponse(response: RequestResult)
    {
        self.hideProgressYLA()

        if  let jsonObject = response.value as? [String : Any?],let model = HomeModel.toObject(value: jsonObject) {
            
             guard let home = model.homeData else{
                
                
                
                
               //             if self.favoriteAds != nil{
//                                self.favoriteAds?.removeAll()
//                                self.favoriteAds = nil
//                            }
//
//
//                            if self.recentAds != nil{
//                                self.recentAds?.removeAll()
//                                self.recentAds = nil
//                            }
//
//
//
//                            if self.viewedAds != nil{
//                                                           self.viewedAds?.removeAll()
//                                                           self.viewedAds = nil
//                                                       }
//
                         
                
                
                
                
                
                self.listTable.reloadData()
                return}
            
            switch self.task_type {
            case .favorite:
                self.favoriteAds = home
                break
            case .recent:
                self.recentAds = home
//                   if self.CurrentPage == 0 {
//                    self.recentAds = home}
//                   else{
//                    self.recentAds!  += home
//                   }
                break
            case .viewed:
                self.viewedAds = home
                break
          
            case .none:
                break
            }
            self.listTable.reloadData()
        }
    }
    
    
    
    
    
    
    func pushToDetail(hData:HomeData)
    {
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboardMain.instantiateViewController(withIdentifier: "YLADetailVCSBID") as! YLADetailVC
        controller.adData = hData
        controller.task_Home = self.task_type
        controller.onPopView = { task in
            self.showProgressYLA()
            self.getHomeData(typeHome: task, off: 0)
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    

}
extension YLAHomeVC: UITableViewDelegate,UITableViewDataSource,SearchDelegate
{
   
    
   
    
  
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
           return 2
    }
       
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    
        var rowCount = 0
    
        switch section {
                  case 0:
                       rowCount = Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES ? 1 : 2
                  break
                  case 1:
                    rowCount = 1
                  break
                 
                 
           default:
        break
        }
        return rowCount
    }
    
    
    func setFirstIndexFocused(cell: itemTableCell,to: Int)
    {
        if to > 0{
        let indexPath = IndexPath(row: 0, section: 0);
        cell.itemCollectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
       }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0
        {
            if Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES{
                if indexPath.row == 0
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "featuredCell",for: indexPath) as! featuredTableCell
                    cell.backgroundColor = UIColor.clear
                    cell.featureDelegate = self
                    if let fAds = self.featuredAds{
                       cell.featuredCollection.homeData = fAds
                    }
                return cell
                }
            }
            else{
                if indexPath.row == 0
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "loginCell",for: indexPath) as! DocTableCell
                  cell.backgroundColor = UIColor.clear
//                    let arrayString = ["Send and receive message","Post and manage your ads","Rate other users"]
//                    cell.lblTitle.attributedText = Utility.add(stringList: arrayString, font: cell.lblTitle.font, bullet: "✔")
                  return cell
                }
                else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "featuredCell",for: indexPath) as! featuredTableCell
                                      cell.backgroundColor = UIColor.clear
                                      cell.featureDelegate = self
                                      if let fAds = self.featuredAds{
                                      cell.featuredCollection.homeData = fAds
                                     }
                                    return cell
                }
            }
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "dataCell",for: indexPath) as! itemTableCell
                         cell.backgroundColor = UIColor.clear
                         cell.hDelegate = self
                             switch self.task_type {
                             case .recent:
                                 if let rAds = self.recentAds{
                                        cell.itemCollectionView.homeData = rAds
                                    self.setFirstIndexFocused(cell: cell, to: rAds.count)
                                 
                                }
                                 else{
                                    cell.itemCollectionView.homeData?.removeAll()
                                    cell.itemCollectionView.reloadData()
                                    }
                                 break
                             case .viewed:
                                 if let vAds = self.viewedAds{
                                                     cell.itemCollectionView.homeData = vAds
                                    self.setFirstIndexFocused(cell: cell, to: vAds.count)

                                                 }
                                 else{
                                                                                cell.itemCollectionView.homeData?.removeAll()
                                                                                cell.itemCollectionView.reloadData()
                                                                            }
                                 break
                                 case .favorite:
                                                     if let fds = self.favoriteAds{
                                                                         cell.itemCollectionView.homeData = fds
                                                        self.setFirstIndexFocused(cell: cell, to: fds.count)

                                                     }
                                                         else{
                                                         cell.itemCollectionView.homeData?.removeAll()
                                                         cell.itemCollectionView.reloadData()
                                                     }
                                                     break
                         
                             case .none:
                                break
            }
                      
                                                          
                             return cell
            
        }
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var hgt: CGFloat = 0.0
        
        if indexPath.section == 0
               {
                hgt = 60.0
               }
               else{
                   
                   if indexPath.row == 0
                   {
                    hgt = 100.0

                   }
                   else{
                     hgt = 360.0

                   }
                   
                   
                   
                   
               }
               
        
        return hgt
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
        if section == 0{
        
            let headerView = UIView(frame: CGRect(x: 15,y: 0,width: tableView.bounds.size.width,height: 1))
//            self.categoryView.frame =   CGRect(x: 0, y: 0, width: headerView.bounds.size.width  , height: headerView.bounds.size.height)
//            headerView.addSubview(categoryView)
        return headerView
        }
        if section == 1{
                   let headerView = UIView(frame: CGRect(x: 15,y: 0,width: tableView.bounds.size.width,height: 44))
                   self.segView.frame =   CGRect(x: 0, y: 0, width: headerView.bounds.size.width  , height: headerView.bounds.size.height)
                   headerView.addSubview(segView)
               return headerView
               }
        else{
            let headerView = UIView(frame: CGRect(x: 0,y: 0,width: tableView.bounds.size.width,height: 15))
            return headerView
        }
        
        
    }

    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {

        let hght = section == 0 ? 1 : 44
        return CGFloat(hght)

    }
    
    
    
    func searchPressed(search: String) {
           
        self.txtSearch.text = ""
        self.txtSearch.resignFirstResponder()
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAFavoriteVCSBID") as! YLAFavoriteVC
        controller.searchText = search
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
 
    
}


extension YLAHomeVC: HomeItemDelegate{

   func didPushUsertoLogin() {
      self.loginPressed(self)
   }
    
    func didFavoriteDone(done: Bool){
        if self.task_type == .favorite
        {
            self.getFavorite()
        }
        
    }
  
  func didSellerAdPressed(homeData: HomeData) {
    
      let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
      let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAFavoriteVCSBID") as! YLAFavoriteVC
      controller.home = homeData
      self.navigationController?.pushViewController(controller, animated: true)
  }
  
}
