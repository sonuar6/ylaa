//
//  YLAWalletVC.swift
//  Ylaa
//
//  Created by Arun on 21/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class WFilterTableCell: UITableViewCell {
    @IBOutlet weak var lblAcStatus: UILabel!
    @IBOutlet weak var btnFltr: UIButton!

    override func awakeFromNib() {
           
       }
    
    func setAccountStatusCell(isActive: Bool, sText: String)
    {
        lblAcStatus.text = sText
        btnFltr.isHidden = isActive ? true : false
    }
       
    
}






class WalletTableCell: UITableViewCell {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblDebt: UILabel!
    @IBOutlet weak var lblCredit: UILabel!
    @IBOutlet weak var lblBalance: UILabel!
    
    override func awakeFromNib() {
        
    }
    
    func setTransactionCell(trans: WalletTransactionModel)
    {
        if let dateStr = trans.tdate{
            self.lblDate.text = dateStr.isEmpty ? "" :  Utility.getStringDate(dateStr: dateStr)
        }
        self.lblDesc.text = trans.tdetail
        self.lblDebt.text = trans.tdebit
        self.lblCredit.text = trans.tcredit
        self.lblBalance.text = trans.tbal
    }
    
}





class YLAWalletVC: BaseTabBarController {
    
    var wDataModel: WalletDataModel?
    
    @IBOutlet weak var lisTable: UITableView!
    @IBOutlet weak var lblAccount: UILabel!
    @IBOutlet weak var btnBalance: UIButton!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblCrncy: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblAccount.text = "9522-" + (Utility.getPlistValueforKey(key: YLA.USER_ID) as String)

        getWalletInfo()
        

        // Do any additional setup after loading the view.
    }
    

  
    
    func getWalletInfo()
    {
        self.showProgressYLA()
        NetworkManager.getWallet(completionHandler: self.walletResponse(response:)) { (ERR) in
            self.hideProgressYLA()
            self.showToast(message: ERR)
        }
    }
    
    
    
    func walletResponse(response:RequestResult)
    {
        self.hideProgressYLA()
        if  let jsonObject = response.value as? [String : Any?],let model = WalletListModel.toObject(value: jsonObject) {
            if model.status == "success"{
            if let mData = model.data
            {
                self.wDataModel = mData
                setUpWalletInfoUI()
                
                
                
            }
        }else{
                //self.showToast(message: model.message!)
        }
        }

     
    }
    
    
    func setUpWalletInfoUI()
    {
        if let uData = self.wDataModel?.userData{
            
            self.btnBalance.setTitle(uData.avail_bal, for: .normal)
            if let dateStr = uData.lastupdateon{
                self.lblDate.text = dateStr.isEmpty ? "" : "Last Updated On " + Utility.getStringDate(dateStr: dateStr)
            }
                self.lblStatus.text = uData.ac_status?.uppercased()
                self.lblCrncy.text = uData.curr?.uppercased()
            self.lisTable.reloadData()


           
            
        }
    }
    
    
    

}

extension YLAWalletVC: UITableViewDelegate,UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.wDataModel?.transctions != nil ? (self.wDataModel?.transctions!.count)! + 1 : 1

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier:"filterCell",
                                                            for: indexPath)as! WFilterTableCell
            self.wDataModel?.transctions != nil ? cell.setAccountStatusCell(isActive: false, sText: "Account Statement") : cell.setAccountStatusCell(isActive: true, sText: "No Transactions")
        return cell
        }
        else  {
        let cell = tableView.dequeueReusableCell(withIdentifier:"stmntCell",
                                                                   for: indexPath) as! WalletTableCell
            if let transData = self.wDataModel?.transctions{
                let tData = transData[indexPath.row - 1]
                cell.setTransactionCell(trans: tData)
            }
        return cell
        }
    }

 
    
}
