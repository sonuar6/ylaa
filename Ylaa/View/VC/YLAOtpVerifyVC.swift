//
//  YLAOtpVerifyVC.swift
//  Ylaa
//
//  Created by Arun on 07/02/2020.
//  Copyright © 2020 Arun. All rights reserved.
//

import UIKit
import SwiftyCodeView
class YLAOtpVerifyVC: BaseViewController,SwiftyCodeViewDelegate {
    
    
    
    @IBOutlet weak var codeView: CustomCodeView!
    var OTPCode: String = ""
    var regUser: UserRegistrationModel?
    var fPassword: ForgotModel?
    var credential: CredentialModel?
    
    var onCredenetialEdited: ((_ isMobile :Bool)->())?



    
    
    
    func codeView(sender: SwiftyCodeView, didFinishInput code: String) {
        OTPCode = code
        if OTPCode.count == 4{
                      //sendOtpVerification()

            }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        codeView.delegate = self


        // Do any additional setup after loading the view.
    }
    
    @IBAction func verifyPressed(_ sender: Any) {
        if OTPCode.count == 4{
            var otpCode = 0

            
            if self.regUser != nil {
            sendOtpVerification()
            }else if self.credential != nil{
                
                var phone = false

                
                if let otp = self.credential?.motp {
                    if otp > 0{
                    phone = true
                    otpCode = otp
                    }
                }
                if let otp = self.credential?.eotp {
                    if otp > 0{
                    phone = false
                    otpCode = otp
                    }
                }
                if OTPCode == String(otpCode)
                {
                    self.onCredenetialEdited!(phone)
                    self.navigationController?.popViewController(animated: true)
                }else{
                    showToast(message: "Invalid OTP")
                }
                
                
            }
            else{
        
            if let otp = self.fPassword?.MOTP {
              otpCode = otp
            }
            if let otp = self.fPassword?.EOTP {
                       otpCode = otp
                     }
            if OTPCode == String(otpCode)
            {
                self.login()
            }else{
                showToast(message: "Invalid OTP")
            }
        }
        }
        
    }
    
    func sendOtpVerification()
    {
        var user_id = 0

        if let regUser = self.regUser{
            user_id = regUser.user_id!
            NetworkManager.otpVerification(userID: user_id, otp: self.OTPCode, completionHandler: self.requestResponse(response:)) { (ERR) in
                                print(ERR)
            }
            
        }
      
     
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func requestResponse(response: RequestResult)
    {
        if  let jsonObject = response.value as? [String : Any?],let model = UserOTPVerifiedModel.toObject(value: jsonObject) {
                   if model.Status == "Success" {
                    let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAHomeVCSBID") as! YLAHomeVC
                    self.navigationController?.pushViewController(controller, animated: false)
                   
            }
                   else{
                    showToast(message: model.message!)
            }
    }
    }
    
    
    
    
    
    
    
    func login()
    {
        var uName = ""
        var type = ""

      
           
        if let uname = self.fPassword?.email, !uname.isEmpty
        {
            uName = uname
            type = "email"
        }
        
        if let uname = self.fPassword?.mobile, !uname.isEmpty
        {
            uName = uname
            type = "phone"

        }


           //Utility.getPlistValueforKey(key: YLA.PASWRD) as String
        NetworkManager.userLogin(username: uName, password:self.fPassword!.password! , type: type, completionHandler: self.loggedInResponse(response:), errorHandler: { (ERR) in
               Utility.resetLoginCredentials()
               self.showToast(message: ERR)

           })
    }
       
       func loggedInResponse(response: RequestResult)
       {
           if  let jsonObject = response.value as? [String : Any?],let model = LoggedUserModel.toObject(value: jsonObject) {
               
               UserDataManager.sharedInstance.user = model
            if UserDataManager.sharedInstance.user!.Status != "Error"{
                if let userID = UserDataManager.sharedInstance.user!.user_id, userID > 0
               {
                   //Utility.resetPlistValueForKey(key: YLA.USER_LOGGEDIN, value: YLASTATUS.YES)
                let storyboardMain = UIStoryboard(name: "Main", bundle: nil)

                let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAProfileVCSBID") as! YLAProfileVC
                self.navigationController?.pushViewController(controller, animated: true)
                   
               }
               
           }
               else{
                showToast(message: UserDataManager.sharedInstance.user!.msg!)
               }
           }

       }
       
       
       
     
      
       
       
       
      
    
    

}
