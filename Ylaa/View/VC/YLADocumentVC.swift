//
//  YLADocumentVC.swift
//  Ylaa
//
//  Created by APPLE on 19/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit
import MobileCoreServices
import Photos


class YLADocumentVC: BaseTabBarController {
    @IBOutlet weak var listTable: UITableView!
    var picker:UIImagePickerController? = UIImagePickerController()

    var uploadItem:Specifications?
    
    
    var docsData: [Specifications]?
    override func viewDidLoad() {
        super.viewDidLoad()
        picker?.delegate = self

        self.getViewDocdata()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func getViewDocdata()
    {
        self.showProgressYLA()

        NetworkManager.viewDocuments(completionHandler: self.viewDocResponse(response:)) { (ERR) in
            self.showToast(message: ERR)
        }
    }

    
    func viewDocResponse(response: RequestResult)
    {
        
        self.hideProgressYLA()

        if  let jsonObject = response.value as? [String : Any?],let model = DocumentListModel.toObject(value: jsonObject) {
            
            guard let docs = model.docSpecs else{return}
            self.docsData = docs
            self.listTable.reloadData()

        }

    }


    func showFilterActionSheet()  {

        let optionMenu = UIAlertController(title: nil, message: "Picture", preferredStyle: .actionSheet)

        let camAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
          self.openCamera()


        })
        let libAction = UIAlertAction(title: "From Library", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
       
          self.openGallary()

        })
     
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })


        // 4
        optionMenu.addAction(camAction)
        optionMenu.addAction(libAction)
        optionMenu.addAction(cancelAction)

        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }







}
extension YLADocumentVC: UITableViewDelegate,UITableViewDataSource,DocumentDelegate{
  
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var count  = 0
        if let dc = self.docsData{
            count = dc.count
        }
       return count
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"docCell",
                                                            for: indexPath) as! DocTableCell
        cell.contentView.layer.masksToBounds = false
        cell.contentView.layer.shadowColor = UIColor.black.cgColor
        cell.contentView.layer.shadowOffset = CGSize(width: -1, height: 1)
        cell.contentView.layer.shadowRadius = 1
        cell.contentView.layer.shadowPath = UIBezierPath(rect: cell.bounds).cgPath
        cell.contentView.layer.shouldRasterize = true
        cell.contentView.layer.rasterizationScale = UIScreen.main.scale
        cell.dDelegate = self
        if let dc = self.docsData{
            cell.setDocCellData(spec: dc[indexPath.section])
        }
        
       

        return cell
    }

   func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
   {
          
       
              let headerView = UIView(frame: CGRect(x: 0,y: 0,width: tableView.bounds.size.width,height: 10))
              headerView.backgroundColor = .clear
              return headerView
       
   }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
    
    
    func didSelectedDocument(forDoc: Specifications) {
        
        self.uploadItem = forDoc
        self.showFilterActionSheet()
        
    }
    
    func didSelectedDocumentforDelete(forDoc: Specifications) {
        
        let alertController = UIAlertController(title: "Delete?", message: "Are you sure you want to delete" + forDoc.item_name! +  "document", preferredStyle: .alert)
              alertController.addAction(UIAlertAction(title: "Submit", style: .default, handler: { alert -> Void in
                self.dleteDoc(forDocs: forDoc)
                  
              }))
              alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)

    }
    
    
    
    func dleteDoc(forDocs: Specifications)
    {
        self.showProgressYLA()
        NetworkManager.deleteDoc(dcType: forDocs.item_name!, completionHandler: self.deleteResponse(response:)) { (ERR) in
            self.showToast(message: ERR)
        }
    }
      
    
    func deleteResponse(response: RequestResult)
    {
        self.hideProgressYLA()

        if  let jsonObject = response.value as? [String : Any?],let model = UserOTPVerifiedModel.toObject(value: jsonObject) {
            if model.Status == "success"
            {
              self.showToast(message: model.message!)
              self.getViewDocdata()

            }
            else{
           self.showToast(message: "Unable to complete operation")

            }
        }
    }
    
}



extension YLADocumentVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
     private func openGallary()
        {
            picker!.allowsEditing = false
            picker!.sourceType = UIImagePickerController.SourceType.photoLibrary
            present(picker!, animated: true, completion: nil)
        }
        
        
        private func openCamera()
        {
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
                picker!.allowsEditing = false
                picker!.sourceType = UIImagePickerController.SourceType.camera
                picker!.cameraCaptureMode = .photo
                present(picker!, animated: true, completion: nil)
            }else{
                let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
                let ok = UIAlertAction(title: "Ok", style:.default, handler: nil)
                alert.addAction(ok)
                present(alert, animated: true, completion: nil)
            }
        }
        
        
        //delegate for gallery or camera picker
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            
            
        
            var selectedImage: UIImage?
            var fileName : String?
            if let editedImage = info[.editedImage] as? UIImage {
                selectedImage = editedImage
                picker.dismiss(animated: true, completion: nil)
            } else if let originalImage = info[.originalImage] as? UIImage {
                selectedImage = originalImage
                picker.dismiss(animated: true, completion: nil)
            }
         /*
            if let assetPath = info[UIImagePickerController.InfoKey.referenceURL] as? NSURL{
                
                if (assetPath.absoluteString?.hasSuffix("JPG"))! || (assetPath.absoluteString?.hasSuffix("JPEG"))! || (assetPath.absoluteString?.hasSuffix("jpg"))! || (assetPath.absoluteString?.hasSuffix("jpeg"))!  {
                    if let jpegdata = selectedImage?.compressTo(3)!.base64EncodedString(){
                        self.IndUserUpdate?.Pic = jpegdata
                    }
                }
                if (assetPath.absoluteString?.hasSuffix("PNG"))! || (assetPath.absoluteString?.hasSuffix("png"))! {
                    if let pngdata = PngImageFileB64(uploadImage: selectedImage){
                         self.IndUserUpdate?.Pic = pngdata
                        
                    }
                }
            }else{
                if let jpegdata = selectedImage?.compressTo(3)!.base64EncodedString(){
                    self.IndUserUpdate?.Pic = jpegdata
                }
                
            }
            
            if #available(iOS 11.0, *) {
                if let asset = info[UIImagePickerController.InfoKey.phAsset] as? PHAsset {
                    if let Name = (asset.value(forKey: "filename")) as? String {
                        fileName = Name
                    }
                }
            } else {
                // Fallback on earlier versions
                if let imageURL = info[UIImagePickerController.InfoKey.referenceURL] as? URL {
                    let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
                    if let first = result.firstObject {
                        if let assetResourcesFirst = PHAssetResource.assetResources(for:first ).first{
                            fileName = assetResourcesFirst.originalFilename
                        }
                    }
                    
                }
            }
 */
            //adding item to uploadList
            
         
            
            //        if let pngdata = PngImageFileB64(uploadImage: selectedImage){
            //            uploadItem.file_uploaded = pngdata
            //            uploadItem.file_extension = "png"
            //        }
            //        else if let jpegdata = JpegImageFileB64(uploadImage: selectedImage){
            //            uploadItem.file_uploaded = jpegdata
            //            uploadItem.file_extension = "jpeg"
            //        }
//            if uploadItem.file_extension == nil{
//                showMessage(message: "Image format not supports".localized(), viewController: self, handler: nil)
//                return
//            }
//            DocList.append(uploadItem)
//            //adding selected item to collection
//            addedItem()
            
//
//            if let pngdata = PngImageFileB64(uploadImage: selectedImage){
//                 self.IndUserUpdate?.Pic = pngdata
//
//            }
            
//
            
            var file = ""
            switch self.uploadItem?.item_name {
                       case "tradelic":
                           file = "tradelic"
                           break
                       case "auth_doc":
                           file = "auth_doc"
                             break
                       case "visapage":
                             file = "visapage"
                             break
                       case "cv":
                            file = "cv"
                            break
                       case "otherdoc1":
                            file = "otherdoc1"
                            break
                       case "otherdoc2":
                            file = "otherdoc2"
                            break
                       case "em_id":
                            file = "em_id"
                            break
                        case "passport":
                            file = "passport"
                                   
                
                       default:
                           break
                       }
            
            
            
            
            NetworkManager.uploadImage(isDOC: true, filename: file, image: selectedImage!, completionHandler: self.imageResponse(response: )) { (ERR) in
                self.showToast(message: ERR)
            }
//
        }
    
  
    
    func imageResponse(response: RequestResult)
    {
        if  let jsonObject = response.value as? [String : Any?],let model = DocumentResponseModel.toObject(value: jsonObject) {
            if model.result == "1"
            {
                self.getViewDocdata()
                self.showToast(message: model.msg!)
            }else{
                self.showToast(message: model.msg!)

            }
    }
   }
//
}
   
