//
//  YLANotificationsVC.swift
//  Ylaa
//
//  Created by APPLE on 17/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class YLANotificationsVC: BaseTabBarController {
    
    @IBOutlet weak var listTable: UITableView!
    
    var notifications: [NotificationItemModel]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showProgressYLA()

      
      

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getNotification()

    }
    
    
    func getNotification()
    {
              NetworkManager.getMYNotifications(completionHandler: self.notification(response:)) { (ERR) in
                  self.showToast(message: ERR)
                  self.hideProgressYLA()
              }
    }
    

   func notification(response: RequestResult)
   {
     self.hideProgressYLA()
    if  let jsonObject = response.value as? [String : Any?],let model = NotificationListModel.toObject(value: jsonObject) {
        
        if let notf = model.data, notf.count > 0 {
            self.notifications = notf
            listTable.reloadData()
        }
        else{
            self.showToast(message: "No Record Found")
        }
        
        
    }


   }

}
extension YLANotificationsVC: UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
         let count = self.notifications == nil ? 0 : self.notifications?.count
         return count!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
             return 1
       }
         
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
               let cell = tableView.dequeueReusableCell(withIdentifier:"notificationCell",
                                                                         for: indexPath) as! DocTableCell
        cell.lblMsg.textColor = .subTitleColor
        cell.lblTitle.textColor = .subTitleColor
        cell.lblDate.textColor = .subTitleColor
        
        if let notf = self.notifications{
            let notif = notf[indexPath.section]
            cell.setNotificationCellData(not: notif)
            
        }
        return cell
          
       
       }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView(frame: CGRect(x: 0,y: 0,width: tableView.bounds.size.width,height: 10))
        return headerView
                          
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
         return true
    }
    
    
   func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?{

    let delAction = UITableViewRowAction(style: .default, title: "Delete", handler: { (_, indexPath) in
        
        if let ntf = self.notifications{
                   if ntf.count > 0
                   {
                       let notf = ntf[indexPath.section]
                       self.deleteNotification(notf: notf)
                   }
               }
        })
      delAction.backgroundColor = .orangeColor

        return [delAction]
    }

    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
      if editingStyle == .delete {
//        self.notifications?.remove(at: indexPath.section)
//        self.listTable.reloadData()
//        if let ntf = self.notifications{
//            if ntf.count > 0
//            {
//                let notf = ntf[indexPath.section]
//                self.deleteNotification(notf: notf)
//            }
//        }

      }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let not = self.notifications{
            let nfc = not[indexPath.section]
         let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboardMain.instantiateViewController(withIdentifier: "YLANotDetailVCSBID") as! YLANotDetailVC
            controller.notificationModel = nfc
                self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    
    func deleteNotification(notf:NotificationItemModel)
    {
        NetworkManager.deleteNotification(isDel: true, notfID: notf.id!, completionHandler: self.deleteResponse(response:)) { (ERR) in
            self.showToast(message: ERR)
        }
    }
       
    func deleteResponse(response: RequestResult)
    {
        if  let jsonObject = response.value as? [String : Any?],let model = UserOTPVerifiedModel.toObject(value: jsonObject) {
            if model.Status == "Success"
           {
             self.getNotification()
            }
            else{
              self.showToast(message: "Unable to complete operation")
            }
        }
    }
    

    
    
}
