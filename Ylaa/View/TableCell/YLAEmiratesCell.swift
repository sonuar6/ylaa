//
//  YLAEmiratesCell.swift
//  Ylaa
//
//  Created by APPLE on 17/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit
protocol OptionalDelegate {
    func didSelectedOption(isOn: Bool, forRow: Int)
}
class YLAEmiratesCell: UITableViewCell {

    @IBOutlet weak var btnOptn: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var thumbImageView: UIImageView!
    var oDelegate: OptionalDelegate?
    
    @IBAction func optionPressed(_ sender: Any) {
        let btn = sender as! UIButton
        
        if btn.backgroundColor == .black {
            btnOptn.backgroundColor = .orangeColor
            self.oDelegate?.didSelectedOption(isOn: true, forRow: btn.tag)
            
        }
        else{
            btnOptn.backgroundColor = .black
            self.oDelegate?.didSelectedOption(isOn: false, forRow: btn.tag)
        }
    }
    
    func setCellTag(tagNum: Int)
    {
        btnOptn.tag = tagNum
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //btnOptn.backgroundColor = .subTitleColor
        // Initialization code
    }
    func setProfileCell(titleSTr: String,thumb: UIImage)
       {
           self.lblTitle.text = titleSTr
           self.thumbImageView.image = thumb
       }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
