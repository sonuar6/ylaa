//
//  textTableCell.swift
//  Ylaa
//
//  Created by Arun on 21/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit
protocol TextDelegate {
    func didTextEditStarted(cellTextField: UITextField)
}


class textTableCell: UITableViewCell,UITextFieldDelegate {

    @IBOutlet weak var thumbImgView: UIImageView!
    @IBOutlet weak var txtInput: UITextField!
    @IBOutlet weak var lblCaption: UILabel!
    
    var textDelegate:TextDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.txtInput.delegate = self
        // Initialization code
    }
    
    
    
    func setTextCell(thumb: UIImage, caption: String, textVal: String,isSecured: Bool,enabled: Bool,tTag: Int)
    {
        self.txtInput.delegate = self
        self.thumbImgView.image = thumb
        self.txtInput.isSecureTextEntry = isSecured
        self.lblCaption.text = caption
        self.txtInput.text = textVal
        self.txtInput.isUserInteractionEnabled = enabled
        self.txtInput.tag = tTag



    }
    func setFilterTextCell(caption: String, textVal: String,isNumber: Bool,enabled: Bool,tTag: Int)
    {
           self.txtInput.text = textVal
           self.txtInput.tag = tTag
           txtInput.attributedPlaceholder = NSAttributedString(string: caption, attributes: [NSAttributedString.Key.foregroundColor: UIColor.textDimColor])
           //self.txtInput.placeholder = caption
           self.txtInput.keyboardType = isNumber == true ? .numberPad : .phonePad
           self.txtInput.isUserInteractionEnabled = enabled

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if !textField.text!.isEmpty{
           self.textDelegate?.didTextEditStarted(cellTextField: textField)
        }
    }
       
       private func textFieldShouldBeginEditing(textField: UITextField) -> Bool {

           return true
       }
       
      
}
