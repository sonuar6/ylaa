//
//  locationTableCell.swift
//  Ylaa
//
//  Created by APPLE on 17/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

protocol NearByDelegate: class {
    func didNearbySelected(isOn: Bool, forRow: Int, value: String)
    func didFilterByMiles(klmtr: Int)


}

class locationTableCell: UITableViewCell {
    
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var dSlider: UISlider!
    @IBOutlet weak var btnOptn: UIButton!
    @IBOutlet weak var lblSliderVal: UILabel!
    
    weak var nByDelegate:NearByDelegate?
    
    
    
    
    
    
    @IBAction func valueChanged(_ sender: Any) {
        
        
        let trackRect = self.dSlider.trackRect(forBounds: self.dSlider.bounds)
        let thumbRect = self.dSlider.thumbRect(forBounds: self.dSlider.bounds, trackRect: trackRect, value: self.dSlider.value)
        lblSliderVal.center = CGPoint(x: (thumbRect.origin.x + self.dSlider.frame.origin.x)+15,y:  self.dSlider.frame.origin.y + 15);
        lblSliderVal.isHidden = false

        lblSliderVal.text = String(Int(dSlider.value)) + "Km"

        if let delegate = self.nByDelegate
        {
            delegate.didFilterByMiles(klmtr: Int(dSlider.value))
        }

        
        
    }
    @IBAction func optionPressed(_ sender: Any) {
        
        let btn = sender as! UIButton

              
              if btn.backgroundColor == .black {
                  btnOptn.backgroundColor = .orangeColor
                  self.dSlider.isEnabled = true
                self.nByDelegate?.didNearbySelected(isOn: true, forRow: btn.tag, value: String(Int(dSlider.value)))
                  
              }
              else{
                  btnOptn.backgroundColor = .black
                  self.dSlider.isEnabled = false
                self.nByDelegate?.didNearbySelected(isOn: false, forRow: btn.tag, value: String(Int(dSlider.value)))
                
              }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblSliderVal.isHidden = true
        self.dSlider.isEnabled = true
        self.dSlider.value = 0.0
        dSlider.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)


        // Initialization code
    }
    
    
    @objc func onSliderValChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
              

                break
                // handle drag began
            case .moved:
                let trackRect = self.dSlider.trackRect(forBounds: self.dSlider.bounds)
                                     let thumbRect = self.dSlider.thumbRect(forBounds: self.dSlider.bounds, trackRect: trackRect, value: self.dSlider.value)
                                     lblSliderVal.center = CGPoint(x: (thumbRect.origin.x + self.dSlider.frame.origin.x)+15,y:  self.dSlider.frame.origin.y + 15);
                                     lblSliderVal.isHidden = false

                                     lblSliderVal.text = String(Int(dSlider.value)) + "Km"
                break
                // handle drag moved
            case .ended:
                if let delegate = self.nByDelegate
                {
                   delegate.didFilterByMiles(klmtr: Int(dSlider.value))
                }
                break
                // handle drag ended
            default:
                break
            }
        }
    }

    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
