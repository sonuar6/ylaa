//
//  DocTableCell.swift
//  Ylaa
//
//  Created by Arun on 02/03/2020.
//  Copyright © 2020 Arun. All rights reserved.
//

import UIKit

protocol DocumentDelegate: class {
    func didSelectedDocument(forDoc: Specifications)
    func didSelectedDocumentforDelete(forDoc: Specifications)

}

class DocTableCell: UITableViewCell {
    
    weak var dDelegate: DocumentDelegate?
    var docItem:  Specifications?

    @IBOutlet weak var thumbImgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var iconAdImgView: UIImageView!
    @IBOutlet weak var lblCount: UILabel!

    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    
    
    func setDocCellData(spec: Specifications)
    {
        thumbImgView.image = nil
        docItem = spec
        if let fname = spec.filename, !fname.isEmpty{
            lblTitle.text = fname
        }else{
            lblTitle.text = "Add " + Utility.getDocumentName(name: spec.item_name!)
        }
        
        lblName.text = Utility.getDocumentName(name: spec.item_name!)
        if spec.item_value!.isEmpty{
            thumbImgView.image = UIImage(named: "placeHolder")
            btnAdd.setTitle("Add", for: .normal)
            iconAdImgView.image = #imageLiteral(resourceName: "add")
        }else{
            btnAdd.setTitle("Edit", for: .normal)
            thumbImgView.setImageForYlaaUser(url: spec.item_value, placeHolder: "placeHolder")
            iconAdImgView.image = #imageLiteral(resourceName: "editCol")


        }
    }
    
    
    func setChatCellData(chat: ChatSessionDataModel)
    {
        if let pic =  chat.ad_detail?.catdetail?.image{
        thumbImgView.setImageWithURL(url: pic, placeholder: "no_pic") //setImageForYlaaUser(url: pic, placeHolder: "placeHolder")
        }
        lblMsg.text = chat.user_detail?.lastTextMessage
        lblTitle.text = chat.ad_detail?.catdetail?.title
        lblDate.text = Utility.getMsgStringDate(dateStr: chat.user_detail!.lastTextDateTime!)
        lblName.text = chat.user_detail?.name
        if let count = chat.user_detail!.unreadMsgsCount
        {
            if count > 0 {
                self.lblCount.isHidden = false
                self.lblCount.text = String(count)
            }
        }
        
    }
    
    func setChatHistoryCellData(chat: ChatHistoryDataModel)
    {
        lblTitle.text = chat.message
        lblDate.text = Utility.getMsgStringDate(dateStr: chat.datetime!)
    }
    
    
    
    
    func setNotificationCellData(not: NotificationItemModel)
      {
        lblTitle.text = not.title
        lblDate.text = Utility.getStringDate(dateStr: not.ndate!)
        lblMsg.text = not.text
        if let pic =  not.pic{
            thumbImgView.setImageForYlaaUser(url: pic, placeHolder: "no_pic")
        }
        
        if let view = not.readStatus{
            if view ==  1
            {
              self.lblMsg.textColor = .subTitleColor
              self.lblTitle.textColor = .subTitleColor
              self.lblDate.textColor = .subTitleColor
            }else{
                self.lblMsg.textColor = .white
                self.lblTitle.textColor = .white
                self.lblDate.textColor = .white

            }
        }


      }
    
    
    @IBAction func actionPressed(_ sender: Any) {
        let btn = sender as! UIButton
        
        if let dco =  self.docItem{
        if btn.tag == 111{
            self.dDelegate?.didSelectedDocument(forDoc: docItem!)
        }else{
            if !dco.item_value!.isEmpty{
            self.dDelegate?.didSelectedDocumentforDelete(forDoc: docItem!)
            }

        }
        }
        
    }
    
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
