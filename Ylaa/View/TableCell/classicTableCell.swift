//
//  classicTableCell.swift
//  Ylaa
//
//  Created by Arun on 25/02/2020.
//  Copyright © 2020 Arun. All rights reserved.
//

import UIKit

protocol classicDelegate {
    func didFeaturedPressed(adData: HomeData)
    func didPushUsertoLogin()
    func didSellerAdPressed(homeData: HomeData)
    func didVideoPlay(homeData: HomeData)



}


class classicTableCell: UITableViewCell,YLACollectionDelegate {
   
    
    
    
   
    

    @IBOutlet weak var classicCollection: YLACollectionView!
    
    var clsDelegate: classicDelegate?

    
    func didFavoriteDone(done: Bool) {
        
    }
    func didVideoPressed(hData: HomeData) {
        self.clsDelegate?.didVideoPlay(homeData: hData)

    }
    
    func didSelectedItem(adData: HomeData) {
                   self.clsDelegate?.didFeaturedPressed(adData: adData)

       }
       
       func didLoadMoreItems(offset: Int) {
           
       }
    
    func movetoSellerAds(home: HomeData) {
        self.clsDelegate?.didSellerAdPressed(homeData: home)
    }
    
    func userLogin() {
        self.clsDelegate?.didPushUsertoLogin()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.classicCollection.initCollection(rowCount: 1, collection: .Classic, scrollingDirection: .vertical)
        self.classicCollection.ylaDelegate = self


        // Initialization code
    }
    
    
    
    
    
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
