//
//  infoTableCell.swift
//  Ylaa
//
//  Created by Arun on 01/01/2020.
//  Copyright © 2020 Arun. All rights reserved.
//

import UIKit


protocol SearchDelegate: class {
    func searchPressed(search: String)
}
protocol InfoDelegate: class {
    func logoPressed()
}
class infoTableCell: UITableViewCell {

    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnLoc: UIButton!
    @IBOutlet weak var dateBtn: UIButton!
    @IBOutlet weak var dlrBtn: UIButton!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblLoc: UILabel!
    @IBOutlet weak var lblPtype: UILabel!
    @IBOutlet weak var aedLabel: UILabel!
    @IBOutlet weak var btnFavCount: UIButton!
    
    weak var sDelegate: SearchDelegate?
    weak var iDelegate: InfoDelegate?

    
    @IBAction func logoClicked(_ sender: Any) {
        
        if let delegate = iDelegate
        {
            delegate.logoPressed()
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        // Initialization code
    }

    func setInfoCellData(adDetail: ADItemDetailData, home: HomeData)
    {
        
        dlrBtn.isHidden =  home.catdetail?.seller_type  == "biz" ? false : true
        
        
        self.btnFavCount.setImage(self.btnFavCount.currentImage!.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnFavCount.tintColor = .orangeColor
        self.btnLoc.setImage(self.btnLoc.currentImage!.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnLoc.tintColor = .orangeColor
        
        btnLoc.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        btnFavCount.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        dateBtn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left

        
        if let rate = home.catdetail!.price{
         if rate > 0 && home.catdetail?.ptype != "Call for Price"
            {
              self.lblRate.text = Utility.setDecimalNumber(valueInt: rate)
            }else{
              self.lblRate.text  =  "Call for Price"
              self.aedLabel.text = ""
              self.lblPtype.isHidden = true
            }
            
        
        }
        if let viewC = home.catdetail?.viewsCount{
            self.btnLoc.setTitle("  " + String(viewC), for: .normal)

        }
        
        if let favC = home.catdetail?.favCount{
            
            if favC > 0 {
                self.btnFavCount.isHidden = false
                self.btnFavCount.setTitle("  " + String(favC), for: .normal)
            }
        }
        
        if let logo = home.logo, !logo.isEmpty{
            self.dlrBtn.setLogoImage(imageUrl: logo)

        }
        
       
        self.lblPtype.text = home.catdetail?.ptype
        self.lblTitle.text = adDetail.title
        self.lblLoc.text = adDetail.location!
        self.dateBtn.setTitle(adDetail.date!, for: .normal)


    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func seearchPressed(_ sender: Any) {
        if let txt = self.txtSearch.text , !txt.isEmpty
        {
            self.sDelegate!.searchPressed(search: txt)
        }
        
    }
    
    
    
    
    

}
