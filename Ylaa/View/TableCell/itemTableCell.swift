//
//  itemTableCell.swift
//  Ylaa
//
//  Created by Arun on 01/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

protocol HomeItemDelegate {
    func didHomeItemPressed(adData: HomeData)
    func didSelectedAdType(type: HomeTask)
    func didLoadMoreAds(off: Int)
    func didPushUsertoLogin()
    func didSellerAdPressed(homeData:HomeData)
    func didVideoPlay(homeData: HomeData)
    func didFavoriteDone(done: Bool)
}


class itemTableCell: UITableViewCell,YLACollectionDelegate {
   
    
  
    
 
    
   
   
    
  
    
  
    @IBOutlet weak var constHeight: NSLayoutConstraint!


    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var itemCollectionView: YLACollectionView!
    
    
    var tempButton:UIButton?
    var hDelegate: HomeItemDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.itemCollectionView.initCollection(rowCount: 2, collection: .Home, scrollingDirection: .vertical)
        self.itemCollectionView.ylaDelegate = self
        self.itemCollectionView.reloadData()
        //self.viewDidLayoutSubviews()
        self.tempButton = self.contentView.viewWithTag(111) as? UIButton
        // Initialization code
    }
    
    
    func setAdCellData(home: [HomeData])
    {
        
    }

    
    @IBAction func btnPressed(_ sender: Any) {
        
        
        tempButton?.setTitleColor(UIColor.subTitleColor, for: .normal)
        let btn = sender as! UIButton
        btn.setTitleColor(UIColor.highlightedColor, for: .normal)
        tempButton = btn
        
        
        switch btn.tag {
        case 111:
            self.hDelegate?.didSelectedAdType(type: .recent)
            break
        case 112:
            self.hDelegate?.didSelectedAdType(type: .viewed)
            break
        case 113:
            self.hDelegate?.didSelectedAdType(type: .favorite)
            break
        default:
            break
        }
        

    }
    
    func viewDidLayoutSubviews() {
           let height = itemCollectionView.collectionViewLayout.collectionViewContentSize.height
           constHeight.constant = height
           self.contentView.layoutIfNeeded()
       }
    
  
   func didSelectedItem(adData: HomeData) {
    self.hDelegate?.didHomeItemPressed(adData: adData)

     }
     
     func didPageScrollToIndex(pageIndex: NSInteger) {
         
     }
    func didLoadMoreItems(offset: Int) {
        
        self.hDelegate?.didLoadMoreAds(off: offset)
    }
    
    
    func userLogin() {
        self.hDelegate?.didPushUsertoLogin()
       }
     
       func movetoSellerAds(home: HomeData) {
                               self.hDelegate?.didSellerAdPressed(homeData: home)

        }
    
    func didFavoriteDone(done: Bool) {
          self.hDelegate?.didFavoriteDone(done: done)
          
      }
    func didVideoPressed(hData: HomeData) {
                   self.hDelegate?.didVideoPlay(homeData: hData)

       }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
