//
//  bannerTableCell.swift
//  Ylaa
//
//  Created by Arun on 10/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

protocol BannerDelegate {
    func didSelectedBanner(image: [Images],index: IndexPath)
}

class bannerTableCell: UITableViewCell,YLABannerDelegate {
   
    
   
    
   
    
  
    
    var bDelegate: BannerDelegate?
    var label:UILabel!
    var HglLabel:UILabel!
    var SoldLabel:UILabel!

    var images: [Images]?
    
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var btnVideo: UIButton!
        @IBOutlet weak var bannerCollection: YLACollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.bannerCollection.initCollection(rowCount: 1, collection: .banner, scrollingDirection: .horizontal)
        self.bannerCollection.bDelegate = self
  
        label = UILabel(frame: CGRect(x:  self.frame.width - 100.0, y: self.contentView.frame.height - 70, width: 50, height: 21))
        //label.center = CGPoint(x:, y: 20)
        label.layer.borderWidth = 0.5
        label.borderColor = UIColor.clear
        label.textColor = .white
        label.layer.cornerRadius = 5
        label.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        label.clipsToBounds = true
        label.font = UIFont(name: "SegoeUI-Regular", size: 5.0)
        label.textAlignment = .center
        self.contentView.addSubview(label)
        
        
        
        HglLabel = UILabel(frame: CGRect(x:  10, y: 0, width: 90, height: 21))
        //label.center = CGPoint(x:, y: 20)
        HglLabel.layer.borderWidth = 0.5
        HglLabel.borderColor = UIColor.clear
        HglLabel.textColor = .orangeColor
        HglLabel.layer.cornerRadius = 5
        HglLabel.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        HglLabel.clipsToBounds = true
        HglLabel.text = "URGENT"

        HglLabel.font = UIFont(name: "SegoeUI-Regular", size: 8.0)
        HglLabel.textAlignment = .center
        HglLabel.isHidden = true
        self.contentView.addSubview(HglLabel)
        
        
        SoldLabel = UILabel(frame: CGRect(x:  80, y: self.contentView.frame.height/2 - 40 , width: UIScreen.main.bounds.size.width - 180, height: 35))
        SoldLabel.transform = CGAffineTransform(rotationAngle: -.pi / 4)
        SoldLabel.layer.borderWidth = 1.0
        SoldLabel.borderColor = UIColor.white
        SoldLabel.textColor = .white
        SoldLabel.layer.cornerRadius = 1
        SoldLabel.backgroundColor = .customBlackColor
        SoldLabel.clipsToBounds = true
        SoldLabel.text = "SOLD OUT"
        SoldLabel.font = UIFont(name: "SegoeUI-Regular", size: 8.0)
        SoldLabel.textAlignment = .center
        SoldLabel.isHidden = true
        self.contentView.addSubview(SoldLabel)
        
        
        
        
        
        // Initialization code
    }
 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setBannercell(image: [Images], home: HomeData)
    {
        self.images  = image
        if image.count > 0
        {
            label.text = "1/" + String(image.count)
            self.bannerCollection.image = image
        }
        
        if let urgent = home.catdetail!.isUrgent, urgent == 1
        {
            HglLabel.isHidden =  false
            SoldLabel.isHidden = true
           
        }
        
        
        if let sold = home.catdetail!.isSold, sold == 1
        {
          //SoldLabel.isHidden =  sold == 1 ? false : true
            HglLabel.isHidden =  true
            SoldLabel.isHidden = false
        }
        
        
        if home.catdetail!.isVideo == 1{
               
               if let str =  home.catdetail!.videourl
               {
                   self.btnVideo.isHidden = str.isEmpty ? true : false
                   
               }
               }else{
                   self.btnVideo.isHidden = true
               }
        
        
        
        
    }
    
      
      func didPageScrollToIndex(pageIndex: NSInteger) {
        
        self.label?.text = String(pageIndex) + "/" + String(self.images!.count)
          
      }
    
    func didSelectedBannerItem(indexPath: IndexPath) {
        self.bDelegate?.didSelectedBanner(image: self.images!, index: indexPath)
       }

}
