//
//  sellerInfoTableCell.swift
//  Ylaa
//
//  Created by Arun on 12/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

protocol SellerInfoCellDelegate {
    func didPressedMobile(phNum: String)
}

class sellerInfoTableCell: UITableViewCell {
    
    
    var sDelegate:SellerInfoCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnPhone: UIButton!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setSellerInfoCell(titleStr: String, sub: String)
    {
       
        self.lbltitle.text =  titleStr.uppercased()
        if titleStr.uppercased() == "MOBILE"
        {
            btnPhone.isHidden = false
            if Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES {
                let str = "+" + sub
                
                self.lblSubTitle.attributedText = NSAttributedString(string: str, attributes:
                [.underlineStyle: NSUnderlineStyle.single.rawValue])
            }else{
                if !sub.isEmpty
                {
                    let endNum = sub.suffix(4)
                    let str = sub.replacingOccurrences(of: endNum, with: "XXXX")
                    self.lblSubTitle.text =  "+" + str
                    
                    
                }
            }
        }else{
        self.lblSubTitle.text =  sub
        }
        
       

    }
    
    
    @IBAction func btnPressed(_ sender: Any) {
        
        if let delegate  = self.sDelegate
        {
            delegate.didPressedMobile(phNum:lblSubTitle.text!)
        }
        
    }
    
    
}
