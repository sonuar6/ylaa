//
//  descTableCell.swift
//  Ylaa
//
//  Created by Arun on 14/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class descTableCell: UITableViewCell {

//    let array = ["Condition","Year","Kilometers","Doors","Color","Body Conditions","Seller Type","Motors Trim","Type","Cyllinders","Transmision","Region","BHP","Fuel"]
//    let arrayInf = ["New","2014","1,200,00","5 Doors, White","Perfect","Dealer","Alloys","Sedan","6","Automatic","GCC","150 bhp","Gasoline"," "]
    
    
    var specData: [Specifications]?
    
    
    @IBOutlet weak var hgtConst: NSLayoutConstraint!
    @IBOutlet weak var listTable: UITableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.listTable.register(UINib(nibName: "sellerInfoTableCell", bundle: nil), forCellReuseIdentifier: "sellerinfoCell")
        self.listTable.reloadData()


        
        // Initialization code
    }
    
    func setDescCell(specs: [Specifications])
    {
        self.specData = specs
        self.listTable.delegate = self
        self.listTable.dataSource = self
        self.hgtConst?.constant = CGFloat(self.specData!.count) * 57.0
              self.updateConstraints()

              self.layoutIfNeeded()
        self.listTable.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
     func viewWillLayoutSubviews() {

        
//        self.hgtConst?.constant = CGFloat(self.specData!.count) * 57.0
//        self.updateConstraints()
//
//        self.layoutIfNeeded()

        
       


      }

}


extension descTableCell: UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
             return 1
         }
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          
        return self.specData!.count

      }
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
          let cell = tableView.dequeueReusableCell(withIdentifier:"sellerinfoCell",
                                                              for: indexPath) as! sellerInfoTableCell
        
        
        
        if let spData = self.specData{
               let sData = spData[indexPath.row]
               var itemName = ""
               var itemValue = ""
               
                   if let nameStr = sData.item_name
                   {
                       itemName = nameStr
                   }

                   if let valueStr = sData.item_value
                             {
                                 itemValue = valueStr
                             }

        
        cell.setSellerInfoCell(titleStr: itemName, sub: itemValue)
        }
          return cell
      }

      func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
          self.viewWillLayoutSubviews()
      }
}
