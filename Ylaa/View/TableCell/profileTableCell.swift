//
//  profileTableCell.swift
//  Ylaa
//
//  Created by Arun on 20/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit


protocol RegsiterTextDelegate {
    func didMoveScrollForStartEdit(cellTextfield: UITextField)
    func textFieldDidEndEditing(textTag: Int, text: String,textfield: UITextField)
}

class profileTableCell: UITableViewCell,UITextFieldDelegate,UITextViewDelegate{

    @IBOutlet weak var txtViewCmnts: UITextView!
    @IBOutlet weak var txtInput: UITextField!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var dropImgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    var isTxt: Bool = true
    
    
    
    var textDelegate: RegsiterTextDelegate?
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.txtInput.placeholder = ""
        
        // Initialization code
        //self.txtInput.delegate = self

    }
    
    func setProfileCell(titleStr: String,subTitleStr: String, isImgHidden: Bool)
    {
        //self.txtInput.delegate = self

        self.lblTitle.text = titleStr
        self.lblSubTitle.text = subTitleStr
        self.dropImgView.isHidden = isImgHidden

    }
    
    func setTextFieldDelegate(textTag: Int,isText: Bool)
    {
        self.isTxt = isText
        self.txtInput.delegate = self
        self.txtInput.tag = textTag
    }
    
    func setCommentTextFieldDelegate(textTag: Int,isText: Bool)
    {
           self.isTxt = isText
           self.txtViewCmnts.delegate = self
           self.txtViewCmnts.tag = textTag
    }
    
    
    func setTextBusinessFieldDelegate(textTag: Int,isText: Bool,image: UIImage, text: String)
       {
           self.isTxt = isText
           self.txtInput.delegate = self
           self.txtInput.tag = textTag
           self.iconImageView.image = image
           self.txtInput.placeholder = text
       }
    
    func setSupportTextFieldDelegate(textTag: Int,isText: Bool, text: String,isEnabled: Bool)
          {
              self.isTxt = isText
              self.txtInput.delegate = self
              self.txtInput.tag = textTag
              self.txtInput.placeholder = text
             self.txtInput.isUserInteractionEnabled = isEnabled
          }
    
    
    func setRegistrationCell(register: RegisterModel,textTag: Int,selectedMode: String)
    {
        self.txtInput.delegate = self

        if register.titleStr == "By Mobile Number" || register.titleStr == "Business Type" || register.titleStr == "By Email" || register.titleStr == "Agent"
        {
            self.txtInput.isUserInteractionEnabled = false
            self.txtInput.placeholder = selectedMode
             self.txtInput.placeholder = register.titleStr

        }
        else{
            if selectedMode == "By Mobile Number"
            {
                self.iconImageView.image = #imageLiteral(resourceName: "flag_uae")
                self.txtInput.placeholder = "+9715"

            }
            else if selectedMode == "By Email"
                       {
                           self.iconImageView.image = #imageLiteral(resourceName: "email-1")
                           self.txtInput.placeholder = "Email"
                        

                       }
            else{
            self.txtInput.placeholder = register.titleStr
                self.iconImageView.image = register.imageThumb

            }

        }
        self.txtInput.delegate = self
        
        self.txtInput.tag = textTag
        self.dropImgView.isHidden = register.isDropDown!

    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        self.textDelegate?.didMoveScrollForStartEdit(cellTextfield: textField)

        
    }
    
    private func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        

        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.textDelegate?.textFieldDidEndEditing(textTag: textField.tag, text: textField.text!, textfield: textField)
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        self.textDelegate?.textFieldDidEndEditing(textTag: textView.tag, text: textView.text!, textfield: UITextField())
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
        {
            if !isTxt{
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            if (textField == self.txtInput)
            {
             
                      
                      if ((self.txtInput.text?.count)!  > 7 && (isBackSpace != -92))
                      {
                          return false
                          
                      }
                    
                      guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string))
                          
                          
                          else {
                              return false
                      }
            }
            print(self.txtInput.text!.count)
    //        if (self.txtPhone.text!.count <= 4)
    //                        {
    //                            self.txtPhone.text = "+971"
    //
    //                        }
                           
           
            }
            return true

    }
}
