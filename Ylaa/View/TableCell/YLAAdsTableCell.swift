//
//  YLAAdsTableCell.swift
//  Ylaa
//
//  Created by APPLE on 19/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit
protocol MYADSDelegate {
    func didEditPressed()
    func didAdDeleted(isDeleted: Bool)
    func didActionDone(isDone: Bool, msg: String)
    func didAlertAction(mData: HomeData, isDelete: String)


}

class YLAAdsTableCell: UITableViewCell {

    @IBOutlet weak var lblDelete: UILabel!
    @IBOutlet weak var delView: UIView!
    @IBOutlet weak var adImgView: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnLoc: UIButton!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnSold: UIButton!
    @IBOutlet weak var btnDel: UIButton!
    @IBOutlet weak var btnRef: UIButton!
    @IBOutlet weak var btnEdit: UIButton!








    var myAdData: HomeData?
    var fltrStr: String?

    var adDelegate: MYADSDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.adImgView.image = #imageLiteral(resourceName: "placeHolder")
        self.delView.isHidden = true
        self.btnDel.setImage(self.btnDel.currentImage!.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnDel.tintColor = .orangeColor
        
        self.btnRef.setImage(self.btnRef.currentImage!.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnRef.tintColor = .orangeColor
        // Initialization code
        
        
        
        self.btnEdit.isHidden = true
        self.btnPlay.isHidden = true
        self.btnSold.isHidden = true
        self.btnDel.isHidden = true
        self.btnRef.isHidden = true
        
        
    }
    
    
    func setMYAdCellData(adData: HomeData, fstr: String)
    {
        myAdData = adData
        fltrStr = fstr
        
        btnLoc.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        
        self.delView.isHidden = true
        
        
        
        switch self.fltrStr {
        case "Approved":
            self.btnEdit.isHidden = false
            self.btnPlay.isHidden = false
            self.btnSold.isHidden = false
            self.btnDel.isHidden = false
            self.btnRef.isHidden = false
            break
        case "Pending":
            self.btnEdit.isHidden = false
            self.btnPlay.isHidden = true
            self.btnSold.isHidden = true
            self.btnDel.isHidden = false
            self.btnRef.isHidden = true
            break
        case "Decline":
            self.btnEdit.isHidden = false
            self.btnPlay.isHidden = true
            self.btnSold.isHidden = true
            self.btnDel.isHidden = false
            self.btnRef.isHidden = true
            break
        case "Expired":
            self.btnEdit.isHidden = false
            self.btnPlay.isHidden = true
            self.btnSold.isHidden = true
            self.btnDel.isHidden = false
            self.btnRef.isHidden = true
            break
        case "Deleted":
            self.btnEdit.isHidden = false
            self.btnPlay.isHidden = true
            self.btnSold.isHidden = true
            self.btnDel.isHidden = false
            self.btnRef.isHidden = true
            break
        case "Paused":
            self.btnEdit.isHidden = false
            self.btnPlay.isHidden = false
            self.btnSold.isHidden = true
            self.btnDel.isHidden = false
            self.btnRef.isHidden = true
            break
        case "Sold":
            self.btnEdit.isHidden = false
            self.btnPlay.isHidden = true
            self.btnSold.isHidden = false
            self.btnDel.isHidden = false
            self.btnRef.isHidden = true
            break
        default:
            break
        }

        
        self.fltrStr == "Paused" ? self.btnPlay.setTitle("Resume", for: .normal) : self.btnPlay.setTitle("Pause", for: .normal)
            //self.btnPlay.setImage(#imageLiteral(resourceName: "video"), for: .normal) : self.btnPlay.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
        self.fltrStr == "Sold" ? self.btnSold.setTitle("Unsold", for: .normal) : self.btnSold.setTitle("Sold", for: .normal)

        
        self.lblDelete.text = self.fltrStr == "Deleted" ? "Are you sure, do you want to move this ad permanently?" : "Are you sure, do you want to move this ad to Deleted?"
        
     
        
        self.lblDesc.text = adData.catdetail?.title
        self.lblPrice.text  = Utility.setDecimalNumber(valueInt: adData.catdetail!.price!) //String(homeData.catdetail?.price).decima
        self.lblDate.text = adData.submitdate
        //self.btnLoc.setTitle(emirate[1], for: .normal)
        self.adImgView.setImageWithURL(url: adData.catdetail?.image, placeholder: "placeHolder")


    }
    
    
    
    

    @IBAction func deletePressed(_ sender: Any) {
        self.lblDelete.text = self.fltrStr == "Deleted" ? "Are you sure, do you want to move this ad permanently?" : "Are you sure, do you want to move this ad to Deleted?"
        self.delView.isHidden = false

    }
    
    
    @IBAction func actionPressed(_ sender: Any) {
        let btn = sender as! UIButton
        
        
        var opStr = ""
        
        switch btn.tag  {
        case 111:
            self.delView.isHidden = true
            break
        case 112:
            self.adDelegate?.didAlertAction(mData: self.myAdData!, isDelete: "EDIT")
            break
        case 113:
            self.lblDelete.text = self.fltrStr == "Paused" ? YLAMSG.MSG_RESUME : YLAMSG.MSG_PAUSE
            self.delView.isHidden = false
            break
        case 114:
            self.lblDelete.text = YLAMSG.MSG_REFRESH
            self.delView.isHidden = false
            break
        case 115:
            if self.fltrStr == "Sold" {
                self.lblDelete.text = YLAMSG.MSG_UNSOLD
                self.delView.isHidden = false

            }else{
            self.adDelegate?.didAlertAction(mData: self.myAdData!, isDelete: "SOLD")
            }
            break
        case 888:
            if  self.lblDelete.text == YLAMSG.MSG_PAUSE{
                opStr = "pause"
                self.delView.isHidden = true

            }else if  self.lblDelete.text == YLAMSG.MSG_REFRESH{
                opStr = "refresh"
                self.delView.isHidden = true

            }else if  self.lblDelete.text == YLAMSG.MSG_RESUME{
                opStr = "play"
                self.delView.isHidden = true

            }else if  self.lblDelete.text == YLAMSG.MSG_UNSOLD{
                self.adDelegate?.didAlertAction(mData: self.myAdData!, isDelete: "UNSOLD")
                self.delView.isHidden = true
            }else{
            deleteMyAd()
            }

            break
        case 889:
            self.delView.isHidden = true
            break
        default:
            break
        }
        
        if !opStr.isEmpty
        {
            makeActionMyAd(actSt: opStr)
        }
        

    }
    
    
    
    
    
    
      func deleteMyAd()
      {

        self.delView.isHidden = true
        if self.fltrStr == "Deleted"{
                      NetworkManager.myAdDeleteAction(adID: self.myAdData!.ID!, completionHandler:  self.requestDeleteResponse(response: )) { (ERR) in
            
                      }
        }else{
        self.adDelegate?.didAlertAction(mData: self.myAdData!, isDelete: "DELETE")
        }
        
      }
    
    
    
    
    func makeActionMyAd(actSt: String)
    {
        NetworkManager.myAdAction(action: actSt, adID: self.myAdData!.ID!, completionHandler: self.requestResponse(response: )) { (ERR) in
            
        }
    }
    
    func requestDeleteResponse(response: RequestResult){
        if  let jsonObject = response.value as? [String : Any?],let model = UserOTPVerifiedModel.toObject(value: jsonObject) {
            self.delView.isHidden = true

            if model.Status == "Success"
            {
              //self.adDelegate?.didActionDone(isDone: true)
                self.adDelegate?.didAlertAction(mData: self.myAdData!, isDelete: "DELETED")


            }
            else{

            }
        }
    }

    
    func requestResponse(response: RequestResult)
    {
        if  let jsonObject = response.value as? [String : Any?],let model = MakeFavoriteModel.toObject(value: jsonObject) {
            self.delView.isHidden = true

                   if model.result != "1"
                   {
                    self.adDelegate?.didActionDone(isDone: true, msg: model.msg!)

                   }
                   else{
                    self.adDelegate?.didActionDone(isDone: true, msg: model.msg!)
                   }
               }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
