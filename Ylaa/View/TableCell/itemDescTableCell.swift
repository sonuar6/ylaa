//
//  itemDescTableCell.swift
//  Ylaa
//
//  Created by Arun on 01/01/2020.
//  Copyright © 2020 Arun. All rights reserved.
//

import UIKit
import MapKit
class itemDescTableCell: UITableViewCell,MKMapViewDelegate {

    @IBOutlet weak var locMap: MKMapView!
    @IBOutlet weak var lblDesc: UILabel!
    let adLoc = MKPointAnnotation()
    var ad_detail:ADItemDetailData?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblDesc.text = ""
        locMap.delegate = self
        

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setAdDescData(detail:ADItemDetailData)
    {
        
        self.ad_detail = detail
        self.lblDesc.text = detail.description!.getHTMLtoText()

        
        
        guard  let lat =  detail.lat_location else {
          return
        }
        
        guard  let long =  detail.long_location else {
          return
        }
        
        adLoc.coordinate = CLLocationCoordinate2D(latitude: Double(lat)!, longitude:  Double(long)!)
        adLoc.title = detail.location
        locMap.addAnnotation(adLoc)

        self.locMap.setVisibleMapRect(self.locMap.visibleMapRect, edgePadding: UIEdgeInsets(top: 40.0, left: 20.0, bottom: 20, right: 20.0), animated: true)
        let viewRegion = MKCoordinateRegion( center: adLoc.coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
        self.locMap.setRegion(viewRegion, animated: false)


    }
    
    
    
    
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?{
        guard annotation is MKPointAnnotation else { return nil }

//        let identifier = "Annotation"
//        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
//
//
//        if annotationView == nil {
//            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
//            annotationView!.canShowCallout = true
//            annotationView!.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
//        } else {
//            annotationView!.annotation = annotation
//        }
//
//        return annotationView
      
               //Handle ImageAnnotations..
               var view: ImageAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: "imageAnnotation") as? ImageAnnotationView
               if view == nil {
                   view = ImageAnnotationView(annotation: annotation, reuseIdentifier: "imageAnnotation")
               }

//               let annotation = annotation as! ImageAnnotation
               view?.image = #imageLiteral(resourceName: "anot-1")
               view?.canShowCallout = true
               view?.annotation = annotation
               view?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)

               return view
        
        
        
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if Utility.checkForLocationEnabled(){
        if let adDetal = ad_detail{
        let delegate = AppDelegate.sharedAppdelegate()
                Utility.OpenMapWith(sLat: delegate.lattitude, sLon: delegate.longitude, dLat: adDetal.lat_location!, dLon: adDetal.long_location!, isWalk: false, titleStr: adDetal.location!)
        }
        }else{
            
        }
        
        
        

    }

}



class ImageAnnotationView: MKAnnotationView {
    private var imageView: UIImageView!

    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)

        self.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        self.imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        self.addSubview(self.imageView)

        self.imageView.layer.cornerRadius = 5.0
        self.imageView.layer.masksToBounds = true
    }

    override var image: UIImage? {
        get {
            return self.imageView.image
        }

        set {
            self.imageView.image = newValue
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
