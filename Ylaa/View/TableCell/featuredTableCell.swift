//
//  featuredTableCell.swift
//  Ylaa
//
//  Created by Arun on 09/02/2020.
//  Copyright © 2020 Arun. All rights reserved.
//

import UIKit

protocol FeaturedDelegate {
    func didFeaturedPressed(adData: HomeData)
    func didPushUsertoLogin()
    func didSellerAdPressed(homeData: HomeData)
    func didVideoPlay(homeData: HomeData)



}


class featuredTableCell: UITableViewCell,YLACollectionDelegate {
    func didVideoPressed(hData: HomeData) {
       self.featureDelegate?.didVideoPlay(homeData: hData)

    }
    
    func didFavoriteDone(done: Bool) {
        
    }
    
    
    
    func movetoSellerAds(home: HomeData) {
        self.featureDelegate?.didSellerAdPressed(homeData: home)
    }
    
    func userLogin() {
        self.featureDelegate?.didPushUsertoLogin()
    }
    
   
    
    
    var featureDelegate: FeaturedDelegate?
    
    
    
    func didSelectedItem(adData: HomeData) {
        self.featureDelegate?.didFeaturedPressed(adData: adData)
    }
    
    func didLoadMoreItems(offset: Int) {
        
    }
    

    @IBOutlet weak var featuredCollection: YLACollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.featuredCollection.initCollection(rowCount: 2, collection: .featured, scrollingDirection: .horizontal)
             self.featuredCollection.ylaDelegate = self
             self.featuredCollection.reloadData()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
//    deinit{
//        self.featuredCollection.timer?.invalidate()
//         self.featuredCollection = nil
//          print("Deinit Called")
//      }

}
