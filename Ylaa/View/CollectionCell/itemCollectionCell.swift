//
//  itemCollectionCell.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit


protocol ADCellDelegate:class {
    func didFavoriteRequestDone(isDone: Bool)
    func pushUserToLogin()
    func didLogoPressed(home:HomeData)
    func didPlayVideo(home:HomeData)

}

class itemCollectionCell: UICollectionViewCell {
    
    
    
    
    
    @IBOutlet weak var autoView: UIView!
    @IBOutlet weak var rEView: UIView!
    
    
    @IBOutlet weak var btnYear: UIButton!
    @IBOutlet weak var btnKm: UIButton!
    @IBOutlet weak var btnRoom: UIButton!
    @IBOutlet weak var btnKtchn: UIButton!



    
    
    
    
    
    @IBOutlet weak var aedLabel: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblTag: UILabel!
    @IBOutlet weak var adImageView: UIImageView!
    @IBOutlet weak var btnDate: UIButton!
    @IBOutlet weak var btnLoc: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var btnOptn: UIButton!
    @IBOutlet weak var hgltView: UIView!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var lblPriceInfo: UILabel!
    @IBOutlet weak var logoImgView: UIImageView!
    
    @IBOutlet weak var SoldLabel:UILabel!
    
    
    var homData: HomeData?
    weak var adDelegate :ADCellDelegate?
    var tempButton: UIButton?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        
//        SoldLabel.frame = CGRect(x: self.adImageView.frame.width/2 - 40, y: self.adImageView.frame.height/2 - 10 , width: 90, height: 25)
        SoldLabel.transform = CGAffineTransform(rotationAngle: -.pi / 4)
        SoldLabel.layer.borderWidth = 1.0
        SoldLabel.borderColor = UIColor.white
        SoldLabel.textColor = .white
        SoldLabel.layer.cornerRadius = 1
        SoldLabel.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        SoldLabel.clipsToBounds = true
        SoldLabel.isHidden = true
              
        
        
        
        
        
        
        
        self.rEView.isHidden = true
        self.autoView.isHidden = true


        
        priceView.isHidden = true
        lblPriceInfo.text = ""
        
        countView.isHidden = true
        lblCount.text = ""

        
        hgltView.isHidden = true
        lblTag.text = ""
        bgView.backgroundColor = .clear
        self.backgroundColor = .customBlackColor


        // Initialization code
    }
    
    @IBAction func playPressed(_ sender: Any) {
        if let hData = self.homData{
        self.adDelegate?.didPlayVideo(home: hData)
        }
        
    }
    
    func setItemCellData(isFavorite: Bool,homeData: HomeData)
    {
        homData = homeData
        homeData.catdetail?.isFavorite == 1 ?  btnOptn.setImage(#imageLiteral(resourceName: "heartfill"), for: .normal) :  btnOptn.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
        self.titleLabel.text = homeData.catdetail?.title
        if let str = homeData.catdetail?.price{
            if str > 0 && self.homData?.catdetail?.ptype != "Call for Price"
            {
                self.rateLabel.text  =   Utility.setDecimalNumber(valueInt: str) //String(str)
                self.aedLabel.text = "AED"

            }else{
                 self.rateLabel.text  =  "Call for Price"
                 self.aedLabel.text = ""
            }
            
            //Utility.setDecimalNumber(valueInt: homeData.catdetail!.price!)
        }//String(homeData.catdetail?.price).decima
        self.btnDate.setTitle(homeData.submitdate, for: .normal)
       
        
       
        

        
        
        
        
        btnLoc.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        btnKm.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left

        btnKtchn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        btnRoom.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left


        
        
        
        if let str =  homeData.catdetail!.emerate
        {
            self.btnLoc.setTitle(" " + str, for: .normal)
        }
        
        
        if let imageCount = homeData.catdetail?.totalImages{
            if imageCount > 0
            {
                self.countView.isHidden = false
                self.lblCount.text = String(imageCount)
            }else{
                self.countView.isHidden = true
            }
        }
        
        
        
        if homeData.catdetail!.isVideo == 1{
        
        if let str =  homeData.catdetail!.videourl
        {
            self.playBtn.isHidden = str.isEmpty ? true : false
            
        }
        }else{
            self.playBtn.isHidden = true
        }
        
        
        if let sold = self.homData?.catdetail?.isSold, sold == 1
        {
//            hgltView.isHidden = false
//            lblTag.text = "SOLD OUT"
            SoldLabel.isHidden = false
        }
        
        if let urgent = self.homData?.catdetail?.isUrgent, urgent == 1
        {
                hgltView.isHidden = false
                lblTag.text = "URGENT"
        }
        
        if let hglt = self.homData?.catdetail?.isAdHighLight
               {
                   if hglt == 1
                   {
                    bgView.backgroundColor = .clear
                    self.backgroundColor = .subTitleColor

                   }
                   else{
                    bgView.backgroundColor = .clear
                    self.backgroundColor = .customBlackColor


                }
                   
               }
        
       
        
        if let sold = self.homData?.catdetail?.ptype{
            if sold.isEmpty || sold == "Call for Price"{
                priceView.isHidden = true
                lblPriceInfo.text = ""
            }else{
            self.priceView.isHidden = false
            self.lblPriceInfo.text = sold
            }
        }else{
            priceView.isHidden = true
            lblPriceInfo.text = ""
        }

              
        
        
        
        
        
        self.adImageView.image = nil
        self.logoImgView.image = nil
        if homeData.catdetail?.seller_type == "biz"{
        if let url = homeData.logo{
            self.logoImgView.setImageForYlaaUser(url: url, placeHolder: "placeHolder")

        }
        }
        
        self.adImageView.setImageWithURL(url: homeData.catdetail?.image, placeholder: "placeHolder")
        
        setAutoView(homeData: homeData)
        setRealEstateView(homeData: homeData)

        
      
    }
    
    
    func setRealEstateView(homeData: HomeData)
       {
           guard let bStr =  homeData.catdetail!.noOfBedRooms else{
               self.rEView.isHidden = true
               return
           }
                                    
           guard let tStr =  homeData.catdetail!.noOfBathRooms else{
               self.rEView.isHidden = true
               return
           }
                        

           if !bStr.isEmpty || !tStr.isEmpty{
               self.rEView.isHidden = false
            btnRoom.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left

               self.btnRoom.setTitle("  " + bStr, for: .normal)
               self.btnKtchn.setTitle("  " + tStr, for: .normal)
               }else{
                   self.rEView.isHidden = true
               }
       }
    
    
    
    
    
    
    func setAutoView(homeData: HomeData)
    {
        
      

        guard let yStr =  homeData.catdetail!.year else{
            self.autoView.isHidden = true
            return
        }
                                 
        guard let kStr =  homeData.catdetail!.mileage else{
            self.autoView.isHidden = true
            return
        }
                     

        if !kStr.isEmpty || !yStr.isEmpty{
            self.autoView.isHidden = false
            btnYear.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left

            self.btnYear.setTitle(" " + yStr , for: .normal)
            self.btnKm.setTitle(" " + kStr + "Km", for: .normal)
            }else{
                self.autoView.isHidden = true
            }
    }
    
    
    @IBAction func logoPressed(_ sender: Any) {
        if let home = self.homData{
            if home.catdetail?.seller_type == "biz"{

            self.adDelegate!.didLogoPressed(home: home)
            }
        }
    }
    
    
    
    
    
    @IBAction func favPressed(_ sender: Any) {
        
         
        if Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES{
        let btn = sender as! UIButton
        tempButton = btn
        var isfav:Bool?
        if btn.currentImage == #imageLiteral(resourceName: "heart")
        {
            isfav = true
            btn.setImage(#imageLiteral(resourceName: "heartfill"), for: .normal)
        }
        else{
            isfav = false
            btn.setImage(#imageLiteral(resourceName: "heart"), for: .normal)

        }
            tempButton = btn

            self.sendfavorite(isFav: isfav!)
            
            
        }
        else{
            self.adDelegate!.pushUserToLogin()
        }
        
    }
    
    
    
    
    func sendfavorite(isFav:Bool)
    {
            NetworkManager.add_to_favorite(isAdFav:isFav ,adID: self.homData!.ID!, completionHandler: self.favoriteResponse(response: ), errorHandler: { (ERR) in
                
            })
        
    }
    
    
    
    
    func favoriteResponse(response: RequestResult)
    {
        if  let jsonObject = response.value as? [String : Any?],let model = MakeFavoriteModel.toObject(value: jsonObject) {
            if model.result != "1"
            {
                let image = tempButton?.currentImage == #imageLiteral(resourceName: "heart") ? #imageLiteral(resourceName: "heartfill") : #imageLiteral(resourceName: "heart")
                tempButton?.setImage(image, for: .normal)
            }
            else{
                self.adDelegate?.didFavoriteRequestDone(isDone: true)
            }
        }

    }
    
    
    @IBAction func segmentPressed(_ sender: Any) {
    }
    

}
