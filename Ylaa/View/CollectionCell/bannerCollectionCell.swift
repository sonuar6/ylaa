//
//  bannerCollectionCell.swift
//  Ylaa
//
//  Created by Arun on 10/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class bannerCollectionCell: UICollectionViewCell {

    @IBOutlet weak var bannerImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setBannerImage(image: Images)
    {
        self.bannerImageView.setImageWithURL(url: image.ad_img, placeholder: "no_pic")
    }

}
