//
//  ImageZoomCell.swift
//  Ylaa
//
//  Created by APPLE on 22/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class ImageZoomCell: UICollectionViewCell,UIGestureRecognizerDelegate {

    @IBOutlet weak var collImageView: UIImageView!
    
       var isZooming = false
       var originalImageCenter:CGPoint?
       var onZoomFinished: ((_ isFinished:Bool)->())?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setZoom()

        // Initialization code
    }
    
    func setZoom()
          {

           
               self.collImageView.isUserInteractionEnabled = true
           
               
                 let pinch = UIPinchGestureRecognizer(target: self, action: #selector(self.pinch(sender:)))
                 pinch.delegate = self
                 self.collImageView.addGestureRecognizer(pinch)
                 
                 let pan = UIPanGestureRecognizer(target: self, action: #selector(self.pan(sender:)))
                 pan.delegate = self
                 self.collImageView.addGestureRecognizer(pan)
           
         

          }
    
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
                return true
            }
       
       @objc func pinch(sender:UIPinchGestureRecognizer) {
           
           if sender.state == .began {
               let currentScale = self.collImageView.frame.size.width / self.collImageView.bounds.size.width
               let newScale = currentScale*sender.scale
               
               if newScale > 1 {
                   self.isZooming = true
               }
           } else if sender.state == .changed {
               
               guard let view = sender.view else {return}
               
               let pinchCenter = CGPoint(x: sender.location(in: view).x - view.bounds.midX,
                                         y: sender.location(in: view).y - view.bounds.midY)
               let transform = view.transform.translatedBy(x: pinchCenter.x, y: pinchCenter.y)
                   .scaledBy(x: sender.scale, y: sender.scale)
                   .translatedBy(x: -pinchCenter.x, y: -pinchCenter.y)
               
               let currentScale = self.collImageView.frame.size.width / self.collImageView.bounds.size.width
               var newScale = currentScale*sender.scale
               
               if newScale < 1 {
                   newScale = 1
                   let transform = CGAffineTransform(scaleX: newScale, y: newScale)
                   self.collImageView.transform = transform
                   sender.scale = 1
               }else {
                   view.transform = transform
                   sender.scale = 1
               }
               
           } else if sender.state == .ended || sender.state == .failed || sender.state == .cancelled {
               
               guard let center = self.originalImageCenter else {return}
               
               UIView.animate(withDuration: 0.3, animations: {
                   self.collImageView.transform = CGAffineTransform.identity
                   self.collImageView.center = center
               }, completion: { _ in
                   self.isZooming = false
                   self.onZoomFinished!(true)

               })
           }
           
       }
       
       @objc func pan(sender: UIPanGestureRecognizer) {
                 if self.isZooming && sender.state == .began {
                   self.onZoomFinished!(false)

                     self.originalImageCenter = sender.view?.center
                 } else if self.isZooming && sender.state == .changed {
                   
                     let translation = sender.translation(in: self)
                     if let view = sender.view {
                         view.center = CGPoint(x:view.center.x + translation.x,
                                               y:view.center.y + translation.y)
                     }
                     sender.setTranslation(CGPoint.zero, in: self.collImageView.superview)
                   
                   if sender.state == .ended{
                       self.onZoomFinished!(true)
                   }
                   
                 }
             }
       

}
