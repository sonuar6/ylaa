//
//  filterCollectionCell.swift
//  Ylaa
//
//  Created by Arun on 07/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit
protocol FilterDelegate {
    func didFilterClosePressed()
}

class filterCollectionCell: UICollectionViewCell{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
    var delegate: FilterDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnClose.isHidden = true
        // Initialization code
    }
    
    func setFilterCell(titleStr: String)
    {
        self.lblTitle.text = titleStr
    }

    @IBAction func closePressed(_ sender: Any) {
        
        self.delegate?.didFilterClosePressed()
    }
}
