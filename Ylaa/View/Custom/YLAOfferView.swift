//
//  YLAOfferView.swift
//  Ylaa
//
//  Created by Arun on 11/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

enum offView
{
    case Offer,Phone
}
protocol YLAOfferDelegate {
    func cancelView()
}


class YLAOfferView: UIView {
    
     @IBOutlet weak var txtOffer: UITextField!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var iconImgView: UIImageView!
    @IBOutlet weak var btnWhatsApp: UIButton!
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblCaption: UILabel!
    @IBOutlet weak var ofView: UIView!
    
    var phoneNumber: String?
    var price_tag: String = ""

    
    var adData: ADItemDetailData?{
                      didSet {
                       guard let hData = adData  else { return }
                      
                      }
                  }

    
    var ofDelegate: YLAOfferDelegate?
    
    
    @IBAction func cancelPressed(_ sender: Any) {
        self.endEditing(true)
        self.ofDelegate?.cancelView()
    }
    
    
    
    func initFor(type: offView, caption: String,titleStr: String,action: String,priceType: String)
    {
        self.lblCaption.text = caption
        self.price_tag = priceType.isEmpty ? "none" : priceType
        self.dismissKeyboard()

        switch type {
        case .Offer:
            self.btnWhatsApp.isHidden = true
            self.ofView.isHidden = false
            self.iconImgView.image = #imageLiteral(resourceName: "Group 1573")

            self.lblTitle.text = titleStr
            self.btnSubmit.setTitle(action, for: .normal)
            self.btnSubmit.setImage(nil, for: .normal)


            break
        case .Phone:
           
          
            
            self.phoneNumber = titleStr.hasPrefix("+") ?  titleStr : ("+" + titleStr)
            self.iconImgView.image = #imageLiteral(resourceName: "Group 1664")
            self.btnWhatsApp.isHidden = false
            self.ofView.isHidden = true
            self.lblTitle.text = self.phoneNumber
            self.btnSubmit.setTitle(action, for: .normal)
            self.btnSubmit.setImage(#imageLiteral(resourceName: "Group 1209"), for: .normal)
            break
      
        }
    }
    
    
    @IBAction func actionPressed(_ sender: Any) {
        self.endEditing(true)
        
        let btn = sender as! UIButton
        var phoneNumber =  ""
        if let number   = self.phoneNumber{
         phoneNumber =  "+\(number)"
        }// you need to change this number

        if btn.tag == 555
        {
                   
                   let appURL = URL(string: "https://api.whatsapp.com/send?phone=\(phoneNumber)")!
                   if UIApplication.shared.canOpenURL(appURL) {
                        UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
                   } else {
                      let webURL = URL(string: "https://wa.me/\(phoneNumber)")!
                       if UIApplication.shared.canOpenURL(webURL) {
                            UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
                       }
                       else{
                           print("error in phone number")
                       }
                   }
        }
        else{
            print(btn.currentTitle)
            if btn.currentTitle  == " Call"
            { if let number   = self.phoneNumber{
                if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                     }
                }
            }else{
                if !self.txtOffer.text!.isEmpty
                {
                    self.sendOffer()
                }
            }
        }
        
        
    }
    
    
    
    
    
    
    
    func sendOffer()
    {
        
        if let detail = self.adData{
        
            print(self.txtOffer.text!)
            print(detail.seller_id!)
            print(detail.id!)


            
            NetworkManager.sendOfferToSeller(status: self.price_tag, rate: self.txtOffer.text!, sendTo: detail.seller_id!, aID: Int(detail.id!)!, msg: "Hi Please see my offer", completionHandler: self.offerResponse(response:)) { (ERR) in
            
        }
        }
                
    }
    
    
    func offerResponse(response: RequestResult)
    {
        if  let jsonObject = response.value as? [String : Any?],let model = UserOTPVerifiedModel.toObject(value: jsonObject) {

                   if model.Status == "Success"
                   {
                    self.ofDelegate?.cancelView()

                   }
                   else{
                    self.ofDelegate?.cancelView()

                   }
               }
    }
    
    
    
    
    
}
