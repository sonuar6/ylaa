//
//  SegmentedView.swift
//  Ylaa
//
//  Created by Arun on 16/03/2020.
//  Copyright © 2020 Arun. All rights reserved.
//

import UIKit

protocol SegmentedDelegate:class {
    func didSelectedAdType(type: HomeTask)

}



class SegmentedView: UIView {

   var tempButton:UIButton?
   weak var sDelegate: SegmentedDelegate?

    
    
    func setTempBtn()
    {
        self.tempButton = self.viewWithTag(111) as? UIButton
    }
    
    
    
    
    @IBAction func btnPressed(_ sender: Any) {
        
        
        if Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String != YLASTATUS.YES{
            self.sDelegate?.didSelectedAdType(type: .none)
            return
        }

        
       // tempButton?.setTitleColor(UIColor.subTitleColor, for: .normal)
        tempButton?.titleLabel?.font = UIFont(name: (tempButton?.titleLabel?.font.fontName)!, size: CGFloat(15.0))
        let btn = sender as! UIButton
        //btn.setTitleColor(UIColor.orangeColor, for: .normal)
        btn.titleLabel?.font = UIFont(name: (btn.titleLabel?.font.fontName)!, size: (btn.titleLabel?.font.pointSize)! +  CGFloat(2.0))

        tempButton = btn
        
        
        switch btn.tag {
        case 111:
            self.sDelegate?.didSelectedAdType(type: .recent)
            break
        case 112:
            self.sDelegate?.didSelectedAdType(type: .viewed)
            break
        case 113:
            self.sDelegate?.didSelectedAdType(type: .favorite)
            break
        default:
            break
        }
        

    }
    

}
