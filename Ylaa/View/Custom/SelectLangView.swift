//
//  SelectLangView.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class SelectLangView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var onComplete: ((_ isFinished:Bool)->())? //an optional function

    
    
    @IBAction func btnPressed(_ sender: Any) {
        let btn = sender as! UIButton
        
        Utility.resetPlistValueForKey(key: YLA.ISLANSEL, value: YLASTATUS.YES)
        
        switch btn.tag {
        case 111:
            Utility.resetPlistValueForKey(key: YLA.CURLANG, value: LANG.ENG)
            break
        case 112:
            Utility.resetPlistValueForKey(key: YLA.CURLANG, value: LANG.ARB)
            break
        default:
            break
        }
        self.onComplete!(true)
        
    }
    
    

}
