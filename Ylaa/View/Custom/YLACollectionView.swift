//
//  YLACollectionView.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

enum Collection {
    case banner,Home,Filter,ZoomImage,featured,Classic
}

enum ScrollingDirection {
    case horizontal,vertical
}
enum CollectionType {
    case List,Column
}


protocol FilterCollectionDelegate {
    func closePressed(catItem: CategoryItemData)
}
protocol YLACollectionDelegate {
    func didSelectedItem(adData: HomeData)
    func didLoadMoreItems(offset: Int)
    func userLogin()
    func movetoSellerAds(home:HomeData)
    func didFavoriteDone(done: Bool)
    func didVideoPressed(hData: HomeData)

}
protocol YLABannerDelegate {
    func didSelectedBannerItem(indexPath: IndexPath)
    func didPageScrollToIndex(pageIndex: NSInteger)

}


class YLACollectionView: UICollectionView,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,FilterDelegate ,ADCellDelegate{
    
    func didPlayVideo(home: HomeData) {
        self.ylaDelegate!.didVideoPressed(hData: home)
    }
    

    
    //MARK: ADCellDelegate
    
    
    
    
    
    func didFavoriteRequestDone(result: RequestResult) {
        
    }
    
    func pushUserToLogin() {
        self.ylaDelegate?.userLogin()
    }
    
    func didLogoPressed(home: HomeData) {
        self.ylaDelegate!.movetoSellerAds(home: home)
    }
    
  
    
    override func layoutSubviews() {
           super.layoutSubviews()
           if bounds.size != intrinsicContentSize() {
               invalidateIntrinsicContentSize()
           }
       }

        func intrinsicContentSize() -> CGSize {
           return self.contentSize
       }
    
    
    
    var collection_Type: CollectionType?{
        didSet {
         guard let colType = collection_Type  else { return }
            if colType == .List {
                 self.register(UINib(nibName: "YLAItemListCell", bundle: nil), forCellWithReuseIdentifier: "itemCollectionListCell")
                 self.itemsPerRow = 1
            }  else
            {
                self.register(UINib(nibName: "itemCollectionCell", bundle: nil), forCellWithReuseIdentifier: "itemCollectionCells")
                 self.itemsPerRow = 2
            }
         self.reloadData()
        }
    }
    var homeData: [HomeData]?{
                   didSet {
                    guard let hData = homeData  else { return }
                    self.ItemsCount = hData.count
                    self.reloadData()
                   }
               }
    
    
    var cellColor: UIColor?
    var image: [Images]?{
                 didSet {
                  guard let fData = image  else { return }
                  self.ItemsCount = fData.count
                  self.reloadData()
                 }
             }
      
    
       var filter: [CategoryItemData]?{
              didSet {
               guard let fData = filter  else { return }
               self.ItemsCount = fData.count
               self.reloadData()
              }
          }

    
    
       fileprivate var scrollDirect: ScrollingDirection = .horizontal
       fileprivate var itemsPerRow: CGFloat = 3
       fileprivate var ItemsCount: NSInteger = 0
       fileprivate var move:NSInteger = 0
       var collRawval: Collection = .banner
       var fDelegate: FilterCollectionDelegate?
       var bDelegate: YLABannerDelegate?
       var ylaDelegate:YLACollectionDelegate?
       var timer:Timer?
     
      
    @objc func scrollToNextCell(){

         if self.move < self.ItemsCount {
           let indexPath = IndexPath(item: move, section: 0)
           self.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
           self.move = self.move + 1
         }else{
            if self.ItemsCount > 0{
           self.move = 0
           self.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
            }
         }


    }
    
    
    
    func initCollection(rowCount: NSInteger, collection: Collection, scrollingDirection: ScrollingDirection)
       {
             self.itemsPerRow = CGFloat(rowCount)
             self.collRawval = collection
             self.scrollDirect = scrollingDirection
        
        switch collection {
                 case .banner:
                    self.register(UINib(nibName: "bannerCollectionCell", bundle: nil), forCellWithReuseIdentifier: "bannerCollectionCells")
                    break
                        
                 case .Home:
                    self.collection_Type = .Column
                       self.register(UINib(nibName: "itemCollectionCell", bundle: nil), forCellWithReuseIdentifier: "itemCollectionCells") 
                        break
                 case .Filter:
                        self.register(UINib(nibName: "filterCollectionCell", bundle: nil), forCellWithReuseIdentifier: "filterCollectionCells")
                       break
            
                 case .ZoomImage:
                       self.register(UINib(nibName: "ImageZoomCell", bundle: nil), forCellWithReuseIdentifier: "ImageZoomCells")

                       break
              
                 case .featured:
                    self.collection_Type = .Column

                          self.register(UINib(nibName: "itemCollectionCell", bundle: nil), forCellWithReuseIdentifier: "itemCollectionCells")
                    timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.scrollToNextCell), userInfo: nil, repeats: true)

                                   break
            
                case .Classic:
                    self.collection_Type = .Column
                          self.register(UINib(nibName: "itemCollectionCell", bundle: nil), forCellWithReuseIdentifier: "itemCollectionCells")
                          break
        }
        self.delegate = self
        self.dataSource = self
        self.reloadData()
     }
    
    func resetCollection(cell: itemCollectionCell)
    {
        cell.SoldLabel.isHidden = true
        cell.playBtn.isHidden = true
        cell.priceView.isHidden = true
        cell.lblPriceInfo.text = ""
        cell.countView.isHidden = true
        cell.lblCount.text = ""
        cell.hgltView.isHidden = true
        cell.lblTag.text = ""
        cell.bgView.backgroundColor = .customBlackColor
        cell.backgroundColor = .customBlackColor
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         let count  =  ItemsCount
         return count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.collRawval == .Home || self.collRawval == .featured{
            if self.collection_Type == .List{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"itemCollectionListCell",for: indexPath)as! itemCollectionCell
                self.resetCollection(cell: cell)
                cell.adDelegate = self
                cell.setItemCellData(isFavorite: true, homeData: homeData![indexPath.row])
                return cell
            }
            else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"itemCollectionCells",for: indexPath)as! itemCollectionCell
                self.resetCollection(cell: cell)
                cell.adDelegate = self
                cell.setItemCellData(isFavorite: true, homeData: homeData![indexPath.row])
            return cell
            }
        }
        else if self.collRawval == .banner
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"bannerCollectionCells",for: indexPath)as! bannerCollectionCell
            cell.setBannerImage(image: self.image![indexPath.row])
            return cell
        }
        else if self.collRawval == .ZoomImage{
                   let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"ImageZoomCells",for: indexPath)as! ImageZoomCell
                   
                   cell.onZoomFinished = { finished in
                       
                       self.isScrollEnabled = finished == true ? true: false
                   }
            
             let str = self.image![indexPath.row]
            cell.collImageView.setImageWithURL(url: str.ad_img, placeholder: "placeHolder")


                   return cell
               }
        else if self.collRawval == .Filter
               {
                   let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"filterCollectionCells",for: indexPath)as! filterCollectionCell
                cell.delegate = self
                cell.setFilterCell(titleStr: filter![indexPath.row].cat_name!)
                cell.btnClose.isHidden =  indexPath.row == filter!.count - 1 ? false : true
                   return cell


               }
        else if self.collRawval == .Classic
        {
//            if indexPath.row == 0{
//                  let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"emptyCell",for: indexPath)
//                return cell
//
//            }else{
                  let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"itemCollectionCells",for: indexPath)as! itemCollectionCell
                cell.adDelegate = self
                cell.backgroundColor = .red
                cell.setItemCellData(isFavorite: true, homeData: homeData![indexPath.row ] )
                return cell

            //}
        }

       
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"itemCollectionCells",for: indexPath)as! itemCollectionCell
            return cell


        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                          layout collectionViewLayout: UICollectionViewLayout,
                          sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.collRawval == .ZoomImage{
            return CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)

        }
        if self.collRawval == .banner{
            return CGSize(width: UIScreen.main.bounds.size.width, height: collectionView.bounds.size.height)

        }else{
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
                         let totalSpace = flowLayout.sectionInset.left
                             + flowLayout.sectionInset.right
                             + (flowLayout.minimumInteritemSpacing * CGFloat(itemsPerRow - 1))
                         let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(itemsPerRow))
            var  hgt = 0
            
            if self.collRawval == .Home || self.collRawval == .Classic || self.collRawval == .featured {
                switch self.collection_Type {
                case .Column:
                   // let hdata = self.homeData![indexPath.row]
                   
                    hgt = 236//216

//                    guard let year = hdata.catdetail?.year, year.isEmpty else {
//                        return CGSize(width: size, height: hgt)
//                    }
//                    guard let mlg = hdata.catdetail?.mileage, mlg.isEmpty else {
//                        return CGSize(width: size, height: hgt)
//                    }
//                    guard let room = hdata.catdetail?.noOfBedRooms, room.isEmpty else {
//                       return CGSize(width: size, height: hgt)
//                    }
//                    guard let broom = hdata.catdetail?.noOfBathRooms, broom.isEmpty else {
//                        return CGSize(width: size, height: hgt)
//                    }
//
//
//
//                    if !year.isEmpty || !mlg.isEmpty || room.isEmpty || !broom.isEmpty{
//                        hgt = 236
//                    }
//
                    break
                case .List:
                    
                    //let hdata = self.homeData![indexPath.row]
                    hgt = 118 //108

//                    guard let year = hdata.catdetail?.year, year.isEmpty else {
//                          return CGSize(width: size, height: hgt)
//                    }
//                    guard let mlg = hdata.catdetail?.mileage, mlg.isEmpty else {
//                          return CGSize(width: size, height: hgt)
//                    }
//                    guard let room = hdata.catdetail?.noOfBedRooms, room.isEmpty else {
//                          return CGSize(width: size, height: hgt)
//                    }
//                    guard let broom = hdata.catdetail?.noOfBathRooms, broom.isEmpty else {
//                          return CGSize(width: size, height: hgt)
//                    }
//
//                    if !year.isEmpty || !mlg.isEmpty || room.isEmpty || !broom.isEmpty{
//                            hgt = 138
//                    }
//
                 
                    break
                    
                    
                case .none:
                    break
                }
                if self.collRawval == .featured{
                    hgt = 216

                }
              
                
            }else {
                hgt = 30
                
            }
        
           return CGSize(width: size, height: hgt)
        }
    }
 
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.collRawval == .banner{
            self.bDelegate?.didSelectedBannerItem(indexPath: indexPath)
        }
        else if self.collRawval != .Filter{
            if self.collRawval == .Classic{
              
                let aData = self.homeData![indexPath.row]
                self.ylaDelegate?.didSelectedItem(adData: aData)
            }else if self.collRawval != .ZoomImage{
             let aData = self.homeData![indexPath.row]
                self.ylaDelegate?.didSelectedItem(adData: aData)}
        }
    }
    
    
    
      func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
         // let scrollVelocity = collectionView.panGestureRecognizer.velocity(in: collectionView.superview)
      
          
          let lastSectionIndex = collectionView.numberOfSections - 1
                let lastRowIndex = collectionView.numberOfItems(inSection: lastSectionIndex) - 1
           
                if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
                  
                    //self.ylaDelegate?.didLoadMoreItems(offset: lastRowIndex + 1)
          
              }
    
              
      }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if(self.contentOffset.y >= (self.contentSize.height - self.bounds.size.height)) {
        self.ylaDelegate?.didLoadMoreItems(offset: 0)

    }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
           if self.collRawval == .banner {
           let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
           self.bDelegate!.didPageScrollToIndex(pageIndex:NSInteger(pageNumber + 1))
           }


       }
    
    func didFavoriteRequestDone(isDone: Bool) {
        self.ylaDelegate?.didFavoriteDone(done: isDone)
    }
    
    
    
    func didFilterClosePressed() {
        if let fltr = self.filter
        {
            let fData = fltr.last
            self.filter?.remove(at: (fltr.count - 1))
             self.reloadData()
            self.fDelegate?.closePressed(catItem:fData!)
        }
       
      }

}
