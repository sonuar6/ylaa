//
//  YLASellerInfoView.swift
//  Ylaa
//
//  Created by Arun on 12/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit
protocol SellerDelegate {
    func cancelView()
    func didSelectSellerPost()
    func didCallSellerMobile(phNum: String)

}
class YLASellerInfoView: UIView {

   
    
    var sDelegate: SellerDelegate?
    var detailInfo: ADItemDetailData?
    var sInfo: SellerInfo?
    var sellerData:[HomeData]?


    
    @IBOutlet weak var hgtConst: NSLayoutConstraint!
    @IBOutlet weak var thumbImgView: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var listTable: UITableView!
    
    @IBOutlet weak var slrCollection: YLACollectionView!

    @IBOutlet weak var btnInfo: UIButton!
    @IBOutlet weak var btnAds: UIButton!
    
    @IBOutlet weak var slrView: UIView!
    @IBOutlet weak var infoView: UIView!



    
    

    
    
    
    @IBAction func btnPressed(_ sender: Any) {
        self.sDelegate?.cancelView()
    }
    
    
    @IBAction func listPressed(_ sender: Any) {
        
        let btn = sender as! UIButton
        if btn == btnInfo
        {
          slrCollection.isHidden  = true
            self.btnInfo.setTitleColor(.ylaaColor, for: .normal)
            self.infoView.backgroundColor = .ylaaColor
            
            self.btnAds.setTitleColor(.white, for: .normal)
            self.slrView.backgroundColor = .white
                   
            
            

        }else{
            slrCollection.isHidden  = false
            
            self.btnAds.setTitleColor(.ylaaColor, for: .normal)
            self.slrView.backgroundColor = .ylaaColor
                       
            self.btnInfo.setTitleColor(.white, for: .normal)
            self.infoView.backgroundColor = .white
            
            NetworkManager.getSellerAds(sellerID: (self.detailInfo?.seller_id)!, completionHandler: self.sellerAdsResponse(response:)) { (ERR) in
                
            }

        }
        
        
        


    }
    
    
    func sellerAdsResponse(response: RequestResult)
    {
        if  let jsonObject = response.value as? [String : Any?],let model = HomeModel.toObject(value: jsonObject) {
                   
            guard let home = model.homeData else{return}
            self.slrCollection.collection_Type = .List
            self.slrCollection.homeData = home
        }
    }
    
    
    
    
    
     func viewWillLayoutSubviews() {
        self.hgtConst?.constant = CGFloat( self.sInfo!.seller_info!.count) * 57.0
            //self.listTable.contentSize.height
    }
    func setInitSellerInfo(detailData: ADItemDetailData)
    {
        self.detailInfo = detailData
        
        self.listTable.register(UINib(nibName: "sellerInfoTableCell", bundle: nil), forCellReuseIdentifier: "sellerinfoCell")
        self.listTable.delegate = self
        self.listTable.dataSource = self
        slrCollection.initCollection(rowCount: 1, collection: .Home, scrollingDirection: .vertical)
        slrCollection.ylaDelegate = self
        slrCollection.isHidden  = true
        if let sID = self.detailInfo!.seller_id{
        NetworkManager.getSellerInfo(sellerID: sID, completionHandler: self.sellerResponse(response :)) { (ERR) in
            
        }
        
        }
        

    }
    
    func sellerResponse(response : RequestResult)
    {
        if  let jsonObject = response.value as? [String : Any?],let model = SellerInfo.toObject(value: jsonObject) {
            
            self.sInfo = model
            self.lblName.text = self.sInfo?.UserName
            if let jData = self.sInfo!.joining_date{
            self.lblDate.text = "Member Since " +  jData
                
            }
            self.listTable.reloadData()
            self.thumbImgView.setImageForYlaaUser(url: self.sInfo?.image, placeHolder: "placeHolder")

        }

    }

}

extension YLASellerInfoView: UITableViewDelegate,UITableViewDataSource,YLACollectionDelegate,SellerInfoCellDelegate{
   
    
    func didSelectedItem(adData: HomeData) {
        
    }
    
    func didLoadMoreItems(offset: Int) {
        
    }
    
    func userLogin() {
        
    }
    
    func movetoSellerAds(home: HomeData) {
        
    }
    
    func didFavoriteDone(done: Bool) {
        
    }
    
    func didVideoPressed(hData: HomeData) {
        
    }
    
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowcount = 0
        if let seller = self.sInfo
        {
            if let sellerDesc = seller.seller_info{
                rowcount = sellerDesc.count
            }
        }
        
        return rowcount

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"sellerinfoCell",
                                                            for: indexPath) as! sellerInfoTableCell
        cell.sDelegate = self
        if let sellers = self.sInfo!.seller_info {
            let seller = sellers[indexPath.row]
            let titleStr = seller.item_name != nil ? seller.item_name! : ""
            let subTitleStr = seller.item_value != nil ? seller.item_value! : ""
            cell.setSellerInfoCell(titleStr: titleStr, sub: subTitleStr)

        }
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //self.viewWillLayoutSubviews()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1
        {
            self.sDelegate?.didSelectSellerPost()
        }
    }
    
    func didPressedMobile(phNum: String) {
        
        self.sDelegate!.didCallSellerMobile(phNum: phNum)
    }
    
}
