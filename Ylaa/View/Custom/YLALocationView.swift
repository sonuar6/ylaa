//
//  YLALocationView.swift
//  Ylaa
//
//  Created by APPLE on 17/12/2019.
//  Copyright © 2019 Arun. All rights reserved.So
//

import UIKit

protocol YLALocFilterDelegate {
    func cancelLocationView()
    func selectedEmirateIndex(distance: Int, location: String)
    func sortAdsNear(kilometer: Int)
}


class YLALocationView: UIView,UITableViewDelegate,UITableViewDataSource,OptionalDelegate,NearByDelegate {
   
    
   
  
    @IBOutlet weak var listTable: UITableView!
    var lDelegate: YLALocFilterDelegate?
    var selectedIndex:Int = 0
    var isSliderEnabled: Bool = true
    let eArray = ["All","Al Ain","Abu Dhabi","Dubai","Sharjah","Fujairah","Ajman","Umm al Quwain","Ras Al Khaimah"]
  
    
    
    @IBAction func closePressed(_ sender: Any) {
        self.lDelegate?.cancelLocationView()
    }
    
    
    func initLocationFilter()
    {
        
        self.layer.shadowOpacity = 0.8
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowRadius = 4.0

        let shadowRect: CGRect = self.bounds.insetBy(dx: 0, dy: 4)
        self.layer.shadowPath = UIBezierPath(rect: shadowRect).cgPath
        
        
        self.listTable.register(UINib(nibName: "locationTableCell", bundle: nil), forCellReuseIdentifier: "locationCell")
        self.listTable.register(UINib(nibName: "YLAEmiratesCell", bundle: nil), forCellReuseIdentifier: "emiratesCell")
        self.listTable.delegate = self
        self.listTable.dataSource = self
        self.listTable.reloadData()
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          
        return 10
    }
      
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier:"locationCell",
                                                                      for: indexPath) as! locationTableCell
            cell.nByDelegate = self
            cell.dSlider.value = 0.0
            cell.lblSliderVal.isHidden = true

            
            if self.selectedIndex != indexPath.row
            {
                       cell.btnOptn.backgroundColor = .black
            }
            if self.isSliderEnabled{
                       cell.dSlider.isEnabled = true
                       cell.btnOptn.backgroundColor = .orangeColor
            }
            else{
                cell.dSlider.isEnabled = false
            }

            cell.lblSliderVal.text = "0"
            cell.btnOptn.tag = indexPath.row
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier:"emiratesCell",
                                                                    for: indexPath) as! YLAEmiratesCell
            cell.oDelegate = self
            if self.selectedIndex != indexPath.row
            {
                cell.btnOptn.backgroundColor = .black
            }
            cell.setCellTag(tagNum: indexPath.row)

            cell.lblTitle.text = eArray[indexPath.row - 1]
            return cell
        }
    }
    
    //MARK: -OptionalDelegate
    func didSelectedOption(isOn: Bool, forRow: Int) {
        self.selectedIndex = forRow
        self.listTable.reloadData()
        self.isSliderEnabled = false
        self.lDelegate?.selectedEmirateIndex(distance: 0, location: eArray[forRow - 1])

    }
    
     //MARK: -NearByDelegate
    
    func didNearbySelected(isOn: Bool, forRow: Int, value: String) {
            self.selectedIndex = forRow
            self.isSliderEnabled = isOn
            self.listTable.reloadData()
            self.lDelegate?.selectedEmirateIndex(distance: Int(value)!, location: eArray[forRow])
    }
    
    func didFilterByMiles(klmtr: Int) {
        self.lDelegate!.sortAdsNear(kilometer: klmtr)
    }

}
