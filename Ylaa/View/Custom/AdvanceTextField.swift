//
//  AdvanceTextField.swift
//  texfield
//
//  Created by APPLE on 7/22/19.
//  Copyright © 2019 APPLE. All rights reserved.
//

import UIKit

class AdvanceTextField: UITextField {


    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    

    func isValidEmail()-> Bool
    {
        if !self.text!.isEmpty{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if emailPred.evaluate(with: self.text)
        {
            return true
        }
        else{
            self.shake()
            return false
        }
      }
        else{
            self.shake()
            return false
        }
    }
    
    
    
    
    
    
    
    
    
    
    
}



public extension UIView {
    
    func shake(count : Float = 4,for duration : TimeInterval = 0.5,withTranslation translation : Float = 5) {
        
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.repeatCount = count
        animation.duration = duration/TimeInterval(animation.repeatCount)
        animation.autoreverses = true
        animation.values = [translation, -translation]
        layer.add(animation, forKey: "shake")
    }
}
