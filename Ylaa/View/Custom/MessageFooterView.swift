//
//  MessageFooterView.swift
//  Ylaa
//
//  Created by Arun on 08/03/2020.
//  Copyright © 2020 Arun. All rights reserved.
//

import UIKit
protocol SendMessageDelegate: class {
    func sendMessage(msgStr: String)
}
class MessageFooterView: UIView {
    
    
    weak var msgDelegate: SendMessageDelegate?
    
    
    @IBOutlet weak var txtMsg: UITextField!
    
    
    
    @IBAction func sendPressed(_ sender: Any) {
        
        guard let msg =  self.txtMsg.text,!msg.isEmpty else{
            self.txtMsg.text = ""
            self.txtMsg.resignFirstResponder()
            return
        }
       // self.txtMsg.resignFirstResponder()
        self.msgDelegate?.sendMessage(msgStr: msg)
    }
    
    
    
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
