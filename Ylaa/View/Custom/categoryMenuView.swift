//
//  categoryMenuView.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit
protocol CategoryDelegate {
    func didSelectCategory(selCat: CategorySegueData)
}

class categoryMenuView: UIView {
    
    var categoryDelegate: CategoryDelegate?

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBAction func categPressed(_ sender: Any) {
        
        let btn = sender as! UIButton
        var segue: CategorySegueData?
        switch btn.tag {
        case 111:
            segue = CategorySegueData(titleName: YLACATEG.AUTO, cID: 1)
            break
        case 112:
            segue = CategorySegueData(titleName: YLACATEG.RESTATE, cID: 2)
            break
        case 113:
            segue = CategorySegueData(titleName: YLACATEG.CLSFDS, cID: 3)
            break
        case 114:
            segue = CategorySegueData(titleName: YLACATEG.CMNTY, cID: 5)
            break
        case 115:
            segue = CategorySegueData(titleName: YLACATEG.JOB, cID: 4)
            break
        default:
            break
        }
        
        self.categoryDelegate?.didSelectCategory(selCat: segue!)
        
        
    }
    
    
    
    
    
    
    
    

}
