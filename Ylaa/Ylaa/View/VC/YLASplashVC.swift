//
//  YLASplashVC.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class YLASplashVC: UIViewController {
    
    var selLngView: SelectLangView = UIView.fromNib()
    
    @IBOutlet weak var logoImgView: UIImageView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.selLngView.frame =   CGRect(x: self.view.frame.origin.x, y: ((self.view.frame.height/2) - self.selLngView.frame.height), width: self.view.frame.width, height: self.selLngView.frame.height)
        self.view.addSubview(self.selLngView)
        self.selLngView.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
        self.selLngView.isHidden = true
        
        self.selLngView.onComplete = { finished in
            
            self.pushToHome()
        }


        
        showAnimate()

        // Do any additional setup after loading the view.
    }
    
    
    
    
    
    
    
    func showAnimate()
       {
           

        
        UIView.animate(withDuration: 2.0, animations: {() -> Void in
            self.logoImgView?.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        }, completion: {(_ finished: Bool) -> Void in
            
            self.logoImgView?.isHidden = false
            UIView.animate(withDuration: 1.0, animations: {() -> Void in
                self.logoImgView?.transform = CGAffineTransform(scaleX: 1, y: 1)
                
                if Utility.getPlistValueforKey(key: YLA.ISLANSEL) as String == YLASTATUS.YES{
                    self.perform(#selector(self.pushToHome), with: self, afterDelay: 1.2)

                }
                
                else{
                    
                    self.perform(#selector(self.setLogoSelectlang), with: self, afterDelay: 1.0)
                    
                    
                }
                
                
            })
        })
           
           
           
       }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @objc func setLogoSelectlang()
    {

        UIView.animate(withDuration: 1.0, animations: {() -> Void in
                               self.logoImgView?.frame.origin.y = 100.0
                                 }, completion: {(_ finished: Bool) -> Void in
                                    
                                    UIView.animate(withDuration: 2.0, animations: {() -> Void in
                                    self.logoImgView?.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
                                      }, completion: {(_ finished: Bool) -> Void in
                                        
                                        
                                           self.selLngView.isHidden = false
                                        self.selLngView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)



                                        })
                                    
                                   
                                   })
                           
    }
    
    
    
    
    @objc func pushToHome()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                   let aVC = storyboard.instantiateViewController(withIdentifier:"YLAHomeVCSBID"
                       ) as! YLAHomeVC
        let navigation = UINavigationController(rootViewController: aVC)
        navigation.modalPresentationStyle = .fullScreen
        navigation.modalTransitionStyle = .crossDissolve
        self.present(navigation, animated: true, completion: nil)
    }
    
    
    
    
}
