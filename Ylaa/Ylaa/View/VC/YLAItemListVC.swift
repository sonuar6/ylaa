//
//  YLAItemListVC.swift
//  Ylaa
//
//  Created by Arun on 14/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class YLAItemListVC: BaseTabBarController,YLACollectionDelegate {
    
    
    
    
    @IBAction func tabPressed(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        switch btn.tag {
        case 777:
            break
        case 778:
            showFilterActionSheet()
            break
        case 779:
            self.itemCollectionView.collection_Type = .List
            break
        case 780:
            self.itemCollectionView.collection_Type = .Column

            break
        case 781:
            break
            
        default:
            break
        }
        
        
        
        
    }
    
    
    
    func showFilterActionSheet()  {

           let optionMenu = UIAlertController(title: nil, message: "Sort By", preferredStyle: .actionSheet)

           let newAction = UIAlertAction(title: "New Ads", style: .default, handler: {
               (alert: UIAlertAction!) -> Void in

           })
           let oldAction = UIAlertAction(title: "Old Ads", style: .default, handler: {
               (alert: UIAlertAction!) -> Void in
           })
          let premiumAction = UIAlertAction(title: "Premium Ads", style: .default, handler: {
                  (alert: UIAlertAction!) -> Void in
              })

           //
           let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
               (alert: UIAlertAction!) -> Void in
           })


           // 4
           optionMenu.addAction(newAction)
           optionMenu.addAction(oldAction)
           optionMenu.addAction(premiumAction)

           optionMenu.addAction(cancelAction)

           // 5
           self.present(optionMenu, animated: true, completion: nil)
       }
       
    
    
    
    
    
    
    
    
    
    
    
    
    func didSelectedItem() {
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                              let controller = storyboardMain.instantiateViewController(withIdentifier: "YLADetailVCSBID") as! YLADetailVC
                              self.navigationController?.pushViewController(controller, animated: true)
                  
    }
    

    @IBOutlet weak var itemCollectionView: YLACollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.itemCollectionView.initCollection(rowCount: 2, collection: .Home, scrollingDirection: .vertical)
        self.itemCollectionView.ylaDelegate = self


        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
