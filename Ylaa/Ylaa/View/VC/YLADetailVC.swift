//
//  YLADetailVC.swift
//  Ylaa
//
//  Created by Arun on 10/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit
import  MessageUI
class YLADetailVC: BaseViewController,YLAOfferDelegate,SellerDelegate,MFMailComposeViewControllerDelegate,SegmentDelegate {
  
   
    
    
    
    @IBOutlet weak var listTable: UITableView!
    @IBOutlet weak var footerView: UIView!
    var offerView: YLAOfferView = UIView.fromNib()
    var sellerView: YLASellerInfoView = UIView.fromNib()


    var isDesc:Bool  = true
    var isSpec:Bool  = false

    
    
    
    
    
    
    @IBAction func actionPressed(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        
        switch btn.tag {
        case 555:
            showReportActionSheet()
            break
        case 556:
            if btn.currentImage == UIImage(named: "Path 3118")
            {
                btn.setImage(UIImage(named: "star_Sel"), for: .normal)
            }
            else{
                btn.setImage(UIImage(named: "Path 3118"), for: .normal)

            }
            break
        case 557:
            let activityController = UIActivityViewController(activityItems: ["Audi A5 Automatic Lorem ipsum dolor sit amet,consectetur adipscing elit", "Yalla"], applicationActivities: nil)
                              activityController.excludedActivityTypes = [.airDrop, .postToTwitter, .assignToContact]

                              self.present(activityController, animated: true, completion: nil)
                           
            break
        default:
            break
        }
        
        
        
    }
    
    
    func showReportActionSheet()  {

        let optionMenu = UIAlertController(title: nil, message: "Report!", preferredStyle: .actionSheet)

        let fullAction = UIAlertAction(title: "Report this Ad", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in

        })
        let addvanceAction = UIAlertAction(title: "Fraud Post", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
        })

        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })


        // 4
        optionMenu.addAction(fullAction)
        optionMenu.addAction(addvanceAction)
        optionMenu.addAction(cancelAction)

        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    
    
    
    
    @IBAction func tabPressed(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        if btn.tag == 114 ||  btn.tag == 112
        {
            switch btn.tag {
            case 114:
                self.offerView.initFor(type: .Offer, caption: "Make Your Best Offer", titleStr:"Make an Offer" , action: "Send Offer")
                break
            case 112:
                self.offerView.initFor(type: .Phone, caption: "Would you like to call?", titleStr: "+971564487542", action: " Call")

                break
            default:
                break
            }
            
            UIView.animate(withDuration: 0.2, animations: {() -> Void in
                                          self.offerView.frame.origin.y = 300
                                            }, completion: {(_ finished: Bool) -> Void in
 
                                              })
                                      
        }
        else if btn.tag == 115
        {
            UIView.animate(withDuration: 0.2, animations: {() -> Void in
                                                     self.sellerView.frame.origin.y = 300
                                                       }, completion: {(_ finished: Bool) -> Void in
            
                                                         })
        }
        else if btn.tag == 113
        {
            let sms: String = "sms:+1234567890&body=Hello Abc How are You I am ios developer."
            let strURL: String = sms.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            UIApplication.shared.open(URL.init(string: strURL)!, options: [:], completionHandler: nil)
        }
        else if btn.tag == 111
        {
            self.sendEmail()
        }
                   
        
        
        
        
        
        
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.offerView.frame =   CGRect(x: 0, y: 1000, width: self.view.bounds.size.width  , height: self.offerView.bounds.size.height)
        self.offerView.ofDelegate = self
        self.view.addSubview(self.offerView)
        
        self.sellerView.frame =   CGRect(x: 0, y: 1000, width: self.view.bounds.size.width  , height: self.sellerView.bounds.size.height)
        self.sellerView.sDelegate = self
        self.sellerView.setInitSellerInfo()
        self.view.addSubview(self.sellerView)


        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func cancelView() {
        
        UIView.animate(withDuration: 0.2, animations: {() -> Void in
                                                 self.offerView.frame.origin.y = 1000
                                                   }, completion: {(_ finished: Bool) -> Void in
        
                                                     })
        
        UIView.animate(withDuration: 0.2, animations: {() -> Void in
                                                        self.sellerView.frame.origin.y = 1000
                                                          }, completion: {(_ finished: Bool) -> Void in
               
                                                            })
           
    }
    
    
    
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["you@yoursite.com"])
            mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)

            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}


extension YLADetailVC: UITableViewDelegate,UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
           return 4
    }
    
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
     if indexPath.row == 0{
        let cell = tableView.dequeueReusableCell(withIdentifier: "bannerTableCell",
                                                         for: indexPath) as! bannerTableCell
        return cell
     }
     else if indexPath.row == 1
     {
        let cell = tableView.dequeueReusableCell(withIdentifier: "infoCell",
                                                               for: indexPath)
        return cell
     }
     else if  indexPath.row == 2 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "segmentCell",
                                                                      for: indexPath) as! segmentTableCell
        cell.sDelegate = self
        return cell
      }
    else
     {
        
        if isDesc{
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell",
                                                                                      for: indexPath)
                  return cell
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "spectableCell",
                                                                            for: indexPath) as! descTableCell
            cell.setDescCell()
        return cell
        }
    }
        
    }
    
    
   
    
    
   
    func didSegmentPressed(isDesc: Bool) {
        
        if isDesc{
            self.isDesc = true
            self.isSpec = false

        }else{
            self.isDesc = false
            self.isSpec = true
        }
        self.listTable.reloadData()
          
    }
      
        
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
   
    }
    
}
