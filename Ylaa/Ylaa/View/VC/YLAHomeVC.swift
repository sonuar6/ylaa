//
//  YLAHomeVC.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class YLAHomeVC: BaseTabBarController, CategoryDelegate{
    
    
    var categoryView: categoryMenuView = UIView.fromNib()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.categoryView.categoryDelegate = self
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    @IBAction func loginPressed(_ sender: Any) {
        
      let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                  let controller = storyboardMain.instantiateViewController(withIdentifier: "YLALoginVCSBID") as! YLALoginVC
                  self.navigationController?.pushViewController(controller, animated: true)
            
    }
    
    // MARK: - CategoryDelegate
    func didSelectCategory(selCat: String) {
        
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
              let controller = storyboardMain.instantiateViewController(withIdentifier: "YLACategoryVCSBID") as! YLACategoryVC
              self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    
    

}
extension YLAHomeVC: UITableViewDelegate,UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
           return 2
    }
       
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    
        var rowCount = 0
    
        switch section {
                  case 0:
                       rowCount = 1
                  break
                  case 1:
                       rowCount = 2
                  break
                 
           default:
        break
        }
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell",
                                                               for: indexPath)
                      
            return cell
        }
        else{
            
            if indexPath.row == 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "loginCell",
                                                                             for: indexPath)
                cell.backgroundColor = UIColor.clear
                                    
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "dataCell",
                                                                                          for: indexPath)
                cell.backgroundColor = UIColor.clear
                                                 
                return cell
            }
            
            
            
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var hgt: CGFloat = 0.0
        
        if indexPath.section == 0
               {
                hgt = 60.0
               }
               else{
                   
                   if indexPath.row == 0
                   {
                    hgt = 100.0

                   }
                   else{
                     hgt = 360.0

                   }
                   
                   
                   
                   
               }
               
        
        return hgt
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
        if section == 1{
        
            let headerView = UIView(frame: CGRect(x: 15,y: 0,width: tableView.bounds.size.width,height: 140))
            self.categoryView.frame =   CGRect(x: 0, y: 0, width: headerView.bounds.size.width  , height: headerView.bounds.size.height)
            headerView.backgroundColor = .red
            headerView.addSubview(categoryView)
        return headerView
        }
        else{
            let headerView = UIView(frame: CGRect(x: 0,y: 0,width: tableView.bounds.size.width,height: 15))
            return headerView
        }
        
        
    }

    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {

        var hght:NSInteger = 0
        print(section)
        hght = section == 0 ?   0: 120
        
        
        
       
        

        return CGFloat(hght)

    }
    
}
