//
//  YLACategoryVC.swift
//  Ylaa
//
//  Created by Arun on 05/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class YLACategoryVC: BaseTabBarController,FilterCollectionDelegate{
   
    
    
    final private let  catItems = ["All Automotive","Used Cars for Sale","Auto Accessories & Parts","Boats","Heavy Vehicles","Number Plates"]
    final private let  brandItems = ["Audi","BMW","VW","Porche","Lexus","Toyota","Honda","Mitsubhishi","Jaguar","Range Rover"]
    final private let  vehicleItems = ["A1","A3","A4","A5","A6","A7","A8","Q3","Q5","Q7","Q8"]
    
    var currentIndex = 0


    final private var  filterItems = [String]()



    
    var filterView: FilterView = UIView.fromNib()

    @IBOutlet weak var listTable: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.filterView.fCollectionView.initCollection(rowCount: 5, collection: .Filter, scrollingDirection: .vertical)
        self.filterView.fCollectionView.fDelegate = self

        // Do any additional setup after loading the view.
    }
    

   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func closePressed() {
          currentIndex = currentIndex - 1
          if currentIndex >= 0
          {
            self.listTable.reloadData()
        }
    }

}


extension YLACategoryVC: UITableViewDelegate,UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        switch currentIndex {
               case 0:
                 count = self.catItems.count
                   break
               case 1:
                count = self.brandItems.count
                  break
                   case 2:
                    count = self.vehicleItems.count
                   break
               default:
                   break
               }
        
           return count
    }
    
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
          let cell = tableView.dequeueReusableCell(withIdentifier: "catItemCells",
                                                   for: indexPath) as! categoryTablecell
    
          var fltStr = ""
          switch currentIndex {
                case 0:
                    fltStr = self.catItems[indexPath.row]
                    break
                case 1:
                    fltStr = self.brandItems[indexPath.row]
                   break
                    case 2:
                    fltStr = self.vehicleItems[indexPath.row]
                    break
                default:
                    break
                }
          cell.lblTitle.text = fltStr
    
          return cell
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
        if section == 0{
        
            let height = self.filterView.fCollectionView.collectionViewLayout.collectionViewContentSize.height

            let headerView = UIView(frame: CGRect(x: 15,y: 0,width: tableView.bounds.size.width,height: height + 20))
            headerView.backgroundColor = .white
            self.filterView.hgtConst.constant = height
            self.filterView.frame =   CGRect(x: 0, y: 0, width: headerView.bounds.size.width  , height:headerView.bounds.size.height)

            headerView.addSubview(filterView)
        return headerView
        }
        else{
            let headerView = UIView(frame: CGRect(x: 0,y: 0,width: tableView.bounds.size.width,height: 15))
            return headerView
        }
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {

     
        return CGFloat(50.0)

       }
    
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      cell.alpha = 0
      UIView.animate(withDuration: 0.3, animations: {
          cell.alpha = 1
      })
  }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        var fltStr: String?
        
        switch currentIndex {
        case 0:
            fltStr = self.catItems[indexPath.row]
            break
        case 1:
            fltStr = self.brandItems[indexPath.row]
           break
            case 2:
            fltStr = self.vehicleItems[indexPath.row]
            break
        default:
            break
        }
        
        self.filterItems.append(fltStr!)
        self.filterView.fCollectionView.filter = self.filterItems
        if currentIndex == 2
        {
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                        let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAItemListVCSBID") as! YLAItemListVC
                        self.navigationController?.pushViewController(controller, animated: true)
            
        }
        else{
            tableView.reloadData()
            currentIndex = currentIndex + 1


        }

   
    }
    
}
