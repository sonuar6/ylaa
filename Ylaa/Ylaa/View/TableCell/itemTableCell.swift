//
//  itemTableCell.swift
//  Ylaa
//
//  Created by Arun on 01/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class itemTableCell: UITableViewCell {

    @IBOutlet weak var itemCollectionView: YLACollectionView!
    
    var tempButton:UIButton?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.itemCollectionView.initCollection(rowCount: 2, collection: .Home, scrollingDirection: .vertical)
        self.tempButton = self.contentView.viewWithTag(111) as? UIButton
        // Initialization code
    }

    
    @IBAction func btnPressed(_ sender: Any) {
        
        
        tempButton?.setTitleColor(UIColor.subTitleColor, for: .normal)
        let btn = sender as! UIButton
        btn.setTitleColor(UIColor.highlightedColor, for: .normal)
        tempButton = btn
        
        
        
        
        
        
        
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
