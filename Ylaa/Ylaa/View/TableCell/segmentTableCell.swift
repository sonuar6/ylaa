//
//  segmentTableCell.swift
//  Ylaa
//
//  Created by Arun on 14/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

protocol SegmentDelegate {
    func didSegmentPressed(isDesc: Bool)
}


class segmentTableCell: UITableViewCell {

    
    var sDelegate: SegmentDelegate?
    var tempButton: UIButton?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tempButton = self.contentView.viewWithTag(888) as? UIButton

        
        // Initialization code
    }
    
    
    @IBAction func segmentPressed(_ sender: Any) {
        
        tempButton?.backgroundColor = .subTitleColor

        let btn = sender as! UIButton
        
        btn.backgroundColor = .orangeColor

        tempButton = btn

        
        switch btn.tag {
        case 888:
            self.sDelegate?.didSegmentPressed(isDesc: true)
            break
        case 889:
            self.sDelegate?.didSegmentPressed(isDesc: false)
            break
        default:
            break
        }
        
        
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
