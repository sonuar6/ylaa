//
//  bannerTableCell.swift
//  Ylaa
//
//  Created by Arun on 10/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class bannerTableCell: UITableViewCell {

    @IBOutlet weak var bannerCollection: YLACollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.bannerCollection.initCollection(rowCount: 1, collection: .banner, scrollingDirection: .horizontal)
        self.bannerCollection.image = ["xx","xx","xx"]
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
