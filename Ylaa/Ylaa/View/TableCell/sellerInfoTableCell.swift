//
//  sellerInfoTableCell.swift
//  Ylaa
//
//  Created by Arun on 12/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class sellerInfoTableCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setSellerInfoCell(titleStr: String, sub: String)
    {
        self.lbltitle.text = "  " + titleStr
        self.lblSubTitle.text = "   " + sub

    }
    
}
