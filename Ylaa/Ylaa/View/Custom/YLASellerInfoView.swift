//
//  YLASellerInfoView.swift
//  Ylaa
//
//  Created by Arun on 12/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit
protocol SellerDelegate {
    func cancelView()
}
class YLASellerInfoView: UIView {

   
    
    var sDelegate: SellerDelegate?
    
    @IBOutlet weak var hgtConst: NSLayoutConstraint!
    @IBOutlet weak var thumbImgView: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var listTable: UITableView!
    
    
     let array = ["Seller Type","No of Ads Posted","Location","Primary Phone"]
     let arrayInf = ["Dealer","35","Dubai, UAE","+971564487542"]

    
    
    
    @IBAction func btnPressed(_ sender: Any) {
        self.sDelegate?.cancelView()
    }
    
    
    
    
    
     func viewWillLayoutSubviews() {
        self.hgtConst?.constant = CGFloat(array.count) * 57.0
            //self.listTable.contentSize.height
    }
    func setInitSellerInfo()
    {
        
        
      
        self.listTable.register(UINib(nibName: "sellerInfoTableCell", bundle: nil), forCellReuseIdentifier: "sellerinfoCell")
        self.listTable.delegate = self
        self.listTable.dataSource = self
        self.listTable.reloadData()

    }

}

extension YLASellerInfoView: UITableViewDelegate,UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return array.count

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"sellerinfoCell",
                                                            for: indexPath) as! sellerInfoTableCell
        cell.setSellerInfoCell(titleStr: array[indexPath.row], sub: arrayInf[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
}
