//
//  YLAOfferView.swift
//  Ylaa
//
//  Created by Arun on 11/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

enum offView
{
    case Offer,Phone
}
protocol YLAOfferDelegate {
    func cancelView()
}


class YLAOfferView: UIView {
    
     @IBOutlet weak var txtOffer: UITextField!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var iconImgView: UIImageView!
    @IBOutlet weak var btnWhatsApp: UIButton!
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblCaption: UILabel!
    @IBOutlet weak var ofView: UIView!
    
    var ofDelegate: YLAOfferDelegate?
    
    
    @IBAction func cancelPressed(_ sender: Any) {
        self.ofDelegate?.cancelView()
    }
    
    
    
    func initFor(type: offView, caption: String,titleStr: String,action: String)
    {
        self.lblCaption.text = caption

        switch type {
        case .Offer:
            self.btnWhatsApp.isHidden = true
            self.ofView.isHidden = false
            self.lblTitle.text = titleStr
                       self.btnSubmit.setTitle(action, for: .normal)
                       self.btnSubmit.setImage(nil, for: .normal)


            break
        case .Phone:
            self.btnWhatsApp.isHidden = false
            self.ofView.isHidden = true
            self.lblTitle.text = titleStr
            self.btnSubmit.setTitle(action, for: .normal)
            self.btnSubmit.setImage(#imageLiteral(resourceName: "smartphone"), for: .normal)
            break
      
        }
    }
    
    
    
    
}
