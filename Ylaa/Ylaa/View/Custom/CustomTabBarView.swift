//
//  CustomTabBarView.swift
//  MOICrimeReporter
//
//  Created by Arun on 15/11/2019.
//  Copyright © 2019 APPLE. All rights reserved.
//

import UIKit
protocol CustomTabBarViewDelegate {
    
    func selectedTab(atIndex: NSInteger)
}

enum TabSelection
{
    case Media,Case,Report,People
}

class CustomTabBarView: UIView {
    
    
    @IBOutlet weak var tabMedia: UIButton!
    
    @IBOutlet weak var tabCase: UIButton!
    
    @IBOutlet weak var tabReport: UIButton!
    
    @IBOutlet weak var tabPeople: UIButton!
    
    var tabDelegate: CustomTabBarViewDelegate?
    var selectTabAtItemIndex: NSInteger?
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    func setCurrentTab(tab: TabSelection)
    {
        switch tab {
        case .Media:
            self.tabMedia.setImage( UIImage(named: "tab_first_icom_sel"), for: .normal)
            break
        case .Case:
            self.tabCase.setImage( UIImage(named: "tab_second_icon_sel"), for: .normal)
              break
        case .Report:
            self.tabReport.setImage( UIImage(named: "tab_third_icon_sel"), for: .normal)
            break
        case .People:
            self.tabPeople.setImage( UIImage(named: "tab_fourth_icon_sel"), for: .normal)

                break
      
        }
    }
     
    
    func resetTabBar()
    {
//        self.tabCase.setImage( UIImage(named: "tab_second_icon"), for: .normal)
//        self.tabMedia.setImage( UIImage(named:"tab_first_icom"), for: .normal)
//        self.tabReport.setImage( UIImage(named: "tab_third_icon"), for: .normal)
//        self.tabPeople.setImage( UIImage(named: "tab_fourth_icon"), for: .normal)



    }
    
    @IBAction func tabBarItemPressed(_ sender: Any) {
        
        let Btn = sender as! UIButton
        
        switch Btn.tag {
        case 111:
            if Btn.currentImage == UIImage(named: "tab_first_icom_sel")
            {
                return
            }
            else{
                self.tabMedia.setImage( UIImage(named: "tab_first_icom_sel"), for: .normal)

            }
            
            break
        case 112:
            if Btn.currentImage == UIImage(named: "tab_second_icon_sel")
            {
               return
            }
            else{
                self.tabCase.setImage( UIImage(named: "tab_second_icon_sel"), for: .normal)

            }
                      
             break
        case 113:
            if Btn.currentImage == UIImage(named: "tab_third_icon_sel")
            {
                                   return
            }
            else{
                           self.tabReport.setImage( UIImage(named: "tab_third_icon_sel"), for: .normal)

            }
            break
        case 114:
            if Btn.currentImage == UIImage(named: "tab_fourth_icon_sel")
            {
                return
            }
            else{
                                     self.tabPeople.setImage( UIImage(named: "tab_fourth_icon_sel"), for: .normal)

                      }
                break
        default:
            break
        }
        
        
        
        
     
        self.tabDelegate?.selectedTab(atIndex: Btn.tag)
        
    }
    
    
    
    
    
    
    
    
    
    

}

