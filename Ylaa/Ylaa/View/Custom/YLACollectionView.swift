//
//  YLACollectionView.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

enum Collection {
    case banner,Home,Filter
}

enum ScrollingDirection {
    case horizontal,vertical
}
enum CollectionType {
    case List,Column
}

protocol FilterCollectionDelegate {
    func closePressed()
}
protocol YLACollectionDelegate {
    func didSelectedItem()
}


class YLACollectionView: UICollectionView,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,FilterDelegate {
  
    
    override func layoutSubviews() {
           super.layoutSubviews()
           if bounds.size != intrinsicContentSize() {
               invalidateIntrinsicContentSize()
           }
       }

        func intrinsicContentSize() -> CGSize {
           return self.contentSize
       }
    
    
    
    var collection_Type: CollectionType?{
        didSet {
         guard let colType = collection_Type  else { return }
            if colType == .List {
                 self.register(UINib(nibName: "YLAItemListCell", bundle: nil), forCellWithReuseIdentifier: "itemCollectionListCell")
                 self.itemsPerRow = 1
            }  else
            {
                self.register(UINib(nibName: "itemCollectionCell", bundle: nil), forCellWithReuseIdentifier: "itemCollectionCells")
                 self.itemsPerRow = 2
            }
         self.reloadData()
        }
    }
    
    var image: [String]?{
                 didSet {
                  guard let fData = image  else { return }
                  self.ItemsCount = fData.count
                  self.reloadData()
                 }
             }
      
    
       var filter: [String]?{
              didSet {
               guard let fData = filter  else { return }
               self.ItemsCount = fData.count
               self.reloadData()
              }
          }

    
    
       fileprivate var scrollDirect: ScrollingDirection = .horizontal
       fileprivate var itemsPerRow: CGFloat = 3
       fileprivate var ItemsCount: NSInteger = 0
       var collRawval: Collection = .banner
       var fDelegate: FilterCollectionDelegate?
       var ylaDelegate:YLACollectionDelegate?
    
    func initCollection(rowCount: NSInteger, collection: Collection, scrollingDirection: ScrollingDirection)
       {
             self.itemsPerRow = CGFloat(rowCount)
             self.collRawval = collection
             self.scrollDirect = scrollingDirection
        
        switch collection {
                 case .banner:
                    self.register(UINib(nibName: "bannerCollectionCell", bundle: nil), forCellWithReuseIdentifier: "bannerCollectionCells")
                    break
                        
                 case .Home:
                    self.collection_Type = .Column
                       self.register(UINib(nibName: "itemCollectionCell", bundle: nil), forCellWithReuseIdentifier: "itemCollectionCells")
                        break
                 case .Filter:
                        self.register(UINib(nibName: "filterCollectionCell", bundle: nil), forCellWithReuseIdentifier: "filterCollectionCells")
                       break
            
        }
        self.delegate = self
        self.dataSource = self
        self.reloadData()
     }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         let count  = self.collRawval == .Home ? 10 :  ItemsCount
         return count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.collRawval == .Home{
            if self.collection_Type == .List{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"itemCollectionListCell",for: indexPath)as! itemCollectionCell

                          return cell
            }
            else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"itemCollectionCells",for: indexPath)as! itemCollectionCell

            return cell
            }
        }
        else if self.collRawval == .banner
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"bannerCollectionCells",for: indexPath)as! bannerCollectionCell
            return cell
        }
        else if self.collRawval == .Filter
               {
                   let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"filterCollectionCells",for: indexPath)as! filterCollectionCell
                cell.delegate = self
                cell.setFilterCell(titleStr: filter![indexPath.row])
                cell.btnClose.isHidden =  indexPath.row == filter!.count - 1 ? false : true
                   return cell


               }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"itemCollectionCells",for: indexPath)as! itemCollectionCell
            return cell


        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                          layout collectionViewLayout: UICollectionViewLayout,
                          sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.collRawval == .banner{
            return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)

        }else{
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
                         let totalSpace = flowLayout.sectionInset.left
                             + flowLayout.sectionInset.right
                             + (flowLayout.minimumInteritemSpacing * CGFloat(itemsPerRow - 1))
                         let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(itemsPerRow))
            var  hgt = 0
            
            if self.collRawval == .Home {
                switch self.collection_Type {
                case .Column:
                    hgt = 216
                    break
                case .List:
                    hgt = 108
                    break
                    
                case .none:
                    break
                }
              
                
            }else {
                hgt = 30
                
            }
        
           return CGSize(width: size, height: hgt)
        }
    }
 
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.ylaDelegate?.didSelectedItem()
    }
    
    
    
    
    func didFilterClosePressed() {
        
        self.filter?.remove(at: (self.filter!.count - 1))
        self.reloadData()
        self.fDelegate?.closePressed()
      }

}
