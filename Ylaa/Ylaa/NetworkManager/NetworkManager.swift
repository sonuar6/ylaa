//
//  NetworkManager.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import SystemConfiguration
import CommonCrypto


public typealias CompletionBlock = (_ finished : Bool, _ statusCode : Int?, _ response: [String :AnyObject]?) -> Void




class NetworkManager{
    
    
    
    
    
    
    

//////////// UTITLITY METHODS ////////////////
    
    
   
    
    
    
    
    private static func request(urlStr: String, method: HTTPMethod = .get, parameters: Parameters = [:],completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void) {
        if ServiceUtils.isConnectedToNetwork() {
            
            var urlRequest = URLRequest(url: URL(string: urlStr)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: ServiceUtils.Network_timeout)
            urlRequest.httpMethod = "GET"
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.setValue("ios", forHTTPHeaderField: "device")
            urlRequest.setValue("SS49974", forHTTPHeaderField: "key")

            let dataRequest = Alamofire.request(urlRequest)
            dataRequest.validate().responseJSON { response in
                switch (response.result) {
                case .success:
                    //print(response)
                    processResponse(response: response, completionHandler: completionHandler, errorHandler: errorHandler)
                    break
                case .failure(let error):
                    errorHandler("Unable to connect to server. Please try again later.")
                    break
                }
            }
            
            
        } else {
            
            errorHandler("Please check your internet connection")
        }
    }
    
    public static func requestRaw(urlStr: String, method: HTTPMethod = .post, parameters: Parameters, completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void) {
        if ServiceUtils.isConnectedToNetwork() {
            let url = URL(string: urlStr)!
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = method == .put ? "PUT" : "POST"
            
            
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            } catch {
                errorHandler("Please check your internet connection")
            }
            
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.setValue("ios", forHTTPHeaderField: "device")
            urlRequest.setValue("SS49974", forHTTPHeaderField: "key")
            
            urlRequest.timeoutInterval = ServiceUtils.Network_timeout
            let dataRequest = Alamofire.request(urlRequest)
            dataRequest.validate().responseJSON { response in
                switch (response.result) {
                case .success:
                    processResponse(response: response, completionHandler: completionHandler, errorHandler: errorHandler)
                    break
                case .failure(let error):
              //      print(error.localizedDescription)
                    errorHandler("Unable to connect to server. Please try again later.")
                    break
                }
            }
        } else {
            
            errorHandler("Please check your internet connection")
            
        }
    }
    
    
    private static func processResponse(response: DataResponse<Any>, completionHandler: @escaping (RequestResult) -> Void, errorHandler: @escaping (String) -> Void) {
        if ServiceUtils.DEBUG_MODE {
            print("Request: \(String(describing: response.request))")
            print("Response: \(String(describing: response.response))")
        }
        
        switch response.result {
        case .success:
            if let JSON = response.result.value {
                if ServiceUtils.DEBUG_MODE {
                    print(JSON)
                }
                
                if let requestResult = Mapper<RequestResult>().map(JSONObject: JSON) {
                    completionHandler(requestResult)
                }
                else {
                    errorHandler("There is something wrong parsing the object from server.")
                }
            }
        case .failure(let error):
            var errorString = error.localizedDescription
            if let data = response.data, let responseDataStr = String(data: data, encoding: String.Encoding.utf8) {
                errorString = responseDataStr
            }
            errorHandler(errorString)
        }
    }
}

class ServiceUtils {
    public static let DEBUG_MODE = false
    public static let BETA_BASE_URL = ""
    /*****************************        Method Names        ********************************/
    
    
    
    class func isConnectedToNetwork() -> Bool {
        return Reachability.isConnectedToNetwork()
    }
    
    
    
    public static func createHashToken(parameters: Parameters) -> String? {
        let SECURE_SECRET = "6CkCNr8lHHcxRfL"
        //let SECOND_SECURE_SECRET = "SS49974"
        
        let sortedKeys = Array(parameters.keys).sorted(by: <)
        
        var string = SECURE_SECRET
        sortedKeys.forEach { (key) in
            if parameters[key] is [[String: Any]] {
            } else if let val = parameters[key] as? Int {
                string += String(describing: val)
            } else if let val = parameters[key] as? Double {
                string += String(format: "%g", val)
            }
            else if  key != "token" {
                string += String(describing: parameters[key]!)
            }
        }
        //string += SECOND_SECURE_SECRET
        
        return  string.sha256().uppercased()
    }
    
    
    
    //****************************** CONSTANTS ****************************************
    
    
    public static let Network_timeout = 60.0
}




extension String {
    
    func sha256() -> String{
        if let stringData = self.data(using: String.Encoding.utf8) {
            return hexStringFromData(input: digest(input: stringData as NSData))
        }
        return ""
    }
    
    private func digest(input : NSData) -> NSData {
        let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
        var hash = [UInt8](repeating: 0, count: digestLength)
        CC_SHA256(input.bytes, UInt32(input.length), &hash)
        return NSData(bytes: hash, length: digestLength)
    }
    
    private  func hexStringFromData(input: NSData) -> String {
        var bytes = [UInt8](repeating: 0, count: input.length)
        input.getBytes(&bytes, length: input.length)
        
        var hexString = ""
        for byte in bytes {
            hexString += String(format:"%02x", UInt8(byte))
        }
        
        return hexString
    }
    
}

