//
//  NetworkModel.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import Foundation

import ObjectMapper

class RequestResult: Mappable {
    var value: Any?
    private var status: StatusModel?
    
    required init?(map: Map) {
        
    }
    
    init() {
    }
    
    init(status: StatusModel) {
        self.status = status
    }
    
    init(value: Any?) {
        self.value = value
    }
    
    func mapping(map: Map) {
        value <- map["results"]
        status <- map["response"]
    }
    func isOK() -> Bool {
        return self.status?.status ?? false
    }
    func message() -> String{
        return self.status?.message ?? ""
    }
    func code() -> String{
        return self.status?.code ?? ""
    }
}


class StatusModel:Mappable{
   
    
    var code: String?
    var status: Bool?
    var message :  String?
    required init?(map: Map) {
        
    }
    
    init() {
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        status <- map["status"]
        message <- map["message"]
    }
    
    
}



class RequestParent: Mappable {
    var request: RequestObject?
    var key: String?
    
    required init?(map: Map) {
        
    }
    
    init() {
    }
    
    init(key: String?, userid: Int) {
        self.key = key
        self.request = RequestObject(userid: userid)
    }
    
    func mapping(map: Map) {
        request <- map["request"]
        key <- map["key"]
    }
}

class RequestObject: Mappable {
    var user_id: Int?
    
    required init?(map: Map) {
        
    }
    
    init() {
    }
    
    init(userid: Int) {
        self.user_id = userid
    }
    
    func mapping(map: Map) {
        user_id <- map["user_id"]
    }
}


class UploadObject: Mappable {
    var form_id: Int?
    var file_description: String?
    var file_name: String?
    var file_uploaded: String?
    var file_extension: String?
    
    required init?(map: Map) {
        
    }
    
    init() {
    }
    
    init(form_id: Int,file_description:String,file_name:String,file_uploaded:String,file_extension:String) {
        self.form_id = form_id
         self.file_description = file_description
         self.file_name = file_name
         self.file_uploaded = file_uploaded
         self.file_extension = file_extension
    }
    
    func mapping(map: Map) {
        form_id <- map["form_id"]
         file_description <- map["file_description"]
         file_name <- map["file_name"]
         file_uploaded <- map["file_uploaded"]
         file_extension <- map["file_extension"]
    }
}


