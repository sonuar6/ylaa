
//
//  YLAConstants.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Co





//import UIKit
//@IBDesignable
//public extension UIView
//{
//
//
//
//
//       @IBInspectable
//       var shadowRadius: CGFloat {
//           get {
//               return layer.shadowRadius
//           }
//           set {
//               layer.masksToBounds = false
//               layer.shadowRadius = newValue
//           }
//       }
//
//       @IBInspectable
//       var shadowOpacity: Float {
//           get {
//               return layer.shadowOpacity
//           }
//           set {
//               layer.masksToBounds = false
//               layer.shadowOpacity = newValue
//           }
//       }
//
//       @IBInspectable
//       var shadowOffset: CGSize {
//           get {
//               return layer.shadowOffset
//           }
//           set {
//               layer.masksToBounds = false
//               layer.shadowOffset = newValue
//           }
//       }
//
//       @IBInspectable
//       var shadowColor: UIColor? {
//           get {
//               if let color = layer.shadowColor {
//                   return UIColor(cgColor: color)
//               }
//               return nil
//           }
//           set {
//               if let color = newValue {
//                   layer.shadowColor = color.cgColor
//               } else {
//                   layer.shadowColor = nil
//               }
//           }
//       }
//
//
//
//
//    @IBInspectable
//    public var cornerRadius: CGFloat
//    {
//        set (radius) {
//            self.layer.cornerRadius = radius
//            self.layer.masksToBounds = radius > 0
//        }
//
//        get {
//            return self.layer.cornerRadius
//        }
//    }
//
//    @IBInspectable
//     var borderWidth: CGFloat
//    {
//        set (borderWidth) {
//            self.layer.borderWidth = borderWidth
//        }
//
//        get {
//            return self.layer.borderWidth
//        }
//    }
//
//    @IBInspectable
//     var borderColor:UIColor?
//    {
//        set (color) {
//            self.layer.borderColor = color?.cgColor
//        }
//
//        get {
//            if let color = self.layer.borderColor
//            {
//                return UIColor(cgColor: color)
//            } else {
//                return nil
//            }
//        }
//    }
//}


//
//  UIView-Extension.swift
//

import Foundation
import UIKit

@IBDesignable
extension UIView {
     // Shadow
     @IBInspectable var shadow: Bool {
          get {
               return layer.shadowOpacity > 0.0
          }
          set {
               if newValue == true {
                    self.addShadow()
               }
          }
     }

     fileprivate func addShadow(shadowColor: CGColor = UIColor.black.cgColor, shadowOffset: CGSize = CGSize(width: 0.0, height: 20.0), shadowOpacity: Float = 0.4, shadowRadius: CGFloat = 50.0) {
          let layer = self.layer
          layer.masksToBounds = false

          layer.shadowColor = shadowColor
          layer.shadowOffset = shadowOffset
          layer.shadowRadius = shadowRadius
          layer.shadowOpacity = shadowOpacity
          layer.shadowPath = UIBezierPath(roundedRect: layer.bounds, cornerRadius: layer.cornerRadius).cgPath

          let backgroundColor = self.backgroundColor?.cgColor
          self.backgroundColor = nil
          layer.backgroundColor =  backgroundColor
     }


     // Corner radius
     @IBInspectable var circle: Bool {
          get {
               return layer.cornerRadius == self.bounds.width*0.5
          }
          set {
               if newValue == true {
                    self.cornerRadius = self.bounds.width*0.5
               }
          }
     }

     @IBInspectable var cornerRadius: CGFloat {
          get {
               return self.layer.cornerRadius
          }

          set {
               self.layer.cornerRadius = newValue
          }
     }


     // Borders
     // Border width
     @IBInspectable
     public var borderWidth: CGFloat {
          set {
               layer.borderWidth = newValue
          }

          get {
               return layer.borderWidth
          }
     }

     // Border color
     @IBInspectable
     public var borderColor: UIColor? {
          set {
               layer.borderColor = newValue?.cgColor
          }

          get {
               if let borderColor = layer.borderColor {
                    return UIColor(cgColor: borderColor)
               }
               return nil
          }
     }
}
