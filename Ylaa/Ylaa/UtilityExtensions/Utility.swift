//
//  Utility.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import Foundation


class Utility
{
    
    // Coder for storing sessions
       class func resetPlistValueForKey(key: String, value: String)
       {
           
           let defaults = UserDefaults.standard
           defaults.set(value, forKey:key)
       }
       
    class func getPlistValueforKey(key: String) ->  NSString
       {
           
           let defaults = UserDefaults.standard
           
           var keyVal: NSString?
           
           keyVal = defaults.string(forKey: key) as NSString?
           if ((keyVal) != nil)
           {
               return keyVal!
           }
           else
           {
               return "0"
           }
           
       }
    
    
}
