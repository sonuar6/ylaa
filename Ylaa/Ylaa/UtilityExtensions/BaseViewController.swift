//
//  BaseViewController.swift
//  Ylaa
//
//  Created by Arun on 05/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func backPressed(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class BaseTabBarController: BaseViewController,CustomTabBarViewDelegate
{
    var tabView: CustomTabBarView?
    
    
    override func viewDidLoad()
    {
        tabView = UIView.fromNib()
              self.tabView?.frame =   CGRect(x: self.view.frame.origin.x, y: self.view.frame.height - self.tabView!.frame.height, width: self.view!.frame.width, height: self.tabView!.frame.height)
                  self.tabView?.tabDelegate = self
                  self.view.addSubview(self.tabView!)
                  self.view.bringSubviewToFront(self.tabView!)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
         super.viewWillDisappear(animated)
         self.tabView!.resetTabBar()
        
        
        
    
    }
    
    
    
    // MARK: - CustomTabBarViewDelegate Methods

        
        func selectedTab(atIndex: NSInteger) {
            let storyboardMain = UIStoryboard(name: "Main", bundle: nil)

               switch atIndex {
                
                      case 111:
                           
                          break
                      case 112:
                                          
                          break
                      case 113:
                        
                        
                                              
                          break
                      case 114:

                          break
                      case 119:
                        
                     
                          break
                          
                      default:
                          break
                      }
                      
           }

}
