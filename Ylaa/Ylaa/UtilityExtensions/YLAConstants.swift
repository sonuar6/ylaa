//
//  YLAConstants.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import Foundation

struct YLA
{
    static let CURLANG = "selLang"
    static let ISLANSEL = "langSelected"

}
struct YLASTATUS
{
    static let YES = "yes"
    static let NO = "no"

}

struct LANG
{
    static let ARB = "ar"
    static let ENG = "en"

}
