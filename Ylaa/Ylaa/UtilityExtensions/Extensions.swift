//
//  Extensions.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage


extension UIView
{
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}


extension UIColor
{
    class var bgColor: UIColor
    {
        return UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1.0)
    }
    class var textDimColor: UIColor
       {
        return UIColor(red: 196.0/255.0, green:196.0/255.0, blue: 196.0/255.0, alpha: 0.8)
       }
    
    class var subTitleColor: UIColor
    {
        return UIColor(red: 102.0/255.0, green:102.0/255.0, blue: 102.0/255.0, alpha: 0.8)
    }
    
    class var ylaaColor: UIColor
       {
        return UIColor(red: 242.0/255.0, green:111.0/255.0, blue: 47.0/255.0, alpha: 0.8)
       }
    class var highlightedColor: UIColor
         {
        return UIColor(red: 242.0/255.0, green:146.0/255.0, blue: 98.0/255.0, alpha: 0.8)
         }
    class var orangeColor: UIColor
           {
        return UIColor(red: 242.0/255.0, green:111.0/255.0, blue: 47.0/255.0, alpha: 1.0)
           }
    
    
}



extension NSObject {
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
}


extension UIViewController {
    
    func setstatusBarColor()
       {
           if #available(iOS 13.0, *) {


              let statusBar1 =  UIView()
               statusBar1.frame = (UIApplication.shared.keyWindow?.windowScene?.statusBarManager!.statusBarFrame)!
               statusBar1.backgroundColor =  UIColor.bgColor

              UIApplication.shared.keyWindow?.addSubview(statusBar1)

           } else {

              let statusBar1: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
              statusBar1.backgroundColor = UIColor.bgColor
           }
       }
    
    
    
    
    var storyBoardID: String {
              return self.className + "SBID"
          }
          
          class var storyBoardID: String {
              return className + "SBID"
          }
}


extension UIImageView{

    func placeAtTheCenterWithView(view: UIView) {
        
        view.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: 0))
        
        view.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: 0))
    }

    func setImageWithURL(url : String? , placeholder : String?)  {
        
        
        
        guard let urlString = url, let urlQueryString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            return
        }
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.style = UIActivityIndicatorView.Style.medium
        activityIndicator.frame = self.bounds
        if self.tag == 11{
            activityIndicator.center = CGPoint(x:UIScreen.main.bounds.size.width/2, y: 90.0)
        }
        if let url = URL(string: urlQueryString) {
            self.addSubview(activityIndicator)
            self.placeAtTheCenterWithView(view: activityIndicator)
            activityIndicator.startAnimating()
            self.af_setImage(withURL: url, completion: { (image) in
                activityIndicator.stopAnimating()
                activityIndicator.isHidden = true
                activityIndicator.removeFromSuperview()
                if let newImage = image.value {
                    
                     self.image = newImage
                    

                }
            })
        }
        
        
        

       
            
            
            
        
    }
}
