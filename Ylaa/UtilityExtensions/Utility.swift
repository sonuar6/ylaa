//
//  Utility.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import Foundation
import UIKit
import  MapKit
import CoreLocation
import AVFoundation
import Photos


class Utility
{
    
    // Coder for storing sessions
       class func resetPlistValueForKey(key: String, value: String)
       {
           
           let defaults = UserDefaults.standard
           defaults.set(value, forKey:key)
       }
       
    class func getPlistValueforKey(key: String) ->  NSString
       {
           
           let defaults = UserDefaults.standard
           
           var keyVal: NSString?
           
           keyVal = defaults.string(forKey: key) as NSString?
           if ((keyVal) != nil)
           {
               return keyVal!
           }
           else
           {
               return "0"
           }
           
       }
    class func setDecimalNumber(valueInt: Int)-> String
    {
      let numberFormatter = NumberFormatter()
      numberFormatter.numberStyle = .decimal
      let formattedNumber = numberFormatter.string(from: NSNumber(value:valueInt))
      return formattedNumber!
    }
    
    
   class func getStringDate(dateStr: String)-> String{
            let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd yyyy"
//"2016-02-29 12:24:26"
    print(dateStr)

        let date = dateFormatterGet.date(from: dateStr)
        print(dateFormatterPrint.string(from: date!))

        
        return dateFormatterPrint.string(from: date!)
    }
    
    
     class func getMsgStringDate(dateStr: String)-> String{
                let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd MMM HH:mm"
        print(dateStr)

            let date = dateFormatterGet.date(from: dateStr)
            print(dateFormatterPrint.string(from: date!))

            
            return dateFormatterPrint.string(from: date!)
        }
    
    
    
    
    
    
   class func add(stringList: [String],
             font: UIFont,
             bullet: String = "\u{2022}",
             indentation: CGFloat = 20,
             lineSpacing: CGFloat = 2,
             paragraphSpacing: CGFloat = 12,
             textColor: UIColor = .white,
             bulletColor: UIColor = .red) -> NSAttributedString {

    let textAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: textColor]
    let bulletAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: bulletColor]

        let paragraphStyle = NSMutableParagraphStyle()
        let nonOptions = [NSTextTab.OptionKey: Any]()
        paragraphStyle.tabStops = [
            NSTextTab(textAlignment: .left, location: indentation, options: nonOptions)]
        paragraphStyle.defaultTabInterval = indentation
        //paragraphStyle.firstLineHeadIndent = 0
        //paragraphStyle.headIndent = 20
        //paragraphStyle.tailIndent = 1
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.paragraphSpacing = paragraphSpacing
        paragraphStyle.headIndent = indentation

        let bulletList = NSMutableAttributedString()
        for string in stringList {
            let formattedString = "\(bullet)\t\(string)\n"
            let attributedString = NSMutableAttributedString(string: formattedString)

            attributedString.addAttributes(
                [NSAttributedString.Key.paragraphStyle : paragraphStyle],
                range: NSMakeRange(0, attributedString.length))

            attributedString.addAttributes(
                textAttributes,
                range: NSMakeRange(0, attributedString.length))

            let string:NSString = NSString(string: formattedString)
            let rangeForBullet:NSRange = string.range(of: bullet)
            attributedString.addAttributes(bulletAttributes, range: rangeForBullet)
            bulletList.append(attributedString)
        }

        return bulletList
    }
    
    
    class func getDocumentName(name: String) -> String{
        
        var type: String = ""
        switch name {
        case "tradelic":
            type = "Trade License"
            break
        case "auth_doc":
            type = "Authorised Document"
            break
        case "visapage":
            type = "Visa Page"
            break
        case "cv":
            type = "CV/Resume"
            break
        case "otherdoc1":
            type = "Other Document 1"
            break
        case "otherdoc2":
            type = "Other Document 2"
            break
        default:
            break
        }
        return type
    }
    
    
    
     class func formatDoBSTR(dateStr: String)-> String{
                let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"

            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "MMM dd yyyy"
    //"2016-02-29 12:24:26"
        print(dateStr)

            let date = dateFormatterGet.date(from: dateStr)
            print(dateFormatterPrint.string(from: date!))

            
            return dateFormatterPrint.string(from: date!)
        }
        
        
    
    
    class func formatedDOB(dateStr: String)-> String{
                let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "MMM dd yyyy"

            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "yyyy-MM-dd"
        print(dateStr)

            let date = dateFormatterGet.date(from: dateStr)
            print(dateFormatterPrint.string(from: date!))

            
            return dateFormatterPrint.string(from: date!)
        }
    
    
    class func resetLoginCredentials()
    {
        Utility.resetPlistValueForKey(key: YLA.UNAME, value: "")
        Utility.resetPlistValueForKey(key: YLA.PASWRD, value: "")
        Utility.resetPlistValueForKey(key: YLA.TYPEUSER, value: "")
        Utility.resetPlistValueForKey(key: YLA.USER_ID, value: "0")
        Utility.resetPlistValueForKey(key: YLA.MSG_CNT, value: "0")
        Utility.resetPlistValueForKey(key: YLA.USER_LOGGEDIN, value: YLASTATUS.NO)

    }
    
//    class func checkPhotoEnabled() -> Bool
//    {
//        var isEnabled:Bool?
//        PHPhotoLibrary.requestAuthorization { (status) in
//            switch status {
//               case .authorized:
//                 return true
//               case .restricted:
//                   return true
//                break
//               case .denied:
//                   return false
//                break
//               default:
//                   // place for .notDetermined - in this callback status is already determined so should never get here
//                return false
//
//                   break
//        }
//        }
//
//        return isEnabled!
//
//    }
    
    class func checkMediaEnabled() -> Bool
    {
        var isEnabled = false
        AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (videoGranted: Bool) -> Void in
            if (videoGranted) {
                 //Do Your stuff here
                isEnabled = true

            } else {
                isEnabled = false
            }
        })
        return isEnabled
    }
    
    class func checkForLocationEnabled()->Bool
      {
          var isEnabled = false
          
          if CLLocationManager.locationServicesEnabled() {
              switch CLLocationManager.authorizationStatus() {
              case .notDetermined, .restricted, .denied:
                  isEnabled = false

              case .authorizedAlways, .authorizedWhenInUse:
                  isEnabled = true

              }
          }
          return isEnabled
      }
    
    class func OpenMapWith(sLat: String, sLon: String, dLat: String, dLon: String,isWalk: Bool,titleStr: String)
       {
           
           
           if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!))
           {
               
               let str = isWalk ? "comgooglemaps://?saddr=" +  sLat + "," +  sLon + "&daddr=" + dLat + "," + dLon + "&zoom=14&&directionsmode=walking&views=traffic" : "comgooglemaps://?saddr=" +  sLat + "," +  sLon + "&daddr=" + dLat + "," + dLon + "&zoom=14&views=traffic"
               
               //print(str)
               // http://maps.google.com/?daddr=\(destinationLocation.latitude),\(destinationLocation.longitude)&directionsmode=driving
               
               
               UIApplication.shared.open(URL(string: str)!, options: [:], completionHandler: nil)
           }
           else {
               var latitude: CLLocationDegrees?
               var longitude: CLLocationDegrees?
               
               latitude = Double(sLat)
               
               
               longitude = Double(sLon)
               
               
               
               let regionDistance:CLLocationDistance = 10000
               let coordinates = CLLocationCoordinate2DMake(latitude!, longitude!)
               let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
               let options = [
                   MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                   MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
               ]
               let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
               let mapItem = MKMapItem(placemark: placemark)
               mapItem.name = titleStr
               mapItem.openInMaps(launchOptions: options)
           }
           
           
           
           
       }
    
    

    
    
    
    
    
}
