//
//  BaseViewController.swift
//  Ylaa
//
//  Created by Arun on 05/12/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import UIKit
import AVKit
class BaseViewController: UIViewController {

    
    private var progressHUD: ProgressHUD?

    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }

    
    func showProgressYLA(text: String = "Loading...") {
         if ServiceUtils.isConnectedToNetwork(){
              self.view.isUserInteractionEnabled = false
              progressHUD = ProgressHUD(text: text)
              self.view.addSubview(progressHUD!)
        }
          }
          
          func hideProgressYLA() {
              if let progressHUD = self.progressHUD {
                  self.view.isUserInteractionEnabled = true
                  progressHUD.removeFromSuperview()
                  self.progressHUD = nil
              }
          }
    func playVideo(urlStr: String?)
    {
//        guard let urlString = urlStr, let urlQueryString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
//            return
//        }
//        print(urlQueryString)
        if let urlString =  urlStr {
        
            if let url = URL(string:ServiceUtils.BETA_DOMAIN_VIDEOURL + urlString){

        let player = AVPlayer(url: url)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
        }
    }
    }
    
    
    
    
    @IBAction func backPressed(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
    
    
   
    func checkFileSize(data : Data,isImage:Bool){
           let fileSize = Double(data.count / (1048576*8)) //Convert in to MB
           
           let allowedSize = isImage ? 3.0 : 5.0
           if fileSize > allowedSize{
               //showMessage(message: isImage ? "File size exceeds 3 MB".localized() : "File size exceeds 5 MB".localized(), viewController: self, handler: nil)
           }
           return
       }
       
    
    func PngImageFileB64(uploadImage : UIImage?)->String?{
           
           if let uploadimg = uploadImage{
               if  let pngImageData = uploadimg.pngData() {
                   checkFileSize(data: pngImageData, isImage: true)
                   return   pngImageData.base64EncodedString()
                   
               }
           }
           return nil
       }
       
       
       func JpegImageFileB64(uploadImage : UIImage?)->String?{
           if let uploadimg = uploadImage{
               if let jpgImageData = uploadimg.jpegData(compressionQuality: 1)  {
                   checkFileSize(data: jpgImageData, isImage: true)
                   return jpgImageData.base64EncodedString()
               }
           }
           return nil
           
       }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func sendDeviceToken(){
        if Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES{
            NetworkManager.updateDevice(completionHandler: self.requestDeviceResponse(response:)) { (ERR) in
                self.showToast(message: ERR)
           }
        }

    }
    
    
    func requestDeviceResponse(response:RequestResult)
    {
           
    }

}

class BaseTabBarController: BaseViewController,CustomTabBarViewDelegate,YLALocFilterDelegate
{
  
    
   
    
    
 
    
    var tabView: CustomTabBarView?
    var locationView: YLALocationView = UIView.fromNib()
    let transition = POPAnimator()
    var onLocationClosed: ((_ isFinished:Bool)->())?
    var onLocationDetected: ((_ location:String,_ dist: Int)->())?




    func removeFilter()
    {
        self.locationView.removeFromSuperview()
    }
        
    override func viewDidLoad()
    {
        var yPos: CGFloat = 95.0
        var hgt: CGFloat = self.view.bounds.size.height - 180

        if Int(UIScreen.main.bounds.size.height) == 667 || Int(UIScreen.main.bounds.size.height) == 736{
            yPos = 70.0
            hgt = self.view.bounds.size.height - 120
        }

        
        self.locationView.frame =   CGRect(x: -300, y: yPos, width: self.locationView.bounds.size.width  , height: hgt)
        self.locationView.initLocationFilter()
        self.locationView.isHidden = true
        self.locationView.lDelegate = self
        self.view.addSubview(self.locationView)
        
        tabView = UIView.fromNib()
        self.tabView?.frame =   CGRect(x: self.view.frame.origin.x, y: self.view.frame.height - self.tabView!.frame.height, width: self.view!.frame.width, height: self.tabView!.frame.height)
        self.tabView?.tabDelegate = self
        self.tabView?.isShowMsg = true
        self.view.addSubview(self.tabView!)
        self.view.bringSubviewToFront(self.tabView!)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
         super.viewWillDisappear(animated)
         self.tabView!.resetTabBar()

    }
    
    
    
    // MARK: - CustomTabBarViewDelegate Methods

        
        func selectedTab(atIndex: NSInteger) {
            let storyboardMain = UIStoryboard(name: "Main", bundle: nil)

               switch atIndex {
                
                      case 111:
                           let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAHomeVCSBID") as! YLAHomeVC
                           controller.transitioningDelegate = self
                           self.navigationController?.pushViewController(controller, animated: false)
                          break
                      case 112:
                        if Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES{

                           let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAMessageListVCSBID") as! YLAMessageListVC
//                           controller.transitioningDelegate = self
//                           controller.modalPresentationStyle = .fullScreen
//                           self.present(controller, animated: true, completion: nil)
                           self.navigationController?.pushViewController(controller, animated: false)
                        }
                        else{
                            let controller = storyboardMain.instantiateViewController(withIdentifier: "YLALoginVCSBID") as! YLALoginVC
                            self.navigationController?.pushViewController(controller, animated: false)
                        }
                          break
                      case 113:
                        if Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES{

                           let controller = storyboardMain.instantiateViewController(withIdentifier: "YLANotificationsVCSBID") as! YLANotificationsVC
                           self.navigationController?.pushViewController(controller, animated: false)
                        }  else{
                            let controller = storyboardMain.instantiateViewController(withIdentifier: "YLALoginVCSBID") as! YLALoginVC
                            self.navigationController?.pushViewController(controller, animated: false)
                        }
                          break
                      case 114:
                        
                        if Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES{

                           let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAMyYlaaVCSBID") as! YLAMyYlaaVC
                           self.navigationController?.pushViewController(controller, animated: false)
                        }else{
                            let controller = storyboardMain.instantiateViewController(withIdentifier: "YLALoginVCSBID") as! YLALoginVC
                            self.navigationController?.pushViewController(controller, animated: false)
                        }
                          break
                      case 119:
                        if Utility.getPlistValueforKey(key: YLA.USER_LOGGEDIN) as String == YLASTATUS.YES{

                           let controller = storyboardMain.instantiateViewController(withIdentifier: "YLAPostAdsVCSBID") as! YLAPostAdsVC
                            controller.urlType = "POSTAD"
                           self.navigationController?.pushViewController(controller, animated: false)
                        }else{
                           let controller = storyboardMain.instantiateViewController(withIdentifier: "YLALoginVCSBID") as! YLALoginVC
                           self.navigationController?.pushViewController(controller, animated: false)
                        }
                       
                          break
                          
                      default:
                          break
                      }
                      
           }
    
    //MARK: YLALocFilterDelegate

    func cancelLocationView() {
        self.onLocationClosed!(true)
     }
    func selectedEmirateIndex(distance: Int, location: String) {
        self.onLocationDetected!(location, distance)
    }
    func sortAdsNear(kilometer: Int) {
          self.onLocationDetected!("", kilometer)
    }
    

}

//MARK: UIViewControllerTransitioningDelegate
extension BaseTabBarController: UIViewControllerTransitioningDelegate {
    
     func animationController(
          forPresented presented: UIViewController,
          presenting: UIViewController, source: UIViewController)
            -> UIViewControllerAnimatedTransitioning? {
                
    //            guard
    //                 let selectedIndexPathCell = listTable.indexPathForSelectedRow,
    //                 let selectedCell = listTable.cellForRow(at: selectedIndexPathCell) as? gameTableCell,
    //                 let selectedCellSuperview = selectedCell.superview
    //                 else {
    //                   return nil
    //               }
    //
    //               transition.originFrame = selectedCellSuperview.convert(selectedCell.frame, to: nil)
    //               transition.originFrame = CGRect(
    //                 x: transition.originFrame.origin.x + 20,
    //                 y: transition.originFrame.origin.y + 20,
    //                 width: transition.originFrame.size.width - 40,
    //                 height: transition.originFrame.size.height - 40
    //               )
    //
                   transition.presenting = true
    //               selectedCell.shadowView.isHidden = true
                   
                
                
          return transition
        }
        
        func animationController(forDismissed dismissed: UIViewController)
            -> UIViewControllerAnimatedTransitioning? {
          transition.presenting = false
          return transition
        }

    
}


class BaseTextFieldController : BaseViewController, UITextViewDelegate,UITextFieldDelegate{
     private weak var _scrollView: UIScrollView!
     internal weak var activeField: UIView?
    override func viewDidLoad() {
        super.viewDidLoad()
          self.automaticallyAdjustsScrollViewInsets = false
            self.hideKeyboardOnTap()
         setDoneOnAllKeyboards()
        _scrollView = findScrollView(view: self.view)
    }
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
        
          self.registerForKeyboardNotifications()
      }
      
      override func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
          
          self.unregisterFromKeyboardNotifications()
      }
      
      // MARK: - Click and Callback Methods
      
      private func setDoneOnAllKeyboards() {
             let keyboardToolbar = UIToolbar()
             keyboardToolbar.sizeToFit()
             let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
             let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.dismissKeyboard))
             keyboardToolbar.items = [flexBarButton, doneBarButton]
           BaseTextFieldController.applyDoneButton(view: self.view, keyboardToolbar: keyboardToolbar)
         }
    private class func applyDoneButton(view: UIView?, keyboardToolbar: UIToolbar) {
           if let view = view {
               for v in view.subviews {
                   if let textField = v as? UITextField {
                       textField.inputAccessoryView = keyboardToolbar
                   } else {
                       applyDoneButton(view: v, keyboardToolbar: keyboardToolbar)
                   }
               }
           }
       }
      
      
      // MARK: - Utility Methods
      
      private func findScrollView(view: UIView?) -> UIScrollView? {
          if let view = view {
              for v in view.subviews {
                  if let scrollView = v as? UIScrollView {
                      return scrollView
                  } else if let scrollView = findScrollView(view: v) {
                      return scrollView
                  }
              }
          }
          
          return nil
      }
      
      
      
      @objc func keyboardWasShown(notification: NSNotification){
          guard self._scrollView != nil else {
              print("SCROLL VIEW IS NULL IN " + self.storyBoardID)
            
            let duration = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
            let curve = notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
            let beginningFrame = (notification.userInfo![UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            let endFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

            let deltaY = (endFrame.origin.y - beginningFrame.origin.y) + 40

            UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: UIView.KeyframeAnimationOptions(rawValue: curve), animations: {
                self.view.frame.origin.y += deltaY
            }, completion: nil)
              return
          }
          
          //give room at the bottom of the scroll view, so it doesn't cover up anything the user needs to tap
          let userInfo = notification.userInfo!
          
          var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
          keyboardFrame = self.view.convert(keyboardFrame, from: nil)
          var contentInset:UIEdgeInsets = _scrollView.contentInset
          contentInset.bottom = keyboardFrame.size.height
          _scrollView.contentInset = contentInset
          
          if activeField != nil
          { var aRect: CGRect = self.view.frame
              aRect.size.height -= (keyboardFrame.height)
              if !aRect.contains(activeField!.frame.origin) {
                  self._scrollView.scrollRectToVisible(activeField!.frame, animated: true)
              }
          }
          
      }
      
      @objc func keyboardWillBeHidden(notification: NSNotification){
          guard self._scrollView != nil else {
              print("SCROLL VIEW IS NULL IN " + self.storyBoardID)
              return
          }
          
          let contentInset:UIEdgeInsets = UIEdgeInsets.zero
          _scrollView.contentInset = contentInset
          _scrollView.scrollIndicatorInsets = contentInset
      }
      
      func registerForKeyboardNotifications()
      {
          //Adding notifies on keyboard appearing
          NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardDidHideNotification, object: nil)
      }
      
      
      func unregisterFromKeyboardNotifications()
      {
          //Removing notifies on keyboard appearing
          NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
          NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
      }
      func hideKeyboardOnTap() {
          let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
          tap.cancelsTouchesInView = false
          view.addGestureRecognizer(tap)
      }
      
      @objc func dismissKeyboard() {
          view.endEditing(true)
      }
   
        
        //MARK:- UITextViewDelegates
        
        
        func textFieldDidBeginEditing(_ textField: UITextField) {
           
            activeField = textField
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            if textField.returnKeyType == .done
            {self.view.endEditing(true)
                return false}
            else{
                return true
            }
        }
        
        func textViewDidBeginEditing(_ textView: UITextView) {
            activeField = textView
            textView.contentInset = .zero
           
        }
     
       
        
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            if text == "\n" {
                textView.resignFirstResponder()
            }
            return true
        }
        
        func textViewDidEndEditing(_ textView: UITextView) {
            activeField = nil
           
        }

    
    
    
}

