//
//  Extensions.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage
import Alamofire
import CRNotifications


extension UIButton{
    func setLogoImage(imageUrl: String?)
        
    {
        
        guard let urlString = imageUrl, let urlQueryString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
                  self.setImage(#imageLiteral(resourceName: "business_logo"), for: .normal)
                  return
               }
        
        Alamofire.request(ServiceUtils.BETA_DOMAIN_URL + urlQueryString).responseData(completionHandler: { response in


           if let image1 = response.result.value {
            let image = UIImage(data: image1)
            if image == nil{
                self.setImage(#imageLiteral(resourceName: "placeHolder"), for: .normal)

            }else{
            self.setImage(image, for: .normal)
            }
           }
       })
    }
}


extension UIImage {
    // MARK: - UIImage+Resize
    func compressTo(_ expectedSizeInMb:Int) -> Data? {
        let sizeInBytes = expectedSizeInMb * 1024 * 1024
        var needCompress:Bool = true
        var imgData:Data?
        var compressingValue:CGFloat = 1.0
        
        while (needCompress && compressingValue > 0.0) {
            if let data:Data = self.jpegData(compressionQuality: compressingValue) {
                if data.count < sizeInBytes {
                    needCompress = false
                    imgData = data
                } else {
                    compressingValue -= 0.1
                }
            }
        }
        
        if let data = imgData {
            if (data.count < sizeInBytes) {
                return data
            }
        }
        return nil
    }
}

extension UILabel {
    @IBInspectable
    var rotation: Int {
        get {
            return 0
        } set {
            //let radians = CGAffineTransform(rotationAngle: -.pi / 4)//CGFloat(CGFloat(Double.pi) * CGFloat(newValue) / CGFloat(180.0))
            self.transform = CGAffineTransform(rotationAngle: -.pi / 4)
        }
    }
}



extension UIViewController{
    func showToast(message : String) {

        /*
           //self.view.frame.size.height-100
           let toastLabel = UILabel(frame: CGRect(x: 20, y: 100, width: self.view.frame.size.width - 40, height: 35))
           toastLabel.backgroundColor = UIColor.white.withAlphaComponent(0.8)
           toastLabel.textColor = UIColor.black
           toastLabel.font = UIFont(name: "SegoeUI-Regular", size: 10.0)
           toastLabel.textAlignment = .center;
           toastLabel.text = message
           toastLabel.alpha = 1.0
           toastLabel.layer.cornerRadius = 10;
           toastLabel.clipsToBounds  =  true
           self.view.addSubview(toastLabel)
           UIView.animate(withDuration: 4.0, delay: 0.02, options: .curveEaseOut, animations: {
                toastLabel.alpha = 0.0
           }, completion: {(isCompleted) in
               toastLabel.removeFromSuperview()
           })
 */
        
        CRNotifications.showNotification(textColor:.white, backgroundColor: UIColor.customBlackColor
            , image:#imageLiteral(resourceName: "warning"), title: "Ylaa", message: message, dismissDelay: 2)
        
       }
}

extension String
{
 
    
    func getHTMLtoText()-> String
    {
        let htmlData = NSString(string: self).data(using: String.Encoding.unicode.rawValue)

               let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]

               let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
        
         return attributedString.string
               

    }
    
    func getPathExtension() -> String {
           return (self as NSString).pathExtension
       }
}
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    

        func validatedText(validationType: textValidatorType) throws -> String {
            let validator = textVaildatorFactory.validatorFor(type: validationType)
            return try validator.validated(self.text!)
        }
        
        func getOnlyNumberField(cString: String)-> Bool
        {
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = cString.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return cString == numberFiltered
        }
  
}

extension UIView
{
    func startRotating(duration: Double = 1) {
             let kAnimationKey = "rotation"
              
           if self.layer.animation(forKey: kAnimationKey) == nil {
                 let animate = CABasicAnimation(keyPath: "transform.rotation")
                 animate.duration = duration
                 animate.repeatCount = Float.infinity
                 animate.fromValue = 0.0
                 animate.toValue = Float(M_PI * 2.0)
               self.layer.add(animate, forKey: kAnimationKey)
             }
         }
         func stopRotating() {
             let kAnimationKey = "rotation"
              
           if self.layer.animation(forKey: kAnimationKey) != nil {
               self.layer.removeAnimation(forKey: kAnimationKey)
             }
         }
    
    
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    func removeKeyBoardObserver()
    {
        NotificationCenter.default.removeObserver(self)

    }

      func bindToMakeOfferKeyboard(){
            NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillChange(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    func bindToKeyboard(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
     @objc func keyboardWillShow(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
        if Int(UIScreen.main.bounds.size.height) == 667 || Int(UIScreen.main.bounds.size.height) == 736{
            self.frame.origin.y = UIScreen.main.bounds.size.height - 315

        }else{
            self.frame.origin.y = UIScreen.main.bounds.size.height - 400

        }

        //}
    }

     @objc func keyboardWillHide(notification: NSNotification) {
        if Int(UIScreen.main.bounds.size.height) == 667 || Int(UIScreen.main.bounds.size.height) == 736{
            self.frame.origin.y = (UIScreen.main.bounds.size.height - self.bounds.size.height)

        }else{
            self.frame.origin.y = (UIScreen.main.bounds.size.height - self.bounds.size.height) - 30

        }

    }
    

    @objc func keyboardWillChange(_ notification: NSNotification){
        let duration = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
        let beginningFrame = (notification.userInfo![UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let endFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

        var deltaY:CGFloat = 0.0
//
//        if self.frame.origin.y  > 550
//        {
            deltaY = (endFrame.origin.y - beginningFrame.origin.y) +  20

//        }else{
//            deltaY = (endFrame.origin.y - beginningFrame.origin.y)
//
//        }
        
       
        
                                                
        UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: UIView.KeyframeAnimationOptions(rawValue: curve), animations: {
            self.frame.origin.y += deltaY
            print(self.frame.origin.y)

        }, completion: nil)
    }
    
    func hideKeyboardOnTap() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        self.endEditing(true)
    }
}


extension UIColor
{
    class var bgColor: UIColor
    {
        return UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1.0)
    }
    class var lineColor: UIColor
    {
        return UIColor(red: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 0.37)
    }
    class var textDimColor: UIColor
       {
        return UIColor(red: 196.0/255.0, green:196.0/255.0, blue: 196.0/255.0, alpha: 0.8)
       }
    
    class var subTitleColor: UIColor
    {
        return UIColor(red: 102.0/255.0, green:102.0/255.0, blue: 102.0/255.0, alpha: 0.8)
    }
    
    class var ylaaColor: UIColor
       {
        return UIColor(red: 242.0/255.0, green:111.0/255.0, blue: 47.0/255.0, alpha: 0.8)
       }
    class var highlightedColor: UIColor
         {
        return UIColor(red: 242.0/255.0, green:146.0/255.0, blue: 98.0/255.0, alpha: 0.8)
         }
    class var orangeColor: UIColor
           {
        return UIColor(red: 242.0/255.0, green:111.0/255.0, blue: 47.0/255.0, alpha: 1.0)
           }
    
    
    class var customBlackColor: UIColor
              {
        return UIColor(red: 21.0/255.0, green:30.0/255.0, blue: 39.0/255.0, alpha: 1.0)
              }
       
    
    
}



extension NSObject {
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
}


extension UIViewController {
    
    func setstatusBarColor()
       {
           if #available(iOS 13.0, *) {


              let statusBar1 =  UIView()
               statusBar1.frame = (UIApplication.shared.keyWindow?.windowScene?.statusBarManager!.statusBarFrame)!
               statusBar1.backgroundColor =  UIColor.bgColor

              UIApplication.shared.keyWindow?.addSubview(statusBar1)

           } else {

              let statusBar1: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
              statusBar1.backgroundColor = UIColor.bgColor
           }
       }
    
    
    
    
    var storyBoardID: String {
              return self.className + "SBID"
          }
          
          class var storyBoardID: String {
              return className + "SBID"
          }
}


extension UIImageView{

    func placeAtTheCenterWithView(view: UIView) {
        
        view.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: 0))
        
        view.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: 0))
    }

    func setImageWithURL(url : String? , placeholder : String?)  {
        
        
        
        guard let urlString = url, let urlQueryString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
           self.image = #imageLiteral(resourceName: "no_pic")
           return
        }
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.style = .gray
        activityIndicator.frame = self.bounds
        if self.tag == 11{
            activityIndicator.center = CGPoint(x:UIScreen.main.bounds.size.width/2, y: 90.0)
        }
        if let url = URL(string:ServiceUtils.BETA_IMGBASE_URL + urlQueryString) {
            self.addSubview(activityIndicator)
            self.placeAtTheCenterWithView(view: activityIndicator)
            activityIndicator.startAnimating()
            self.af_setImage(withURL: url,placeholderImage: #imageLiteral(resourceName: "no_pic"), filter: nil, imageTransition: UIImageView.ImageTransition.crossDissolve(0.5), completion: { (image) in
                activityIndicator.stopAnimating()
                activityIndicator.isHidden = true
                activityIndicator.removeFromSuperview()
                
                if let newImage = image.value {
                    self.image = newImage
                    
                }
                else{
                    self.image = #imageLiteral(resourceName: "no_pic")

                }
            })
        }
    }
        
        
        
        func setImageForYlaaUser(url: String?, placeHolder: String?)
        {
            guard let urlString = url, let urlQueryString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
                   self.image = UIImage(named: placeHolder!)

                       return
                   }
                   let activityIndicator = UIActivityIndicatorView()
                   activityIndicator.style = .gray
                   activityIndicator.frame = self.bounds
                   if self.tag == 11{
                       activityIndicator.center = CGPoint(x:UIScreen.main.bounds.size.width/2, y: 90.0)
                   }
                   if let url = URL(string:ServiceUtils.BETA_USERIMGBASE_URL + urlQueryString) {
                       self.addSubview(activityIndicator)
                       self.placeAtTheCenterWithView(view: activityIndicator)
                       activityIndicator.startAnimating()
                       self.af_setImage(withURL: url, completion: { (image) in
                           activityIndicator.stopAnimating()
                           activityIndicator.isHidden = true
                           activityIndicator.removeFromSuperview()
                           if let newImage = image.value {
                               
                                self.image = newImage
                               

                           }  else{
                                        self.image = UIImage(named: placeHolder!)
                            }
                       })
        }
        }
    
    
    
    func setImageForLogoUser(url: String?, placeHolder: String?)
         {
             guard let urlString = url, let urlQueryString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
                        return
                    }
                    let activityIndicator = UIActivityIndicatorView()
                    activityIndicator.style = .gray
                    activityIndicator.frame = self.bounds
                    if self.tag == 11{
                        activityIndicator.center = CGPoint(x:UIScreen.main.bounds.size.width/2, y: 90.0)
                    }
                    if let url = URL(string:ServiceUtils.BETA_DOMAIN_URL + urlQueryString) {
                        self.addSubview(activityIndicator)
                        self.placeAtTheCenterWithView(view: activityIndicator)
                        activityIndicator.startAnimating()
                        self.af_setImage(withURL: url, completion: { (image) in
                            activityIndicator.stopAnimating()
                            activityIndicator.isHidden = true
                            activityIndicator.removeFromSuperview()
                           if let newImage = image.value {
                                                          
                                                           self.image = newImage
                                                          

                                                      }  else{
                                                                         self.image = #imageLiteral(resourceName: "placeHolder")

                                                                     }
                        })
         }
         }
        
        
}
