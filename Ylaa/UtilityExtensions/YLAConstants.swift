//
//  YLAConstants.swift
//  Ylaa
//
//  Created by Arun on 30/11/2019.
//  Copyright © 2019 Arun. All rights reserved.
//

import Foundation

struct YLA
{
    static let CURLANG = "selLang"
    static let ISLANSEL = "langSelected"
    static let PROF_OVRVIEW = "Profile Overview"
    static let SETTINGS = "Settings"
    static let IND_USER = "Individual"
    static let BSNS_USER = "Business"
    static let USER_ID = "userID"
    static let USER_LOGGEDIN = "loggedIN"
    static let UNAME = "username"
    static let PASWRD = "password"
    static let MSG_CNT = "msgcnt"

    static let TYPEUSER = "typeuser"
    static let DEVICE_TOKEN = "device_tokens"
}
struct YLAMSG {
    static let MSG_PAUSE = "Warning!!! This Ad will be PAUSED and will not display until you resume. Click YES to continue."
    static let MSG_RESUME = "This Ad will resume to display on Ylaa.com. Click YES to continue."
    static let MSG_UNSOLD = "Is this product available now!"
    static let MSG_REFRESH = "Please note:You can refresh same or any of your Ads only 4 times in 24 hours.Do you want to continue?"
    static let MSG_INCOMPLT = "Unable to complete operation"

}
struct YLACATEG
{
    static let AUTO = "Automotive"
    static let RESTATE = "Real Estate"
    static let CLSFDS = "Classifieds"
    static let CMNTY = "Community"
    static let JOB = "Jobs"



}
struct YLASTATUS
{
    static let YES = "Yes"
    static let NO = "No"
}

struct LANG
{
    static let ARB = "ar"
    static let ENG = "en"

}
