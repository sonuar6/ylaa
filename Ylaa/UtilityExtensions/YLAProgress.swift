//
//  YLAProgress.swift
//  Ylaa
//
//  Created by Arun on 15/03/2020.
//  Copyright © 2020 Arun. All rights reserved.
//

import UIKit

class ProgressHUD: UIView {
    
    var text: String? {
        didSet {
            label.text = text
        }
    }
    let activityIndictor: UIActivityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.white)
    let label: UILabel = UILabel()
    let rotateImageView:UIImageView =  UIImageView()
    
    init(text: String) {
        self.text = text
        super.init(frame: UIScreen.main.bounds)
        self.setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.text = ""
        super.init(coder: aDecoder)!
        self.setup()
        
    }
    
    func setup() {
        //contentView.addSubview(vibrancyView)
        //vibrancyView.contentView.addSubview(activityIndictor)
        rotateImageView.image = UIImage.gifImageWithURL("https://media.giphy.com/media/ZXlDCPOJc6CX76dI8B/giphy.gif") //UIImage.gifImageWithName("loading gif.gif")
        //vibrancyView.contentView.addSubview(rotateImageView)
        self.addSubview(rotateImageView)
//        vibrancyView.contentView.addSubview(label)
        //rotateImageView.startRotating()

        //activityIndictor.startAnimating()
        
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        if let superview = self.superview {
            
            let width = superview.frame.size.width / 2.1
            let height: CGFloat = 80.0
           // CGRect(x: superview.frame.size.width / 2 - width / 2, y: superview.frame.height / 2 - height / 2, width: width, height: height)
//            self.frame = CGRect(x: 0, y: 90, width: superview.frame.size.width, height: superview.frame.height - 100)
            self.frame = CGRect(x: superview.frame.size.width / 2 - width / 2, y: superview.frame.height / 2 - height / 2, width: width, height: height)
                //CGRect(x: 0, y: 90, width: width, height: height)

            
            
            let activityIndicatorSize: CGFloat = 60.0
            
            

//            activityIndictor.frame = CGRect(x: 5, y: height / 2 - activityIndicatorSize / 2, width: activityIndicatorSize, height: activityIndicatorSize)
            rotateImageView.frame = CGRect(x: (self.frame.size.width / 2) - (activityIndicatorSize / 2), y: self.frame.size.height / 2 - activityIndicatorSize / 2, width: activityIndicatorSize, height: activityIndicatorSize)
            rotateImageView.backgroundColor = .white
            rotateImageView.cornerRadius = 30.0

            

            
            layer.cornerRadius = 8.0
            layer.masksToBounds = true
//            //label.text = text
//            label.textAlignment = NSTextAlignment.center
//            label.frame = CGRect(x: activityIndicatorSize + 2, y: 0, width: width - activityIndicatorSize - 5, height: height)
//            label.textColor = UIColor.gray
//            label.font = UIFont.boldSystemFont(ofSize: 16)
        }
    }
    
    func show() {
        self.isHidden = false
    }
    
    func hide() {
        self.isHidden = true
    }
}

