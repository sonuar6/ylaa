//
//  UITextValidator.swift
//  texfield
//
//  Created by APPLE on 7/23/19.
//  Copyright © 2019 APPLE. All rights reserved.
//

import Foundation

class textValidationError: Error {
    var message: String
    
    init(_ message: String) {
        self.message = message
    }
}

protocol textValidatorConvertible {
    func validated(_ value: String) throws -> String
}

enum textValidatorType {
    case email
    case password
    case username
    case requiredField(field: String)
    case age
    case phonenumber

}


enum textVaildatorFactory {
    static func validatorFor(type: textValidatorType) -> textValidatorConvertible {
        switch type {
        case .email: return EmailValidator()
        case .password: return PasswordValidator()
        case .username: return UserNameValidator()
        case .requiredField(let fieldName): return RequiredFieldValidator(fieldName)
        case .age: return AgeValidator()
      
        case .phonenumber:  return PhoneValidator()
            
        }
}
}






class PhoneValidator: textValidatorConvertible{
    func validated(_ value: String) throws -> String {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: value, options: [], range: NSMakeRange(0, value.count))
            if let res = matches.first {
               print(res)
            } else {
                 throw textValidationError("Invalid phone number!")
            }
        } catch {
            throw textValidationError("Invalid phone number!")
    }

        return value
}
    
    
}


    class AgeValidator: textValidatorConvertible {
        func validated(_ value: String) throws -> String {
            guard value.count > 0 else {throw textValidationError("Age is required")}
            guard let age = Int(value) else {throw textValidationError("Age must be a number!")}
            guard value.count < 3 else {throw textValidationError("Invalid age number!")}
            guard age >= 18 else {throw textValidationError("You have to be over 18 years old to user our app :)")}
            return value
        }
    }
    
    struct RequiredFieldValidator: textValidatorConvertible {
        private let fieldName: String
        
        init(_ field: String) {
            fieldName = field
        }
        
        func validated(_ value: String) throws -> String {
            guard !value.isEmpty else {
                throw textValidationError("Required field " + fieldName)
            }
            return value
        }
    }
    
    struct UserNameValidator: textValidatorConvertible {
        func validated(_ value: String) throws -> String {
            guard value.count >= 3 else {
                throw textValidationError("Username must contain more than three characters" )
            }
            guard value.count < 18 else {
                throw textValidationError("Username shoudn't conain more than 18 characters" )
            }
            
            do {
                if try NSRegularExpression(pattern: "^[a-z]{1,18}$",  options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                    throw textValidationError("Invalid username, username should not contain whitespaces, numbers or special characters")
                }
            } catch {
                throw textValidationError("Invalid username, username should not contain whitespaces,  or special characters")
            }
            return value
        }
    }
    
    struct PasswordValidator: textValidatorConvertible {
        func validated(_ value: String) throws -> String {
            guard value != "" else {throw textValidationError("Password is Required")}
            guard value.count >= 8 else { throw textValidationError("Password must have at least 8 characters") }
            
            do {
                if try NSRegularExpression(pattern: "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$",  options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                    throw textValidationError("Must have one character and one numeric")
                }
            } catch {
                throw textValidationError("Must have one character and one numeric")
            }
            return value
        }
    }
    
    struct EmailValidator: textValidatorConvertible {
        func validated(_ value: String) throws -> String {
            do {
                if try NSRegularExpression(pattern: "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$", options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                    throw textValidationError("Please check e-mail field may be invalid or empty")
                }
            } catch {
                throw textValidationError("Please check e-mail field may be invalid or empty")
            }
            return value
        }
}
